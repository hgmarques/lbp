
import numpy as np

class HGMDataAligner:

    etimes = None       # event times
    nevents = None      # number of events
    adata = None        # aligned data
    trange = None       # time range around the event
    label = None        # aligner label
    adata = None        # aligned data

    # histogram data
    hdata_raw = None    # histogram data absolute counts
    hdata = None        # histogram data normalized 
    hcenters = None     # histogram bin centers
    hedges = None       # histogram edges
    hbin_size = None    # histogram bin size
    href_idx = None     # index of the reference event in the histogram array
    

    @staticmethod
    def create(input_data, event_times, time_range, label):
        print("Creating data aligner {}" + label)
        print("Input data dims: {}".format(input_data.ndim))
        if(input_data.ndim == 1):
            return HGMEventAlignerDiscrete(input_data, event_times, time_range,label)
        return 3


class HGMDataAlignerDiscrete(HGMDataAligner):
    
    def histogram(self, nbins):
        self.compute()
        self.hedges = np.linspace(self.trange[0], self.trange[1], nbins + 1)
        self.hcenters = (self.hedges[0:nbins] + self.hedges[1:]) / 2
        self.hbin_size = self.hedges[1] - self.hedges[0]
        self.hdata_raw = np.histogram(self.adata[:,0], bins = self.hedges)[0]
        print(self.hdata_raw)

        self.hdata = self.hdata_raw / (self.nevents * self.hbin_size)




class HGMEventAlignerDiscrete(HGMDataAlignerDiscrete): 

    def __init__(self, input_data, event_times, time_range, label):
        self.idata = input_data
        self.etimes = event_times
        self.trange = time_range
        self.label = label
        self.nevents = event_times.shape[0]

    def compute(self):
        self.adata = np.empty((0,2))
        ecounter = 0

        for etime in self.etimes:
            # get discrete values in idata that are within trange
            idxs = np.where(((self.idata - etime) > self.trange[0]) & 
                            ((self.idata - etime) < self.trange[1]))[0]
            #print(idxs)
            #print(self.idata[idxs] - etime)
            
            #print(etime)
            # concatenate
            
            arr = np.array([self.idata[idxs] - etime, np.zeros(idxs.shape) + ecounter])
            #print(arr)
            if idxs.size != 0:
                self.adata = np.concatenate((self.adata, arr.transpose()))
            
            ecounter = ecounter + 1
        print(self.adata)

#HGMDataAligner.create(np.array([10, 19]),1,[0, 2], 'me')

