
//#ifndef LBPWifi_h
//#define LBPWifi_h
//#include "C:\Users\HugoMarques\Documents\Code\MyReps\lbp\firmware\LBPWifi.h"
#include <LBP.h>
#include <OSCMessage.h>

LBPWifi mywifi;
LBPBumpers bumpers;
LBPWheels wheels;

void setup() 
{

  // put your setup code here, to run once:
  mywifi.setup("MEO-443C10", "eda3cdd2cf", "192.168.1.51", 2390);
  //mywifi.begin("TP-Link_72DE", "86171342", "192.168.0.51");

  int bumper_ports[] = {0,8,10,13};
  bumpers.setup(bumper_ports, 4);

  wheels.setup();
}


void loop() 
{
  // put your main code here, to run repeatedly:
  mywifi.loop();
  
  if(mywifi.hasMsg())
 {
    wheels.loop(mywifi.hasMsg(), mywifi.getMsg());
    wheels.print();
}
    


}
