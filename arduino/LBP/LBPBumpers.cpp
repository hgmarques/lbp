#include<Arduino.h>
#include "LBPBumpers.h"

using namespace lbp;

int LBPBumpers::setup(int ports_in[], int nports_in)
{
    ports = ports_in;
    nports = nports_in;

    for (int i=0; i<nports; i++)    
    {
        Serial.print("Bumper added to port: ");
        Serial.println(ports[i]);
        pinMode(ports[i], INPUT_PULLUP);
    }


    Serial.println("LBPBumpers set!");
    return 0;
}

void LBPBumpers::loop()
{
}