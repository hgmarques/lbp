#include<Arduino.h>
#include "LBPWifi.h"

//#include "C:\Users\HugoMarques\Documents\Code\MyReps\lbp\firmware\LBPWifi.h"

//#include "LBPWifi.h"
//#include <WiFiNINA.h>

using namespace lbp;

int LBPWifi::setup(const char* ssid, const char* pass, const char* ip, unsigned int port)
{

    // check for the presence of the shield:
    Serial.println("Starting Wifi... ");
                
    if (WiFi.status() == WL_NO_SHIELD) {
        Serial.println("WiFi shield not present");
        return -1;
    }

    localIP.fromString(ip);
    localPort = port;
    // attempt to connect to WiFi network:
    int attemptNumber = 0;
    while (status != WL_CONNECTED) {

        attemptNumber++;
        if(attemptNumber > 3)
            return -1;
            
        Serial.print(attemptNumber);
        Serial.print(" - Trying to connect to SSID: ");
        Serial.print(ssid);
        Serial.print(", ");
        Serial.println(status);

        WiFi.disconnect();  // WORKAROUND!!!!!!!!

        IPAddress gateway(192, 168, 0, 1);  // Set your Gateway IP address
//        IPAddress gateway(192, 168, 1, 254);  // Set your Gateway IP address
        IPAddress subnet(255, 255, 255, 0);

        WiFi.config(localIP, gateway, subnet);
                
        // Connect to WPA/WPA2 network. Change this line if using open or WEP network:
        status = WiFi.begin(ssid, pass);
              
        //status = WiFi.begin(ssid);
        delay(1000);

    }

    print();
    Udp.begin(localPort);
    return 0;

}


OSCMessage LBPWifi::getMsg()
{
    return msgIN;
}

int LBPWifi::loop()
{
    newMsg = false;
    msgSize = Udp.parsePacket();
  
    if (msgSize > 0)
    {
        Serial.println("before read msg");
        readMsg();
        newMsg = true;
        Serial.println("after read msg");
    } 
    
    return 0;
}


bool LBPWifi::isConnected()
{
    return (status == WL_CONNECTED);
}

bool LBPWifi::hasMsg()
{
    return newMsg;
}


void LBPWifi::readMsg()
{
  
  while (msgSize--) {
    msgIN.fill(Udp.read());
  }

}

void LBPWifi::print() 
{

    // print the SSID of the network you're attached to:
    Serial.print("SSID: ");
    Serial.println(WiFi.SSID());

    // print your WiFi shield's IP address:
    IPAddress ip = WiFi.localIP();
    Serial.print("IP Address: ");
    Serial.println(ip);

    // print the received signal strength:
    long rssi = WiFi.RSSI();
    Serial.print("signal strength (RSSI):");
    Serial.print(rssi);
    Serial.println(" dBm");
        
}

