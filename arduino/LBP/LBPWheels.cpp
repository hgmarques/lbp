#include <Arduino.h>
#include "LBPWheels.h"

using namespace lbp;

int LBPWheels::wheel1;
int LBPWheels::wheel2;

int LBPWheels::setup()
{
    LBPWheels::setWheels(0, 0);
    Serial.println("LBPMotors set!");
    return 0;
}

void LBPWheels::loop(bool hasMsg, OSCMessage msg)
{
    Serial.println("entrance");
    Serial.println(hasMsg);

    if(hasMsg)
    {
        Serial.println("before route");
        msg.route(oscAddress, LBPWheels::processMessage);
        Serial.println("after route");
    }
    Serial.println("exit");

}


void LBPWheels::print()
{
    Serial.print("Weels: ");
    Serial.print(LBPWheels::wheel1);
    Serial.print(",  ");
    Serial.println(LBPWheels::wheel2);
}


void LBPWheels::processMessage(OSCMessage &msg, int offset)
{
    LBPWheels::wheel1 = msg.getInt(0);
    LBPWheels::wheel2 = msg.getInt(1);
}

void LBPWheels::setWheels(int w1, int w2)
{
    LBPWheels::wheel1 = w1;
    LBPWheels::wheel2 = w2;
}