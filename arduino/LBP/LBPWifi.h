//#ifndef LBPWIFI_h
//#define LBPWIFI_h

//#include <WiFi101.h>
#include <WiFiNINA.h>
#include <WiFiUdp.h>
#include <OSCMessage.h>

namespace lbp {

class LBPWifi{

    public:
        LBPWifi(){}
        int setup(const char* ssid_in, const char* pass_in, const char* ip, unsigned int port);
        int loop();        
        bool isConnected();
        bool hasMsg();
        OSCMessage getMsg();
        void print();
        

    private:    
        int status = WL_IDLE_STATUS;
        IPAddress localIP;
        unsigned int localPort = 2390;         // local port to listen on
        bool serial_log = true;
        
        char* ssid; // = "TP-Link_72DE";          // your network SSID (name)
        char* pass; //= "86171342";              // your network password (use for WPA, or use as key for WEP)

        // DO WE NEED SERIAL HERE?
        WiFiUDP Udp;

        char msgBuffer[255];                //buffer to hold incoming packet
        char ReplyBuffer[15] = "/acknowledged";  // a string to send back
        int msgNumber = 0;
        int msgSize = 0;
        bool newMsg;
        OSCMessage msgIN;
        
        void initSocket();
        int readSocket();
        void readMsg();

};

}