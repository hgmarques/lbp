
namespace lbp{

class LBPBumpers
{
    public:
        LBPBumpers(){}
        int setup(int ports_in[], int nports_in);
        void loop();

    private:
        const int* ports;
        int nports;

};

}