#include <OSCMessage.h>

namespace lbp{

class LBPWheels
{
    public:

        static int wheel1;
        static int wheel2;
        
        LBPWheels(){}
        int setup();
        void loop(bool hasMsg, OSCMessage msg);
        void print();
        static void processMessage(OSCMessage &msg, int offset);
        static void setWheels(int w1, int w2);


    private:
        const char* oscAddress = "/wheels";
        const int* ports;
        int nports;
        
};

}