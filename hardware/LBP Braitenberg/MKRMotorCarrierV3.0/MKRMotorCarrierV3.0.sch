<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.1.3">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting keepoldvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="10" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="10" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="7" fill="1" visible="no" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="fp3" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="Beschreib" color="9" fill="1" visible="yes" active="yes"/>
<layer number="106" name="BGA-Top" color="4" fill="1" visible="yes" active="yes"/>
<layer number="107" name="BD-Top" color="5" fill="1" visible="yes" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="no" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="ReferenceLS" color="7" fill="1" visible="no" active="no"/>
<layer number="114" name="FRNTMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="FRNTMAAT2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="yes" active="yes"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="top_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="134" name="silk_top" color="7" fill="1" visible="yes" active="yes"/>
<layer number="135" name="silk_bottom" color="7" fill="1" visible="yes" active="yes"/>
<layer number="136" name="silktop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="137" name="silkbottom" color="7" fill="1" visible="yes" active="yes"/>
<layer number="138" name="EEE" color="7" fill="1" visible="yes" active="yes"/>
<layer number="139" name="_tKeepout" color="7" fill="1" visible="yes" active="yes"/>
<layer number="141" name="ASSEMBLY_TOP" color="7" fill="1" visible="yes" active="yes"/>
<layer number="143" name="PLACE_BOUND_TOP" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="145" name="DrillLegend_01-16" color="7" fill="1" visible="yes" active="yes"/>
<layer number="146" name="DrillLegend_01-20" color="7" fill="1" visible="yes" active="yes"/>
<layer number="147" name="PIN_NUMBER" color="7" fill="1" visible="yes" active="yes"/>
<layer number="148" name="DrillLegend_01-20" color="7" fill="1" visible="yes" active="yes"/>
<layer number="149" name="DrillLegend_02-15" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="166" name="AntennaArea" color="7" fill="1" visible="yes" active="yes"/>
<layer number="168" name="4mmHeightArea" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="no" active="no"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="7" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="7" fill="1" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="Accent" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="supply1">
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="jumper">
<packages>
<package name="SJ">
<description>&lt;b&gt;Solder jumper&lt;/b&gt;</description>
<wire x1="1.397" y1="-1.016" x2="-1.397" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.397" y1="1.016" x2="1.651" y2="0.762" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.651" y1="0.762" x2="-1.397" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.651" y1="-0.762" x2="-1.397" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="1.397" y1="-1.016" x2="1.651" y2="-0.762" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="-0.762" x2="1.651" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-1.651" y1="-0.762" x2="-1.651" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-1.397" y1="1.016" x2="1.397" y2="1.016" width="0.1524" layer="21"/>
<wire x1="1.016" y1="0" x2="1.524" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.016" y1="0" x2="-1.524" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="-0.127" x2="-0.254" y2="0.127" width="1.27" layer="51" curve="-180" cap="flat"/>
<wire x1="0.254" y1="0.127" x2="0.254" y2="-0.127" width="1.27" layer="51" curve="-180" cap="flat"/>
<smd name="1" x="-0.762" y="0" dx="1.1684" dy="1.6002" layer="1"/>
<smd name="2" x="0.762" y="0" dx="1.1684" dy="1.6002" layer="1"/>
<text x="-1.651" y="1.143" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0.4001" y="0" size="0.02" layer="27">&gt;VALUE</text>
<rectangle x1="-0.0762" y1="-0.9144" x2="0.0762" y2="0.9144" layer="29"/>
</package>
<package name="SJW">
<description>&lt;b&gt;Solder jumper&lt;/b&gt;</description>
<wire x1="1.905" y1="-1.524" x2="-1.905" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.524" x2="2.159" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="1.27" x2="-1.905" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-1.27" x2="-1.905" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="1.905" y1="-1.524" x2="2.159" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="2.159" y1="-1.27" x2="2.159" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="-1.27" x2="-2.159" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.524" x2="1.905" y2="1.524" width="0.1524" layer="21"/>
<wire x1="0.762" y1="0.762" x2="0.762" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="0.762" x2="-0.762" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="1.524" y1="0" x2="2.032" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.524" y1="0" x2="-2.032" y2="0" width="0.1524" layer="51"/>
<wire x1="0.762" y1="0.762" x2="0.762" y2="-0.762" width="0.1524" layer="51" curve="-180"/>
<wire x1="-0.762" y1="0.762" x2="-0.762" y2="-0.762" width="0.1524" layer="51" curve="180"/>
<smd name="1" x="-1.27" y="0" dx="1.27" dy="2.54" layer="1"/>
<smd name="2" x="1.27" y="0" dx="1.27" dy="2.54" layer="1"/>
<text x="-2.159" y="1.778" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="1" y="0" size="0.02" layer="27">&gt;VALUE</text>
<rectangle x1="0.762" y1="-0.762" x2="1.016" y2="0.762" layer="51"/>
<rectangle x1="1.016" y1="-0.635" x2="1.27" y2="0.635" layer="51"/>
<rectangle x1="1.27" y1="-0.508" x2="1.397" y2="0.508" layer="51"/>
<rectangle x1="1.397" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.016" y1="-0.762" x2="-0.762" y2="0.762" layer="51"/>
<rectangle x1="-1.27" y1="-0.635" x2="-1.016" y2="0.635" layer="51"/>
<rectangle x1="-1.397" y1="-0.508" x2="-1.27" y2="0.508" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.397" y2="0.254" layer="51"/>
<rectangle x1="0.9652" y1="-0.7112" x2="1.0922" y2="-0.5842" layer="51"/>
<rectangle x1="1.3462" y1="-0.3556" x2="1.4732" y2="-0.2286" layer="51"/>
<rectangle x1="1.3462" y1="0.2032" x2="1.4732" y2="0.3302" layer="51"/>
<rectangle x1="0.9652" y1="0.5842" x2="1.0922" y2="0.7112" layer="51"/>
<rectangle x1="-1.0922" y1="-0.7112" x2="-0.9652" y2="-0.5842" layer="51"/>
<rectangle x1="-1.4478" y1="-0.3302" x2="-1.3208" y2="-0.2032" layer="51"/>
<rectangle x1="-1.4732" y1="0.2032" x2="-1.3462" y2="0.3302" layer="51"/>
<rectangle x1="-1.1176" y1="0.5842" x2="-0.9906" y2="0.7112" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="SJ">
<wire x1="0.381" y1="0.635" x2="0.381" y2="-0.635" width="1.27" layer="94" curve="-180" cap="flat"/>
<wire x1="-0.381" y1="-0.635" x2="-0.381" y2="0.635" width="1.27" layer="94" curve="-180" cap="flat"/>
<wire x1="2.54" y1="0" x2="1.651" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.651" y2="0" width="0.1524" layer="94"/>
<text x="-2.54" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SJ" prefix="SJ" uservalue="yes">
<description>SMD solder &lt;b&gt;JUMPER&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="SJ" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SJ">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="W" package="SJW">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply2">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
Please keep in mind, that these devices are necessary for the
automatic wiring of the supply signals.&lt;p&gt;
The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="+05V">
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="0.635" x2="0" y2="1.905" width="0.1524" layer="94"/>
<circle x="0" y="1.27" radius="1.27" width="0.254" layer="94"/>
<text x="-1.905" y="3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="+5V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="PWRIN">
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="0.635" x2="0" y2="1.905" width="0.1524" layer="94"/>
<circle x="0" y="1.27" radius="1.27" width="0.254" layer="94"/>
<text x="-1.905" y="3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="PWRIN" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="+5V" prefix="SUPPLY">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="+5V" symbol="+05V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PWRIN">
<gates>
<gate name="G$1" symbol="PWRIN" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Arduino-utility">
<description>Copyright (c) 2016-2017 Arduino LLC.  All right reserved.</description>
<packages>
<package name="FRAME-NO-BAN">
</package>
<package name="FD-1-2">
<circle x="0" y="0" radius="0.5" width="2.1844" layer="29"/>
<circle x="0" y="0" radius="1.5" width="0.127" layer="41"/>
<smd name="P$1" x="0" y="0" dx="1.016" dy="1.016" layer="1" roundness="100"/>
</package>
<package name="FD-1-1.5">
<circle x="0" y="0" radius="0.5" width="1.2" layer="29"/>
<circle x="0" y="0" radius="1" width="0.127" layer="41"/>
<smd name="P$1" x="0" y="0" dx="1.016" dy="1.016" layer="1" roundness="100" cream="no"/>
</package>
<package name="TP-0.8MM">
<smd name="X" x="0" y="0" dx="0.8" dy="0.8" layer="16" roundness="100" thermals="no" cream="no"/>
<text x="-1.016" y="0.635" size="0.4064" layer="47" rot="SR0">&gt;NAME</text>
</package>
<package name="TP-1.27MM">
<circle x="0" y="0" radius="1.27" width="0.127" layer="47"/>
<smd name="X" x="0" y="0" dx="1.27" dy="1.27" layer="16" roundness="100" thermals="no" cream="no"/>
<text x="-2.54" y="1.27" size="1.27" layer="47" rot="SR0">&gt;NAME</text>
</package>
<package name="TP-1.00MM">
<smd name="X" x="0" y="0" dx="1" dy="1" layer="16" roundness="100" thermals="no" cream="no"/>
<text x="0" y="0.889" size="0.3048" layer="22" rot="SMR0" align="center">&gt;NAME</text>
</package>
<package name="TP-0.50MM">
<circle x="0" y="0" radius="0.05" width="0.4064" layer="30"/>
<smd name="X" x="0" y="0" dx="0.5" dy="0.5" layer="16" roundness="100" stop="no" thermals="no" cream="no"/>
<text x="0" y="0.335" size="0.3048" layer="22" rot="SMR0" align="bottom-center">&gt;NAME</text>
</package>
<package name="FRAME">
<text x="0" y="-17.78" size="1.27" layer="51">Reference Designs ARE PROVIDED "AS IS" AND "WITH ALL FAULTS. Arduino SA DISCLAIMS ALL OTHER WARRANTIES, EXPRESS OR IMPLIED,
REGARDING PRODUCTS, INCLUDING BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A 
PARTICULAR PURPOSE 
Arduino SA may make changes to specifications and product descriptions at any time, without notice. The Customer must not
rely on the absence or characteristics of any features or instructions marked "reserved" or "undefined." Arduino SA reserves
these for future definition and shall have no responsibility whatsoever for conflicts or incompatibilities arising from future changes to them.
The product information on the Web Site or Materials is subject to change without notice. Do not finalize a design with this info

ARDUINO and other Arduino brands and logos and Trademarks of Arduino SA. All Arduino SA Trademarks cannot be used without owner's formal permission

This file is licensed under : CC-SA-BY-NC</text>
</package>
</packages>
<symbols>
<symbol name="A3L-LOC">
<wire x1="288.29" y1="3.81" x2="342.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="342.265" y1="3.81" x2="373.38" y2="3.81" width="0.1016" layer="94"/>
<wire x1="373.38" y1="3.81" x2="383.54" y2="3.81" width="0.1016" layer="94"/>
<wire x1="383.54" y1="3.81" x2="383.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="383.54" y1="8.89" x2="383.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="383.54" y1="13.97" x2="383.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="383.54" y1="19.05" x2="383.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="288.29" y1="3.81" x2="288.29" y2="24.13" width="0.1016" layer="94"/>
<wire x1="288.29" y1="24.13" x2="342.265" y2="24.13" width="0.1016" layer="94"/>
<wire x1="342.265" y1="24.13" x2="383.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="373.38" y1="3.81" x2="373.38" y2="8.89" width="0.1016" layer="94"/>
<wire x1="373.38" y1="8.89" x2="383.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="373.38" y1="8.89" x2="342.265" y2="8.89" width="0.1016" layer="94"/>
<wire x1="342.265" y1="8.89" x2="342.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="342.265" y1="8.89" x2="342.265" y2="13.97" width="0.1016" layer="94"/>
<wire x1="342.265" y1="13.97" x2="383.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="342.265" y1="13.97" x2="342.265" y2="19.05" width="0.1016" layer="94"/>
<wire x1="342.265" y1="19.05" x2="383.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="342.265" y1="19.05" x2="342.265" y2="24.13" width="0.1016" layer="94"/>
<text x="344.17" y="15.24" size="2.54" layer="94" font="vector">&gt;DRAWING_NAME</text>
<text x="344.17" y="10.16" size="2.286" layer="94" font="vector">&gt;LAST_DATE_TIME</text>
<text x="357.505" y="5.08" size="2.54" layer="94" font="vector">&gt;SHEET</text>
<text x="343.916" y="4.953" size="2.54" layer="94" font="vector">Sheet:</text>
<frame x1="0" y1="0" x2="387.35" y2="260.35" columns="8" rows="5" layer="94"/>
<text x="381" y="25.4" size="1.6764" layer="98" rot="R90">Reference Designs ARE PROVIDED "AS IS" AND "WITH ALL FAULTS. Arduino SA DISCLAIMS ALL OTHER WARRANTIES, EXPRESS OR IMPLIED,
REGARDING PRODUCTS, INCLUDING BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A 
PARTICULAR PURPOSE 
Arduino SA may make changes to specifications and product descriptions at any time, without notice. The Customer must not
rely on the absence or characteristics of any features or instructions marked "reserved" or "undefined." Arduino SA reserves
these for future definition and shall have no responsibility whatsoever for conflicts or incompatibilities arising from future changes to them.
The product information on the Web Site or Materials is subject to change without notice. Do not finalize a design with this info

ARDUINO and other Arduino brands and logos and Trademarks of Arduino SA. All Arduino SA Trademarks cannot be used without owner's formal permission

This file is licensed under : CC-SA-BY-NC</text>
</symbol>
<symbol name="ARDUINO_FIDUCIAL">
<circle x="0" y="0" radius="2.54" width="0.254" layer="94"/>
</symbol>
<symbol name="TESTPOINT">
<circle x="0" y="-0.508" radius="0.4" width="0" layer="94"/>
<circle x="0" y="-0.508" radius="0.7" width="0.1" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.1524" layer="94"/>
<pin name="P$1" x="0" y="-2.54" visible="off" length="point" direction="pas" rot="R90"/>
<text x="0" y="0.508" size="1.778" layer="95" align="bottom-center">&gt;NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="A3-FRAME" prefix="FRAME" uservalue="yes">
<description>Schematic frame</description>
<gates>
<gate name="G$1" symbol="A3L-LOC" x="0" y="0"/>
</gates>
<devices>
<device name="-STANDARD" package="FRAME">
<technologies>
<technology name="">
<attribute name="CODE-ARDUINO" value="DNP" constant="no"/>
<attribute name="DATASHEET" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="DNP" constant="no"/>
<attribute name="MANUFACTURER" value="DNP" constant="no"/>
<attribute name="MANUFACTURER-PN" value="DNP" constant="no"/>
<attribute name="VALUE" value="DNP" constant="no"/>
</technology>
</technologies>
</device>
<device name="-NO-BAN" package="FRAME-NO-BAN">
<technologies>
<technology name="">
<attribute name="CODE-ARDUINO" value="DNP" constant="no"/>
<attribute name="DATASHEET" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="DNP" constant="no"/>
<attribute name="MANUFACTURER" value="DNP" constant="no"/>
<attribute name="MANUFACTURER-PN" value="DNP" constant="no"/>
<attribute name="VALUE" value="DNP" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FIDUCIAL">
<description>Fiducial mount</description>
<gates>
<gate name="G$1" symbol="ARDUINO_FIDUCIAL" x="0" y="0"/>
</gates>
<devices>
<device name="-2MM" package="FD-1-2">
<technologies>
<technology name="">
<attribute name="CODE-BCE" value="DNP" constant="no"/>
<attribute name="CODE-BCMI" value="DNP" constant="no"/>
<attribute name="CODE-TK2" value="DNP" constant="no"/>
<attribute name="DATASHEET" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="DNP" constant="no"/>
<attribute name="MANUFACTURER" value="DNP" constant="no"/>
<attribute name="MANUFACTURER-PN" value="DNP" constant="no"/>
<attribute name="VALUE" value="DNP" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1.5MM" package="FD-1-1.5">
<technologies>
<technology name="">
<attribute name="CODE-BCE" value="DNP" constant="no"/>
<attribute name="CODE-BCMI" value="DNP" constant="no"/>
<attribute name="CODE-TK2" value="DNP" constant="no"/>
<attribute name="DATASHEET" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="DNP" constant="no"/>
<attribute name="MANUFACTURER" value="DNP" constant="no"/>
<attribute name="MANUFACTURER-PN" value="DNP" constant="no"/>
<attribute name="VALUE" value="DNP" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TP" prefix="TP">
<description>Testpoint</description>
<gates>
<gate name="G$1" symbol="TESTPOINT" x="0" y="2.54"/>
</gates>
<devices>
<device name="TP-0.8MM" package="TP-0.8MM">
<connects>
<connect gate="G$1" pin="P$1" pad="X"/>
</connects>
<technologies>
<technology name="">
<attribute name="DATASHEET" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="DNP" constant="no"/>
<attribute name="MANUFACTURER" value="DNP" constant="no"/>
<attribute name="MANUFACTURER-PN" value="DNP" constant="no"/>
<attribute name="VALUE" value="DNP" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP-1.27MM" package="TP-1.27MM">
<connects>
<connect gate="G$1" pin="P$1" pad="X"/>
</connects>
<technologies>
<technology name="">
<attribute name="DATASHEET" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="DNP" constant="no"/>
<attribute name="MANUFACTURER" value="DNP" constant="no"/>
<attribute name="MANUFACTURER-PN" value="DNP" constant="no"/>
<attribute name="VALUE" value="DNP" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP-1.00MM" package="TP-1.00MM">
<connects>
<connect gate="G$1" pin="P$1" pad="X"/>
</connects>
<technologies>
<technology name="">
<attribute name="DATASHEET" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="DNP" constant="no"/>
<attribute name="MANUFACTURER" value="DNP" constant="no"/>
<attribute name="MANUFACTURER-PN" value="DNP" constant="no"/>
<attribute name="VALUE" value="DNP" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP-0.5MM" package="TP-0.50MM">
<connects>
<connect gate="G$1" pin="P$1" pad="X"/>
</connects>
<technologies>
<technology name="">
<attribute name="DATASHEET" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="DNP" constant="no"/>
<attribute name="MANUFACTURER" value="DNP" constant="no"/>
<attribute name="MANUFACTURER-PN" value="DNP" constant="no"/>
<attribute name="VALUE" value="DNP" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Arduino-rcl">
<packages>
<package name="0402-1005X55N">
<circle x="0" y="0" radius="0.25" width="0.05" layer="39"/>
<wire x1="-0.5" y1="-0.25" x2="0.5" y2="-0.25" width="0.025" layer="51"/>
<wire x1="0.5" y1="-0.25" x2="0.5" y2="0.25" width="0.025" layer="51"/>
<wire x1="0.5" y1="0.25" x2="-0.5" y2="0.25" width="0.025" layer="51"/>
<wire x1="-0.5" y1="0.25" x2="-0.5" y2="-0.25" width="0.025" layer="51"/>
<wire x1="-0.35" y1="0" x2="0.35" y2="0" width="0.05" layer="39"/>
<wire x1="0" y1="-0.35" x2="0" y2="0.35" width="0.05" layer="39"/>
<wire x1="-0.95" y1="0.45" x2="0.95" y2="0.45" width="0.05" layer="39"/>
<wire x1="0.95" y1="0.45" x2="0.95" y2="-0.45" width="0.05" layer="39"/>
<wire x1="0.95" y1="-0.45" x2="-0.95" y2="-0.45" width="0.05" layer="39"/>
<wire x1="-0.95" y1="-0.45" x2="-0.95" y2="0.45" width="0.05" layer="39"/>
<smd name="1" x="-0.45" y="0" dx="0.62" dy="0.62" layer="1" roundness="3"/>
<smd name="2" x="0.45" y="0" dx="0.62" dy="0.62" layer="1" roundness="3"/>
<text x="-0.9" y="0.5" size="0.4064" layer="51" font="vector" ratio="10">&gt;NAME</text>
</package>
<package name="0805-R2013X50N">
<circle x="0" y="0" radius="0.25" width="0.05" layer="39"/>
<wire x1="1" y1="-0.625" x2="1" y2="0.625" width="0.025" layer="51"/>
<wire x1="1" y1="0.625" x2="-1" y2="0.625" width="0.025" layer="51"/>
<wire x1="-1" y1="0.625" x2="-1" y2="-0.625" width="0.025" layer="51"/>
<wire x1="-1" y1="-0.625" x2="1" y2="-0.625" width="0.025" layer="51"/>
<wire x1="0" y1="-0.35" x2="0" y2="0.35" width="0.05" layer="39"/>
<wire x1="0.35" y1="0" x2="-0.35" y2="0" width="0.05" layer="39"/>
<wire x1="-1.75" y1="1" x2="1.75" y2="1" width="0.05" layer="39"/>
<wire x1="1.75" y1="1" x2="1.75" y2="-1" width="0.05" layer="39"/>
<wire x1="1.75" y1="-1" x2="-1.75" y2="-1" width="0.05" layer="39"/>
<wire x1="-1.75" y1="-1" x2="-1.75" y2="1" width="0.05" layer="39"/>
<smd name="1" x="-1" y="0" dx="0.95" dy="1.45" layer="1" roundness="3"/>
<smd name="2" x="1" y="0" dx="0.95" dy="1.45" layer="1" roundness="3"/>
<text x="0" y="1.27" size="0.6096" layer="25" font="vector" ratio="10" align="center">&gt;NAME</text>
</package>
<package name="PWR">
<description>Multilayer SMD</description>
<wire x1="-2.8004" y1="4.375" x2="2.8004" y2="4.375" width="0.127" layer="21"/>
<wire x1="2.8004" y1="-4.375" x2="-2.8004" y2="-4.375" width="0.127" layer="21"/>
<smd name="1" x="-5.4" y="0" dx="5.2" dy="8.75" layer="1"/>
<smd name="2" x="5.4" y="0" dx="5.2" dy="8.75" layer="1"/>
<text x="0" y="0" size="0.6096" layer="25" align="center">&gt;NAME</text>
</package>
<package name="C0201">
<smd name="1" x="0" y="0.35" dx="0.45" dy="0.45" layer="1" roundness="20"/>
<smd name="2" x="0" y="-0.33" dx="0.45" dy="0.45" layer="1" roundness="20"/>
<rectangle x1="-0.3" y1="-0.15" x2="0.3" y2="0.15" layer="21" rot="R270"/>
<text x="-0.9525" y="0" size="0.6096" layer="25" font="vector" ratio="10" rot="R270" align="center">&gt;NAME</text>
</package>
<package name="C1206">
<smd name="1" x="0" y="1.4" dx="1.8" dy="1.45" layer="1" roundness="20"/>
<smd name="2" x="0" y="-1.4" dx="1.8" dy="1.45" layer="1" roundness="20"/>
<wire x1="-0.85" y1="1.6" x2="-0.85" y2="1" width="0.127" layer="21"/>
<wire x1="-0.85" y1="1" x2="-0.85" y2="-1" width="0.127" layer="21"/>
<wire x1="-0.85" y1="-1" x2="-0.85" y2="-1.6" width="0.127" layer="21"/>
<wire x1="0.85" y1="1.6" x2="0.85" y2="1" width="0.127" layer="21"/>
<wire x1="0.85" y1="1" x2="0.85" y2="-1" width="0.127" layer="21"/>
<wire x1="0.85" y1="-1" x2="0.85" y2="-1.6" width="0.127" layer="21"/>
<wire x1="0.85" y1="1.6" x2="-0.85" y2="1.6" width="0.127" layer="21"/>
<wire x1="0.85" y1="-1.6" x2="-0.85" y2="-1.6" width="0.127" layer="21"/>
<wire x1="0.85" y1="1" x2="-0.85" y2="1" width="0.127" layer="21"/>
<wire x1="0.85" y1="-1" x2="-0.85" y2="-1" width="0.127" layer="21"/>
<text x="-1.5875" y="0" size="0.8128" layer="25" font="vector" ratio="10" rot="R270" align="center">&gt;NAME</text>
</package>
<package name="0603-1608X90N">
<circle x="0" y="0" radius="0.25" width="0.05" layer="39"/>
<wire x1="-0.814" y1="-0.4" x2="0.786" y2="-0.4" width="0.025" layer="51"/>
<wire x1="0.786" y1="-0.4" x2="0.786" y2="0.4" width="0.025" layer="51"/>
<wire x1="0.786" y1="0.4" x2="-0.814" y2="0.4" width="0.025" layer="51"/>
<wire x1="-0.814" y1="0.4" x2="-0.814" y2="-0.4" width="0.025" layer="51"/>
<wire x1="-0.35" y1="0" x2="0.35" y2="0" width="0.05" layer="39"/>
<wire x1="0" y1="-0.35" x2="0" y2="0.35" width="0.05" layer="39"/>
<wire x1="-1.6" y1="0.8" x2="-1.6" y2="-0.8" width="0.05" layer="39"/>
<wire x1="-1.6" y1="-0.8" x2="1.6" y2="-0.8" width="0.05" layer="39"/>
<wire x1="1.6" y1="-0.8" x2="1.6" y2="0.8" width="0.05" layer="39"/>
<wire x1="1.6" y1="0.8" x2="-1.6" y2="0.8" width="0.05" layer="39"/>
<smd name="1" x="-0.8" y="0" dx="0.95" dy="1" layer="1" roundness="3"/>
<smd name="2" x="0.8" y="0" dx="0.95" dy="1" layer="1" roundness="3"/>
<text x="-1.27" y="0.9525" size="0.6096" layer="25" font="vector" ratio="10">&gt;NAME</text>
</package>
<package name="C1210">
<smd name="1" x="0" y="1.4" dx="2.7" dy="1.45" layer="1" roundness="20"/>
<smd name="2" x="0" y="-1.35" dx="2.7" dy="1.45" layer="1" roundness="20"/>
<text x="-2.2225" y="0" size="0.8128" layer="25" font="vector" ratio="10" rot="R270" align="center">&gt;NAME</text>
<text x="2.2225" y="0" size="0.8128" layer="27" font="vector" ratio="10" rot="R270" align="center">&gt;VALUE</text>
<wire x1="-1.3" y1="1.6" x2="-1.3" y2="1" width="0.127" layer="21"/>
<wire x1="-1.3" y1="1" x2="-1.3" y2="-1" width="0.127" layer="21"/>
<wire x1="-1.3" y1="-1" x2="-1.3" y2="-1.6" width="0.127" layer="21"/>
<wire x1="1.3" y1="1.6" x2="1.3" y2="1" width="0.127" layer="21"/>
<wire x1="1.3" y1="1" x2="1.3" y2="-1" width="0.127" layer="21"/>
<wire x1="1.3" y1="-1" x2="1.3" y2="-1.6" width="0.127" layer="21"/>
<wire x1="1.3" y1="1.6" x2="-1.3" y2="1.6" width="0.127" layer="21"/>
<wire x1="1.3" y1="-1.6" x2="-1.3" y2="-1.6" width="0.127" layer="21"/>
<wire x1="1.3" y1="1" x2="-1.3" y2="1" width="0.127" layer="21"/>
<wire x1="1.3" y1="-1" x2="-1.3" y2="-1" width="0.127" layer="21"/>
</package>
<package name="SRP4020">
<wire x1="-2.6" y1="2.03" x2="2.6" y2="2.03" width="0.1524" layer="21"/>
<wire x1="2.6" y1="2.03" x2="2.6" y2="-2.03" width="0.1524" layer="21"/>
<wire x1="2.6" y1="-2.03" x2="-2.6" y2="-2.03" width="0.1524" layer="21"/>
<wire x1="-2.6" y1="-2.03" x2="-2.6" y2="2.03" width="0.1524" layer="21"/>
<smd name="1" x="-1.85" y="0" dx="2.4" dy="1.5" layer="1" rot="R90"/>
<smd name="2" x="1.85" y="0" dx="2.4" dy="1.5" layer="1" rot="R90"/>
<text x="0" y="0" size="0.8128" layer="21" rot="R90" align="center">&gt;NAME</text>
</package>
<package name="SRN6045">
<wire x1="-3" y1="3" x2="3" y2="3" width="0.1524" layer="21"/>
<wire x1="3" y1="3" x2="3" y2="-3" width="0.1524" layer="21"/>
<wire x1="3" y1="-3" x2="-3" y2="-3" width="0.1524" layer="21"/>
<wire x1="-3" y1="-3" x2="-3" y2="3" width="0.1524" layer="21"/>
<smd name="1" x="-2.85" y="0" dx="6" dy="3" layer="1" rot="R90"/>
<smd name="2" x="2.85" y="0" dx="6" dy="3" layer="1" rot="R90"/>
<text x="0" y="0" size="0.8128" layer="21" rot="R90" align="center">&gt;NAME</text>
</package>
<package name="5X5_NON-STANDARD">
<smd name="P$1" x="-1.975" y="0" dx="4.5" dy="1.55" layer="1" rot="R90"/>
<smd name="P$2" x="1.875" y="0" dx="4.5" dy="1.55" layer="1" rot="R90"/>
<wire x1="-2.55" y1="-2.52" x2="2.45" y2="-2.52" width="0.127" layer="21"/>
<wire x1="2.45" y1="-2.52" x2="2.45" y2="2.51" width="0.127" layer="21"/>
<wire x1="2.45" y1="2.51" x2="-2.55" y2="2.51" width="0.127" layer="21"/>
<wire x1="-2.55" y1="2.51" x2="-2.55" y2="-2.52" width="0.127" layer="21"/>
<wire x1="-2.54" y1="1.21" x2="-2.54" y2="1.41" width="0.127" layer="21"/>
<wire x1="-2.54" y1="1.41" x2="-1.74" y2="2.21" width="0.127" layer="21"/>
<wire x1="-1.74" y1="2.21" x2="1.67" y2="2.21" width="0.127" layer="21"/>
<wire x1="1.67" y1="2.21" x2="2.44" y2="1.44" width="0.127" layer="21"/>
<wire x1="2.44" y1="1.44" x2="2.44" y2="-1.57" width="0.127" layer="21"/>
<wire x1="2.44" y1="-1.57" x2="1.81" y2="-2.2" width="0.127" layer="21"/>
<wire x1="1.81" y1="-2.2" x2="-1.81" y2="-2.2" width="0.127" layer="21"/>
<wire x1="-1.81" y1="-2.2" x2="-2.54" y2="-1.47" width="0.127" layer="21"/>
</package>
<package name="6X6_NON-STANDARD">
<smd name="P$1" x="-2.1" y="0" dx="1.9" dy="5.1" layer="1"/>
<smd name="P$2" x="2.1" y="0" dx="1.9" dy="5.1" layer="1"/>
<wire x1="2.92" y1="-2.95" x2="2.92" y2="2.95" width="0.127" layer="21"/>
<wire x1="2.92" y1="2.95" x2="-3.01" y2="2.95" width="0.127" layer="21"/>
<wire x1="-3.01" y1="2.95" x2="-3.01" y2="2.57" width="0.127" layer="21"/>
<wire x1="-3.01" y1="-2.24" x2="-3.01" y2="-2.95" width="0.127" layer="21"/>
<wire x1="-3.01" y1="-2.95" x2="2.92" y2="-2.95" width="0.127" layer="21"/>
<wire x1="-2.21" y1="-2.94" x2="-3.01" y2="-2.24" width="0.127" layer="21"/>
<wire x1="-3.01" y1="-2.24" x2="-3.01" y2="2.57" width="0.127" layer="21"/>
<wire x1="-3.01" y1="2.57" x2="-2.24" y2="2.94" width="0.127" layer="21"/>
<wire x1="-2.24" y1="2.94" x2="2.27" y2="2.94" width="0.127" layer="21"/>
<wire x1="2.27" y1="2.94" x2="2.9" y2="2.51" width="0.127" layer="21"/>
<wire x1="2.9" y1="2.51" x2="2.9" y2="-2.41" width="0.127" layer="21"/>
<wire x1="2.9" y1="-2.41" x2="2.17" y2="-2.94" width="0.127" layer="21"/>
</package>
<package name="CAY16">
<description>&lt;b&gt;BOURNS&lt;/b&gt; Chip Resistor Array&lt;p&gt;
Source: RS Component / BUORNS</description>
<wire x1="-1.55" y1="0.75" x2="-1" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-0.6" y1="0.75" x2="-0.2" y2="0.75" width="0.1016" layer="51"/>
<wire x1="0.2" y1="0.75" x2="0.6" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1" y1="0.75" x2="1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.55" y1="0.75" x2="1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="-0.75" x2="-1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1" y1="0.75" x2="-0.6" y2="0.75" width="0.1016" layer="51" curve="180"/>
<wire x1="-0.2" y1="0.75" x2="0.2" y2="0.75" width="0.1016" layer="51" curve="180"/>
<wire x1="0.6" y1="0.75" x2="1" y2="0.75" width="0.1016" layer="51" curve="180"/>
<wire x1="1.55" y1="-0.75" x2="1" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="0.6" y1="-0.75" x2="0.2" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-0.2" y1="-0.75" x2="-0.6" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1" y1="-0.75" x2="-1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="1" y1="-0.75" x2="0.6" y2="-0.75" width="0.1016" layer="51" curve="180"/>
<wire x1="0.2" y1="-0.75" x2="-0.2" y2="-0.75" width="0.1016" layer="51" curve="180"/>
<wire x1="-0.6" y1="-0.75" x2="-1" y2="-0.75" width="0.1016" layer="51" curve="180"/>
<smd name="1" x="-1.2" y="-0.675" dx="0.5" dy="0.65" layer="1"/>
<smd name="2" x="-0.4" y="-0.675" dx="0.5" dy="0.65" layer="1"/>
<smd name="3" x="0.4" y="-0.675" dx="0.5" dy="0.65" layer="1"/>
<smd name="4" x="1.2" y="-0.675" dx="0.5" dy="0.65" layer="1"/>
<smd name="5" x="1.2" y="0.675" dx="0.5" dy="0.65" layer="1"/>
<smd name="6" x="0.4" y="0.675" dx="0.5" dy="0.65" layer="1"/>
<smd name="7" x="-0.4" y="0.675" dx="0.5" dy="0.65" layer="1"/>
<smd name="8" x="-1.2" y="0.675" dx="0.5" dy="0.65" layer="1"/>
<text x="-1.905" y="-2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.905" y="1.27" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="TANT">
<wire x1="-5.11" y1="-2.8" x2="5.11" y2="-2.8" width="0.1" layer="39"/>
<wire x1="5.11" y1="2.8" x2="-5.11" y2="2.8" width="0.1" layer="39"/>
<wire x1="-5.11" y1="-2.8" x2="-5.11" y2="2.8" width="0.1" layer="39"/>
<wire x1="5.11" y1="-2.8" x2="5.11" y2="2.8" width="0.1" layer="39"/>
<smd name="+" x="-3.4" y="0" dx="2.6" dy="2.77" layer="1" rot="R90"/>
<smd name="-" x="3.4" y="0" dx="2.6" dy="2.77" layer="1" rot="R90"/>
<wire x1="-4.5" y1="2" x2="-4.5" y2="-2" width="0.1" layer="21"/>
<wire x1="-4.5" y1="-2" x2="4.5" y2="-2" width="0.1" layer="21"/>
<wire x1="4.5" y1="-2" x2="4.5" y2="2" width="0.1" layer="21"/>
<wire x1="4.5" y1="2" x2="-4.5" y2="2" width="0.1" layer="21"/>
<text x="-5.1" y="2" size="0.8128" layer="21">+</text>
<text x="0" y="0" size="0.8128" layer="25" align="center">&gt;NAME</text>
</package>
<package name="PANASONIC_D">
<description>&lt;b&gt;Panasonic Aluminium Electrolytic Capacitor VS-Serie Package D&lt;/b&gt;</description>
<circle x="0" y="0" radius="3.1" width="0.1016" layer="51"/>
<wire x1="-3.25" y1="3.25" x2="1.55" y2="3.25" width="0.1016" layer="51"/>
<wire x1="1.55" y1="3.25" x2="3.25" y2="1.55" width="0.1016" layer="51"/>
<wire x1="3.25" y1="1.55" x2="3.25" y2="-1.55" width="0.1016" layer="51"/>
<wire x1="3.25" y1="-1.55" x2="1.55" y2="-3.25" width="0.1016" layer="51"/>
<wire x1="1.55" y1="-3.25" x2="-3.25" y2="-3.25" width="0.1016" layer="51"/>
<wire x1="-3.25" y1="-3.25" x2="-3.25" y2="3.25" width="0.1016" layer="51"/>
<wire x1="-3.25" y1="0.95" x2="-3.25" y2="3.25" width="0.1016" layer="21"/>
<wire x1="-3.25" y1="3.25" x2="1.55" y2="3.25" width="0.1016" layer="21"/>
<wire x1="1.55" y1="3.25" x2="3.25" y2="1.55" width="0.1016" layer="21"/>
<wire x1="3.25" y1="1.55" x2="3.25" y2="0.95" width="0.1016" layer="21"/>
<wire x1="3.25" y1="-0.95" x2="3.25" y2="-1.55" width="0.1016" layer="21"/>
<wire x1="3.25" y1="-1.55" x2="1.55" y2="-3.25" width="0.1016" layer="21"/>
<wire x1="1.55" y1="-3.25" x2="-3.25" y2="-3.25" width="0.1016" layer="21"/>
<wire x1="-3.25" y1="-3.25" x2="-3.25" y2="-0.95" width="0.1016" layer="21"/>
<wire x1="-2.95" y1="0.95" x2="2.95" y2="0.95" width="0.1016" layer="21" curve="-144.299363"/>
<wire x1="-2.95" y1="-0.95" x2="2.95" y2="-0.95" width="0.1016" layer="21" curve="144.299363"/>
<wire x1="-2.1" y1="2.25" x2="-2.1" y2="-2.2" width="0.1016" layer="51"/>
<rectangle x1="-3.65" y1="-0.35" x2="-3.05" y2="0.35" layer="51"/>
<rectangle x1="3.05" y1="-0.35" x2="3.65" y2="0.35" layer="51"/>
<smd name="+" x="2.4" y="0" dx="3" dy="1.4" layer="1"/>
<smd name="-" x="-2.4" y="0" dx="3" dy="1.4" layer="1"/>
<text x="0" y="0" size="1.016" layer="25" rot="R90" align="center">&gt;NAME</text>
<polygon width="0.1016" layer="51">
<vertex x="-2.15" y="2.15"/>
<vertex x="-2.6" y="1.6"/>
<vertex x="-2.9" y="0.9"/>
<vertex x="-3.05" y="0"/>
<vertex x="-2.9" y="-0.95"/>
<vertex x="-2.55" y="-1.65"/>
<vertex x="-2.15" y="-2.15"/>
<vertex x="-2.15" y="2.1"/>
</polygon>
</package>
<package name="PANASONIC_C">
<smd name="-" x="0" y="2.15" dx="1.6" dy="2.8" layer="1"/>
<smd name="+" x="0" y="-2.15" dx="1.6" dy="2.8" layer="1"/>
<wire x1="-1.2961625" y1="-2.7" x2="1.3961625" y2="-2.7" width="0.127" layer="21"/>
<wire x1="1.3961625" y1="-2.7" x2="2.7" y2="-1.3961625" width="0.127" layer="21"/>
<wire x1="2.7" y1="-1.3961625" x2="2.7" y2="2.6" width="0.127" layer="21"/>
<wire x1="-1.2961625" y1="-2.7" x2="-2.6" y2="-1.3961625" width="0.127" layer="21"/>
<wire x1="-2.6" y1="-1.3961625" x2="-2.6" y2="2.6" width="0.127" layer="21"/>
<wire x1="-2.6" y1="2.6" x2="2.7" y2="2.6" width="0.127" layer="21"/>
<text x="-2.54" y="-0.5" size="1.016" layer="21">&gt;NAME</text>
<circle x="0" y="0" radius="2.5" width="0.127" layer="21"/>
<rectangle x1="-0.3" y1="-2.8" x2="0.3" y2="-0.8" layer="51"/>
<rectangle x1="-0.3" y1="0.8" x2="0.3" y2="2.8" layer="51"/>
</package>
<package name="0204V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 2.5 mm</description>
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.508" layer="51"/>
<wire x1="-0.127" y1="0" x2="0.127" y2="0" width="0.508" layer="21"/>
<circle x="-1.27" y="0" radius="0.889" width="0.1524" layer="51"/>
<circle x="-1.27" y="0" radius="0.635" width="0.0508" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="square"/>
<text x="-2.1336" y="1.1684" size="1.27" layer="25" ratio="12">&gt;NAME</text>
</package>
<package name="C0603">
<wire x1="0.45" y1="0.9" x2="0.45" y2="0.5" width="0.127" layer="21"/>
<wire x1="0.45" y1="0.5" x2="0.45" y2="-0.5" width="0.127" layer="21"/>
<wire x1="0.45" y1="-0.5" x2="0.45" y2="-0.9" width="0.127" layer="21"/>
<wire x1="-0.45" y1="0.9" x2="-0.45" y2="0.5" width="0.127" layer="21"/>
<wire x1="-0.45" y1="0.5" x2="-0.45" y2="-0.5" width="0.127" layer="21"/>
<wire x1="-0.45" y1="-0.5" x2="-0.45" y2="-0.9" width="0.127" layer="21"/>
<wire x1="0.45" y1="-0.9" x2="-0.45" y2="-0.9" width="0.127" layer="21"/>
<wire x1="0.45" y1="0.9" x2="-0.45" y2="0.9" width="0.127" layer="21"/>
<wire x1="0.45" y1="0.5" x2="-0.45" y2="0.5" width="0.127" layer="21"/>
<wire x1="0.45" y1="-0.5" x2="-0.45" y2="-0.5" width="0.127" layer="21"/>
<text x="-1.27" y="0" size="0.8128" layer="25" font="vector" ratio="10" rot="R270" align="center">&gt;NAME</text>
<smd name="1" x="0" y="0.8" dx="1" dy="0.95" layer="1" roundness="20"/>
<smd name="2" x="0" y="-0.8" dx="1" dy="0.95" layer="1" roundness="20"/>
</package>
<package name="C0805">
<smd name="1" x="0" y="0.95" dx="1.5" dy="1.25" layer="1" roundness="20"/>
<smd name="2" x="0" y="-0.95" dx="1.5" dy="1.25" layer="1" roundness="20"/>
<wire x1="0.675" y1="1" x2="0.675" y2="0.6" width="0.127" layer="21"/>
<wire x1="0.675" y1="0.6" x2="0.675" y2="-0.6" width="0.127" layer="21"/>
<wire x1="0.675" y1="-0.6" x2="0.675" y2="-1" width="0.127" layer="21"/>
<wire x1="-0.675" y1="1" x2="-0.675" y2="0.6" width="0.127" layer="21"/>
<wire x1="-0.675" y1="0.6" x2="-0.675" y2="-0.6" width="0.127" layer="21"/>
<wire x1="-0.675" y1="-0.6" x2="-0.675" y2="-1" width="0.127" layer="21"/>
<wire x1="0.675" y1="-1" x2="-0.675" y2="-1" width="0.127" layer="21"/>
<wire x1="0.675" y1="1" x2="-0.675" y2="1" width="0.127" layer="21"/>
<wire x1="0.675" y1="-0.6" x2="-0.675" y2="-0.6" width="0.127" layer="21"/>
<wire x1="0.675" y1="0.6" x2="-0.675" y2="0.6" width="0.127" layer="21"/>
<text x="-1.5875" y="0" size="0.8128" layer="25" font="vector" ratio="10" rot="R270" align="center">&gt;NAME</text>
</package>
<package name="C0402">
<smd name="1" x="0" y="0.5" dx="0.75" dy="0.7" layer="1" roundness="20"/>
<smd name="2" x="0" y="-0.5" dx="0.75" dy="0.7" layer="1" roundness="20"/>
<text x="-1.27" y="0" size="0.8128" layer="25" font="vector" ratio="10" rot="R270" align="center">&gt;NAME</text>
<wire x1="-0.275" y1="0.5" x2="0.275" y2="0.5" width="0.127" layer="21"/>
<wire x1="-0.275" y1="-0.5" x2="0.275" y2="-0.5" width="0.127" layer="21"/>
<wire x1="0.275" y1="0.5" x2="0.275" y2="0.3" width="0.127" layer="21"/>
<wire x1="0.275" y1="0.3" x2="0.275" y2="-0.3" width="0.127" layer="21"/>
<wire x1="0.275" y1="-0.3" x2="0.275" y2="-0.5" width="0.127" layer="21"/>
<wire x1="-0.275" y1="0.5" x2="-0.275" y2="0.3" width="0.127" layer="21"/>
<wire x1="-0.275" y1="0.3" x2="-0.275" y2="-0.3" width="0.127" layer="21"/>
<wire x1="-0.275" y1="-0.3" x2="-0.275" y2="-0.5" width="0.127" layer="21"/>
<wire x1="-0.275" y1="0.3" x2="0.275" y2="0.3" width="0.127" layer="21"/>
<wire x1="-0.275" y1="-0.3" x2="0.275" y2="-0.3" width="0.127" layer="21"/>
</package>
<package name="COILCRAFT-10MM">
<smd name="1" x="-4.7" y="0" dx="3.56" dy="1.78" layer="1" rot="R90"/>
<smd name="2" x="4.7" y="0" dx="3.56" dy="1.78" layer="1" rot="R90"/>
<wire x1="-5.1" y1="-5" x2="5.1" y2="-5" width="0.1" layer="21"/>
<wire x1="5.1" y1="-5" x2="5.1" y2="5" width="0.1" layer="21"/>
<wire x1="5.1" y1="5" x2="-5.1" y2="5" width="0.1" layer="21"/>
<wire x1="-5.1" y1="5" x2="-5.1" y2="-5" width="0.1" layer="21"/>
<text x="0" y="0" size="1.778" layer="21" rot="R270" align="center">&gt;NAME</text>
</package>
<package name="C1206+">
<smd name="+" x="0" y="1.4" dx="1.8" dy="1.45" layer="1" roundness="20"/>
<smd name="-" x="0" y="-1.4" dx="1.8" dy="1.45" layer="1" roundness="20"/>
<wire x1="-0.85" y1="1.6" x2="-0.85" y2="1" width="0.127" layer="21"/>
<wire x1="-0.85" y1="1" x2="-0.85" y2="-1" width="0.127" layer="21"/>
<wire x1="-0.85" y1="-1" x2="-0.85" y2="-1.6" width="0.127" layer="21"/>
<wire x1="0.85" y1="1.6" x2="0.85" y2="1" width="0.127" layer="21"/>
<wire x1="0.85" y1="1" x2="0.85" y2="-1" width="0.127" layer="21"/>
<wire x1="0.85" y1="-1" x2="0.85" y2="-1.6" width="0.127" layer="21"/>
<wire x1="0.85" y1="1.6" x2="-0.85" y2="1.6" width="0.127" layer="21"/>
<wire x1="0.85" y1="-1.6" x2="-0.85" y2="-1.6" width="0.127" layer="21"/>
<wire x1="0.85" y1="1" x2="-0.85" y2="1" width="0.127" layer="21"/>
<wire x1="0.85" y1="-1" x2="-0.85" y2="-1" width="0.127" layer="21"/>
<text x="-1.5875" y="0" size="0.8128" layer="25" font="vector" ratio="10" rot="R270" align="center">&gt;NAME</text>
<wire x1="-0.635" y1="0.635" x2="0.635" y2="0.635" width="0.4064" layer="21"/>
</package>
<package name="SMC_B">
<description>&lt;b&gt;Chip Capacitor &lt;/b&gt; Polar tantalum capacitors with solid electrolyte&lt;p&gt;
Siemens Matsushita Components B 45 194, B 45 197, B 45 198&lt;br&gt;
Source: www.farnell.com/datasheets/247.pdf</description>
<wire x1="-1.6" y1="1.35" x2="1.6" y2="1.35" width="0.1016" layer="51"/>
<wire x1="1.6" y1="1.35" x2="1.6" y2="-1.35" width="0.1016" layer="51"/>
<wire x1="1.6" y1="-1.35" x2="-1.6" y2="-1.35" width="0.1016" layer="51"/>
<wire x1="-1.6" y1="-1.35" x2="-1.6" y2="1.35" width="0.1016" layer="51"/>
<wire x1="-2.43" y1="1.43" x2="2.43" y2="1.43" width="0.0508" layer="39"/>
<wire x1="2.43" y1="1.43" x2="2.43" y2="-1.42" width="0.0508" layer="39"/>
<wire x1="2.43" y1="-1.42" x2="-2.43" y2="-1.42" width="0.0508" layer="39"/>
<wire x1="-2.43" y1="-1.42" x2="-2.43" y2="1.43" width="0.0508" layer="39"/>
<rectangle x1="-1.75" y1="-1.1" x2="-1.55" y2="1.1" layer="51"/>
<rectangle x1="1.55" y1="-1.1" x2="1.75" y2="1.1" layer="51" rot="R180"/>
<rectangle x1="-1.6" y1="-1.35" x2="-0.95" y2="1.35" layer="51"/>
<smd name="+" x="-1.5" y="0" dx="1.6" dy="2.4" layer="1"/>
<smd name="-" x="1.5" y="0" dx="1.6" dy="2.4" layer="1" rot="R180"/>
<text x="-1.905" y="1.905" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.016" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="C">
<rectangle x1="-2.032" y1="-0.762" x2="2.032" y2="-0.254" layer="94"/>
<rectangle x1="-2.032" y1="0.254" x2="2.032" y2="0.762" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="0.762" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-0.762" width="0.1524" layer="94"/>
<pin name="1" x="0" y="2.54" visible="off" length="point" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="point" direction="pas" swaplevel="1" rot="R90"/>
<text x="1.524" y="1.651" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-3.429" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="R">
<description>Resistor</description>
<wire x1="-2.286" y1="0.762" x2="2.286" y2="0.762" width="0.2794" layer="94"/>
<wire x1="-2.286" y1="-0.762" x2="2.286" y2="-0.762" width="0.2794" layer="94"/>
<wire x1="-2.286" y1="-0.762" x2="-2.286" y2="0" width="0.2794" layer="94"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="0.762" width="0.2794" layer="94"/>
<wire x1="2.286" y1="0.762" x2="2.286" y2="0" width="0.2794" layer="94"/>
<wire x1="2.286" y1="0" x2="2.286" y2="-0.762" width="0.2794" layer="94"/>
<wire x1="2.286" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-2.286" y2="0" width="0.1524" layer="94"/>
<pin name="1" x="-2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
<pin name="2" x="2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
<text x="0" y="1.27" size="1.778" layer="95" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.27" size="1.778" layer="96" align="top-center">&gt;VALUE</text>
</symbol>
<symbol name="L">
<wire x1="-5.08" y1="0" x2="-3.81" y2="0" width="0.254" layer="94"/>
<wire x1="3.81" y1="0" x2="5.08" y2="0" width="0.254" layer="94"/>
<wire x1="3.81" y1="0" x2="1.27" y2="0" width="0.254" layer="94" curve="180"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="0" width="0.254" layer="94" curve="180"/>
<wire x1="-1.27" y1="0" x2="-3.81" y2="0" width="0.254" layer="94" curve="180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
<text x="0" y="2.54" size="1.778" layer="95" align="center">&gt;NAME</text>
<text x="0" y="-1.27" size="1.778" layer="96" align="center">&gt;VALUE</text>
</symbol>
<symbol name="R1NV">
<wire x1="-2.54" y1="-0.762" x2="2.54" y2="-0.762" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.762" x2="-2.54" y2="0.762" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.762" x2="2.54" y2="0.762" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0.762" x2="-2.54" y2="-0.762" width="0.254" layer="94"/>
<pin name="1" x="-5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="2.54" y="-3.048" size="1.778" layer="96">&gt;VALUE</text>
<text x="-5.08" y="-3.048" size="1.778" layer="95">&gt;NAME</text>
</symbol>
<symbol name="C+">
<rectangle x1="-2.032" y1="-0.762" x2="2.032" y2="-0.254" layer="94"/>
<rectangle x1="-2.032" y1="0.254" x2="2.032" y2="0.762" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="0.762" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-0.762" width="0.1524" layer="94"/>
<pin name="+" x="0" y="2.54" visible="off" length="point" direction="pas" swaplevel="1" rot="R270"/>
<pin name="-" x="0" y="-2.54" visible="off" length="point" direction="pas" swaplevel="1" rot="R90"/>
<text x="1.524" y="1.651" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-3.429" size="1.778" layer="96">&gt;VALUE</text>
<text x="-1.905" y="0.889" size="1.4224" layer="94">+</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="C" prefix="C" uservalue="yes">
<description>SMD Capacitor</description>
<gates>
<gate name="G$1" symbol="C" x="0" y="0"/>
</gates>
<devices>
<device name="-0402" package="0402-1005X55N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="-100NF">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="http://www.tdk.com/pdf/TDKMLCCCapRange.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="http://www.digikey.com/product-detail/en/tdk-corporation/C1005X5R1H104K050BB/445-5942-1-ND/2443982" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="445-5942-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="TDK" constant="no"/>
<attribute name="MANUFACTURER-PN" value="C1005X5R1H104K050BB" constant="no"/>
<attribute name="VALUE" value="100n" constant="no"/>
</technology>
<technology name="-10NF">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="http://search.murata.co.jp/Ceramy/image/img/A01X/partnumbering_e_01.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="http://www.digikey.com/product-detail/en/murata-electronics-north-america/GRM155R71H103KA88J/490-6351-1-ND/3845548" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="490-6351-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Murata " constant="no"/>
<attribute name="MANUFACTURER-PN" value="GRM155R71H103KA88J " constant="no"/>
<attribute name="VALUE" value="10n" constant="no"/>
</technology>
<technology name="-10PF">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="http://search.murata.co.jp/Ceramy/image/img/A01X/partnumbering_e_01.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/murata-electronics-north-america/GRM1555C1H100FA01D/490-6186-1-ND/3845386" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="490-6186-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Murata" constant="no"/>
<attribute name="MANUFACTURER-PN" value="GRM1555C1H100FA01D" constant="no"/>
<attribute name="VALUE" value="10p" constant="no"/>
</technology>
<technology name="-110P">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="http://search.murata.co.jp/Ceramy/image/img/A01X/G101/ENG/GRM1555C1H111GA01-01.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/murata-electronics-north-america/GRM1555C1H111GA01D/490-6195-1-ND/3845392" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="490-6195-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Murata" constant="no"/>
<attribute name="MANUFACTURER-PN" value="GRM1555C1H111GA01D" constant="no"/>
<attribute name="VALUE" value="110p" constant="no"/>
</technology>
<technology name="-15PF">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="http://search.murata.co.jp/Ceramy/image/img/A01X/partnumbering_e_01.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/murata-electronics-north-america/GRM1555C1H150JA01D/490-5888-1-ND/3315234" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="490-5888-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Murata" constant="no"/>
<attribute name="MANUFACTURER-PN" value="GRM1555C1H150JA01D" constant="no"/>
<attribute name="VALUE" value="15p" constant="no"/>
</technology>
<technology name="-1NF">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="http://www.samsungsem.com/kr/support/product-search/mlcc/__icsFiles/afieldfile/2016/08/18/S_CL05B102KB5NNNC.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="http://www.digikey.com/product-detail/en/samsung-electro-mechanics-america-inc/CL05B102KB5NNNC/1276-1032-1-ND/3889118" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="1276-1032-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Samsung" constant="no"/>
<attribute name="MANUFACTURER-PN" value="CL05B102KB5NNNC " constant="no"/>
<attribute name="VALUE" value="1n" constant="no"/>
</technology>
<technology name="-1UF">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="http://search.murata.co.jp/Ceramy/image/img/A01X/partnumbering_e_01.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="http://www.digikey.com/product-detail/en/murata-electronics-north-america/GRM155R61E105KA12D/490-10017-1-ND/5026367" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="490-10017-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Murata" constant="no"/>
<attribute name="MANUFACTURER-PN" value="GRM155R61E105KA12D " constant="no"/>
<attribute name="VALUE" value="1u" constant="no"/>
</technology>
<technology name="-2.2UF">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="http://search.murata.co.jp/Ceramy/image/img/A01X/partnumbering_e_01.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/murata-electronics-north-america/GRM155R61A225KE95D/490-10451-1-ND/5026361" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="490-10451-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Murata" constant="no"/>
<attribute name="MANUFACTURER-PN" value="GRM155R61A225KE95D" constant="no"/>
<attribute name="VALUE" value="2u2" constant="no"/>
</technology>
<technology name="-20PF">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="http://www.kemet.com/docfinder?Partnumber=CBR04C200J5GAC" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="http://www.digikey.com/product-detail/en/kemet/CBR04C200J5GAC/399-6173-1-ND/2732143" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="399-6173-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Kemet" constant="no"/>
<attribute name="MANUFACTURER-PN" value="CBR04C200J5GAC " constant="no"/>
<attribute name="VALUE" value="20p" constant="no"/>
</technology>
<technology name="-22N">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="http://www.murata.com/~/media/webrenewal/support/library/catalog/products/capacitor/mlcc/c02e.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="http://www.digikey.com/product-detail/en/murata-electronics-north-america/GRM155R71H223KA12D/490-3884-1-ND/965926" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="490-3884-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Murata" constant="no"/>
<attribute name="MANUFACTURER-PN" value="GRM155R71H223KA12D" constant="no"/>
<attribute name="VALUE" value="22n" constant="no"/>
</technology>
<technology name="-22PF">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="http://search.murata.co.jp/Ceramy/image/img/A01X/partnumbering_e_01.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.se/products/en?keywords=490-8589-1-ND" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="490-8589-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Murata" constant="no"/>
<attribute name="MANUFACTURER-PN" value="GRM1555C1H220FA01D" constant="no"/>
<attribute name="VALUE" value="22p" constant="no"/>
</technology>
<technology name="-27PF">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="http://search.murata.co.jp/Ceramy/image/img/A01X/partnumbering_e_01.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/products/en?keywords=%20490-6176-1-ND%20" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="490-6176-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Murata" constant="no"/>
<attribute name="MANUFACTURER-PN" value="GRM1555C1E270JA01D" constant="no"/>
<attribute name="VALUE" value="27p" constant="no"/>
</technology>
<technology name="-33PF">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="http://www.murata.com/~/media/webrenewal/support/library/catalog/products/capacitor/mlcc/c02e.pdf " constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="http://www.digikey.com/product-search/en?keywords=490-5936-1-ND" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="490-5936-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Murata" constant="no"/>
<attribute name="MANUFACTURER-PN" value="GRM1555C1H330JA01D" constant="no"/>
<attribute name="VALUE" value="33p" constant="no"/>
</technology>
<technology name="-4.7UF">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="http://search.murata.co.jp/Ceramy/image/img/A01X/partnumbering_e_01.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="http://www.digikey.com/product-detail/en/murata-electronics-north-america/GRM155R60J475ME47D/490-5915-1-ND/3719860" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="490-5915-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Murata" constant="no"/>
<attribute name="MANUFACTURER-PN" value="GRM155R60J475ME47D " constant="no"/>
<attribute name="VALUE" value="4u7" constant="no"/>
</technology>
<technology name="-47N">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="http://www.murata.com/~/media/webrenewal/support/library/catalog/products/capacitor/mlcc/c02e.pdf " constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/murata-electronics-north-america/GRM155R71E473KA88D/490-3254-1-ND/702795" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="490-3254-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Murata" constant="no"/>
<attribute name="MANUFACTURER-PN" value="GRM155R71E473KA88D" constant="no"/>
<attribute name="VALUE" value="47n" constant="no"/>
</technology>
<technology name="-4N7">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="http://search.murata.co.jp/Ceramy/image/img/A01X/partnumbering_e_01.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/murata-electronics-north-america/GRM155R71H472KA01J/490-8259-1-ND/4380553" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="490-8259-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Murata" constant="no"/>
<attribute name="MANUFACTURER-PN" value="GRM155R71H472KA01J" constant="no"/>
<attribute name="VALUE" value="4n7" constant="no"/>
</technology>
<technology name="-56P">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="http://www.mouser.com/ds/2/281/c02e-2905.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="http://www.mouser.it/ProductDetail/Murata-Electronics/GRM1555C1E560JA01D/?qs=sGAEpiMZZMs0AnBnWHyRQA3eyUP2RfX77sTVhhB4SFb8UyJFwKUI8g%3d%3d" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="81-GRM1555C1E560JA1D" constant="no"/>
<attribute name="MANUFACTURER" value="Murata" constant="no"/>
<attribute name="MANUFACTURER-PN" value=" GRM1555C1E560JA01D " constant="no"/>
<attribute name="VALUE" value="56p" constant="no"/>
</technology>
<technology name="-5N6">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="http://search.murata.co.jp/Ceramy/image/img/A01X/partnumbering_e_01.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/murata-electronics-north-america/GRM155R71H562KA88D/490-6364-1-ND/3845561" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="490-6364-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Murata" constant="no"/>
<attribute name="MANUFACTURER-PN" value="GRM155R71H562KA88D" constant="no"/>
<attribute name="VALUE" value="5n6" constant="no"/>
</technology>
<technology name="-68PF">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="http://search.murata.co.jp/Ceramy/image/img/A01X/partnumbering_e_01.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/murata-electronics-north-america/GRM1555C2A680JA01D/490-7309-1-ND/4213238" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="490-7309-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Murata" constant="no"/>
<attribute name="MANUFACTURER-PN" value="GRM1555C2A680JA01D" constant="no"/>
<attribute name="VALUE" value="68p" constant="no"/>
</technology>
<technology name="-6N8">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="http://search.murata.co.jp/Ceramy/image/img/A01X/G101/ENG/GRM155R71H682KA88-01.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="http://www.digikey.com/product-detail/en/murata-electronics-north-america/GRM155R71H682KA88D/490-4515-1-ND/1033274" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="490-4515-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Murata" constant="no"/>
<attribute name="MANUFACTURER-PN" value="GRM155R71H682KA88D" constant="no"/>
<attribute name="VALUE" value="6n8" constant="no"/>
</technology>
<technology name="-8N2">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="http://www.yageo.com/documents/recent/UPY-GPHC_X7R_6.3V-to-50V_18.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/yageo/CC0402KRX7R7BB822/311-1041-1-ND/302958" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="311-1041-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER-PN" value="CC0402KRX7R7BB822" constant="no"/>
<attribute name="VALUE" value="8n2" constant="no"/>
</technology>
<technology name="DNP">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="DNP" constant="no"/>
<attribute name="MANUFACTURER" value="DNP" constant="no"/>
<attribute name="MANUFACTURER-PN" value="DNP" constant="no"/>
<attribute name="VALUE" value="DNP" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0805" package="0805-R2013X50N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="-22U">
<attribute name="DATASHEET" value="https://media.digikey.com/pdf/Data%20Sheets/Samsung%20PDFs/CL21A226MQCLQNC_character.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/samsung-electro-mechanics-america-inc/CL21A226MQCLQNC/1276-2412-1-ND/3890498" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="1276-2412-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Samsung" constant="no"/>
<attribute name="MANUFACTURER-PN" value="CL21A226MQCLQNC" constant="no"/>
<attribute name="VALUE" value="22u" constant="no"/>
</technology>
<technology name="4.7NF">
<attribute name="DATASHEET" value="http://www.kemet.com/Lists/ProductCatalog/Attachments/40/KEM_C1010_X7R_HV_SMD.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="www.kemet.com/docfinder?Partnumber=C0805C472KDRACTU" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="399-6738-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Kemet" constant="no"/>
<attribute name="MANUFACTURER-PN" value="C0805C472KDRACTU" constant="no"/>
<attribute name="VALUE" value="4n7" constant="no"/>
</technology>
<technology name="DNP">
<attribute name="DATASHEET" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="DNP" constant="no"/>
<attribute name="MANUFACTURER" value="DNP" constant="no"/>
<attribute name="MANUFACTURER-PN" value="DNP" constant="no"/>
<attribute name="VALUE" value="DNP" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0603" package="0603-1608X90N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="-100NF">
<attribute name="CODE-BCE" value="SP0015" constant="no"/>
<attribute name="CODE-BCMI" value="0015" constant="no"/>
<attribute name="CODE-TK2" value="CL10B104KB8NNNC" constant="no"/>
<attribute name="DATASHEET" value="http://search.murata.co.jp/Ceramy/image/img/A01X/G101/ENG/GRM188R71C104KA01-01.pdf " constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/murata-electronics-north-america/GRM188R71C104KA01D/490-1532-1-ND/587771" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="490-1532-2-ND " constant="no"/>
<attribute name="MANUFACTURER" value="Murata" constant="no"/>
<attribute name="MANUFACTURER-PN" value="GRM188R71C104KA01D" constant="no"/>
<attribute name="VALUE" value="100n" constant="no"/>
</technology>
<technology name="-10NF">
<attribute name="CODE-BCE" value="SP0142" constant="no"/>
<attribute name="CODE-BCMI" value="0142" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="http://www.murata.com/~/media/webrenewal/support/library/catalog/products/capacitor/mlcc/c02e.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/murata-electronics-north-america/GRM188R71H103KA01D/490-1512-1-ND/587862" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="490-1512-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Murata" constant="no"/>
<attribute name="MANUFACTURER-PN" value="GRM188R71H103KA01D" constant="no"/>
<attribute name="VALUE" value="10n" constant="no"/>
</technology>
<technology name="-10P">
<attribute name="CODE-BCE" value="SP0245" constant="no"/>
<attribute name="CODE-BCMI" value="0245" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="-" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="-" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="-" constant="no"/>
<attribute name="MANUFACTURER" value="-" constant="no"/>
<attribute name="MANUFACTURER-PN" value="-" constant="no"/>
<attribute name="VALUE" value="10p" constant="no"/>
</technology>
<technology name="-10UF">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="http://www.murata.com/~/media/webrenewal/support/library/catalog/products/capacitor/mlcc/c02e.pdf " constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="http://www.digikey.com/product-search/en?keywords=490-10728-1-ND " constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="490-10728-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Murata" constant="no"/>
<attribute name="MANUFACTURER-PN" value="GRM188R61C106KAALD" constant="no"/>
<attribute name="VALUE" value="10u" constant="no"/>
</technology>
<technology name="-1UF">
<attribute name="CODE-BCE" value="SP0006" constant="no"/>
<attribute name="CODE-BCMI" value="0006" constant="no"/>
<attribute name="CODE-TK2" value="0603X105K250CG" constant="no"/>
<attribute name="DATASHEET" value="http://www.samsungsem.com/kr/support/product-search/mlcc/__icsFiles/afieldfile/2016/08/18/S_CL10B105KP8NNNC.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/samsung-electro-mechanics-america-inc/CL10B105KP8NNNC/1276-1946-1-ND/3890032" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="1276-1946-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Samsung" constant="no"/>
<attribute name="MANUFACTURER-PN" value="CL10B105KP8NNNC" constant="no"/>
<attribute name="VALUE" value="1u" constant="no"/>
</technology>
<technology name="-22PF">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="0014" constant="no"/>
<attribute name="CODE-TK2" value="CL10C220JB8NNNC" constant="no"/>
<attribute name="DATASHEET" value="http://www.samsungsem.com/kr/support/product-search/mlcc/__icsFiles/afieldfile/2016/08/18/S_CL10C220JB8NNNC.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/samsung-electro-mechanics-america-inc/CL10C220JB8NNNC/1276-1023-1-ND/3889109" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="1276-1023-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Samsung" constant="no"/>
<attribute name="MANUFACTURER-PN" value="CL10C220JB8NNNC" constant="no"/>
<attribute name="VALUE" value="22p" constant="no"/>
</technology>
<technology name="-4.7UF">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="http://www.murata.com/~/media/webrenewal/support/library/catalog/products/capacitor/mlcc/c02e.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="http://www.digikey.com/product-search/en?keywords=490-7203-1-ND" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="490-7203-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Murata" constant="no"/>
<attribute name="MANUFACTURER-PN" value="GRM188R61E475KE11D" constant="no"/>
<attribute name="VALUE" value="4u7" constant="no"/>
</technology>
<technology name="-DNP">
<attribute name="CODE-BCE" value="DNP" constant="no"/>
<attribute name="CODE-BCMI" value="DNP" constant="no"/>
<attribute name="CODE-TK2" value="DNP" constant="no"/>
<attribute name="DATASHEET" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="DNP" constant="no"/>
<attribute name="MANUFACTURER" value="DNP" constant="no"/>
<attribute name="MANUFACTURER-PN" value="DNP" constant="no"/>
<attribute name="VALUE" value="DNP" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1206" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="-100UF">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="http://search.murata.co.jp/Ceramy/image/img/A01X/partnumbering_e_01.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/murata-electronics-north-america/GRM31CD80J107ME39L/490-10525-1-ND/5026461" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="490-10525-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Murata" constant="no"/>
<attribute name="MANUFACTURER-PN" value="GRM31CD80J107ME39L" constant="no"/>
<attribute name="VALUE" value="100uF" constant="no"/>
</technology>
<technology name="-10UF">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="https://product.tdk.com/en/products/common/pdf/mlcc_partnumber_description.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.se/products/en?keywords=445-14694-1-ND" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="445-14694-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="TDK Corporation" constant="no"/>
<attribute name="MANUFACTURER-PN" value="C3216X5R1V106K160AB" constant="no"/>
<attribute name="VALUE" value="10uF" constant="no"/>
</technology>
<technology name="DNP">
<attribute name="CODE-BCE" value="DNP" constant="no"/>
<attribute name="CODE-BCMI" value="DNP" constant="no"/>
<attribute name="CODE-TK2" value="DNP" constant="no"/>
<attribute name="DATASHEET" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="DNP" constant="no"/>
<attribute name="MANUFACTURER" value="DNP" constant="no"/>
<attribute name="MANUFACTURER-PN" value="DNP" constant="no"/>
<attribute name="VALUE" value="DNP" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1210" package="C1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="-10UF">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="ds.yuden.co.jp/TYCOMPAS/ut/detail.do?productNo=GMK325BJ106MN-T&amp;fileName=GMK325BJ106MN-T_SS&amp;mode=specSheetDownload" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/it/taiyo-yuden/GMK325BJ106MN-T/587-2832-1-ND/2607799" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="587-2832-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Taiyo Yuden" constant="no"/>
<attribute name="MANUFACTURER-PN" value="GMK325BJ106MN-T" constant="no"/>
<attribute name="VALUE" value="10u" constant="no"/>
</technology>
<technology name="-22U">
<attribute name="CODE-BCE" value="SP0167" constant="no"/>
<attribute name="CODE-BCMI" value="0167" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="-" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="-" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="-" constant="no"/>
<attribute name="MANUFACTURER" value="-" constant="no"/>
<attribute name="MANUFACTURER-PN" value="-" constant="no"/>
<attribute name="VALUE" value="22u" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="R" prefix="R" uservalue="yes">
<description>SMD Resistor</description>
<gates>
<gate name="G$1" symbol="R" x="0" y="0"/>
</gates>
<devices>
<device name="-0402" package="0402-1005X55N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="-0R">
<attribute name="CODE-BCE" value="SP0039" constant="no"/>
<attribute name="CODE-BCMI" value="0039" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="www.yageo.com.tw/exep/pages/download/literatures/PYu-R_INT-thick_7.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="http://www.digikey.com/product-detail/en/yageo/RC0402FR-070RL/311-0.0LRCT-ND/2827888" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="311-0.0LRCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER-PN" value="RC0402FR-070RL" constant="no"/>
<attribute name="VALUE" value="0R" constant="no"/>
</technology>
<technology name="-100K">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="www.yageo.com.tw/exep/pages/download/literatures/PYu-R_INT-thick_7.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="http://www.digikey.com/product-detail/en/yageo/RC0402FR-07100KL/311-100KLRCT-ND/729473" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="311-100KLRCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo " constant="no"/>
<attribute name="MANUFACTURER-PN" value="RC0402FR-07100KL " constant="no"/>
<attribute name="VALUE" value="100k" constant="no"/>
</technology>
<technology name="-100R">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="www.yageo.com.tw/exep/pages/download/literatures/PYu-R_INT-thick_7.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="http://www.digikey.com/product-search/en?keywords=311-100LRCT-ND" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="311-100LRCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER-PN" value="RC0402FR-07100RL" constant="no"/>
<attribute name="VALUE" value="100R" constant="no"/>
</technology>
<technology name="-10K">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="www.yageo.com.tw/exep/pages/download/literatures/PYu-R_INT-thick_7.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="http://www.digikey.com/product-detail/en/yageo/RC0402FR-0710KL/311-10.0KLRCT-ND/729470" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="311-10.0KLRCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER-PN" value="RC0402FR-0710KL" constant="no"/>
<attribute name="VALUE" value="10k" constant="no"/>
</technology>
<technology name="-10K2">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="www.yageo.com.tw/exep/pages/download/literatures/PYu-R_INT-thick_7.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/yageo/RT0402DRD0710K2L/311-2129-1-ND/6128548" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="311-2129-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER-PN" value="RT0402DRD0710K2L" constant="no"/>
<attribute name="VALUE" value="10k2" constant="no"/>
</technology>
<technology name="-10R">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="www.yageo.com.tw/exep/pages/download/literatures/PYu-R_INT-thick_7.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="http://www.digikey.com/product-detail/en/yageo/RC0402JR-0710KL/311-10KJRCT-ND/729365 " constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="311-10KJRCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER-PN" value="RC0402JR-0710KL" constant="no"/>
<attribute name="VALUE" value="10R" constant="no"/>
</technology>
<technology name="-120R">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="www.yageo.com.tw/exep/pages/download/literatures/PYu-R_INT-thick_7.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/yageo/RC0402JR-07120RL/311-120JRCT-ND/2827967" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="311-120JRCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER-PN" value="RC0402JR-07120RL" constant="no"/>
<attribute name="VALUE" value="120R" constant="no"/>
</technology>
<technology name="-12K">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="www.yageo.com.tw/exep/pages/download/literatures/PYu-R_INT-thick_7.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/yageo/RC0402JR-0712KL/311-12KJRCT-ND/729370" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="311-12KJRCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER-PN" value="RC0402JR-0712KL" constant="no"/>
<attribute name="VALUE" value="12k" constant="no"/>
</technology>
<technology name="-12K7">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="www.yageo.com.tw/exep/pages/download/literatures/PYu-R_INT-thick_7.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/yageo/RC0402FR-0712K7L/YAG2964CT-ND/5281829" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="YAG2964CT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER-PN" value="RC0402FR-0712K7L" constant="no"/>
<attribute name="VALUE" value="12k7" constant="no"/>
</technology>
<technology name="-137R">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="www.yageo.com.tw/exep/pages/download/literatures/PYu-R_INT-thick_7.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/yageo/RC0402FR-07137RL/YAG2973CT-ND/5281838" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="YAG2973CT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER-PN" value="RC0402FR-07137RL" constant="no"/>
<attribute name="VALUE" value="137R" constant="no"/>
</technology>
<technology name="-1K">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="www.yageo.com.tw/exep/pages/download/literatures/PYu-R_INT-thick_7.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="http://www.digikey.com/product-detail/en/yageo/RC0402FR-071KL/311-1.00KLRCT-ND/729460 " constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="311-1.00KLRCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER-PN" value="RC0402FR-071KL" constant="no"/>
<attribute name="VALUE" value="1k" constant="no"/>
</technology>
<technology name="-1K7">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="www.yageo.com.tw/exep/pages/download/literatures/PYu-R_INT-thick_7.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/yageo/RC0402FR-071K69L/YAG3044CT-ND/5281909" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="YAG3044CT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER-PN" value="RC0402FR-071K69L" constant="no"/>
<attribute name="VALUE" value="1k69" constant="no"/>
</technology>
<technology name="-1K91">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="www.yageo.com.tw/exep/pages/download/literatures/PYu-R_INT-thick_7.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/yageo/RC0402FR-071K91L/YAG3049CT-ND/5281914" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="YAG3049CT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER-PN" value="RC0402FR-071K91L" constant="no"/>
<attribute name="VALUE" value="1k91" constant="no"/>
</technology>
<technology name="-1M">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="www.yageo.com.tw/exep/pages/download/literatures/PYu-R_INT-thick_7.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="http://www.digikey.com/product-detail/en/yageo/RC0402JR-07330RL/311-330JRCT-ND/729412 " constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="311-330JRCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER-PN" value="RC0402JR-07330RL" constant="no"/>
<attribute name="VALUE" value="1M" constant="no"/>
</technology>
<technology name="-1M2">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="www.yageo.com.tw/exep/pages/download/literatures/PYu-R_INT-thick_7.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="http://www.digikey.com/products/en?keywords=RC0402FR-071M2L-ND" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="RC0402FR-071M2L-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER-PN" value="RC0402FR-071M2L" constant="no"/>
<attribute name="VALUE" value="1M2" constant="no"/>
</technology>
<technology name="-200R">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="www.yageo.com.tw/exep/pages/download/literatures/PYu-R_INT-thick_7.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/yageo/RC0402FR-07200RL/311-200LRCT-ND/2827887" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="311-200LRCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER-PN" value="RC0402FR-07200RL" constant="no"/>
<attribute name="VALUE" value="200R" constant="no"/>
</technology>
<technology name="-220K">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="www.yageo.com.tw/exep/pages/download/literatures/PYu-R_INT-thick_7.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/yageo/RC0402JR-07220KL/311-220KJRCT-ND/729390" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="311-220KJRCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER-PN" value="RC0402JR-07220KL" constant="no"/>
<attribute name="VALUE" value="220k" constant="no"/>
</technology>
<technology name="-22K">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="http://www.yageo.com/NewPortal/yageodocoutput?fileName=/pdf/R-Chip/PYu-AC_51_RoHS_L_6.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.se/products/en?keywords=%09YAG3532CT-ND" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="YAG3532CT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER-PN" value="AC0402JR-0722KL" constant="no"/>
<attribute name="VALUE" value="22K" constant="no"/>
</technology>
<technology name="-22R">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="www.yageo.com.tw/exep/pages/download/literatures/PYu-R_INT-thick_7.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="http://www.digikey.com/products/en?keywords=311-22.0LRCT-ND" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="311-22.0LRCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER-PN" value="RC0402FR-0722RL" constant="no"/>
<attribute name="VALUE" value="22R" constant="no"/>
</technology>
<technology name="-270R">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="www.yageo.com.tw/exep/pages/download/literatures/PYu-R_INT-thick_7.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="http://www.digikey.com/product-detail/en/yageo/RC0402JR-07270RL/311-270JRCT-ND/729395" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="311-270JRCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER-PN" value="RC0402JR-07270RL" constant="no"/>
<attribute name="VALUE" value="270R" constant="no"/>
</technology>
<technology name="-2K87">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="www.yageo.com.tw/exep/pages/download/literatures/PYu-R_INT-thick_7.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="http://www.digikey.com/product-detail/en/yageo/RC0402JR-07330RL/311-330JRCT-ND/729412" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="YAG3105CT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER-PN" value="RC0402FR-072K87L" constant="no"/>
<attribute name="VALUE" value="2k87" constant="no"/>
</technology>
<technology name="-2M">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="www.yageo.com.tw/exep/pages/download/literatures/PYu-R_INT-thick_7.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/yageo/RC0402JR-072ML/YAG3295CT-ND/5282161" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="YAG3295CT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER-PN" value="RC0402JR-072ML" constant="no"/>
<attribute name="VALUE" value="2M" constant="no"/>
</technology>
<technology name="-330K">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="www.yageo.com.tw/exep/pages/download/literatures/PYu-R_INT-thick_7.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="http://www.digikey.com/product-detail/en/yageo/RC0402JR-07330KL/311-330KJRCT-ND/729413 " constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="311-330KJRCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER-PN" value="RC0402JR-07330KL" constant="no"/>
<attribute name="VALUE" value="330k" constant="no"/>
</technology>
<technology name="-330R">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="www.yageo.com.tw/exep/pages/download/literatures/PYu-R_INT-thick_7.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="http://www.digikey.com/product-detail/en/yageo/RC0402FR-07330RL/311-330LRCT-ND/729541" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="311-330LRCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo " constant="no"/>
<attribute name="MANUFACTURER-PN" value="RC0402FR-07330RL " constant="no"/>
<attribute name="VALUE" value="330R" constant="no"/>
</technology>
<technology name="-390R">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="www.yageo.com.tw/exep/pages/download/literatures/PYu-R_INT-thick_7.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/yageo/RC0402FR-07390RL/311-390LRCT-ND/2827929" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="311-390LRCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER-PN" value="RC0402FR-07390RL" constant="no"/>
<attribute name="VALUE" value="390R" constant="no"/>
</technology>
<technology name="-39K">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="www.yageo.com.tw/exep/pages/download/literatures/PYu-R_INT-thick_7.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/yageo/RC0402JR-0739KL/311-39KJRCT-ND/729420" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="311-39KJRCT-ND " constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER-PN" value="RC0402JR-0739KL" constant="no"/>
<attribute name="VALUE" value="39k" constant="no"/>
</technology>
<technology name="-39R">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="www.yageo.com.tw/exep/pages/download/literatures/PYu-R_INT-thick_7.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="http://www.digikey.com/product-detail/en/yageo/RC0402FR-0739RL/311-39.0LRCT-ND/729549" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="311-39.0LRCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo " constant="no"/>
<attribute name="MANUFACTURER-PN" value="RC0402FR-0739RL " constant="no"/>
<attribute name="VALUE" value="39R" constant="no"/>
</technology>
<technology name="-3K3">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="www.yageo.com.tw/exep/pages/download/literatures/PYu-R_INT-thick_7.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/yageo/RC0402FR-073K3L/311-3.30KLRCT-ND/729527" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="311-3.30KLRCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER-PN" value="RC0402FR-073K3L" constant="no"/>
<attribute name="VALUE" value="3k3" constant="no"/>
</technology>
<technology name="-3K83">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="www.yageo.com.tw/exep/pages/download/literatures/PYu-R_INT-thick_7.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/yageo/RC0402FR-073K83L/311-3.83KLRCT-ND/2827952" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="311-3.83KLRCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER-PN" value="RC0402FR-073K83L" constant="no"/>
<attribute name="VALUE" value="3k83" constant="no"/>
</technology>
<technology name="-402R">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="www.yageo.com.tw/exep/pages/download/literatures/PYu-R_INT-thick_7.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/yageo/RC0402FR-07402RL/YAG3152CT-ND/5282017" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="YAG3152CT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER-PN" value="RC0402FR-07402RL" constant="no"/>
<attribute name="VALUE" value="402R" constant="no"/>
</technology>
<technology name="-43K">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="www.yageo.com.tw/exep/pages/download/literatures/PYu-R_INT-thick_7.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/yageo/RC0402FR-0743KL/311-43.0KLRCT-ND/729556" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="311-43.0KLRCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER-PN" value="RC0402FR-0743KL" constant="no"/>
<attribute name="VALUE" value="43k" constant="no"/>
</technology>
<technology name="-470K">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="www.yageo.com.tw/exep/pages/download/literatures/PYu-R_INT-thick_7.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/yageo/RC0402JR-07470KL/311-470KJRCT-ND/729430" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="311-470KJRCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER-PN" value="RC0402JR-07470KL" constant="no"/>
<attribute name="VALUE" value="470k" constant="no"/>
</technology>
<technology name="-47K">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="www.yageo.com.tw/exep/pages/download/literatures/PYu-R_INT-thick_7.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="http://www.digikey.com/product-detail/en/yageo/RC0402JR-0747KL/311-47KJRCT-ND/729432" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="311-47KJRCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER-PN" value="RC0402JR-0747KL" constant="no"/>
<attribute name="VALUE" value="47k" constant="no"/>
</technology>
<technology name="-4K7">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="www.yageo.com.tw/exep/pages/download/literatures/PYu-R_INT-thick_7.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="http://www.digikey.com/product-detail/en/yageo/RC0402FR-074K7L/311-4.7KLRCT-ND/2827881" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="311-4.7KLRCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo " constant="no"/>
<attribute name="MANUFACTURER-PN" value="RC0402FR-074K7L " constant="no"/>
<attribute name="VALUE" value="4k7" constant="no"/>
</technology>
<technology name="-5K49">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="www.yageo.com.tw/exep/pages/download/literatures/PYu-R_INT-thick_7.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/yageo/RC0402FR-075K49L/311-5.49KLRCT-ND/729573" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="311-5.49KLRCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER-PN" value="RC0402FR-075K49L" constant="no"/>
<attribute name="VALUE" value="5k49" constant="no"/>
</technology>
<technology name="-660R">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="www.yageo.com.tw/exep/pages/download/literatures/PYu-R_INT-thick_7.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="http://www.digikey.com/product-detail/en/yageo/RC0402FR-07665RL/YAG3208CT-ND/5282073" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="YAG3208CT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER-PN" value="RC0402FR-07665RL" constant="no"/>
<attribute name="VALUE" value="665R" constant="no"/>
</technology>
<technology name="-680K">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="www.yageo.com.tw/exep/pages/download/literatures/PYu-R_INT-thick_7.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="http://www.digikey.com/product-detail/it/yageo/RC0402JR-07680KL/311-680KJRTR-ND/726501 " constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="311-680KJRTR-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER-PN" value="RC0402JR-07680KL" constant="no"/>
<attribute name="VALUE" value="680k" constant="no"/>
</technology>
<technology name="-82R">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="www.yageo.com.tw/exep/pages/download/literatures/PYu-R_INT-thick_7.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/yageo/RC0402JR-0782RL/311-82JRCT-ND/729456" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="311-82JRCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER-PN" value="RC0402JR-0782RL" constant="no"/>
<attribute name="VALUE" value="82R" constant="no"/>
</technology>
<technology name="-8K2">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="www.yageo.com.tw/exep/pages/download/literatures/PYu-R_INT-thick_7.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/yageo/RC0402JR-078K2L/311-8.2KJRCT-ND/729454" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="311-8.2KJRCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER-PN" value="RC0402JR-078K2L" constant="no"/>
<attribute name="VALUE" value="8k2" constant="no"/>
</technology>
<technology name="-910K">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="www.yageo.com.tw/exep/pages/download/literatures/PYu-R_INT-thick_7.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/yageo/RC0402FR-07910KL/YAG3257CT-ND/5282123" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="YAG3257CT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER-PN" value="RC0402FR-07910KL" constant="no"/>
<attribute name="VALUE" value="910k" constant="no"/>
</technology>
<technology name="-9K76">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="www.yageo.com.tw/exep/pages/download/literatures/PYu-R_INT-thick_7.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/yageo/RT0402DRD079K76L/311-2213-1-ND/6128632" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="311-2213-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER-PN" value="RT0402DRD079K76L" constant="no"/>
<attribute name="VALUE" value="9k76" constant="no"/>
</technology>
<technology name="-DNP">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="DNP" constant="no"/>
<attribute name="MANUFACTURER" value="DNP" constant="no"/>
<attribute name="MANUFACTURER-PN" value="DNP" constant="no"/>
<attribute name="VALUE" value="DNP" constant="no"/>
</technology>
<technology name="12K-1%">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="http://www.yageo.com/NewPortal/yageodocoutput?fileName=/pdf/R-Chip/PYu-RC_Group_51_RoHS_L_4.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/yageo/RC0402FR-0712KL/311-12.0KLRCT-ND/729479" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="311-12.0KLRCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER-PN" value="RC0402FR-0712KL" constant="no"/>
<attribute name="VALUE" value="12k-1%" constant="no"/>
</technology>
<technology name="12K4-1%">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="http://www.yageo.com/NewPortal/yageodocoutput?fileName=/pdf/R-Chip/PYu-RC_Group_51_RoHS_L_4.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/yageo/RC0402FR-0712K4L/311-12.4KLRCT-ND/2827940" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="311-12.4KLRCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER-PN" value="RC0402FR-0712K4L" constant="no"/>
<attribute name="VALUE" value="12k4-1%" constant="no"/>
</technology>
<technology name="390K">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="www.yageo.com.tw/exep/pages/download/literatures/PYu-R_INT-thick_7.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="http://www.digikey.com/product-detail/en/yageo/RC0402FR-07390KL/YAG3143CT-ND/5282008" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="YAG3143CT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER-PN" value="RC0402FR-07390KL" constant="no"/>
<attribute name="VALUE" value="390k" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0805" package="0805-R2013X50N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="-0.012R">
<attribute name="DATASHEET" value="https://industrial.panasonic.com/cdbs/www-data/pdf/RDA0000/AOA0000C295.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="http://www.digikey.com/product-detail/en/panasonic-electronic-components/ERJ-6CWFR012V/P19198CT-ND/6004553" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="P19198CT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Panasonic" constant="no"/>
<attribute name="MANUFACTURER-PN" value="ERJ-6CWFR012V" constant="no"/>
<attribute name="VALUE" value="0.012R" constant="no"/>
</technology>
<technology name="-100R">
<attribute name="DATASHEET" value="http://www.yageo.com.tw/exep/pages/download/literatures/PYu-R_INT-thick_7.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="http://www.digikey.de/product-detail/en/yageo/RC0805FR-07100RL/311-100CRTR-ND/727543?cur=EUR&amp;lang=en " constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="311-100CRTR-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER-PN" value="RC0805FR-07100RL" constant="no"/>
<attribute name="VALUE" value="100R" constant="no"/>
</technology>
<technology name="-47R">
<attribute name="DATASHEET" value="http://www.yageo.com.tw/exep/pages/download/literatures/PYu-R_INT-thick_7.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.de/product-detail/en/yageo/RC0805FR-0747RL/311-47.0CRCT-ND/730919" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="311-47.0CRCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER-PN" value="RC0805FR-0747RL" constant="no"/>
<attribute name="VALUE" value="47R" constant="no"/>
</technology>
<technology name="-DNP">
<attribute name="DATASHEET" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="DNP" constant="no"/>
<attribute name="MANUFACTURER" value="DNP" constant="no"/>
<attribute name="MANUFACTURER-PN" value="DNP" constant="no"/>
<attribute name="VALUE" value="DNP" constant="no"/>
</technology>
</technologies>
</device>
<device name="TH-VERTICAL" package="0204V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DATASHEET" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="DNP" constant="no"/>
<attribute name="MANUFACTURER" value="DNP" constant="no"/>
<attribute name="MANUFACTURER-PN" value="DNP" constant="no"/>
<attribute name="VALUE" value="DNP" constant="no"/>
</technology>
</technologies>
</device>
<device name="PWR" package="PWR">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="-.03MR">
<attribute name="DATASHEET" value="http://www.vishay.com/docs/30176/wslp3921.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/products/en?keywords=%20WSLPD-.0003CT-ND%20" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="WSLPD-.0003CT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MANUFCATURER-PN" value="WSLP5931L3000FEB" constant="no"/>
<attribute name="VALUE" value="0.3mR" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0201" package="C0201">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="-0R">
<attribute name="DATASHEET" value="http://www.yageo.com.tw/exep/pages/download/literatures/PYu-R_INT-thick_7.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/yageo/RC0201JR-070RL/311-0.0NCT-ND/1949004" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="311-0.0NCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER-PN" value="RC0201JR-070RL" constant="no"/>
<attribute name="VALUE" value="0R" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0603" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="-0R">
<attribute name="CODE-BCE" value="SP0233" constant="no"/>
<attribute name="CODE-BCMI" value="0233" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="http://www.yageo.com/exep/pages/download/literatures/PYu-R_Marking_2.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/yageo/RC0603JR-070RL/311-0.0GRCT-ND/729622" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="311-0.0GRCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER-PN" value="RC0603JR-070RL" constant="no"/>
<attribute name="VALUE" value="0R" constant="no"/>
</technology>
<technology name="-100K">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="http://www.yageo.com/documents/recent/PYu-RC_Group_51_RoHS_L_8.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/yageo/RC0603FR-07100KL/311-100KHRCT-ND/729836" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="311-100KHRCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER-PN" value="RC0603FR-07100KL" constant="no"/>
<attribute name="VALUE" value="100k" constant="no"/>
</technology>
<technology name="-100R">
<attribute name="CODE-BCE" value="SP0131" constant="no"/>
<attribute name="CODE-BCMI" value="0131" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="-" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="-" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="-" constant="no"/>
<attribute name="MANUFACTURER" value="-" constant="no"/>
<attribute name="MANUFACTURER-PN" value="-" constant="no"/>
<attribute name="VALUE" value="100R" constant="no"/>
</technology>
<technology name="-10K">
<attribute name="CODE-BCE" value="SP0899" constant="no"/>
<attribute name="CODE-BCMI" value="0899" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="http://www.yageo.com/documents/recent/PYu-RC_Group_51_RoHS_L_8.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/yageo/RC0603JR-0710KL/311-10KGRCT-ND/729647" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="311-10KGRCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER-PN" value="RC0603JR-0710KL" constant="no"/>
<attribute name="VALUE" value="10k" constant="no"/>
</technology>
<technology name="-12K4">
<attribute name="CODE-BCE" value="SP0984" constant="no"/>
<attribute name="CODE-BCMI" value="0984" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="-" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="-" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="-" constant="no"/>
<attribute name="MANUFACTURER" value="-" constant="no"/>
<attribute name="MANUFACTURER-PN" value="-" constant="no"/>
<attribute name="VALUE" value="12k4" constant="no"/>
</technology>
<technology name="-1M">
<attribute name="CODE-BCE" value="SP0004" constant="no"/>
<attribute name="CODE-BCMI" value="0004" constant="no"/>
<attribute name="CODE-TK2" value="CR0603JW105ELF" constant="no"/>
<attribute name="DATASHEET" value="industrial.panasonic.com/www-cgi/jvcr13pz.cgi?E+PZ+3+AOA0001+ERJ3GEYJ105V+7+WW" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/panasonic-electronic-components/ERJ-3GEYJ105V/P1.0MGCT-ND/134885" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="P1.0MGCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Panasonic" constant="no"/>
<attribute name="MANUFACTURER-PN" value="ERJ-3GEYJ105V" constant="no"/>
<attribute name="VALUE" value="1M" constant="no"/>
</technology>
<technology name="-22R">
<attribute name="CODE-BCE" value="SP0247" constant="no"/>
<attribute name="CODE-BCMI" value="0247" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="-" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="-" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="-" constant="no"/>
<attribute name="MANUFACTURER" value="-" constant="no"/>
<attribute name="MANUFACTURER-PN" value="-" constant="no"/>
<attribute name="VALUE" value="22R" constant="no"/>
</technology>
<technology name="DNP">
<attribute name="CODE-BCE" value="DNP" constant="no"/>
<attribute name="CODE-BCMI" value="DNP" constant="no"/>
<attribute name="CODE-TK2" value="DNP" constant="no"/>
<attribute name="DATASHEET" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="DNP" constant="no"/>
<attribute name="MANUFACTURER" value="DNP" constant="no"/>
<attribute name="MANUFACTURER-PN" value="DNP" constant="no"/>
<attribute name="VALUE" value="DNP" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1206" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="-0.01R">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="http://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&amp;DocNm=1773269&amp;DocType=DS&amp;DocLang=English" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/te-connectivity-passive-product/RLP73V2BR010FTDF/A109621CT-ND/4032387" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="A109621CT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="TE" constant="no"/>
<attribute name="MANUFACTURER-PN" value="RLP73V2BR010FTDF" constant="no"/>
<attribute name="VALUE" value="10m" constant="no"/>
</technology>
<technology name="-0.15R">
<attribute name="CODE-BCE" value="SP0361" constant="no"/>
<attribute name="CODE-BCMI" value="0361" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="http://www.susumu.co.jp/common/pdf/n_catalog_partition08_en.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/it/susumu/RL1632R-R150-F/RL16R.15FCT-ND/714518" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="RL1632R-R150-F" constant="no"/>
<attribute name="MANUFACTURER" value="Susumu" constant="no"/>
<attribute name="MANUFACTURER-PN" value="RL16R.15FCT-ND" constant="no"/>
<attribute name="VALUE" value="150m" constant="no"/>
</technology>
<technology name="-20R">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="http://www.susumu.co.jp/common/pdf/n_catalog_partition24_en.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/susumu/HRG3216Q-20R0-D-T1/408-1893-1-ND/5762673" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="408-1893-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Susumu" constant="no"/>
<attribute name="MANUFACTURER-PN" value="HRG3216Q-20R0-D-T1" constant="no"/>
<attribute name="VALUE" value="20R" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="L" prefix="L" uservalue="yes">
<description>Inductors</description>
<gates>
<gate name="&gt;NAME" symbol="L" x="0" y="0"/>
</gates>
<devices>
<device name="-0805" package="C0805">
<connects>
<connect gate="&gt;NAME" pin="1" pad="1"/>
<connect gate="&gt;NAME" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="-12NH">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="http://www.abracon.com/Magnetics/new/AIMC-0805.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/it/bourns-inc/CV201210-100K/CV201210-100KCT-ND/3438035" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="535-11555-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Abracon" constant="no"/>
<attribute name="MANUFACTURER-PN" value="AIMC-0805-12NJ-T" constant="no"/>
<attribute name="VALUE" value="12n" constant="no"/>
</technology>
<technology name="-15NH">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="http://www.abracon.com/Magnetics/new/AIMC-0805.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/abracon-llc/AIMC-0805-15NJ-T/535-11556-1-ND/2782801" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="535-11556-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Abracon" constant="no"/>
<attribute name="MANUFACTURER-PN" value="AIMC-0805-15NJ-T" constant="no"/>
<attribute name="VALUE" value="18nH" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1206" package="C1206">
<connects>
<connect gate="&gt;NAME" pin="1" pad="1"/>
<connect gate="&gt;NAME" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="-1UH">
<attribute name="DATASHEET" value="http://search.murata.co.jp/Ceramy/image/img/P02/JELF243A-0032.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/murata-electronics-north-america/LQH31MN1R0K03L/490-6604-1-ND/3845801" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="490-6604-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Murata" constant="no"/>
<attribute name="MANUFACTURER-PN" value="LQH31MN1R0K03L" constant="no"/>
<attribute name="VALUE" value="1uH" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1210" package="C1210">
<connects>
<connect gate="&gt;NAME" pin="1" pad="1"/>
<connect gate="&gt;NAME" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0402" package="C0402">
<connects>
<connect gate="&gt;NAME" pin="1" pad="1"/>
<connect gate="&gt;NAME" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="-39NH">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="http://search.murata.co.jp/Ceramy/image/img/P02/JELF243B-0009.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/murata-electronics-north-america/LQG15HN39NJ02D/490-1092-1-ND/584538" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="490-1092-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Murata" constant="no"/>
<attribute name="MANUFACTURER-PN" value="LQG15HN39NJ02D" constant="no"/>
<attribute name="VALUE" value="39nH" constant="no"/>
</technology>
<technology name="-68NH">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="http://search.murata.co.jp/Ceramy/image/img/P02/JELF243B-0009.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/murata-electronics-north-america/LQG15HS68NJ02D/490-2633-1-ND/662913" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="490-2633-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Murata" constant="no"/>
<attribute name="MANUFACTURER-PN" value="LQG15HS68NJ02D" constant="no"/>
<attribute name="VALUE" value="68nH" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0603" package="0603-1608X90N">
<connects>
<connect gate="&gt;NAME" pin="1" pad="1"/>
<connect gate="&gt;NAME" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="-10NH">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="http://www.abracon.com/Magnetics/new/AIMC-0603.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/abracon-llc/AIMC-0603-10NJ-T/535-11525-1-ND/2782771" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="535-11525-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Abracon" constant="no"/>
<attribute name="MANUFACTURER-PN" value="AIMC-0603-10NJ-T" constant="no"/>
<attribute name="VALUE" value="10nH" constant="no"/>
</technology>
<technology name="-22UH-230MA">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="http://www.yuden.co.jp/productdata/catalog/en/wound02_e.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="http://www.digikey.com/product-search/en?keywords=587-1721-1-ND" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="587-1721-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Tayo Yuden" constant="no"/>
<attribute name="MANUFACTURER-PN" value="CBMF1608T220K" constant="no"/>
<attribute name="VALUE" value="22uH" constant="no"/>
</technology>
</technologies>
</device>
<device name="SRP" package="SRP4020">
<connects>
<connect gate="&gt;NAME" pin="1" pad="1"/>
<connect gate="&gt;NAME" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DATASHEET" value="http://www.bourns.com/docs/Product-Datasheets/SRP4020TA.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/bourns-inc/SRP4020TA-2R2M/SRP4020TA-2R2MCT-ND/4901068" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="SRP4020TA-2R2MCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Bourns" constant="no"/>
<attribute name="MANUFACTURER-PN" value="SRP4020TA-2R2M" constant="no"/>
<attribute name="VALUE" value="2u2" constant="no"/>
</technology>
</technologies>
</device>
<device name="SRN" package="SRN6045">
<connects>
<connect gate="&gt;NAME" pin="1" pad="1"/>
<connect gate="&gt;NAME" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DATASHEET" value="http://pdf1.alldatasheet.com/datasheet-pdf/view/442254/BOURNS/SRN6045-150M.html" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/bourns-inc/SRN6045-150M/SRN6045-150MCT-ND/2756165" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="SRN6045-150M" constant="no"/>
<attribute name="MANUFACTURER" value="Bourns" constant="no"/>
<attribute name="MANUFACTURER-PN" value="SRN6045-150M" constant="no"/>
<attribute name="VALUE" value="15uH" constant="no"/>
</technology>
</technologies>
</device>
<device name="74404054047" package="5X5_NON-STANDARD">
<connects>
<connect gate="&gt;NAME" pin="1" pad="P$1"/>
<connect gate="&gt;NAME" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DATASHEET" value="http://katalog.we-online.de/pbs/datasheet/74404054047.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.se/products/en?keywords=732-5517-1-ND" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="732-5517-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Wurth Electronics Inc." constant="no"/>
<attribute name="MANUFACTURER-PN" value="74404054047" constant="no"/>
<attribute name="VALUE" value="4.7UH/3A/30mO" constant="no"/>
</technology>
</technologies>
</device>
<device name="VLS6045EX-4R7M" package="6X6_NON-STANDARD">
<connects>
<connect gate="&gt;NAME" pin="1" pad="P$1"/>
<connect gate="&gt;NAME" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DATASHEET" value="https://product.tdk.com/info/en/catalog/datasheets/inductor_commercial_power_vls6045ex_en.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.se/products/en?keywords=445-173055-1-ND" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="445-173055-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="TDK Corporation" constant="no"/>
<attribute name="MANUFACTURER-PN" value="VLS6045EX-4R7M" constant="no"/>
<attribute name="VALUE" value="4.7uH/4A/35mO" constant="no"/>
</technology>
</technologies>
</device>
<device name="10X10.2-COILCRAFT" package="COILCRAFT-10MM">
<connects>
<connect gate="&gt;NAME" pin="1" pad="1"/>
<connect gate="&gt;NAME" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="-5U2">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="https://www.mouser.com/ds/2/597/mss1038t-270707.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.mouser.it/ProductDetail/Coilcraft/MSS1038-522NLC?qs=sGAEpiMZZMsg%252by3WlYCkU2kWFds1hA9DjFggbcSzVu8%3d" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="994-MSS1038-522NLC" constant="no"/>
<attribute name="MANUFACTURER" value="Coilcraft" constant="no"/>
<attribute name="MANUFACTURER-PN" value="MSS1038-522NLC" constant="no"/>
<attribute name="VALUE" value="5u2" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="4R-N" prefix="RN" uservalue="yes">
<description>&lt;b&gt;Array Chip Resistor&lt;/b&gt;&lt;p&gt;
Source: RS Component / Phycomp</description>
<gates>
<gate name="A" symbol="R1NV" x="0" y="7.62" addlevel="always" swaplevel="1"/>
<gate name="B" symbol="R1NV" x="0" y="2.54" addlevel="always" swaplevel="1"/>
<gate name="C" symbol="R1NV" x="0" y="-2.54" addlevel="always" swaplevel="1"/>
<gate name="D" symbol="R1NV" x="0" y="-7.62" addlevel="always" swaplevel="1"/>
</gates>
<devices>
<device name="CAY16" package="CAY16">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="8"/>
<connect gate="B" pin="1" pad="2"/>
<connect gate="B" pin="2" pad="7"/>
<connect gate="C" pin="1" pad="3"/>
<connect gate="C" pin="2" pad="6"/>
<connect gate="D" pin="1" pad="4"/>
<connect gate="D" pin="2" pad="5"/>
</connects>
<technologies>
<technology name="-10K">
<attribute name="CODE-BCE" value="SP0016" constant="no"/>
<attribute name="CODE-BCMI" value="0016" constant="no"/>
<attribute name="CODE-TK2" value="CAY16-103J4GLF" constant="no"/>
<attribute name="DATASHEET" value="http://www.bourns.com/docs/Product-Datasheets/CATCAY.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/products/en?keywords=CAY16-103J4LFTR-ND" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="CAY16-103J4LFTR-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Bourns Inc." constant="no"/>
<attribute name="MANUFACTURER-PN" value="CAY16-103J4LF" constant="no"/>
<attribute name="VALUE" value="10k" constant="no"/>
</technology>
<technology name="-1K">
<attribute name="CODE-BCE" value="SP0005" constant="no"/>
<attribute name="CODE-BCMI" value="0005" constant="no"/>
<attribute name="CODE-TK2" value="CAY16-102J4GLF" constant="no"/>
<attribute name="DATASHEET" value="http://www.bourns.com/docs/Product-Datasheets/CATCAY.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/bourns-inc/CAY16-102J4LF/CAY16-102J4LFCT-ND/3437828" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="CAY16-102J4LFCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Bourns Inc." constant="no"/>
<attribute name="MANUFACTURER-PN" value="CAY16-102J4LF" constant="no"/>
<attribute name="VALUE" value="1k" constant="no"/>
</technology>
<technology name="-22R">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="0003" constant="no"/>
<attribute name="CODE-TK2" value="CAY16-220J4GLF" constant="no"/>
<attribute name="DATASHEET" value="http://www.bourns.com/docs/Product-Datasheets/CATCAY.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/bourns-inc/CAY16-220J4LF/CAY16-220J4LFCT-ND/3437988" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="CAY16-220J4LFCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Bourns Inc." constant="no"/>
<attribute name="MANUFACTURER-PN" value="CAY16-220J4LF" constant="no"/>
<attribute name="VALUE" value="22R" constant="no"/>
</technology>
<technology name="-470R">
<attribute name="CODE-BCE" value="SP0540" constant="no"/>
<attribute name="CODE-BCMI" value="0540" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="http://www.bourns.com/docs/Product-Datasheets/CATCAY.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/bourns-inc/CAY16-471J4LF/CAY16-471J4LFCT-ND/3437962" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="CAY16-471J4LFCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Bourns Inc." constant="no"/>
<attribute name="MANUFACTURER-PN" value="CAY16-471J4LF" constant="no"/>
<attribute name="VALUE" value="470R" constant="no"/>
</technology>
<technology name="-49.9R">
<attribute name="CODE-BCE" value="SP0036" constant="no"/>
<attribute name="CODE-BCMI" value="0036" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="http://www.bourns.com/docs/Product-Datasheets/CATCAY.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/bourns-inc/CAY16-49R9F4LF/CAY16-49R9F4LFCT-ND/3437972" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="CAY16-49R9F4LFCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Bourns Inc." constant="no"/>
<attribute name="MANUFACTURER-PN" value="CAY16-49R9F4LF" constant="no"/>
<attribute name="VALUE" value="49.9R" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="C+" prefix="C">
<description>Polarized Capacitor</description>
<gates>
<gate name="G$1" symbol="C+" x="0" y="0"/>
</gates>
<devices>
<device name="KEMET-D" package="TANT">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="DATASHEET" value="http://www.kemet.com/Lists/ProductCatalog/Attachments/684/KEM_T2076_T52X-530.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/kemet/T520D337M006ATE045/399-4055-2-ND/818916" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="399-4055-2-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Kemet" constant="no"/>
<attribute name="MANUFACTURER-PN" value="T520D337M006ATE045" constant="no"/>
<attribute name="VALUE" value="330u" constant="no"/>
</technology>
</technologies>
</device>
<device name="PAN-D" package="PANASONIC_D">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="-47UF">
<attribute name="CODE-BCE" value="SP0113" constant="no"/>
<attribute name="CODE-BCMI" value="0113" constant="no"/>
<attribute name="CODE-TK2" value="CES 47UF 25V" constant="no"/>
<attribute name="DATASHEET" value="https://industrial.panasonic.com/cdbs/www-data/pdf/RDE0000/DME0000COL90.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/panasonic-electronic-components/EEE-1EA470WP/PCE3908CT-ND/766284" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="PCE3908CT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Panasonic" constant="no"/>
<attribute name="MANUFACTURER-PN" value="EEE-1EA470WP" constant="no"/>
<attribute name="VALUE" value="47u" constant="no"/>
</technology>
</technologies>
</device>
<device name="PAN-C" package="PANASONIC_C">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="DATASHEET" value="https://industrial.panasonic.com/cdbs/www-data/pdf/RDE0000/DME0000COL92.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.se/products/en?keywords=P15082CT-ND" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="P15082CT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Panasonic" constant="no"/>
<attribute name="MANUFACTURER-PN" value="EEE-FT1A151AR" constant="no"/>
<attribute name="VALUE" value="150uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="TANT" package="C1206+">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="CODE-BCE" value="SP0474" constant="no"/>
<attribute name="CODE-BCMI" value="0474" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="-" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="-" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="-" constant="no"/>
<attribute name="MANUFACTURER" value="-" constant="no"/>
<attribute name="MANUFACTURER-PN" value="-" constant="no"/>
<attribute name="VALUE" value="4u7" constant="no"/>
</technology>
</technologies>
</device>
<device name="SMC_B" package="SMC_B">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="CODE-BCE" value="SP0167" constant="no"/>
<attribute name="CODE-BCMI" value="0167" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="-" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="-" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="-" constant="no"/>
<attribute name="MANUFACTURER" value="-" constant="no"/>
<attribute name="MANUFACTURER-PN" value="-" constant="no"/>
<attribute name="VALUE" value="22u" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Arduino-connectors">
<description>Copyright (c) 2016-2017 Arduino LLC.  All right reserved.</description>
<packages>
<package name="MODULINO-3">
<description>&lt;b&gt;AMP MTA connector&lt;/b&gt;&lt;p&gt;
Source: http://ecommas.tycoelectronics.com .. ENG_CD_640456_W.pdf</description>
<wire x1="-3.81" y1="-3.2" x2="-3.81" y2="-2.04" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-2.04" x2="-3.81" y2="-2.04" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-3.2" x2="3.81" y2="-3.2" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-2.04" x2="-3.81" y2="2.6" width="0.1524" layer="21"/>
<wire x1="3.81" y1="2.6" x2="-3.81" y2="2.6" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-2.04" x2="3.81" y2="-3.2" width="0.1524" layer="21"/>
<wire x1="3.81" y1="2.6" x2="3.81" y2="-2.04" width="0.1524" layer="21"/>
<rectangle x1="-2.794" y1="-0.254" x2="-2.286" y2="0.254" layer="21" rot="R180"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="21" rot="R180"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="21" rot="R180"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="long" rot="R270"/>
<pad name="2" x="0" y="0" drill="1.016" shape="long" rot="R270"/>
<pad name="3" x="2.54" y="0" drill="1.016" shape="long" rot="R270"/>
<text x="0" y="-2.3641" size="1.27" layer="27" align="center">&gt;NAME</text>
</package>
<package name="MODULINO-4">
<description>&lt;b&gt;AMP MTA connector&lt;/b&gt;&lt;p&gt;
Source: http://ecommas.tycoelectronics.com .. ENG_CD_640456_W.pdf</description>
<wire x1="6.35" y1="3.2" x2="6.35" y2="-1.77" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-1.77" x2="6.35" y2="-1.77" width="0.1524" layer="21"/>
<wire x1="6.35" y1="3.2" x2="-3.81" y2="3.2" width="0.1524" layer="21"/>
<wire x1="6.35" y1="-1.77" x2="6.35" y2="-2.6" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-2.6" x2="6.35" y2="-2.6" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-1.77" x2="-3.81" y2="3.2" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-2.6" x2="-3.81" y2="-1.77" width="0.1524" layer="21"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="21"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="21"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="21"/>
<pad name="1" x="5.08" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="0" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="1.27" y="2.3641" size="1.27" layer="27" rot="R180" align="center">&gt;NAME</text>
<rectangle x1="-2.794" y1="-0.254" x2="-2.286" y2="0.254" layer="21"/>
<pad name="4" x="-2.54" y="0" drill="1.016" shape="long" rot="R90"/>
</package>
<package name="MODULINO-3SM">
<description>&lt;b&gt;AMP MTA connector&lt;/b&gt;&lt;p&gt;
Source: http://ecommas.tycoelectronics.com .. ENG_CD_640456_W.pdf</description>
<wire x1="3.81" y1="3.2" x2="3.81" y2="-1.77" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-1.77" x2="3.81" y2="-1.77" width="0.1524" layer="21"/>
<wire x1="3.81" y1="3.2" x2="-3.81" y2="3.2" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-1.77" x2="3.81" y2="-2.6" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-2.6" x2="3.81" y2="-2.6" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-1.77" x2="-3.81" y2="3.2" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-2.6" x2="-3.81" y2="-1.77" width="0.1524" layer="21"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="21"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="21"/>
<rectangle x1="-2.794" y1="-0.254" x2="-2.286" y2="0.254" layer="21"/>
<pad name="1" x="2.54" y="0" drill="1.016" rot="R90"/>
<pad name="2" x="0" y="0" drill="1.016" rot="R90"/>
<pad name="3" x="-2.54" y="0" drill="1.016" rot="R90"/>
<text x="0" y="2.3641" size="1.27" layer="27" rot="R180" align="center">&gt;NAME</text>
</package>
<package name="MODULINO-4SM">
<description>&lt;b&gt;AMP MTA connector&lt;/b&gt;&lt;p&gt;
Source: http://ecommas.tycoelectronics.com .. ENG_CD_640456_W.pdf</description>
<wire x1="6.35" y1="3.2" x2="6.35" y2="-1.77" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-1.77" x2="6.35" y2="-1.77" width="0.1524" layer="21"/>
<wire x1="6.35" y1="3.2" x2="-3.81" y2="3.2" width="0.1524" layer="21"/>
<wire x1="6.35" y1="-1.77" x2="6.35" y2="-2.6" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-2.6" x2="6.35" y2="-2.6" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-1.77" x2="-3.81" y2="3.2" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-2.6" x2="-3.81" y2="-1.77" width="0.1524" layer="21"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="21"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="21"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="21"/>
<pad name="1" x="5.08" y="0" drill="1.016" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" rot="R90"/>
<pad name="3" x="0" y="0" drill="1.016" rot="R90"/>
<text x="1.27" y="2.3641" size="1.27" layer="27" rot="R180" align="center">&gt;NAME</text>
<rectangle x1="-2.794" y1="-0.254" x2="-2.286" y2="0.254" layer="21"/>
<pad name="4" x="-2.54" y="0" drill="1.016" rot="R90"/>
</package>
<package name="PINHEAD-2X5-P127">
<wire x1="-1.27" y1="3.175" x2="-0.635" y2="3.175" width="0.127" layer="21"/>
<wire x1="-0.635" y1="3.175" x2="1.27" y2="3.175" width="0.127" layer="21"/>
<wire x1="1.27" y1="3.175" x2="1.27" y2="-3.175" width="0.127" layer="21"/>
<wire x1="1.27" y1="-3.175" x2="-1.27" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-3.175" x2="-1.27" y2="2.54" width="0.127" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="-1.27" y2="3.175" width="0.127" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="-1.397" y2="2.54" width="0.127" layer="21"/>
<wire x1="-1.397" y1="2.54" x2="-1.397" y2="3.302" width="0.127" layer="21"/>
<wire x1="-1.397" y1="3.302" x2="-0.635" y2="3.302" width="0.127" layer="21"/>
<wire x1="-0.635" y1="3.302" x2="-0.635" y2="3.175" width="0.127" layer="21"/>
<wire x1="-0.635" y1="3.302" x2="-0.635" y2="3.429" width="0.127" layer="21"/>
<wire x1="-0.635" y1="3.429" x2="-1.524" y2="3.429" width="0.127" layer="21"/>
<wire x1="-1.524" y1="3.429" x2="-1.524" y2="2.54" width="0.127" layer="21"/>
<wire x1="-1.524" y1="2.54" x2="-1.397" y2="2.54" width="0.127" layer="21"/>
<wire x1="-1.27" y1="3.175" x2="1.27" y2="3.175" width="0.127" layer="51"/>
<wire x1="1.27" y1="3.175" x2="1.27" y2="-3.175" width="0.127" layer="51"/>
<wire x1="1.27" y1="-3.175" x2="-1.27" y2="-3.175" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-3.175" x2="-1.27" y2="3.175" width="0.127" layer="51"/>
<wire x1="-1.016" y1="2.921" x2="-0.254" y2="2.921" width="0.127" layer="51"/>
<wire x1="-0.254" y1="2.921" x2="-0.254" y2="2.159" width="0.127" layer="51"/>
<wire x1="-0.254" y1="2.159" x2="-1.016" y2="2.159" width="0.127" layer="51"/>
<wire x1="-1.016" y1="2.159" x2="-1.016" y2="2.921" width="0.127" layer="51"/>
<wire x1="-0.889" y1="2.794" x2="-0.889" y2="2.286" width="0.127" layer="51"/>
<wire x1="-0.889" y1="2.286" x2="-0.381" y2="2.286" width="0.127" layer="51"/>
<wire x1="-0.381" y1="2.286" x2="-0.381" y2="2.794" width="0.127" layer="51"/>
<wire x1="-0.381" y1="2.794" x2="-0.889" y2="2.794" width="0.127" layer="51"/>
<wire x1="-0.762" y1="2.667" x2="-0.762" y2="2.413" width="0.127" layer="51"/>
<wire x1="-0.762" y1="2.413" x2="-0.635" y2="2.413" width="0.127" layer="51"/>
<wire x1="-0.635" y1="2.413" x2="-0.508" y2="2.413" width="0.127" layer="51"/>
<wire x1="-0.508" y1="2.413" x2="-0.508" y2="2.667" width="0.127" layer="51"/>
<wire x1="-0.508" y1="2.667" x2="-0.635" y2="2.667" width="0.127" layer="51"/>
<wire x1="-0.635" y1="2.667" x2="-0.635" y2="2.413" width="0.127" layer="51"/>
<wire x1="-2.3" y1="3.6" x2="2.3" y2="3.6" width="0.05" layer="39"/>
<wire x1="2.3" y1="3.6" x2="2.3" y2="-3.3" width="0.05" layer="39"/>
<wire x1="2.3" y1="-3.3" x2="-2.3" y2="-3.3" width="0.05" layer="39"/>
<wire x1="-2.3" y1="-3.3" x2="-2.3" y2="3.6" width="0.05" layer="39"/>
<pad name="1" x="-0.635" y="2.54" drill="0.6096" diameter="1.016" shape="offset" rot="R180"/>
<pad name="2" x="0.635" y="2.54" drill="0.6096" diameter="1.016" shape="offset"/>
<pad name="3" x="-0.635" y="1.27" drill="0.6096" diameter="1.016" shape="offset" rot="R180"/>
<pad name="4" x="0.635" y="1.27" drill="0.6096" diameter="1.016" shape="offset"/>
<pad name="5" x="-0.635" y="0" drill="0.6096" diameter="1.016" shape="offset" rot="R180"/>
<pad name="6" x="0.635" y="0" drill="0.6096" diameter="1.016" shape="offset"/>
<pad name="7" x="-0.635" y="-1.27" drill="0.6096" diameter="1.016" shape="offset" rot="R180"/>
<pad name="8" x="0.635" y="-1.27" drill="0.6096" diameter="1.016" shape="offset"/>
<pad name="9" x="-0.635" y="-2.54" drill="0.6096" diameter="1.016" shape="offset" rot="R180"/>
<pad name="10" x="0.635" y="-2.54" drill="0.6096" diameter="1.016" shape="offset"/>
<text x="-1.27" y="3.81" size="0.6096" layer="27">&gt;VALUE</text>
<text x="-1.27" y="-4.445" size="0.6096" layer="25">&gt;NAME</text>
</package>
<package name="PINHEAD_2X05_127">
<wire x1="-1.27" y1="3.175" x2="-0.635" y2="3.175" width="0.127" layer="21"/>
<wire x1="-0.635" y1="3.175" x2="1.27" y2="3.175" width="0.127" layer="21"/>
<wire x1="1.27" y1="3.175" x2="1.27" y2="-3.175" width="0.127" layer="21"/>
<wire x1="1.27" y1="-3.175" x2="-1.27" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-3.175" x2="-1.27" y2="2.54" width="0.127" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="-1.27" y2="3.175" width="0.127" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="-1.397" y2="2.54" width="0.127" layer="21"/>
<wire x1="-1.397" y1="2.54" x2="-1.397" y2="3.302" width="0.127" layer="21"/>
<wire x1="-1.397" y1="3.302" x2="-0.635" y2="3.302" width="0.127" layer="21"/>
<wire x1="-0.635" y1="3.302" x2="-0.635" y2="3.175" width="0.127" layer="21"/>
<wire x1="-0.635" y1="3.302" x2="-0.635" y2="3.429" width="0.127" layer="21"/>
<wire x1="-0.635" y1="3.429" x2="-1.524" y2="3.429" width="0.127" layer="21"/>
<wire x1="-1.524" y1="3.429" x2="-1.524" y2="2.54" width="0.127" layer="21"/>
<wire x1="-1.524" y1="2.54" x2="-1.397" y2="2.54" width="0.127" layer="21"/>
<wire x1="-1.27" y1="3.175" x2="1.27" y2="3.175" width="0.127" layer="51"/>
<wire x1="1.27" y1="3.175" x2="1.27" y2="-3.175" width="0.127" layer="51"/>
<wire x1="1.27" y1="-3.175" x2="-1.27" y2="-3.175" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-3.175" x2="-1.27" y2="3.175" width="0.127" layer="51"/>
<wire x1="-1.016" y1="2.921" x2="-0.254" y2="2.921" width="0.127" layer="51"/>
<wire x1="-0.254" y1="2.921" x2="-0.254" y2="2.159" width="0.127" layer="51"/>
<wire x1="-0.254" y1="2.159" x2="-1.016" y2="2.159" width="0.127" layer="51"/>
<wire x1="-1.016" y1="2.159" x2="-1.016" y2="2.921" width="0.127" layer="51"/>
<wire x1="-0.889" y1="2.794" x2="-0.889" y2="2.286" width="0.127" layer="51"/>
<wire x1="-0.889" y1="2.286" x2="-0.381" y2="2.286" width="0.127" layer="51"/>
<wire x1="-0.381" y1="2.286" x2="-0.381" y2="2.794" width="0.127" layer="51"/>
<wire x1="-0.381" y1="2.794" x2="-0.889" y2="2.794" width="0.127" layer="51"/>
<wire x1="-0.762" y1="2.667" x2="-0.762" y2="2.413" width="0.127" layer="51"/>
<wire x1="-0.762" y1="2.413" x2="-0.635" y2="2.413" width="0.127" layer="51"/>
<wire x1="-0.635" y1="2.413" x2="-0.508" y2="2.413" width="0.127" layer="51"/>
<wire x1="-0.508" y1="2.413" x2="-0.508" y2="2.667" width="0.127" layer="51"/>
<wire x1="-0.508" y1="2.667" x2="-0.635" y2="2.667" width="0.127" layer="51"/>
<wire x1="-0.635" y1="2.667" x2="-0.635" y2="2.413" width="0.127" layer="51"/>
<wire x1="-1.6" y1="2.46" x2="-1.6" y2="3.5" width="0.05" layer="39"/>
<wire x1="-1.6" y1="3.5" x2="-0.55" y2="3.5" width="0.05" layer="39"/>
<wire x1="-0.55" y1="3.5" x2="-0.55" y2="3.26" width="0.05" layer="39"/>
<wire x1="-0.55" y1="3.26" x2="1.36" y2="3.26" width="0.05" layer="39"/>
<wire x1="1.36" y1="3.26" x2="1.35" y2="-3.26" width="0.05" layer="39"/>
<wire x1="1.35" y1="-3.26" x2="-1.36" y2="-3.26" width="0.05" layer="39"/>
<wire x1="-1.36" y1="-3.26" x2="-1.35" y2="2.46" width="0.05" layer="39"/>
<wire x1="-1.35" y1="2.46" x2="-1.6" y2="2.46" width="0.05" layer="39"/>
<pad name="1" x="-0.635" y="2.54" drill="0.6" diameter="1.016" shape="square"/>
<pad name="2" x="0.635" y="2.54" drill="0.6" diameter="1.016"/>
<pad name="3" x="-0.635" y="1.27" drill="0.6" diameter="1.016"/>
<pad name="4" x="0.635" y="1.27" drill="0.6" diameter="1.016"/>
<pad name="5" x="-0.635" y="0" drill="0.6" diameter="1.016"/>
<pad name="6" x="0.635" y="0" drill="0.6" diameter="1.016"/>
<pad name="7" x="-0.635" y="-1.27" drill="0.6" diameter="1.016"/>
<pad name="8" x="0.635" y="-1.27" drill="0.6" diameter="1.016"/>
<pad name="9" x="-0.635" y="-2.54" drill="0.6" diameter="1.016"/>
<pad name="10" x="0.635" y="-2.54" drill="0.6" diameter="1.016"/>
</package>
<package name="HW4-2.0">
<wire x1="-5" y1="1" x2="-5" y2="-1" width="0.127" layer="21"/>
<wire x1="5" y1="1.5" x2="5" y2="-1.5" width="0.254" layer="21"/>
<wire x1="-5" y1="2.2" x2="-5" y2="-2.2" width="0.254" layer="21"/>
<wire x1="-5" y1="-2.2" x2="-3.2" y2="-2.2" width="0.254" layer="21"/>
<wire x1="3.2" y1="-2.2" x2="5" y2="-2.2" width="0.254" layer="21"/>
<wire x1="5" y1="-2.2" x2="5" y2="2.2" width="0.254" layer="21"/>
<wire x1="-5" y1="2.2" x2="5" y2="2.2" width="0.254" layer="21"/>
<wire x1="-5" y1="-2.2" x2="-5" y2="-2.8" width="0.254" layer="21"/>
<wire x1="-5" y1="-2.8" x2="-3.2" y2="-2.8" width="0.254" layer="21"/>
<wire x1="-3.2" y1="-2.8" x2="-3.2" y2="-2.2" width="0.254" layer="21"/>
<wire x1="-3.2" y1="-2.2" x2="3.2" y2="-2.2" width="0.254" layer="21"/>
<wire x1="3.2" y1="-2.2" x2="3.2" y2="-2.8" width="0.254" layer="21"/>
<wire x1="3.2" y1="-2.8" x2="5" y2="-2.8" width="0.254" layer="21"/>
<wire x1="5" y1="-2.8" x2="5" y2="-2.2" width="0.254" layer="21"/>
<wire x1="-5" y1="2.2" x2="5" y2="2.2" width="0.254" layer="39"/>
<wire x1="5" y1="2.2" x2="5" y2="-2.8" width="0.254" layer="39"/>
<wire x1="-5" y1="-2.8" x2="-5" y2="2.2" width="0.254" layer="39"/>
<wire x1="-4.953" y1="-2.794" x2="-3.175" y2="-2.794" width="0.254" layer="39"/>
<wire x1="-3.175" y1="-2.794" x2="-3.175" y2="-2.159" width="0.254" layer="39"/>
<wire x1="-3.175" y1="-2.159" x2="-1.778" y2="-2.159" width="0.254" layer="39"/>
<wire x1="-1.778" y1="-2.159" x2="-1.778" y2="-4.191" width="0.254" layer="39"/>
<wire x1="-1.778" y1="-4.191" x2="1.778" y2="-4.191" width="0.254" layer="39"/>
<wire x1="1.778" y1="-4.191" x2="1.778" y2="-2.159" width="0.254" layer="39"/>
<wire x1="1.778" y1="-2.159" x2="3.175" y2="-2.159" width="0.254" layer="39"/>
<wire x1="3.175" y1="-2.159" x2="3.175" y2="-2.794" width="0.254" layer="39"/>
<wire x1="3.175" y1="-2.794" x2="4.953" y2="-2.794" width="0.254" layer="39"/>
<pad name="1" x="-3" y="0" drill="0.8" diameter="1.27" shape="square"/>
<text x="-2.54" y="2.54" size="0.889" layer="25" font="vector" ratio="11">&gt;NAME</text>
<text x="-2.54" y="-1.905" size="0.889" layer="27" font="vector" ratio="11">&gt;VALUE</text>
<pad name="2" x="-1" y="0" drill="0.8" diameter="1.27"/>
<pad name="3" x="1" y="0" drill="0.8" diameter="1.27"/>
<pad name="4" x="3" y="0" drill="0.8" diameter="1.27"/>
</package>
</packages>
<symbols>
<symbol name="MODULINO-3">
<circle x="-2.54" y="0" radius="0.635" width="0.254" layer="94"/>
<circle x="0" y="0" radius="0.635" width="0.254" layer="94"/>
<circle x="2.54" y="0" radius="0.635" width="0.254" layer="94"/>
<wire x1="-3.81" y1="1.27" x2="-3.81" y2="-1.905" width="0.254" layer="94"/>
<wire x1="3.81" y1="-1.905" x2="-3.81" y2="-1.905" width="0.254" layer="94"/>
<wire x1="3.81" y1="-1.905" x2="3.81" y2="1.27" width="0.254" layer="94"/>
<wire x1="-3.81" y1="1.27" x2="3.81" y2="1.27" width="0.254" layer="94"/>
<pin name="1" x="-2.54" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="3" x="2.54" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<text x="-5.08" y="6.35" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="1.27" size="1.27" layer="95" rot="R90">GND</text>
<text x="0" y="1.27" size="1.27" layer="95" rot="R90">SIG</text>
<text x="-2.54" y="1.27" size="1.27" layer="95" rot="R90">+V</text>
</symbol>
<symbol name="MODULINO-4">
<circle x="-2.54" y="0" radius="0.635" width="0.254" layer="94"/>
<circle x="0" y="0" radius="0.635" width="0.254" layer="94"/>
<circle x="2.54" y="0" radius="0.635" width="0.254" layer="94"/>
<wire x1="-3.81" y1="1.27" x2="-3.81" y2="-1.905" width="0.254" layer="94"/>
<wire x1="6.35" y1="-1.905" x2="-3.81" y2="-1.905" width="0.254" layer="94"/>
<wire x1="6.35" y1="-1.905" x2="6.35" y2="1.27" width="0.254" layer="94"/>
<wire x1="-3.81" y1="1.27" x2="6.35" y2="1.27" width="0.254" layer="94"/>
<pin name="1" x="-2.54" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="3" x="2.54" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<text x="-5.08" y="8.89" size="1.778" layer="95">&gt;NAME</text>
<text x="5.08" y="1.27" size="1.27" layer="95" rot="R90">GND</text>
<circle x="5.08" y="0" radius="0.635" width="0.254" layer="94"/>
<pin name="4" x="5.08" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<text x="2.54" y="1.27" size="1.27" layer="95" rot="R90">+V</text>
<text x="0" y="1.27" size="1.27" layer="95" rot="R90">TX/SDA</text>
<text x="-2.54" y="1.27" size="1.27" layer="95" rot="R90">RX/SCL</text>
</symbol>
<symbol name="PINHEAD_2X05_127">
<rectangle x1="-1.524" y1="4.572" x2="-0.508" y2="5.588" layer="94"/>
<rectangle x1="-1.524" y1="2.032" x2="-0.508" y2="3.048" layer="94"/>
<rectangle x1="-1.524" y1="-0.508" x2="-0.508" y2="0.508" layer="94"/>
<rectangle x1="-1.524" y1="-3.048" x2="-0.508" y2="-2.032" layer="94"/>
<rectangle x1="-1.524" y1="-5.588" x2="-0.508" y2="-4.572" layer="94"/>
<rectangle x1="0.508" y1="4.572" x2="1.524" y2="5.588" layer="94"/>
<rectangle x1="0.508" y1="2.032" x2="1.524" y2="3.048" layer="94"/>
<rectangle x1="0.508" y1="-0.508" x2="1.524" y2="0.508" layer="94"/>
<rectangle x1="0.508" y1="-3.048" x2="1.524" y2="-2.032" layer="94"/>
<rectangle x1="0.508" y1="-5.588" x2="1.524" y2="-4.572" layer="94"/>
<wire x1="-2.54" y1="-6.604" x2="2.54" y2="-6.604" width="0.4064" layer="94"/>
<wire x1="2.54" y1="-6.604" x2="2.54" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="2.54" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="2.54" y2="0" width="0.4064" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="2.54" width="0.4064" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="5.08" width="0.4064" layer="94"/>
<wire x1="2.54" y1="5.08" x2="2.54" y2="6.858" width="0.4064" layer="94"/>
<wire x1="2.54" y1="6.858" x2="-1.524" y2="6.858" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="5.842" x2="-2.54" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="2.54" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="0" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-2.54" y2="-6.604" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="5.842" x2="-1.524" y2="6.858" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-1.016" y2="5.08" width="0.127" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-1.016" y2="2.54" width="0.127" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.016" y2="0" width="0.127" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-1.016" y2="-2.54" width="0.127" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-1.016" y2="-5.08" width="0.127" layer="94"/>
<wire x1="1.016" y1="5.08" x2="2.54" y2="5.08" width="0.127" layer="94"/>
<wire x1="1.016" y1="2.54" x2="2.54" y2="2.54" width="0.127" layer="94"/>
<wire x1="1.016" y1="0" x2="2.54" y2="0" width="0.127" layer="94"/>
<wire x1="1.016" y1="-2.54" x2="2.54" y2="-2.54" width="0.127" layer="94"/>
<wire x1="1.016" y1="-5.08" x2="2.54" y2="-5.08" width="0.127" layer="94"/>
<pin name="1" x="-5.08" y="5.08" visible="pad" length="short" direction="pas"/>
<pin name="2" x="5.08" y="5.08" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="3" x="-5.08" y="2.54" visible="pad" length="short" direction="pas"/>
<pin name="4" x="5.08" y="2.54" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="5" x="-5.08" y="0" visible="pad" length="short" direction="pas"/>
<pin name="6" x="5.08" y="0" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="7" x="-5.08" y="-2.54" visible="pad" length="short" direction="pas"/>
<pin name="8" x="5.08" y="-2.54" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="9" x="-5.08" y="-5.08" visible="pad" length="short" direction="pas"/>
<pin name="10" x="5.08" y="-5.08" visible="pad" length="short" direction="pas" rot="R180"/>
<text x="-2.54" y="7.239" size="1.778" layer="95">&gt;NAME</text>
</symbol>
<symbol name="GROVE-CONNECTOR-DIP">
<description>Grove connector</description>
<wire x1="-1.27" y1="5.08" x2="3.81" y2="5.08" width="0.254" layer="94"/>
<wire x1="3.81" y1="5.08" x2="3.81" y2="-5.08" width="0.254" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="-1.27" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-5.08" x2="-1.27" y2="5.08" width="0.254" layer="94"/>
<text x="-1.27" y="6.35" size="1.27" layer="95" ratio="10">&gt;name</text>
<pin name="1" x="-3.81" y="3.81" visible="pad" length="middle" function="dotclk"/>
<pin name="2" x="-3.81" y="1.27" visible="pad" length="middle" function="dot"/>
<pin name="3" x="-3.81" y="-1.27" visible="pad" length="middle" function="dot"/>
<pin name="4" x="-3.81" y="-3.81" visible="pad" length="middle" function="dot"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MODULINO-3" prefix="J" uservalue="yes">
<description>Modulino 4 connector</description>
<gates>
<gate name="G$1" symbol="MODULINO-3" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MODULINO-3">
<connects>
<connect gate="G$1" pin="1" pad="3"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="1"/>
</connects>
<technologies>
<technology name="">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="https://www.alliedelec.com/m/d/412bc68f123f3befd9509afa982df40c.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.alliedelec.com/adam-tech-lha-03-ts/70949176/" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value=" 70949176 " constant="no"/>
<attribute name="MANUFACTURER" value=" Adam Tech" constant="no"/>
<attribute name="MANUFACTURER-PN" value="LHA-03-TS" constant="no"/>
<attribute name="VALUE" value="-" constant="no"/>
</technology>
<technology name="-IN">
<attribute name="CODE-BCE" value="SP1695" constant="no"/>
<attribute name="CODE-BCMI" value="1695" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="-" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="-" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="-" constant="no"/>
<attribute name="MANUFACTURER" value="-" constant="no"/>
<attribute name="MANUFACTURER-PN" value="-" constant="no"/>
<attribute name="VALUE" value="-" constant="no"/>
</technology>
<technology name="-OUT">
<attribute name="CODE-BCE" value="SP1696" constant="no"/>
<attribute name="CODE-BCMI" value="1696" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="-" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="-" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="-" constant="no"/>
<attribute name="MANUFACTURER" value="-" constant="no"/>
<attribute name="MANUFACTURER-PN" value="-" constant="no"/>
<attribute name="VALUE" value="OUT" constant="no"/>
</technology>
</technologies>
</device>
<device name="SM" package="MODULINO-3SM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="-DNP">
<attribute name="DATASHEET" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="DNP" constant="no"/>
<attribute name="MANUFACTURER" value="DNP" constant="no"/>
<attribute name="MANUFACTURER-PN" value="DNP" constant="no"/>
<attribute name="VALUE" value="DNP" constant="no"/>
</technology>
<technology name="-SCREW">
<attribute name="DATASHEET" value="http://www.on-shore.com/wp-content/uploads/2015/09/ostvnxxa150.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/it/on-shore-technology-inc/OSTVN03A150/ED10562-ND/1588863" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="ED10562-ND" constant="no"/>
<attribute name="MANUFACTURER" value="On Shore" constant="no"/>
<attribute name="MANUFACTURER-PN" value="OSTVN03A150" constant="no"/>
<attribute name="VALUE" value="-" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MODULINO-4" prefix="J">
<description>Modulino 4 connector</description>
<gates>
<gate name="G$1" symbol="MODULINO-4" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MODULINO-4">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="-" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="-" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="-" constant="no"/>
<attribute name="MANUFACTURER" value="-" constant="no"/>
<attribute name="MANUFACTURER-PN" value="-" constant="no"/>
<attribute name="VALUE" value="-" constant="no"/>
</technology>
</technologies>
</device>
<device name="SM" package="MODULINO-4SM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="-DNP">
<attribute name="DATASHEET" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="DNP" constant="no"/>
<attribute name="MANUFACTURER" value="DNP" constant="no"/>
<attribute name="MANUFACTURER-PN" value="DNP" constant="no"/>
<attribute name="VALUE" value="DNP" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="JTAG_CONNECTOR" prefix="CN" uservalue="yes">
<description>2x5 PASSO 1,27</description>
<gates>
<gate name="G$1" symbol="PINHEAD_2X05_127" x="0" y="0"/>
</gates>
<devices>
<device name="-LONG" package="PINHEAD-2X5-P127">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="CODE-BCE" value="DNP" constant="no"/>
<attribute name="CODE-TK2" value="DNP" constant="no"/>
<attribute name="DATASHEET" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="DNP" constant="no"/>
<attribute name="MANUFACTURER" value="DNP" constant="no"/>
<attribute name="MANUFACTURER-PN" value="DNP" constant="no"/>
</technology>
</technologies>
</device>
<device name="-ROUND" package="PINHEAD_2X05_127">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="CODE-BCE" value="DNP" constant="no"/>
<attribute name="CODE-TK2" value="DNP" constant="no"/>
<attribute name="DATASHEET" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="DNP" constant="no"/>
<attribute name="MANUFACTURER" value="DNP" constant="no"/>
<attribute name="MANUFACTURER-PN" value="DNP" constant="no"/>
<attribute name="VALUE" value="DNP" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GROVE-CONNECTOR" prefix="J" uservalue="yes">
<description>Grove connector</description>
<gates>
<gate name="G$1" symbol="GROVE-CONNECTOR-DIP" x="0" y="0"/>
</gates>
<devices>
<device name="" package="HW4-2.0">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="http://www.jst-mfg.com/product/pdf/eng/ePH.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/jst-sales-america-inc/B4B-PH-K-S(LF)(SN)/455-1706-ND/926613" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="455-1706-ND" constant="no"/>
<attribute name="MANUFACTURER" value="JST" constant="no"/>
<attribute name="MANUFACTURER-PN" value="B4B-PH-K-S(LF)(SN)" constant="no"/>
<attribute name="VALUE" value="-" constant="no"/>
</technology>
<technology name="-DNP">
<attribute name="CODE-BCE" value="DNP" constant="no"/>
<attribute name="CODE-TK2" value="DNP" constant="no"/>
<attribute name="DATASHEET" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="DNP" constant="no"/>
<attribute name="MANUFACTURER" value="DNP" constant="no"/>
<attribute name="MANUFACTURER-PN" value="DNP" constant="no"/>
<attribute name="VALUE" value="DNP" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="BCMI-utility">
<description>Copyright (c) 2016-2017 Arduino LLC.  All right reserved.</description>
<packages>
<package name="MKRBOARD">
<wire x1="0" y1="2.25" x2="2.25" y2="0" width="0.1" layer="21" curve="90"/>
<wire x1="2.25" y1="0" x2="59.25" y2="0" width="0.1" layer="21"/>
<wire x1="59.25" y1="0" x2="61.5" y2="2.25" width="0.1" layer="21" curve="90"/>
<wire x1="61.5" y1="2.25" x2="61.5" y2="22.75" width="0.1" layer="21"/>
<wire x1="61.5" y1="22.75" x2="59.25" y2="25" width="0.1" layer="21" curve="90"/>
<wire x1="59.25" y1="25" x2="2.25" y2="25" width="0.1" layer="21"/>
<wire x1="2.25" y1="25" x2="0" y2="22.75" width="0.1" layer="21" curve="90"/>
<wire x1="0" y1="22.75" x2="0" y2="2.25" width="0.1" layer="21"/>
<pad name="6" x="55.1548" y="22.66" drill="0.8" diameter="1.524" shape="square"/>
<pad name="AREF" x="22.1348" y="2.34" drill="0.8" diameter="1.524" shape="square"/>
<pad name="7" x="52.6148" y="22.66" drill="0.8" diameter="1.524"/>
<pad name="8" x="50.0748" y="22.66" drill="0.8" diameter="1.524"/>
<pad name="9" x="47.5348" y="22.66" drill="0.8" diameter="1.524"/>
<pad name="10" x="44.9948" y="22.66" drill="0.8" diameter="1.524"/>
<pad name="11" x="42.4548" y="22.66" drill="0.8" diameter="1.524"/>
<pad name="12" x="39.9148" y="22.66" drill="0.8" diameter="1.524"/>
<pad name="13" x="37.3748" y="22.66" drill="0.8" diameter="1.524"/>
<pad name="14" x="34.8348" y="22.66" drill="0.8" diameter="1.524"/>
<pad name="RST" x="32.2948" y="22.66" drill="0.8" diameter="1.524"/>
<pad name="GND" x="29.7548" y="22.66" drill="0.8" diameter="1.524"/>
<pad name="VCC" x="27.2148" y="22.66" drill="0.8" diameter="1.524"/>
<pad name="VIN" x="24.6748" y="22.66" drill="0.8" diameter="1.524"/>
<pad name="5V" x="22.1348" y="22.66" drill="0.8" diameter="1.524"/>
<pad name="A0" x="24.6748" y="2.34" drill="0.8" diameter="1.524"/>
<pad name="A1" x="27.2148" y="2.34" drill="0.8" diameter="1.524"/>
<pad name="A2" x="29.7548" y="2.34" drill="0.8" diameter="1.524"/>
<pad name="A3" x="32.2948" y="2.34" drill="0.8" diameter="1.524"/>
<pad name="A4" x="34.8348" y="2.34" drill="0.8" diameter="1.524"/>
<pad name="A5" x="37.3748" y="2.34" drill="0.8" diameter="1.524"/>
<pad name="A6" x="39.9148" y="2.34" drill="0.8" diameter="1.524"/>
<pad name="0" x="42.4548" y="2.34" drill="0.8" diameter="1.524"/>
<pad name="1" x="44.9948" y="2.34" drill="0.8" diameter="1.524"/>
<pad name="2" x="47.5348" y="2.34" drill="0.8" diameter="1.524"/>
<pad name="3" x="50.0748" y="2.34" drill="0.8" diameter="1.524"/>
<pad name="4" x="52.6148" y="2.34" drill="0.8" diameter="1.524"/>
<pad name="5" x="55.1548" y="2.34" drill="0.8" diameter="1.524"/>
<hole x="59.25" y="2.25" drill="2.25"/>
<hole x="59.25" y="22.75" drill="2.25"/>
<hole x="2.25" y="2.25" drill="2.25"/>
<hole x="2.25" y="22.75" drill="2.25"/>
<text x="22.86" y="3.81" size="1.016" layer="21" rot="R90">AREF</text>
<text x="25.4" y="3.81" size="1.016" layer="21" rot="R90">DAC0/A0</text>
<text x="27.94" y="3.81" size="1.016" layer="21" rot="R90">A1</text>
<text x="30.48" y="3.81" size="1.016" layer="21" rot="R90">A2</text>
<text x="33.02" y="3.81" size="1.016" layer="21" rot="R90">A3</text>
<text x="35.56" y="3.81" size="1.016" layer="21" rot="R90">A4</text>
<text x="38.1" y="3.81" size="1.016" layer="21" rot="R90">A5</text>
<text x="40.64" y="3.81" size="1.016" layer="21" rot="R90">A6</text>
<text x="43.18" y="3.81" size="1.016" layer="21" rot="R90">0</text>
<text x="45.72" y="3.81" size="1.016" layer="21" rot="R90">1</text>
<text x="48.26" y="3.81" size="1.016" layer="21" rot="R90">2</text>
<text x="50.8" y="3.81" size="1.016" layer="21" rot="R90">3</text>
<text x="53.34" y="3.81" size="1.016" layer="21" rot="R90">4</text>
<text x="55.88" y="3.81" size="1.016" layer="21" rot="R90">5</text>
<text x="22.86" y="20.32" size="1.016" layer="21" rot="R270" align="top-left">5V</text>
<text x="25.4" y="20.32" size="1.016" layer="21" rot="R270" align="top-left">VIN</text>
<text x="27.94" y="20.32" size="1.016" layer="21" rot="R270" align="top-left">VCC</text>
<text x="30.48" y="20.32" size="1.016" layer="21" rot="R270" align="top-left">GND</text>
<text x="33.02" y="20.32" size="1.016" layer="21" rot="R270" align="top-left">RST</text>
<text x="35.56" y="20.32" size="1.016" layer="21" rot="R270" align="top-left">14/TX</text>
<text x="38.1" y="20.32" size="1.016" layer="21" rot="R270" align="top-left">13/RX</text>
<text x="40.64" y="20.32" size="1.016" layer="21" rot="R270" align="top-left">12/SCL</text>
<text x="43.18" y="20.32" size="1.016" layer="21" rot="R270" align="top-left">11/SDA</text>
<text x="45.72" y="20.32" size="1.016" layer="21" rot="R270" align="top-left">10/MISO</text>
<text x="48.26" y="20.32" size="1.016" layer="21" rot="R270" align="top-left">9/SCK</text>
<text x="50.8" y="20.32" size="1.016" layer="21" rot="R270" align="top-left">8/MOSI</text>
<text x="53.34" y="20.32" size="1.016" layer="21" rot="R270" align="top-left">7</text>
<text x="55.88" y="20.32" size="1.016" layer="21" rot="R270" align="top-left">6</text>
</package>
<package name="MKRBOARD-NOOUTLINE">
<pad name="6" x="55.1548" y="22.66" drill="0.8" shape="square"/>
<pad name="AREF" x="22.1348" y="2.34" drill="0.8" shape="square"/>
<pad name="7" x="52.6148" y="22.66" drill="0.8"/>
<pad name="8" x="50.0748" y="22.66" drill="0.8"/>
<pad name="9" x="47.5348" y="22.66" drill="0.8"/>
<pad name="10" x="44.9948" y="22.66" drill="0.8"/>
<pad name="11" x="42.4548" y="22.66" drill="0.8"/>
<pad name="12" x="39.9148" y="22.66" drill="0.8"/>
<pad name="13" x="37.3748" y="22.66" drill="0.8"/>
<pad name="14" x="34.8348" y="22.66" drill="0.8"/>
<pad name="RST" x="32.2948" y="22.66" drill="0.8"/>
<pad name="GND" x="29.7548" y="22.66" drill="0.8"/>
<pad name="VCC" x="27.2148" y="22.66" drill="0.8"/>
<pad name="VIN" x="24.6748" y="22.66" drill="0.8"/>
<pad name="5V" x="22.1348" y="22.66" drill="0.8"/>
<pad name="A0" x="24.6748" y="2.34" drill="0.8"/>
<pad name="A1" x="27.2148" y="2.34" drill="0.8"/>
<pad name="A2" x="29.7548" y="2.34" drill="0.8"/>
<pad name="A3" x="32.2948" y="2.34" drill="0.8"/>
<pad name="A4" x="34.8348" y="2.34" drill="0.8"/>
<pad name="A5" x="37.3748" y="2.34" drill="0.8"/>
<pad name="A6" x="39.9148" y="2.34" drill="0.8"/>
<pad name="0" x="42.4548" y="2.34" drill="0.8"/>
<pad name="1" x="44.9948" y="2.34" drill="0.8"/>
<pad name="2" x="47.5348" y="2.34" drill="0.8"/>
<pad name="3" x="50.0748" y="2.34" drill="0.8"/>
<pad name="4" x="52.6148" y="2.34" drill="0.8"/>
<pad name="5" x="55.1548" y="2.34" drill="0.8"/>
<text x="22.86" y="5.08" size="1.016" layer="21" rot="R90">AREF</text>
<text x="25.4" y="5.08" size="1.016" layer="21" rot="R90">DAC0/A0</text>
<text x="27.94" y="5.08" size="1.016" layer="21" rot="R90">A1</text>
<text x="30.48" y="5.08" size="1.016" layer="21" rot="R90">A2</text>
<text x="33.02" y="5.08" size="1.016" layer="21" rot="R90">A3</text>
<text x="35.56" y="5.08" size="1.016" layer="21" rot="R90">A4</text>
<text x="38.1" y="5.08" size="1.016" layer="21" rot="R90">A5</text>
<text x="40.64" y="5.08" size="1.016" layer="21" rot="R90">A6</text>
<text x="43.18" y="5.08" size="1.016" layer="21" rot="R90">0</text>
<text x="45.72" y="5.08" size="1.016" layer="21" rot="R90">1</text>
<text x="48.26" y="5.08" size="1.016" layer="21" rot="R90">2</text>
<text x="50.8" y="5.08" size="1.016" layer="21" rot="R90">3</text>
<text x="53.34" y="5.08" size="1.016" layer="21" rot="R90">4</text>
<text x="55.88" y="5.08" size="1.016" layer="21" rot="R90">5</text>
<text x="22.86" y="20.32" size="1.016" layer="21" rot="R270" align="top-left">5V</text>
<text x="25.4" y="20.32" size="1.016" layer="21" rot="R270" align="top-left">VIN</text>
<text x="27.94" y="20.32" size="1.016" layer="21" rot="R270" align="top-left">VCC</text>
<text x="30.48" y="20.32" size="1.016" layer="21" rot="R270" align="top-left">GND</text>
<text x="33.02" y="20.32" size="1.016" layer="21" rot="R270" align="top-left">RST</text>
<text x="35.56" y="20.32" size="1.016" layer="21" rot="R270" align="top-left">14/TX</text>
<text x="38.1" y="20.32" size="1.016" layer="21" rot="R270" align="top-left">13/RX</text>
<text x="40.64" y="20.32" size="1.016" layer="21" rot="R270" align="top-left">12/SCL</text>
<text x="43.18" y="20.32" size="1.016" layer="21" rot="R270" align="top-left">11/SDA</text>
<text x="45.72" y="20.32" size="1.016" layer="21" rot="R270" align="top-left">10/MISO</text>
<text x="48.26" y="20.32" size="1.016" layer="21" rot="R270" align="top-left">9/SCK</text>
<text x="50.8" y="20.32" size="1.016" layer="21" rot="R270" align="top-left">8/MOSI</text>
<text x="53.34" y="20.32" size="1.016" layer="21" rot="R270" align="top-left">7</text>
<text x="55.88" y="20.32" size="1.016" layer="21" rot="R270" align="top-left">6</text>
</package>
<package name="MKRBOARD-W-SMD">
<wire x1="22.1348" y1="1.99" x2="22.1348" y2="2.69" width="0.05" layer="39"/>
<pad name="AREF" x="22.1348" y="2.34" drill="1" first="yes"/>
<pad name="DAC0/A0" x="24.6748" y="2.34" drill="1"/>
<pad name="A1" x="27.2148" y="2.34" drill="1"/>
<pad name="A2" x="29.7548" y="2.34" drill="1"/>
<pad name="A3" x="32.2948" y="2.34" drill="1"/>
<pad name="A4" x="34.8348" y="2.34" drill="1"/>
<pad name="A5" x="37.3748" y="2.34" drill="1"/>
<pad name="A6" x="39.9148" y="2.34" drill="1"/>
<pad name="0" x="42.4548" y="2.34" drill="1"/>
<pad name="1" x="44.9948" y="2.34" drill="1"/>
<pad name="2" x="47.5348" y="2.34" drill="1"/>
<pad name="3" x="50.0748" y="2.34" drill="1"/>
<pad name="4" x="52.6148" y="2.34" drill="1"/>
<pad name="5" x="55.1548" y="2.34" drill="1"/>
<polygon width="0.0254" layer="29">
<vertex x="21.2585" y="3.3687"/>
<vertex x="23.0111" y="3.3687"/>
<vertex x="23.0111" y="1.3113"/>
<vertex x="21.2585" y="1.3113"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="21.2585" y="3.3687"/>
<vertex x="21.2585" y="1.5653"/>
<vertex x="21.8046" y="1.5653" curve="-90"/>
<vertex x="21.9062" y="1.4637"/>
<vertex x="21.3855" y="1.4637" curve="90"/>
<vertex x="21.2585" y="1.3367"/>
<vertex x="21.2585" y="0.4858" curve="90"/>
<vertex x="21.3855" y="0.3588"/>
<vertex x="22.8841" y="0.3588" curve="90"/>
<vertex x="23.0111" y="0.4858"/>
<vertex x="23.0111" y="1.3367" curve="90"/>
<vertex x="22.8841" y="1.4637"/>
<vertex x="22.3634" y="1.4637" curve="-90"/>
<vertex x="22.465" y="1.5653"/>
<vertex x="23.0111" y="1.5653"/>
<vertex x="23.0111" y="3.3687"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="23.7985" y="2.4924" curve="-90"/>
<vertex x="24.6621" y="3.356"/>
<vertex x="24.6875" y="3.356" curve="-90"/>
<vertex x="25.5511" y="2.4924"/>
<vertex x="25.5511" y="2.1876"/>
<vertex x="25.5257" y="1.9971"/>
<vertex x="25.4622" y="1.8066"/>
<vertex x="25.3098" y="1.5907"/>
<vertex x="25.259" y="1.5653"/>
<vertex x="25.005" y="1.5653" curve="90"/>
<vertex x="24.9034" y="1.4637"/>
<vertex x="25.4241" y="1.4637" curve="-90"/>
<vertex x="25.5511" y="1.3367"/>
<vertex x="25.5511" y="0.4858" curve="-90"/>
<vertex x="25.4241" y="0.3588"/>
<vertex x="23.9255" y="0.3588" curve="-90"/>
<vertex x="23.7985" y="0.4858"/>
<vertex x="23.7985" y="1.3367" curve="-90"/>
<vertex x="23.9255" y="1.4637"/>
<vertex x="24.4462" y="1.4637" curve="90"/>
<vertex x="24.3446" y="1.5653"/>
<vertex x="24.0906" y="1.5653"/>
<vertex x="24.0398" y="1.5907"/>
<vertex x="23.8874" y="1.8066"/>
<vertex x="23.7985" y="2.1876"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="23.7985" y="2.4924" curve="-90"/>
<vertex x="24.6621" y="3.356"/>
<vertex x="24.6875" y="3.356" curve="-90"/>
<vertex x="25.5511" y="2.4924"/>
<vertex x="25.5511" y="2.1876"/>
<vertex x="25.5257" y="1.9971"/>
<vertex x="25.4622" y="1.8066"/>
<vertex x="25.3098" y="1.5907"/>
<vertex x="25.259" y="1.5653"/>
<vertex x="25.005" y="1.5653" curve="90"/>
<vertex x="24.9034" y="1.4637"/>
<vertex x="25.4241" y="1.4637" curve="-90"/>
<vertex x="25.5511" y="1.3367"/>
<vertex x="25.5511" y="0.4858" curve="-90"/>
<vertex x="25.4241" y="0.3588"/>
<vertex x="23.9255" y="0.3588" curve="-90"/>
<vertex x="23.7985" y="0.4858"/>
<vertex x="23.7985" y="1.3367" curve="-90"/>
<vertex x="23.9255" y="1.4637"/>
<vertex x="24.4462" y="1.4637" curve="90"/>
<vertex x="24.3446" y="1.5653"/>
<vertex x="24.0906" y="1.5653"/>
<vertex x="24.0398" y="1.5907"/>
<vertex x="23.8874" y="1.8066"/>
<vertex x="23.7985" y="2.1876"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="26.3385" y="2.4924" curve="-90"/>
<vertex x="27.2021" y="3.356"/>
<vertex x="27.2275" y="3.356" curve="-90"/>
<vertex x="28.0911" y="2.4924"/>
<vertex x="28.0911" y="2.1876"/>
<vertex x="28.0657" y="1.9971"/>
<vertex x="28.0022" y="1.8066"/>
<vertex x="27.8498" y="1.5907"/>
<vertex x="27.799" y="1.5653"/>
<vertex x="27.545" y="1.5653" curve="90"/>
<vertex x="27.4434" y="1.4637"/>
<vertex x="27.9641" y="1.4637" curve="-90"/>
<vertex x="28.0911" y="1.3367"/>
<vertex x="28.0911" y="0.4858" curve="-90"/>
<vertex x="27.9641" y="0.3588"/>
<vertex x="26.4655" y="0.3588" curve="-90"/>
<vertex x="26.3385" y="0.4858"/>
<vertex x="26.3385" y="1.3367" curve="-90"/>
<vertex x="26.4655" y="1.4637"/>
<vertex x="26.9862" y="1.4637" curve="90"/>
<vertex x="26.8846" y="1.5653"/>
<vertex x="26.6306" y="1.5653"/>
<vertex x="26.5798" y="1.5907"/>
<vertex x="26.4274" y="1.8066"/>
<vertex x="26.3385" y="2.1876"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="26.3385" y="2.4924" curve="-90"/>
<vertex x="27.2021" y="3.356"/>
<vertex x="27.2275" y="3.356" curve="-90"/>
<vertex x="28.0911" y="2.4924"/>
<vertex x="28.0911" y="2.1876"/>
<vertex x="28.0657" y="1.9971"/>
<vertex x="28.0022" y="1.8066"/>
<vertex x="27.8498" y="1.5907"/>
<vertex x="27.799" y="1.5653"/>
<vertex x="27.545" y="1.5653" curve="90"/>
<vertex x="27.4434" y="1.4637"/>
<vertex x="27.9641" y="1.4637" curve="-90"/>
<vertex x="28.0911" y="1.3367"/>
<vertex x="28.0911" y="0.4858" curve="-90"/>
<vertex x="27.9641" y="0.3588"/>
<vertex x="26.4655" y="0.3588" curve="-90"/>
<vertex x="26.3385" y="0.4858"/>
<vertex x="26.3385" y="1.3367" curve="-90"/>
<vertex x="26.4655" y="1.4637"/>
<vertex x="26.9862" y="1.4637" curve="90"/>
<vertex x="26.8846" y="1.5653"/>
<vertex x="26.6306" y="1.5653"/>
<vertex x="26.5798" y="1.5907"/>
<vertex x="26.4274" y="1.8066"/>
<vertex x="26.3385" y="2.1876"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="28.8785" y="2.4924" curve="-90"/>
<vertex x="29.7421" y="3.356"/>
<vertex x="29.7675" y="3.356" curve="-90"/>
<vertex x="30.6311" y="2.4924"/>
<vertex x="30.6311" y="2.1876"/>
<vertex x="30.6057" y="1.9971"/>
<vertex x="30.5422" y="1.8066"/>
<vertex x="30.3898" y="1.5907"/>
<vertex x="30.339" y="1.5653"/>
<vertex x="30.085" y="1.5653" curve="90"/>
<vertex x="29.9834" y="1.4637"/>
<vertex x="30.5041" y="1.4637" curve="-90"/>
<vertex x="30.6311" y="1.3367"/>
<vertex x="30.6311" y="0.4858" curve="-90"/>
<vertex x="30.5041" y="0.3588"/>
<vertex x="29.0055" y="0.3588" curve="-90"/>
<vertex x="28.8785" y="0.4858"/>
<vertex x="28.8785" y="1.3367" curve="-90"/>
<vertex x="29.0055" y="1.4637"/>
<vertex x="29.5262" y="1.4637" curve="90"/>
<vertex x="29.4246" y="1.5653"/>
<vertex x="29.1706" y="1.5653"/>
<vertex x="29.1198" y="1.5907"/>
<vertex x="28.9674" y="1.8066"/>
<vertex x="28.8785" y="2.1876"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="28.8785" y="2.4924" curve="-90"/>
<vertex x="29.7421" y="3.356"/>
<vertex x="29.7675" y="3.356" curve="-90"/>
<vertex x="30.6311" y="2.4924"/>
<vertex x="30.6311" y="2.1876"/>
<vertex x="30.6057" y="1.9971"/>
<vertex x="30.5422" y="1.8066"/>
<vertex x="30.3898" y="1.5907"/>
<vertex x="30.339" y="1.5653"/>
<vertex x="30.085" y="1.5653" curve="90"/>
<vertex x="29.9834" y="1.4637"/>
<vertex x="30.5041" y="1.4637" curve="-90"/>
<vertex x="30.6311" y="1.3367"/>
<vertex x="30.6311" y="0.4858" curve="-90"/>
<vertex x="30.5041" y="0.3588"/>
<vertex x="29.0055" y="0.3588" curve="-90"/>
<vertex x="28.8785" y="0.4858"/>
<vertex x="28.8785" y="1.3367" curve="-90"/>
<vertex x="29.0055" y="1.4637"/>
<vertex x="29.5262" y="1.4637" curve="90"/>
<vertex x="29.4246" y="1.5653"/>
<vertex x="29.1706" y="1.5653"/>
<vertex x="29.1198" y="1.5907"/>
<vertex x="28.9674" y="1.8066"/>
<vertex x="28.8785" y="2.1876"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="31.4185" y="2.4924" curve="-90"/>
<vertex x="32.2821" y="3.356"/>
<vertex x="32.3075" y="3.356" curve="-90"/>
<vertex x="33.1711" y="2.4924"/>
<vertex x="33.1711" y="2.1876"/>
<vertex x="33.1457" y="1.9971"/>
<vertex x="33.0822" y="1.8066"/>
<vertex x="32.9298" y="1.5907"/>
<vertex x="32.879" y="1.5653"/>
<vertex x="32.625" y="1.5653" curve="90"/>
<vertex x="32.5234" y="1.4637"/>
<vertex x="33.0441" y="1.4637" curve="-90"/>
<vertex x="33.1711" y="1.3367"/>
<vertex x="33.1711" y="0.4858" curve="-90"/>
<vertex x="33.0441" y="0.3588"/>
<vertex x="31.5455" y="0.3588" curve="-90"/>
<vertex x="31.4185" y="0.4858"/>
<vertex x="31.4185" y="1.3367" curve="-90"/>
<vertex x="31.5455" y="1.4637"/>
<vertex x="32.0662" y="1.4637" curve="90"/>
<vertex x="31.9646" y="1.5653"/>
<vertex x="31.7106" y="1.5653"/>
<vertex x="31.6598" y="1.5907"/>
<vertex x="31.5074" y="1.8066"/>
<vertex x="31.4185" y="2.1876"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="31.4185" y="2.4924" curve="-90"/>
<vertex x="32.2821" y="3.356"/>
<vertex x="32.3075" y="3.356" curve="-90"/>
<vertex x="33.1711" y="2.4924"/>
<vertex x="33.1711" y="2.1876"/>
<vertex x="33.1457" y="1.9971"/>
<vertex x="33.0822" y="1.8066"/>
<vertex x="32.9298" y="1.5907"/>
<vertex x="32.879" y="1.5653"/>
<vertex x="32.625" y="1.5653" curve="90"/>
<vertex x="32.5234" y="1.4637"/>
<vertex x="33.0441" y="1.4637" curve="-90"/>
<vertex x="33.1711" y="1.3367"/>
<vertex x="33.1711" y="0.4858" curve="-90"/>
<vertex x="33.0441" y="0.3588"/>
<vertex x="31.5455" y="0.3588" curve="-90"/>
<vertex x="31.4185" y="0.4858"/>
<vertex x="31.4185" y="1.3367" curve="-90"/>
<vertex x="31.5455" y="1.4637"/>
<vertex x="32.0662" y="1.4637" curve="90"/>
<vertex x="31.9646" y="1.5653"/>
<vertex x="31.7106" y="1.5653"/>
<vertex x="31.6598" y="1.5907"/>
<vertex x="31.5074" y="1.8066"/>
<vertex x="31.4185" y="2.1876"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="33.9585" y="2.4924" curve="-90"/>
<vertex x="34.8221" y="3.356"/>
<vertex x="34.8475" y="3.356" curve="-90"/>
<vertex x="35.7111" y="2.4924"/>
<vertex x="35.7111" y="2.1876"/>
<vertex x="35.6857" y="1.9971"/>
<vertex x="35.6222" y="1.8066"/>
<vertex x="35.4698" y="1.5907"/>
<vertex x="35.419" y="1.5653"/>
<vertex x="35.165" y="1.5653" curve="90"/>
<vertex x="35.0634" y="1.4637"/>
<vertex x="35.5841" y="1.4637" curve="-90"/>
<vertex x="35.7111" y="1.3367"/>
<vertex x="35.7111" y="0.4858" curve="-90"/>
<vertex x="35.5841" y="0.3588"/>
<vertex x="34.0855" y="0.3588" curve="-90"/>
<vertex x="33.9585" y="0.4858"/>
<vertex x="33.9585" y="1.3367" curve="-90"/>
<vertex x="34.0855" y="1.4637"/>
<vertex x="34.6062" y="1.4637" curve="90"/>
<vertex x="34.5046" y="1.5653"/>
<vertex x="34.2506" y="1.5653"/>
<vertex x="34.1998" y="1.5907"/>
<vertex x="34.0474" y="1.8066"/>
<vertex x="33.9585" y="2.1876"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="33.9585" y="2.4924" curve="-90"/>
<vertex x="34.8221" y="3.356"/>
<vertex x="34.8475" y="3.356" curve="-90"/>
<vertex x="35.7111" y="2.4924"/>
<vertex x="35.7111" y="2.1876"/>
<vertex x="35.6857" y="1.9971"/>
<vertex x="35.6222" y="1.8066"/>
<vertex x="35.4698" y="1.5907"/>
<vertex x="35.419" y="1.5653"/>
<vertex x="35.165" y="1.5653" curve="90"/>
<vertex x="35.0634" y="1.4637"/>
<vertex x="35.5841" y="1.4637" curve="-90"/>
<vertex x="35.7111" y="1.3367"/>
<vertex x="35.7111" y="0.4858" curve="-90"/>
<vertex x="35.5841" y="0.3588"/>
<vertex x="34.0855" y="0.3588" curve="-90"/>
<vertex x="33.9585" y="0.4858"/>
<vertex x="33.9585" y="1.3367" curve="-90"/>
<vertex x="34.0855" y="1.4637"/>
<vertex x="34.6062" y="1.4637" curve="90"/>
<vertex x="34.5046" y="1.5653"/>
<vertex x="34.2506" y="1.5653"/>
<vertex x="34.1998" y="1.5907"/>
<vertex x="34.0474" y="1.8066"/>
<vertex x="33.9585" y="2.1876"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="36.4985" y="2.4924" curve="-90"/>
<vertex x="37.3621" y="3.356"/>
<vertex x="37.3875" y="3.356" curve="-90"/>
<vertex x="38.2511" y="2.4924"/>
<vertex x="38.2511" y="2.1876"/>
<vertex x="38.2257" y="1.9971"/>
<vertex x="38.1622" y="1.8066"/>
<vertex x="38.0098" y="1.5907"/>
<vertex x="37.959" y="1.5653"/>
<vertex x="37.705" y="1.5653" curve="90"/>
<vertex x="37.6034" y="1.4637"/>
<vertex x="38.1241" y="1.4637" curve="-90"/>
<vertex x="38.2511" y="1.3367"/>
<vertex x="38.2511" y="0.4858" curve="-90"/>
<vertex x="38.1241" y="0.3588"/>
<vertex x="36.6255" y="0.3588" curve="-90"/>
<vertex x="36.4985" y="0.4858"/>
<vertex x="36.4985" y="1.3367" curve="-90"/>
<vertex x="36.6255" y="1.4637"/>
<vertex x="37.1462" y="1.4637" curve="90"/>
<vertex x="37.0446" y="1.5653"/>
<vertex x="36.7906" y="1.5653"/>
<vertex x="36.7398" y="1.5907"/>
<vertex x="36.5874" y="1.8066"/>
<vertex x="36.4985" y="2.1876"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="36.4985" y="2.4924" curve="-90"/>
<vertex x="37.3621" y="3.356"/>
<vertex x="37.3875" y="3.356" curve="-90"/>
<vertex x="38.2511" y="2.4924"/>
<vertex x="38.2511" y="2.1876"/>
<vertex x="38.2257" y="1.9971"/>
<vertex x="38.1622" y="1.8066"/>
<vertex x="38.0098" y="1.5907"/>
<vertex x="37.959" y="1.5653"/>
<vertex x="37.705" y="1.5653" curve="90"/>
<vertex x="37.6034" y="1.4637"/>
<vertex x="38.1241" y="1.4637" curve="-90"/>
<vertex x="38.2511" y="1.3367"/>
<vertex x="38.2511" y="0.4858" curve="-90"/>
<vertex x="38.1241" y="0.3588"/>
<vertex x="36.6255" y="0.3588" curve="-90"/>
<vertex x="36.4985" y="0.4858"/>
<vertex x="36.4985" y="1.3367" curve="-90"/>
<vertex x="36.6255" y="1.4637"/>
<vertex x="37.1462" y="1.4637" curve="90"/>
<vertex x="37.0446" y="1.5653"/>
<vertex x="36.7906" y="1.5653"/>
<vertex x="36.7398" y="1.5907"/>
<vertex x="36.5874" y="1.8066"/>
<vertex x="36.4985" y="2.1876"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="39.0385" y="2.4924" curve="-90"/>
<vertex x="39.9021" y="3.356"/>
<vertex x="39.9275" y="3.356" curve="-90"/>
<vertex x="40.7911" y="2.4924"/>
<vertex x="40.7911" y="2.1876"/>
<vertex x="40.7657" y="1.9971"/>
<vertex x="40.7022" y="1.8066"/>
<vertex x="40.5498" y="1.5907"/>
<vertex x="40.499" y="1.5653"/>
<vertex x="40.245" y="1.5653" curve="90"/>
<vertex x="40.1434" y="1.4637"/>
<vertex x="40.6641" y="1.4637" curve="-90"/>
<vertex x="40.7911" y="1.3367"/>
<vertex x="40.7911" y="0.4858" curve="-90"/>
<vertex x="40.6641" y="0.3588"/>
<vertex x="39.1655" y="0.3588" curve="-90"/>
<vertex x="39.0385" y="0.4858"/>
<vertex x="39.0385" y="1.3367" curve="-90"/>
<vertex x="39.1655" y="1.4637"/>
<vertex x="39.6862" y="1.4637" curve="90"/>
<vertex x="39.5846" y="1.5653"/>
<vertex x="39.3306" y="1.5653"/>
<vertex x="39.2798" y="1.5907"/>
<vertex x="39.1274" y="1.8066"/>
<vertex x="39.0385" y="2.1876"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="39.0385" y="2.4924" curve="-90"/>
<vertex x="39.9021" y="3.356"/>
<vertex x="39.9275" y="3.356" curve="-90"/>
<vertex x="40.7911" y="2.4924"/>
<vertex x="40.7911" y="2.1876"/>
<vertex x="40.7657" y="1.9971"/>
<vertex x="40.7022" y="1.8066"/>
<vertex x="40.5498" y="1.5907"/>
<vertex x="40.499" y="1.5653"/>
<vertex x="40.245" y="1.5653" curve="90"/>
<vertex x="40.1434" y="1.4637"/>
<vertex x="40.6641" y="1.4637" curve="-90"/>
<vertex x="40.7911" y="1.3367"/>
<vertex x="40.7911" y="0.4858" curve="-90"/>
<vertex x="40.6641" y="0.3588"/>
<vertex x="39.1655" y="0.3588" curve="-90"/>
<vertex x="39.0385" y="0.4858"/>
<vertex x="39.0385" y="1.3367" curve="-90"/>
<vertex x="39.1655" y="1.4637"/>
<vertex x="39.6862" y="1.4637" curve="90"/>
<vertex x="39.5846" y="1.5653"/>
<vertex x="39.3306" y="1.5653"/>
<vertex x="39.2798" y="1.5907"/>
<vertex x="39.1274" y="1.8066"/>
<vertex x="39.0385" y="2.1876"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="41.5785" y="2.4924" curve="-90"/>
<vertex x="42.4421" y="3.356"/>
<vertex x="42.4675" y="3.356" curve="-90"/>
<vertex x="43.3311" y="2.4924"/>
<vertex x="43.3311" y="2.1876"/>
<vertex x="43.3057" y="1.9971"/>
<vertex x="43.2422" y="1.8066"/>
<vertex x="43.0898" y="1.5907"/>
<vertex x="43.039" y="1.5653"/>
<vertex x="42.785" y="1.5653" curve="90"/>
<vertex x="42.6834" y="1.4637"/>
<vertex x="43.2041" y="1.4637" curve="-90"/>
<vertex x="43.3311" y="1.3367"/>
<vertex x="43.3311" y="0.4858" curve="-90"/>
<vertex x="43.2041" y="0.3588"/>
<vertex x="41.7055" y="0.3588" curve="-90"/>
<vertex x="41.5785" y="0.4858"/>
<vertex x="41.5785" y="1.3367" curve="-90"/>
<vertex x="41.7055" y="1.4637"/>
<vertex x="42.2262" y="1.4637" curve="90"/>
<vertex x="42.1246" y="1.5653"/>
<vertex x="41.8706" y="1.5653"/>
<vertex x="41.8198" y="1.5907"/>
<vertex x="41.6674" y="1.8066"/>
<vertex x="41.5785" y="2.1876"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="41.5785" y="2.4924" curve="-90"/>
<vertex x="42.4421" y="3.356"/>
<vertex x="42.4675" y="3.356" curve="-90"/>
<vertex x="43.3311" y="2.4924"/>
<vertex x="43.3311" y="2.1876"/>
<vertex x="43.3057" y="1.9971"/>
<vertex x="43.2422" y="1.8066"/>
<vertex x="43.0898" y="1.5907"/>
<vertex x="43.039" y="1.5653"/>
<vertex x="42.785" y="1.5653" curve="90"/>
<vertex x="42.6834" y="1.4637"/>
<vertex x="43.2041" y="1.4637" curve="-90"/>
<vertex x="43.3311" y="1.3367"/>
<vertex x="43.3311" y="0.4858" curve="-90"/>
<vertex x="43.2041" y="0.3588"/>
<vertex x="41.7055" y="0.3588" curve="-90"/>
<vertex x="41.5785" y="0.4858"/>
<vertex x="41.5785" y="1.3367" curve="-90"/>
<vertex x="41.7055" y="1.4637"/>
<vertex x="42.2262" y="1.4637" curve="90"/>
<vertex x="42.1246" y="1.5653"/>
<vertex x="41.8706" y="1.5653"/>
<vertex x="41.8198" y="1.5907"/>
<vertex x="41.6674" y="1.8066"/>
<vertex x="41.5785" y="2.1876"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="44.1185" y="2.4924" curve="-90"/>
<vertex x="44.9821" y="3.356"/>
<vertex x="45.0075" y="3.356" curve="-90"/>
<vertex x="45.8711" y="2.4924"/>
<vertex x="45.8711" y="2.1876"/>
<vertex x="45.8457" y="1.9971"/>
<vertex x="45.7822" y="1.8066"/>
<vertex x="45.6298" y="1.5907"/>
<vertex x="45.579" y="1.5653"/>
<vertex x="45.325" y="1.5653" curve="90"/>
<vertex x="45.2234" y="1.4637"/>
<vertex x="45.7441" y="1.4637" curve="-90"/>
<vertex x="45.8711" y="1.3367"/>
<vertex x="45.8711" y="0.4858" curve="-90"/>
<vertex x="45.7441" y="0.3588"/>
<vertex x="44.2455" y="0.3588" curve="-90"/>
<vertex x="44.1185" y="0.4858"/>
<vertex x="44.1185" y="1.3367" curve="-90"/>
<vertex x="44.2455" y="1.4637"/>
<vertex x="44.7662" y="1.4637" curve="90"/>
<vertex x="44.6646" y="1.5653"/>
<vertex x="44.4106" y="1.5653"/>
<vertex x="44.3598" y="1.5907"/>
<vertex x="44.2074" y="1.8066"/>
<vertex x="44.1185" y="2.1876"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="44.1185" y="2.4924" curve="-90"/>
<vertex x="44.9821" y="3.356"/>
<vertex x="45.0075" y="3.356" curve="-90"/>
<vertex x="45.8711" y="2.4924"/>
<vertex x="45.8711" y="2.1876"/>
<vertex x="45.8457" y="1.9971"/>
<vertex x="45.7822" y="1.8066"/>
<vertex x="45.6298" y="1.5907"/>
<vertex x="45.579" y="1.5653"/>
<vertex x="45.325" y="1.5653" curve="90"/>
<vertex x="45.2234" y="1.4637"/>
<vertex x="45.7441" y="1.4637" curve="-90"/>
<vertex x="45.8711" y="1.3367"/>
<vertex x="45.8711" y="0.4858" curve="-90"/>
<vertex x="45.7441" y="0.3588"/>
<vertex x="44.2455" y="0.3588" curve="-90"/>
<vertex x="44.1185" y="0.4858"/>
<vertex x="44.1185" y="1.3367" curve="-90"/>
<vertex x="44.2455" y="1.4637"/>
<vertex x="44.7662" y="1.4637" curve="90"/>
<vertex x="44.6646" y="1.5653"/>
<vertex x="44.4106" y="1.5653"/>
<vertex x="44.3598" y="1.5907"/>
<vertex x="44.2074" y="1.8066"/>
<vertex x="44.1185" y="2.1876"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="46.6585" y="2.4924" curve="-90"/>
<vertex x="47.5221" y="3.356"/>
<vertex x="47.5475" y="3.356" curve="-90"/>
<vertex x="48.4111" y="2.4924"/>
<vertex x="48.4111" y="2.1876"/>
<vertex x="48.3857" y="1.9971"/>
<vertex x="48.3222" y="1.8066"/>
<vertex x="48.1698" y="1.5907"/>
<vertex x="48.119" y="1.5653"/>
<vertex x="47.865" y="1.5653" curve="90"/>
<vertex x="47.7634" y="1.4637"/>
<vertex x="48.2841" y="1.4637" curve="-90"/>
<vertex x="48.4111" y="1.3367"/>
<vertex x="48.4111" y="0.4858" curve="-90"/>
<vertex x="48.2841" y="0.3588"/>
<vertex x="46.7855" y="0.3588" curve="-90"/>
<vertex x="46.6585" y="0.4858"/>
<vertex x="46.6585" y="1.3367" curve="-90"/>
<vertex x="46.7855" y="1.4637"/>
<vertex x="47.3062" y="1.4637" curve="90"/>
<vertex x="47.2046" y="1.5653"/>
<vertex x="46.9506" y="1.5653"/>
<vertex x="46.8998" y="1.5907"/>
<vertex x="46.7474" y="1.8066"/>
<vertex x="46.6585" y="2.1876"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="46.6585" y="2.4924" curve="-90"/>
<vertex x="47.5221" y="3.356"/>
<vertex x="47.5475" y="3.356" curve="-90"/>
<vertex x="48.4111" y="2.4924"/>
<vertex x="48.4111" y="2.1876"/>
<vertex x="48.3857" y="1.9971"/>
<vertex x="48.3222" y="1.8066"/>
<vertex x="48.1698" y="1.5907"/>
<vertex x="48.119" y="1.5653"/>
<vertex x="47.865" y="1.5653" curve="90"/>
<vertex x="47.7634" y="1.4637"/>
<vertex x="48.2841" y="1.4637" curve="-90"/>
<vertex x="48.4111" y="1.3367"/>
<vertex x="48.4111" y="0.4858" curve="-90"/>
<vertex x="48.2841" y="0.3588"/>
<vertex x="46.7855" y="0.3588" curve="-90"/>
<vertex x="46.6585" y="0.4858"/>
<vertex x="46.6585" y="1.3367" curve="-90"/>
<vertex x="46.7855" y="1.4637"/>
<vertex x="47.3062" y="1.4637" curve="90"/>
<vertex x="47.2046" y="1.5653"/>
<vertex x="46.9506" y="1.5653"/>
<vertex x="46.8998" y="1.5907"/>
<vertex x="46.7474" y="1.8066"/>
<vertex x="46.6585" y="2.1876"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="49.1985" y="2.4924" curve="-90"/>
<vertex x="50.0621" y="3.356"/>
<vertex x="50.0875" y="3.356" curve="-90"/>
<vertex x="50.9511" y="2.4924"/>
<vertex x="50.9511" y="2.1876"/>
<vertex x="50.9257" y="1.9971"/>
<vertex x="50.8622" y="1.8066"/>
<vertex x="50.7098" y="1.5907"/>
<vertex x="50.659" y="1.5653"/>
<vertex x="50.405" y="1.5653" curve="90"/>
<vertex x="50.3034" y="1.4637"/>
<vertex x="50.8241" y="1.4637" curve="-90"/>
<vertex x="50.9511" y="1.3367"/>
<vertex x="50.9511" y="0.4858" curve="-90"/>
<vertex x="50.8241" y="0.3588"/>
<vertex x="49.3255" y="0.3588" curve="-90"/>
<vertex x="49.1985" y="0.4858"/>
<vertex x="49.1985" y="1.3367" curve="-90"/>
<vertex x="49.3255" y="1.4637"/>
<vertex x="49.8462" y="1.4637" curve="90"/>
<vertex x="49.7446" y="1.5653"/>
<vertex x="49.4906" y="1.5653"/>
<vertex x="49.4398" y="1.5907"/>
<vertex x="49.2874" y="1.8066"/>
<vertex x="49.1985" y="2.1876"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="49.1985" y="2.4924" curve="-90"/>
<vertex x="50.0621" y="3.356"/>
<vertex x="50.0875" y="3.356" curve="-90"/>
<vertex x="50.9511" y="2.4924"/>
<vertex x="50.9511" y="2.1876"/>
<vertex x="50.9257" y="1.9971"/>
<vertex x="50.8622" y="1.8066"/>
<vertex x="50.7098" y="1.5907"/>
<vertex x="50.659" y="1.5653"/>
<vertex x="50.405" y="1.5653" curve="90"/>
<vertex x="50.3034" y="1.4637"/>
<vertex x="50.8241" y="1.4637" curve="-90"/>
<vertex x="50.9511" y="1.3367"/>
<vertex x="50.9511" y="0.4858" curve="-90"/>
<vertex x="50.8241" y="0.3588"/>
<vertex x="49.3255" y="0.3588" curve="-90"/>
<vertex x="49.1985" y="0.4858"/>
<vertex x="49.1985" y="1.3367" curve="-90"/>
<vertex x="49.3255" y="1.4637"/>
<vertex x="49.8462" y="1.4637" curve="90"/>
<vertex x="49.7446" y="1.5653"/>
<vertex x="49.4906" y="1.5653"/>
<vertex x="49.4398" y="1.5907"/>
<vertex x="49.2874" y="1.8066"/>
<vertex x="49.1985" y="2.1876"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="51.7385" y="2.4924" curve="-90"/>
<vertex x="52.6021" y="3.356"/>
<vertex x="52.6275" y="3.356" curve="-90"/>
<vertex x="53.4911" y="2.4924"/>
<vertex x="53.4911" y="2.1876"/>
<vertex x="53.4657" y="1.9971"/>
<vertex x="53.4022" y="1.8066"/>
<vertex x="53.2498" y="1.5907"/>
<vertex x="53.199" y="1.5653"/>
<vertex x="52.945" y="1.5653" curve="90"/>
<vertex x="52.8434" y="1.4637"/>
<vertex x="53.3641" y="1.4637" curve="-90"/>
<vertex x="53.4911" y="1.3367"/>
<vertex x="53.4911" y="0.4858" curve="-90"/>
<vertex x="53.3641" y="0.3588"/>
<vertex x="51.8655" y="0.3588" curve="-90"/>
<vertex x="51.7385" y="0.4858"/>
<vertex x="51.7385" y="1.3367" curve="-90"/>
<vertex x="51.8655" y="1.4637"/>
<vertex x="52.3862" y="1.4637" curve="90"/>
<vertex x="52.2846" y="1.5653"/>
<vertex x="52.0306" y="1.5653"/>
<vertex x="51.9798" y="1.5907"/>
<vertex x="51.8274" y="1.8066"/>
<vertex x="51.7385" y="2.1876"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="51.7385" y="2.4924" curve="-90"/>
<vertex x="52.6021" y="3.356"/>
<vertex x="52.6275" y="3.356" curve="-90"/>
<vertex x="53.4911" y="2.4924"/>
<vertex x="53.4911" y="2.1876"/>
<vertex x="53.4657" y="1.9971"/>
<vertex x="53.4022" y="1.8066"/>
<vertex x="53.2498" y="1.5907"/>
<vertex x="53.199" y="1.5653"/>
<vertex x="52.945" y="1.5653" curve="90"/>
<vertex x="52.8434" y="1.4637"/>
<vertex x="53.3641" y="1.4637" curve="-90"/>
<vertex x="53.4911" y="1.3367"/>
<vertex x="53.4911" y="0.4858" curve="-90"/>
<vertex x="53.3641" y="0.3588"/>
<vertex x="51.8655" y="0.3588" curve="-90"/>
<vertex x="51.7385" y="0.4858"/>
<vertex x="51.7385" y="1.3367" curve="-90"/>
<vertex x="51.8655" y="1.4637"/>
<vertex x="52.3862" y="1.4637" curve="90"/>
<vertex x="52.2846" y="1.5653"/>
<vertex x="52.0306" y="1.5653"/>
<vertex x="51.9798" y="1.5907"/>
<vertex x="51.8274" y="1.8066"/>
<vertex x="51.7385" y="2.1876"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="54.2785" y="2.4924" curve="-90"/>
<vertex x="55.1421" y="3.356"/>
<vertex x="55.1675" y="3.356" curve="-90"/>
<vertex x="56.0311" y="2.4924"/>
<vertex x="56.0311" y="2.1876"/>
<vertex x="56.0057" y="1.9971"/>
<vertex x="55.9422" y="1.8066"/>
<vertex x="55.7898" y="1.5907"/>
<vertex x="55.739" y="1.5653"/>
<vertex x="55.485" y="1.5653" curve="90"/>
<vertex x="55.3834" y="1.4637"/>
<vertex x="55.9041" y="1.4637" curve="-90"/>
<vertex x="56.0311" y="1.3367"/>
<vertex x="56.0311" y="0.4858" curve="-90"/>
<vertex x="55.9041" y="0.3588"/>
<vertex x="54.4055" y="0.3588" curve="-90"/>
<vertex x="54.2785" y="0.4858"/>
<vertex x="54.2785" y="1.3367" curve="-90"/>
<vertex x="54.4055" y="1.4637"/>
<vertex x="54.9262" y="1.4637" curve="90"/>
<vertex x="54.8246" y="1.5653"/>
<vertex x="54.5706" y="1.5653"/>
<vertex x="54.5198" y="1.5907"/>
<vertex x="54.3674" y="1.8066"/>
<vertex x="54.2785" y="2.1876"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="54.2785" y="2.4924" curve="-90"/>
<vertex x="55.1421" y="3.356"/>
<vertex x="55.1675" y="3.356" curve="-90"/>
<vertex x="56.0311" y="2.4924"/>
<vertex x="56.0311" y="2.1876"/>
<vertex x="56.0057" y="1.9971"/>
<vertex x="55.9422" y="1.8066"/>
<vertex x="55.7898" y="1.5907"/>
<vertex x="55.739" y="1.5653"/>
<vertex x="55.485" y="1.5653" curve="90"/>
<vertex x="55.3834" y="1.4637"/>
<vertex x="55.9041" y="1.4637" curve="-90"/>
<vertex x="56.0311" y="1.3367"/>
<vertex x="56.0311" y="0.4858" curve="-90"/>
<vertex x="55.9041" y="0.3588"/>
<vertex x="54.4055" y="0.3588" curve="-90"/>
<vertex x="54.2785" y="0.4858"/>
<vertex x="54.2785" y="1.3367" curve="-90"/>
<vertex x="54.4055" y="1.4637"/>
<vertex x="54.9262" y="1.4637" curve="90"/>
<vertex x="54.8246" y="1.5653"/>
<vertex x="54.5706" y="1.5653"/>
<vertex x="54.5198" y="1.5907"/>
<vertex x="54.3674" y="1.8066"/>
<vertex x="54.2785" y="2.1876"/>
</polygon>
<polygon width="0.0254" layer="29">
<vertex x="23.7985" y="2.2003"/>
<vertex x="23.7985" y="2.467" curve="-90"/>
<vertex x="24.6748" y="3.356" curve="-90"/>
<vertex x="25.5511" y="2.467"/>
<vertex x="25.5511" y="2.2003" curve="-90"/>
<vertex x="24.6748" y="1.324" curve="-90"/>
</polygon>
<polygon width="0.0254" layer="29">
<vertex x="26.3385" y="2.2003"/>
<vertex x="26.3385" y="2.467" curve="-90"/>
<vertex x="27.2148" y="3.356" curve="-90"/>
<vertex x="28.0911" y="2.467"/>
<vertex x="28.0911" y="2.2003" curve="-90"/>
<vertex x="27.2148" y="1.324" curve="-90"/>
</polygon>
<polygon width="0.0254" layer="29">
<vertex x="28.8785" y="2.2003"/>
<vertex x="28.8785" y="2.467" curve="-90"/>
<vertex x="29.7548" y="3.356" curve="-90"/>
<vertex x="30.6311" y="2.467"/>
<vertex x="30.6311" y="2.2003" curve="-90"/>
<vertex x="29.7548" y="1.324" curve="-90"/>
</polygon>
<polygon width="0.0254" layer="29">
<vertex x="31.4185" y="2.2003"/>
<vertex x="31.4185" y="2.467" curve="-90"/>
<vertex x="32.2948" y="3.356" curve="-90"/>
<vertex x="33.1711" y="2.467"/>
<vertex x="33.1711" y="2.2003" curve="-90"/>
<vertex x="32.2948" y="1.324" curve="-90"/>
</polygon>
<polygon width="0.0254" layer="29">
<vertex x="33.9585" y="2.2003"/>
<vertex x="33.9585" y="2.467" curve="-90"/>
<vertex x="34.8348" y="3.356" curve="-90"/>
<vertex x="35.7111" y="2.467"/>
<vertex x="35.7111" y="2.2003" curve="-90"/>
<vertex x="34.8348" y="1.324" curve="-90"/>
</polygon>
<polygon width="0.0254" layer="29">
<vertex x="36.4985" y="2.2003"/>
<vertex x="36.4985" y="2.467" curve="-90"/>
<vertex x="37.3748" y="3.356" curve="-90"/>
<vertex x="38.2511" y="2.467"/>
<vertex x="38.2511" y="2.2003" curve="-90"/>
<vertex x="37.3748" y="1.324" curve="-90"/>
</polygon>
<polygon width="0.0254" layer="29">
<vertex x="39.0385" y="2.2003"/>
<vertex x="39.0385" y="2.467" curve="-90"/>
<vertex x="39.9148" y="3.356" curve="-90"/>
<vertex x="40.7911" y="2.467"/>
<vertex x="40.7911" y="2.2003" curve="-90"/>
<vertex x="39.9148" y="1.324" curve="-90"/>
</polygon>
<polygon width="0.0254" layer="29">
<vertex x="41.5785" y="2.2003"/>
<vertex x="41.5785" y="2.467" curve="-90"/>
<vertex x="42.4548" y="3.356" curve="-90"/>
<vertex x="43.3311" y="2.467"/>
<vertex x="43.3311" y="2.2003" curve="-90"/>
<vertex x="42.4548" y="1.324" curve="-90"/>
</polygon>
<polygon width="0.0254" layer="29">
<vertex x="44.1185" y="2.2003"/>
<vertex x="44.1185" y="2.467" curve="-90"/>
<vertex x="44.9948" y="3.356" curve="-90"/>
<vertex x="45.8711" y="2.467"/>
<vertex x="45.8711" y="2.2003" curve="-90"/>
<vertex x="44.9948" y="1.324" curve="-90"/>
</polygon>
<polygon width="0.0254" layer="29">
<vertex x="46.6585" y="2.2003"/>
<vertex x="46.6585" y="2.467" curve="-90"/>
<vertex x="47.5348" y="3.356" curve="-90"/>
<vertex x="48.4111" y="2.467"/>
<vertex x="48.4111" y="2.2003" curve="-90"/>
<vertex x="47.5348" y="1.324" curve="-90"/>
</polygon>
<polygon width="0.0254" layer="29">
<vertex x="49.1985" y="2.2003"/>
<vertex x="49.1985" y="2.467" curve="-90"/>
<vertex x="50.0748" y="3.356" curve="-90"/>
<vertex x="50.9511" y="2.467"/>
<vertex x="50.9511" y="2.2003" curve="-90"/>
<vertex x="50.0748" y="1.324" curve="-90"/>
</polygon>
<polygon width="0.0254" layer="29">
<vertex x="51.7385" y="2.2003"/>
<vertex x="51.7385" y="2.467" curve="-90"/>
<vertex x="52.6148" y="3.356" curve="-90"/>
<vertex x="53.4911" y="2.467"/>
<vertex x="53.4911" y="2.2003" curve="-90"/>
<vertex x="52.6148" y="1.324" curve="-90"/>
</polygon>
<polygon width="0.0254" layer="29">
<vertex x="54.2785" y="2.2003"/>
<vertex x="54.2785" y="2.467" curve="-90"/>
<vertex x="55.1548" y="3.356" curve="-90"/>
<vertex x="56.0311" y="2.467"/>
<vertex x="56.0311" y="2.2003" curve="-90"/>
<vertex x="55.1548" y="1.324" curve="-90"/>
</polygon>
<polygon width="0.127" layer="16">
<vertex x="24.6748" y="3.229" curve="90"/>
<vertex x="23.9255" y="2.4797"/>
<vertex x="23.9255" y="2.1749"/>
<vertex x="23.9509" y="2.0225"/>
<vertex x="24.0017" y="1.8574"/>
<vertex x="24.116" y="1.6923"/>
<vertex x="24.3573" y="1.6923" curve="-90"/>
<vertex x="24.5732" y="1.4764"/>
<vertex x="24.5732" y="1.3494" curve="-90"/>
<vertex x="24.5605" y="1.3367"/>
<vertex x="23.9255" y="1.3367"/>
<vertex x="23.9255" y="0.4858"/>
<vertex x="25.4241" y="0.4858"/>
<vertex x="25.4241" y="1.3367"/>
<vertex x="24.7764" y="1.3367"/>
<vertex x="24.7764" y="1.4764" curve="-90"/>
<vertex x="24.9923" y="1.6923"/>
<vertex x="25.2336" y="1.6923"/>
<vertex x="25.3479" y="1.8574"/>
<vertex x="25.3987" y="2.0225"/>
<vertex x="25.4241" y="2.1749"/>
<vertex x="25.4241" y="2.4797" curve="90"/>
</polygon>
<polygon width="0.127" layer="16">
<vertex x="55.1548" y="3.229" curve="90"/>
<vertex x="54.4055" y="2.4797"/>
<vertex x="54.4055" y="2.1749"/>
<vertex x="54.4309" y="2.0225"/>
<vertex x="54.4817" y="1.8574"/>
<vertex x="54.596" y="1.6923"/>
<vertex x="54.8373" y="1.6923" curve="-90"/>
<vertex x="55.0532" y="1.4764"/>
<vertex x="55.0532" y="1.3494" curve="-90"/>
<vertex x="55.0405" y="1.3367"/>
<vertex x="54.4055" y="1.3367"/>
<vertex x="54.4055" y="0.4858"/>
<vertex x="55.9041" y="0.4858"/>
<vertex x="55.9041" y="1.3367"/>
<vertex x="55.2564" y="1.3367"/>
<vertex x="55.2564" y="1.4764" curve="-90"/>
<vertex x="55.4723" y="1.6923"/>
<vertex x="55.7136" y="1.6923"/>
<vertex x="55.8279" y="1.8574"/>
<vertex x="55.8787" y="2.0225"/>
<vertex x="55.9041" y="2.1749"/>
<vertex x="55.9041" y="2.4797" curve="90"/>
</polygon>
<polygon width="0.127" layer="16">
<vertex x="27.2148" y="3.229" curve="90"/>
<vertex x="26.4655" y="2.4797"/>
<vertex x="26.4655" y="2.1749"/>
<vertex x="26.4909" y="2.0225"/>
<vertex x="26.5417" y="1.8574"/>
<vertex x="26.656" y="1.6923"/>
<vertex x="26.8973" y="1.6923" curve="-90"/>
<vertex x="27.1132" y="1.4764"/>
<vertex x="27.1132" y="1.3494" curve="-90"/>
<vertex x="27.1005" y="1.3367"/>
<vertex x="26.4655" y="1.3367"/>
<vertex x="26.4655" y="0.4858"/>
<vertex x="27.9641" y="0.4858"/>
<vertex x="27.9641" y="1.3367"/>
<vertex x="27.3164" y="1.3367"/>
<vertex x="27.3164" y="1.4764" curve="-90"/>
<vertex x="27.5323" y="1.6923"/>
<vertex x="27.7736" y="1.6923"/>
<vertex x="27.8879" y="1.8574"/>
<vertex x="27.9387" y="2.0225"/>
<vertex x="27.9641" y="2.1749"/>
<vertex x="27.9641" y="2.4797" curve="90"/>
</polygon>
<polygon width="0.127" layer="16">
<vertex x="29.7548" y="3.229" curve="90"/>
<vertex x="29.0055" y="2.4797"/>
<vertex x="29.0055" y="2.1749"/>
<vertex x="29.0309" y="2.0225"/>
<vertex x="29.0817" y="1.8574"/>
<vertex x="29.196" y="1.6923"/>
<vertex x="29.4373" y="1.6923" curve="-90"/>
<vertex x="29.6532" y="1.4764"/>
<vertex x="29.6532" y="1.3494" curve="-90"/>
<vertex x="29.6405" y="1.3367"/>
<vertex x="29.0055" y="1.3367"/>
<vertex x="29.0055" y="0.4858"/>
<vertex x="30.5041" y="0.4858"/>
<vertex x="30.5041" y="1.3367"/>
<vertex x="29.8564" y="1.3367"/>
<vertex x="29.8564" y="1.4764" curve="-90"/>
<vertex x="30.0723" y="1.6923"/>
<vertex x="30.3136" y="1.6923"/>
<vertex x="30.4279" y="1.8574"/>
<vertex x="30.4787" y="2.0225"/>
<vertex x="30.5041" y="2.1749"/>
<vertex x="30.5041" y="2.4797" curve="90"/>
</polygon>
<polygon width="0.127" layer="16">
<vertex x="32.2948" y="3.229" curve="90"/>
<vertex x="31.5455" y="2.4797"/>
<vertex x="31.5455" y="2.1749"/>
<vertex x="31.5709" y="2.0225"/>
<vertex x="31.6217" y="1.8574"/>
<vertex x="31.736" y="1.6923"/>
<vertex x="31.9773" y="1.6923" curve="-90"/>
<vertex x="32.1932" y="1.4764"/>
<vertex x="32.1932" y="1.3494" curve="-90"/>
<vertex x="32.1805" y="1.3367"/>
<vertex x="31.5455" y="1.3367"/>
<vertex x="31.5455" y="0.4858"/>
<vertex x="33.0441" y="0.4858"/>
<vertex x="33.0441" y="1.3367"/>
<vertex x="32.3964" y="1.3367"/>
<vertex x="32.3964" y="1.4764" curve="-90"/>
<vertex x="32.6123" y="1.6923"/>
<vertex x="32.8536" y="1.6923"/>
<vertex x="32.9679" y="1.8574"/>
<vertex x="33.0187" y="2.0225"/>
<vertex x="33.0441" y="2.1749"/>
<vertex x="33.0441" y="2.4797" curve="90"/>
</polygon>
<polygon width="0.127" layer="16">
<vertex x="34.8348" y="3.229" curve="90"/>
<vertex x="34.0855" y="2.4797"/>
<vertex x="34.0855" y="2.1749"/>
<vertex x="34.1109" y="2.0225"/>
<vertex x="34.1617" y="1.8574"/>
<vertex x="34.276" y="1.6923"/>
<vertex x="34.5173" y="1.6923" curve="-90"/>
<vertex x="34.7332" y="1.4764"/>
<vertex x="34.7332" y="1.3494" curve="-90"/>
<vertex x="34.7205" y="1.3367"/>
<vertex x="34.0855" y="1.3367"/>
<vertex x="34.0855" y="0.4858"/>
<vertex x="35.5841" y="0.4858"/>
<vertex x="35.5841" y="1.3367"/>
<vertex x="34.9364" y="1.3367"/>
<vertex x="34.9364" y="1.4764" curve="-90"/>
<vertex x="35.1523" y="1.6923"/>
<vertex x="35.3936" y="1.6923"/>
<vertex x="35.5079" y="1.8574"/>
<vertex x="35.5587" y="2.0225"/>
<vertex x="35.5841" y="2.1749"/>
<vertex x="35.5841" y="2.4797" curve="90"/>
</polygon>
<polygon width="0.127" layer="16">
<vertex x="37.3748" y="3.229" curve="90"/>
<vertex x="36.6255" y="2.4797"/>
<vertex x="36.6255" y="2.1749"/>
<vertex x="36.6509" y="2.0225"/>
<vertex x="36.7017" y="1.8574"/>
<vertex x="36.816" y="1.6923"/>
<vertex x="37.0573" y="1.6923" curve="-90"/>
<vertex x="37.2732" y="1.4764"/>
<vertex x="37.2732" y="1.3494" curve="-90"/>
<vertex x="37.2605" y="1.3367"/>
<vertex x="36.6255" y="1.3367"/>
<vertex x="36.6255" y="0.4858"/>
<vertex x="38.1241" y="0.4858"/>
<vertex x="38.1241" y="1.3367"/>
<vertex x="37.4764" y="1.3367"/>
<vertex x="37.4764" y="1.4764" curve="-90"/>
<vertex x="37.6923" y="1.6923"/>
<vertex x="37.9336" y="1.6923"/>
<vertex x="38.0479" y="1.8574"/>
<vertex x="38.0987" y="2.0225"/>
<vertex x="38.1241" y="2.1749"/>
<vertex x="38.1241" y="2.4797" curve="90"/>
</polygon>
<polygon width="0.127" layer="16">
<vertex x="39.9148" y="3.229" curve="90"/>
<vertex x="39.1655" y="2.4797"/>
<vertex x="39.1655" y="2.1749"/>
<vertex x="39.1909" y="2.0225"/>
<vertex x="39.2417" y="1.8574"/>
<vertex x="39.356" y="1.6923"/>
<vertex x="39.5973" y="1.6923" curve="-90"/>
<vertex x="39.8132" y="1.4764"/>
<vertex x="39.8132" y="1.3494" curve="-90"/>
<vertex x="39.8005" y="1.3367"/>
<vertex x="39.1655" y="1.3367"/>
<vertex x="39.1655" y="0.4858"/>
<vertex x="40.6641" y="0.4858"/>
<vertex x="40.6641" y="1.3367"/>
<vertex x="40.0164" y="1.3367"/>
<vertex x="40.0164" y="1.4764" curve="-90"/>
<vertex x="40.2323" y="1.6923"/>
<vertex x="40.4736" y="1.6923"/>
<vertex x="40.5879" y="1.8574"/>
<vertex x="40.6387" y="2.0225"/>
<vertex x="40.6641" y="2.1749"/>
<vertex x="40.6641" y="2.4797" curve="90"/>
</polygon>
<polygon width="0.127" layer="16">
<vertex x="42.4548" y="3.229" curve="90"/>
<vertex x="41.7055" y="2.4797"/>
<vertex x="41.7055" y="2.1749"/>
<vertex x="41.7309" y="2.0225"/>
<vertex x="41.7817" y="1.8574"/>
<vertex x="41.896" y="1.6923"/>
<vertex x="42.1373" y="1.6923" curve="-90"/>
<vertex x="42.3532" y="1.4764"/>
<vertex x="42.3532" y="1.3494" curve="-90"/>
<vertex x="42.3405" y="1.3367"/>
<vertex x="41.7055" y="1.3367"/>
<vertex x="41.7055" y="0.4858"/>
<vertex x="43.2041" y="0.4858"/>
<vertex x="43.2041" y="1.3367"/>
<vertex x="42.5564" y="1.3367"/>
<vertex x="42.5564" y="1.4764" curve="-90"/>
<vertex x="42.7723" y="1.6923"/>
<vertex x="43.0136" y="1.6923"/>
<vertex x="43.1279" y="1.8574"/>
<vertex x="43.1787" y="2.0225"/>
<vertex x="43.2041" y="2.1749"/>
<vertex x="43.2041" y="2.4797" curve="90"/>
</polygon>
<polygon width="0.127" layer="16">
<vertex x="47.5348" y="3.229" curve="90"/>
<vertex x="46.7855" y="2.4797"/>
<vertex x="46.7855" y="2.1749"/>
<vertex x="46.8109" y="2.0225"/>
<vertex x="46.8617" y="1.8574"/>
<vertex x="46.976" y="1.6923"/>
<vertex x="47.2173" y="1.6923" curve="-90"/>
<vertex x="47.4332" y="1.4764"/>
<vertex x="47.4332" y="1.3494" curve="-90"/>
<vertex x="47.4205" y="1.3367"/>
<vertex x="46.7855" y="1.3367"/>
<vertex x="46.7855" y="0.4858"/>
<vertex x="48.2841" y="0.4858"/>
<vertex x="48.2841" y="1.3367"/>
<vertex x="47.6364" y="1.3367"/>
<vertex x="47.6364" y="1.4764" curve="-90"/>
<vertex x="47.8523" y="1.6923"/>
<vertex x="48.0936" y="1.6923"/>
<vertex x="48.2079" y="1.8574"/>
<vertex x="48.2587" y="2.0225"/>
<vertex x="48.2841" y="2.1749"/>
<vertex x="48.2841" y="2.4797" curve="90"/>
</polygon>
<polygon width="0.127" layer="16">
<vertex x="50.0748" y="3.229" curve="90"/>
<vertex x="49.3255" y="2.4797"/>
<vertex x="49.3255" y="2.1749"/>
<vertex x="49.3509" y="2.0225"/>
<vertex x="49.4017" y="1.8574"/>
<vertex x="49.516" y="1.6923"/>
<vertex x="49.7573" y="1.6923" curve="-90"/>
<vertex x="49.9732" y="1.4764"/>
<vertex x="49.9732" y="1.3494" curve="-90"/>
<vertex x="49.9605" y="1.3367"/>
<vertex x="49.3255" y="1.3367"/>
<vertex x="49.3255" y="0.4858"/>
<vertex x="50.8241" y="0.4858"/>
<vertex x="50.8241" y="1.3367"/>
<vertex x="50.1764" y="1.3367"/>
<vertex x="50.1764" y="1.4764" curve="-90"/>
<vertex x="50.3923" y="1.6923"/>
<vertex x="50.6336" y="1.6923"/>
<vertex x="50.7479" y="1.8574"/>
<vertex x="50.7987" y="2.0225"/>
<vertex x="50.8241" y="2.1749"/>
<vertex x="50.8241" y="2.4797" curve="90"/>
</polygon>
<polygon width="0.127" layer="16">
<vertex x="52.6148" y="3.229" curve="90"/>
<vertex x="51.8655" y="2.4797"/>
<vertex x="51.8655" y="2.1749"/>
<vertex x="51.8909" y="2.0225"/>
<vertex x="51.9417" y="1.8574"/>
<vertex x="52.056" y="1.6923"/>
<vertex x="52.2973" y="1.6923" curve="-90"/>
<vertex x="52.5132" y="1.4764"/>
<vertex x="52.5132" y="1.3494" curve="-90"/>
<vertex x="52.5005" y="1.3367"/>
<vertex x="51.8655" y="1.3367"/>
<vertex x="51.8655" y="0.4858"/>
<vertex x="53.3641" y="0.4858"/>
<vertex x="53.3641" y="1.3367"/>
<vertex x="52.7164" y="1.3367"/>
<vertex x="52.7164" y="1.4764" curve="-90"/>
<vertex x="52.9323" y="1.6923"/>
<vertex x="53.1736" y="1.6923"/>
<vertex x="53.2879" y="1.8574"/>
<vertex x="53.3387" y="2.0225"/>
<vertex x="53.3641" y="2.1749"/>
<vertex x="53.3641" y="2.4797" curve="90"/>
</polygon>
<polygon width="0.127" layer="16">
<vertex x="44.9948" y="3.229" curve="90"/>
<vertex x="44.2455" y="2.4797"/>
<vertex x="44.2455" y="2.1749"/>
<vertex x="44.2709" y="2.0225"/>
<vertex x="44.3217" y="1.8574"/>
<vertex x="44.436" y="1.6923"/>
<vertex x="44.6773" y="1.6923" curve="-90"/>
<vertex x="44.8932" y="1.4764"/>
<vertex x="44.8932" y="1.3494" curve="-90"/>
<vertex x="44.8805" y="1.3367"/>
<vertex x="44.2455" y="1.3367"/>
<vertex x="44.2455" y="0.4858"/>
<vertex x="45.7441" y="0.4858"/>
<vertex x="45.7441" y="1.3367"/>
<vertex x="45.0964" y="1.3367"/>
<vertex x="45.0964" y="1.4764" curve="-90"/>
<vertex x="45.3123" y="1.6923"/>
<vertex x="45.5536" y="1.6923"/>
<vertex x="45.6679" y="1.8574"/>
<vertex x="45.7187" y="2.0225"/>
<vertex x="45.7441" y="2.1749"/>
<vertex x="45.7441" y="2.4797" curve="90"/>
</polygon>
<polygon width="0.127" layer="16">
<vertex x="21.3855" y="3.2417"/>
<vertex x="21.3855" y="1.6923"/>
<vertex x="21.8173" y="1.6923" curve="-90"/>
<vertex x="22.0332" y="1.4764"/>
<vertex x="22.0332" y="1.3494" curve="-90"/>
<vertex x="22.0205" y="1.3367"/>
<vertex x="21.3855" y="1.3367"/>
<vertex x="21.3855" y="0.4858"/>
<vertex x="22.8841" y="0.4858"/>
<vertex x="22.8841" y="1.3367"/>
<vertex x="22.2364" y="1.3367"/>
<vertex x="22.2364" y="1.4764" curve="-90"/>
<vertex x="22.4523" y="1.6923"/>
<vertex x="22.8841" y="1.6923"/>
<vertex x="22.8841" y="3.2417"/>
</polygon>
<polygon width="0.127" layer="1">
<vertex x="24.6748" y="3.229" curve="90"/>
<vertex x="23.9255" y="2.4797"/>
<vertex x="23.9255" y="2.1368" curve="90"/>
<vertex x="24.6748" y="1.3875" curve="90"/>
<vertex x="25.4241" y="2.1368"/>
<vertex x="25.4241" y="2.4797" curve="90"/>
</polygon>
<polygon width="0.127" layer="1">
<vertex x="22.8841" y="3.229"/>
<vertex x="21.3855" y="3.229"/>
<vertex x="21.3855" y="1.4383"/>
<vertex x="22.8841" y="1.4383"/>
</polygon>
<polygon width="0.127" layer="1">
<vertex x="27.2148" y="3.229" curve="90"/>
<vertex x="26.4655" y="2.4797"/>
<vertex x="26.4655" y="2.1368" curve="90"/>
<vertex x="27.2148" y="1.3875" curve="90"/>
<vertex x="27.9641" y="2.1368"/>
<vertex x="27.9641" y="2.4797" curve="90"/>
</polygon>
<polygon width="0.127" layer="1">
<vertex x="29.7548" y="3.229" curve="90"/>
<vertex x="29.0055" y="2.4797"/>
<vertex x="29.0055" y="2.1368" curve="90"/>
<vertex x="29.7548" y="1.3875" curve="90"/>
<vertex x="30.5041" y="2.1368"/>
<vertex x="30.5041" y="2.4797" curve="90"/>
</polygon>
<polygon width="0.127" layer="1">
<vertex x="32.2948" y="3.229" curve="90"/>
<vertex x="31.5455" y="2.4797"/>
<vertex x="31.5455" y="2.1368" curve="90"/>
<vertex x="32.2948" y="1.3875" curve="90"/>
<vertex x="33.0441" y="2.1368"/>
<vertex x="33.0441" y="2.4797" curve="90"/>
</polygon>
<polygon width="0.127" layer="1">
<vertex x="34.8348" y="3.229" curve="90"/>
<vertex x="34.0855" y="2.4797"/>
<vertex x="34.0855" y="2.1368" curve="90"/>
<vertex x="34.8348" y="1.3875" curve="90"/>
<vertex x="35.5841" y="2.1368"/>
<vertex x="35.5841" y="2.4797" curve="90"/>
</polygon>
<polygon width="0.127" layer="1">
<vertex x="37.3748" y="3.229" curve="90"/>
<vertex x="36.6255" y="2.4797"/>
<vertex x="36.6255" y="2.1368" curve="90"/>
<vertex x="37.3748" y="1.3875" curve="90"/>
<vertex x="38.1241" y="2.1368"/>
<vertex x="38.1241" y="2.4797" curve="90"/>
</polygon>
<polygon width="0.127" layer="1">
<vertex x="39.9148" y="3.229" curve="90"/>
<vertex x="39.1655" y="2.4797"/>
<vertex x="39.1655" y="2.1368" curve="90"/>
<vertex x="39.9148" y="1.3875" curve="90"/>
<vertex x="40.6641" y="2.1368"/>
<vertex x="40.6641" y="2.4797" curve="90"/>
</polygon>
<polygon width="0.127" layer="1">
<vertex x="42.4548" y="3.229" curve="90"/>
<vertex x="41.7055" y="2.4797"/>
<vertex x="41.7055" y="2.1368" curve="90"/>
<vertex x="42.4548" y="1.3875" curve="90"/>
<vertex x="43.2041" y="2.1368"/>
<vertex x="43.2041" y="2.4797" curve="90"/>
</polygon>
<polygon width="0.127" layer="1">
<vertex x="44.9948" y="3.229" curve="90"/>
<vertex x="44.2455" y="2.4797"/>
<vertex x="44.2455" y="2.1368" curve="90"/>
<vertex x="44.9948" y="1.3875" curve="90"/>
<vertex x="45.7441" y="2.1368"/>
<vertex x="45.7441" y="2.4797" curve="90"/>
</polygon>
<polygon width="0.127" layer="1">
<vertex x="47.5348" y="3.229" curve="90"/>
<vertex x="46.7855" y="2.4797"/>
<vertex x="46.7855" y="2.1368" curve="90"/>
<vertex x="47.5348" y="1.3875" curve="90"/>
<vertex x="48.2841" y="2.1368"/>
<vertex x="48.2841" y="2.4797" curve="90"/>
</polygon>
<polygon width="0.127" layer="1">
<vertex x="50.0748" y="3.229" curve="90"/>
<vertex x="49.3255" y="2.4797"/>
<vertex x="49.3255" y="2.1368" curve="90"/>
<vertex x="50.0748" y="1.3875" curve="90"/>
<vertex x="50.8241" y="2.1368"/>
<vertex x="50.8241" y="2.4797" curve="90"/>
</polygon>
<polygon width="0.127" layer="1">
<vertex x="52.6148" y="3.229" curve="90"/>
<vertex x="51.8655" y="2.4797"/>
<vertex x="51.8655" y="2.1368" curve="90"/>
<vertex x="52.6148" y="1.3875" curve="90"/>
<vertex x="53.3641" y="2.1368"/>
<vertex x="53.3641" y="2.4797" curve="90"/>
</polygon>
<polygon width="0.127" layer="1">
<vertex x="55.1548" y="3.229" curve="90"/>
<vertex x="54.4055" y="2.4797"/>
<vertex x="54.4055" y="2.1368" curve="90"/>
<vertex x="55.1548" y="1.3875" curve="90"/>
<vertex x="55.9041" y="2.1368"/>
<vertex x="55.9041" y="2.4797" curve="90"/>
</polygon>
<wire x1="55.1652" y1="22.97" x2="55.1652" y2="22.27" width="0.05" layer="39"/>
<pad name="6" x="55.1652" y="22.62" drill="1" rot="R180" first="yes"/>
<pad name="7" x="52.6252" y="22.62" drill="1" rot="R180"/>
<pad name="8" x="50.0852" y="22.62" drill="1" rot="R180"/>
<pad name="9" x="47.5452" y="22.62" drill="1" rot="R180"/>
<pad name="10" x="45.0052" y="22.62" drill="1" rot="R180"/>
<pad name="11" x="42.4652" y="22.62" drill="1" rot="R180"/>
<pad name="12" x="39.9252" y="22.62" drill="1" rot="R180"/>
<pad name="13" x="37.3852" y="22.62" drill="1" rot="R180"/>
<pad name="14" x="34.8452" y="22.62" drill="1" rot="R180"/>
<pad name="RST" x="32.3052" y="22.62" drill="1" rot="R180"/>
<pad name="GND" x="29.7652" y="22.62" drill="1" rot="R180"/>
<pad name="VCC" x="27.2252" y="22.62" drill="1" rot="R180"/>
<pad name="5V" x="24.6852" y="22.62" drill="1" rot="R180"/>
<pad name="VIN" x="22.1452" y="22.62" drill="1" rot="R180"/>
<polygon width="0.0254" layer="29">
<vertex x="56.0415" y="21.5913"/>
<vertex x="54.2889" y="21.5913"/>
<vertex x="54.2889" y="23.6487"/>
<vertex x="56.0415" y="23.6487"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="56.0415" y="21.5913"/>
<vertex x="56.0415" y="23.3947"/>
<vertex x="55.4954" y="23.3947" curve="-90"/>
<vertex x="55.3938" y="23.4963"/>
<vertex x="55.9145" y="23.4963" curve="90"/>
<vertex x="56.0415" y="23.6233"/>
<vertex x="56.0415" y="24.4742" curve="90"/>
<vertex x="55.9145" y="24.6012"/>
<vertex x="54.4159" y="24.6012" curve="90"/>
<vertex x="54.2889" y="24.4742"/>
<vertex x="54.2889" y="23.6233" curve="90"/>
<vertex x="54.4159" y="23.4963"/>
<vertex x="54.9366" y="23.4963" curve="-90"/>
<vertex x="54.835" y="23.3947"/>
<vertex x="54.2889" y="23.3947"/>
<vertex x="54.2889" y="21.5913"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="53.5015" y="22.4676" curve="-90"/>
<vertex x="52.6379" y="21.604"/>
<vertex x="52.6125" y="21.604" curve="-90"/>
<vertex x="51.7489" y="22.4676"/>
<vertex x="51.7489" y="22.7724"/>
<vertex x="51.7743" y="22.9629"/>
<vertex x="51.8378" y="23.1534"/>
<vertex x="51.9902" y="23.3693"/>
<vertex x="52.041" y="23.3947"/>
<vertex x="52.295" y="23.3947" curve="90"/>
<vertex x="52.3966" y="23.4963"/>
<vertex x="51.8759" y="23.4963" curve="-90"/>
<vertex x="51.7489" y="23.6233"/>
<vertex x="51.7489" y="24.4742" curve="-90"/>
<vertex x="51.8759" y="24.6012"/>
<vertex x="53.3745" y="24.6012" curve="-90"/>
<vertex x="53.5015" y="24.4742"/>
<vertex x="53.5015" y="23.6233" curve="-90"/>
<vertex x="53.3745" y="23.4963"/>
<vertex x="52.8538" y="23.4963" curve="90"/>
<vertex x="52.9554" y="23.3947"/>
<vertex x="53.2094" y="23.3947"/>
<vertex x="53.2602" y="23.3693"/>
<vertex x="53.4126" y="23.1534"/>
<vertex x="53.5015" y="22.7724"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="53.5015" y="22.4676" curve="-90"/>
<vertex x="52.6379" y="21.604"/>
<vertex x="52.6125" y="21.604" curve="-90"/>
<vertex x="51.7489" y="22.4676"/>
<vertex x="51.7489" y="22.7724"/>
<vertex x="51.7743" y="22.9629"/>
<vertex x="51.8378" y="23.1534"/>
<vertex x="51.9902" y="23.3693"/>
<vertex x="52.041" y="23.3947"/>
<vertex x="52.295" y="23.3947" curve="90"/>
<vertex x="52.3966" y="23.4963"/>
<vertex x="51.8759" y="23.4963" curve="-90"/>
<vertex x="51.7489" y="23.6233"/>
<vertex x="51.7489" y="24.4742" curve="-90"/>
<vertex x="51.8759" y="24.6012"/>
<vertex x="53.3745" y="24.6012" curve="-90"/>
<vertex x="53.5015" y="24.4742"/>
<vertex x="53.5015" y="23.6233" curve="-90"/>
<vertex x="53.3745" y="23.4963"/>
<vertex x="52.8538" y="23.4963" curve="90"/>
<vertex x="52.9554" y="23.3947"/>
<vertex x="53.2094" y="23.3947"/>
<vertex x="53.2602" y="23.3693"/>
<vertex x="53.4126" y="23.1534"/>
<vertex x="53.5015" y="22.7724"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="50.9615" y="22.4676" curve="-90"/>
<vertex x="50.0979" y="21.604"/>
<vertex x="50.0725" y="21.604" curve="-90"/>
<vertex x="49.2089" y="22.4676"/>
<vertex x="49.2089" y="22.7724"/>
<vertex x="49.2343" y="22.9629"/>
<vertex x="49.2978" y="23.1534"/>
<vertex x="49.4502" y="23.3693"/>
<vertex x="49.501" y="23.3947"/>
<vertex x="49.755" y="23.3947" curve="90"/>
<vertex x="49.8566" y="23.4963"/>
<vertex x="49.3359" y="23.4963" curve="-90"/>
<vertex x="49.2089" y="23.6233"/>
<vertex x="49.2089" y="24.4742" curve="-90"/>
<vertex x="49.3359" y="24.6012"/>
<vertex x="50.8345" y="24.6012" curve="-90"/>
<vertex x="50.9615" y="24.4742"/>
<vertex x="50.9615" y="23.6233" curve="-90"/>
<vertex x="50.8345" y="23.4963"/>
<vertex x="50.3138" y="23.4963" curve="90"/>
<vertex x="50.4154" y="23.3947"/>
<vertex x="50.6694" y="23.3947"/>
<vertex x="50.7202" y="23.3693"/>
<vertex x="50.8726" y="23.1534"/>
<vertex x="50.9615" y="22.7724"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="50.9615" y="22.4676" curve="-90"/>
<vertex x="50.0979" y="21.604"/>
<vertex x="50.0725" y="21.604" curve="-90"/>
<vertex x="49.2089" y="22.4676"/>
<vertex x="49.2089" y="22.7724"/>
<vertex x="49.2343" y="22.9629"/>
<vertex x="49.2978" y="23.1534"/>
<vertex x="49.4502" y="23.3693"/>
<vertex x="49.501" y="23.3947"/>
<vertex x="49.755" y="23.3947" curve="90"/>
<vertex x="49.8566" y="23.4963"/>
<vertex x="49.3359" y="23.4963" curve="-90"/>
<vertex x="49.2089" y="23.6233"/>
<vertex x="49.2089" y="24.4742" curve="-90"/>
<vertex x="49.3359" y="24.6012"/>
<vertex x="50.8345" y="24.6012" curve="-90"/>
<vertex x="50.9615" y="24.4742"/>
<vertex x="50.9615" y="23.6233" curve="-90"/>
<vertex x="50.8345" y="23.4963"/>
<vertex x="50.3138" y="23.4963" curve="90"/>
<vertex x="50.4154" y="23.3947"/>
<vertex x="50.6694" y="23.3947"/>
<vertex x="50.7202" y="23.3693"/>
<vertex x="50.8726" y="23.1534"/>
<vertex x="50.9615" y="22.7724"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="48.4215" y="22.4676" curve="-90"/>
<vertex x="47.5579" y="21.604"/>
<vertex x="47.5325" y="21.604" curve="-90"/>
<vertex x="46.6689" y="22.4676"/>
<vertex x="46.6689" y="22.7724"/>
<vertex x="46.6943" y="22.9629"/>
<vertex x="46.7578" y="23.1534"/>
<vertex x="46.9102" y="23.3693"/>
<vertex x="46.961" y="23.3947"/>
<vertex x="47.215" y="23.3947" curve="90"/>
<vertex x="47.3166" y="23.4963"/>
<vertex x="46.7959" y="23.4963" curve="-90"/>
<vertex x="46.6689" y="23.6233"/>
<vertex x="46.6689" y="24.4742" curve="-90"/>
<vertex x="46.7959" y="24.6012"/>
<vertex x="48.2945" y="24.6012" curve="-90"/>
<vertex x="48.4215" y="24.4742"/>
<vertex x="48.4215" y="23.6233" curve="-90"/>
<vertex x="48.2945" y="23.4963"/>
<vertex x="47.7738" y="23.4963" curve="90"/>
<vertex x="47.8754" y="23.3947"/>
<vertex x="48.1294" y="23.3947"/>
<vertex x="48.1802" y="23.3693"/>
<vertex x="48.3326" y="23.1534"/>
<vertex x="48.4215" y="22.7724"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="48.4215" y="22.4676" curve="-90"/>
<vertex x="47.5579" y="21.604"/>
<vertex x="47.5325" y="21.604" curve="-90"/>
<vertex x="46.6689" y="22.4676"/>
<vertex x="46.6689" y="22.7724"/>
<vertex x="46.6943" y="22.9629"/>
<vertex x="46.7578" y="23.1534"/>
<vertex x="46.9102" y="23.3693"/>
<vertex x="46.961" y="23.3947"/>
<vertex x="47.215" y="23.3947" curve="90"/>
<vertex x="47.3166" y="23.4963"/>
<vertex x="46.7959" y="23.4963" curve="-90"/>
<vertex x="46.6689" y="23.6233"/>
<vertex x="46.6689" y="24.4742" curve="-90"/>
<vertex x="46.7959" y="24.6012"/>
<vertex x="48.2945" y="24.6012" curve="-90"/>
<vertex x="48.4215" y="24.4742"/>
<vertex x="48.4215" y="23.6233" curve="-90"/>
<vertex x="48.2945" y="23.4963"/>
<vertex x="47.7738" y="23.4963" curve="90"/>
<vertex x="47.8754" y="23.3947"/>
<vertex x="48.1294" y="23.3947"/>
<vertex x="48.1802" y="23.3693"/>
<vertex x="48.3326" y="23.1534"/>
<vertex x="48.4215" y="22.7724"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="45.8815" y="22.4676" curve="-90"/>
<vertex x="45.0179" y="21.604"/>
<vertex x="44.9925" y="21.604" curve="-90"/>
<vertex x="44.1289" y="22.4676"/>
<vertex x="44.1289" y="22.7724"/>
<vertex x="44.1543" y="22.9629"/>
<vertex x="44.2178" y="23.1534"/>
<vertex x="44.3702" y="23.3693"/>
<vertex x="44.421" y="23.3947"/>
<vertex x="44.675" y="23.3947" curve="90"/>
<vertex x="44.7766" y="23.4963"/>
<vertex x="44.2559" y="23.4963" curve="-90"/>
<vertex x="44.1289" y="23.6233"/>
<vertex x="44.1289" y="24.4742" curve="-90"/>
<vertex x="44.2559" y="24.6012"/>
<vertex x="45.7545" y="24.6012" curve="-90"/>
<vertex x="45.8815" y="24.4742"/>
<vertex x="45.8815" y="23.6233" curve="-90"/>
<vertex x="45.7545" y="23.4963"/>
<vertex x="45.2338" y="23.4963" curve="90"/>
<vertex x="45.3354" y="23.3947"/>
<vertex x="45.5894" y="23.3947"/>
<vertex x="45.6402" y="23.3693"/>
<vertex x="45.7926" y="23.1534"/>
<vertex x="45.8815" y="22.7724"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="45.8815" y="22.4676" curve="-90"/>
<vertex x="45.0179" y="21.604"/>
<vertex x="44.9925" y="21.604" curve="-90"/>
<vertex x="44.1289" y="22.4676"/>
<vertex x="44.1289" y="22.7724"/>
<vertex x="44.1543" y="22.9629"/>
<vertex x="44.2178" y="23.1534"/>
<vertex x="44.3702" y="23.3693"/>
<vertex x="44.421" y="23.3947"/>
<vertex x="44.675" y="23.3947" curve="90"/>
<vertex x="44.7766" y="23.4963"/>
<vertex x="44.2559" y="23.4963" curve="-90"/>
<vertex x="44.1289" y="23.6233"/>
<vertex x="44.1289" y="24.4742" curve="-90"/>
<vertex x="44.2559" y="24.6012"/>
<vertex x="45.7545" y="24.6012" curve="-90"/>
<vertex x="45.8815" y="24.4742"/>
<vertex x="45.8815" y="23.6233" curve="-90"/>
<vertex x="45.7545" y="23.4963"/>
<vertex x="45.2338" y="23.4963" curve="90"/>
<vertex x="45.3354" y="23.3947"/>
<vertex x="45.5894" y="23.3947"/>
<vertex x="45.6402" y="23.3693"/>
<vertex x="45.7926" y="23.1534"/>
<vertex x="45.8815" y="22.7724"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="43.3415" y="22.4676" curve="-90"/>
<vertex x="42.4779" y="21.604"/>
<vertex x="42.4525" y="21.604" curve="-90"/>
<vertex x="41.5889" y="22.4676"/>
<vertex x="41.5889" y="22.7724"/>
<vertex x="41.6143" y="22.9629"/>
<vertex x="41.6778" y="23.1534"/>
<vertex x="41.8302" y="23.3693"/>
<vertex x="41.881" y="23.3947"/>
<vertex x="42.135" y="23.3947" curve="90"/>
<vertex x="42.2366" y="23.4963"/>
<vertex x="41.7159" y="23.4963" curve="-90"/>
<vertex x="41.5889" y="23.6233"/>
<vertex x="41.5889" y="24.4742" curve="-90"/>
<vertex x="41.7159" y="24.6012"/>
<vertex x="43.2145" y="24.6012" curve="-90"/>
<vertex x="43.3415" y="24.4742"/>
<vertex x="43.3415" y="23.6233" curve="-90"/>
<vertex x="43.2145" y="23.4963"/>
<vertex x="42.6938" y="23.4963" curve="90"/>
<vertex x="42.7954" y="23.3947"/>
<vertex x="43.0494" y="23.3947"/>
<vertex x="43.1002" y="23.3693"/>
<vertex x="43.2526" y="23.1534"/>
<vertex x="43.3415" y="22.7724"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="43.3415" y="22.4676" curve="-90"/>
<vertex x="42.4779" y="21.604"/>
<vertex x="42.4525" y="21.604" curve="-90"/>
<vertex x="41.5889" y="22.4676"/>
<vertex x="41.5889" y="22.7724"/>
<vertex x="41.6143" y="22.9629"/>
<vertex x="41.6778" y="23.1534"/>
<vertex x="41.8302" y="23.3693"/>
<vertex x="41.881" y="23.3947"/>
<vertex x="42.135" y="23.3947" curve="90"/>
<vertex x="42.2366" y="23.4963"/>
<vertex x="41.7159" y="23.4963" curve="-90"/>
<vertex x="41.5889" y="23.6233"/>
<vertex x="41.5889" y="24.4742" curve="-90"/>
<vertex x="41.7159" y="24.6012"/>
<vertex x="43.2145" y="24.6012" curve="-90"/>
<vertex x="43.3415" y="24.4742"/>
<vertex x="43.3415" y="23.6233" curve="-90"/>
<vertex x="43.2145" y="23.4963"/>
<vertex x="42.6938" y="23.4963" curve="90"/>
<vertex x="42.7954" y="23.3947"/>
<vertex x="43.0494" y="23.3947"/>
<vertex x="43.1002" y="23.3693"/>
<vertex x="43.2526" y="23.1534"/>
<vertex x="43.3415" y="22.7724"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="40.8015" y="22.4676" curve="-90"/>
<vertex x="39.9379" y="21.604"/>
<vertex x="39.9125" y="21.604" curve="-90"/>
<vertex x="39.0489" y="22.4676"/>
<vertex x="39.0489" y="22.7724"/>
<vertex x="39.0743" y="22.9629"/>
<vertex x="39.1378" y="23.1534"/>
<vertex x="39.2902" y="23.3693"/>
<vertex x="39.341" y="23.3947"/>
<vertex x="39.595" y="23.3947" curve="90"/>
<vertex x="39.6966" y="23.4963"/>
<vertex x="39.1759" y="23.4963" curve="-90"/>
<vertex x="39.0489" y="23.6233"/>
<vertex x="39.0489" y="24.4742" curve="-90"/>
<vertex x="39.1759" y="24.6012"/>
<vertex x="40.6745" y="24.6012" curve="-90"/>
<vertex x="40.8015" y="24.4742"/>
<vertex x="40.8015" y="23.6233" curve="-90"/>
<vertex x="40.6745" y="23.4963"/>
<vertex x="40.1538" y="23.4963" curve="90"/>
<vertex x="40.2554" y="23.3947"/>
<vertex x="40.5094" y="23.3947"/>
<vertex x="40.5602" y="23.3693"/>
<vertex x="40.7126" y="23.1534"/>
<vertex x="40.8015" y="22.7724"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="40.8015" y="22.4676" curve="-90"/>
<vertex x="39.9379" y="21.604"/>
<vertex x="39.9125" y="21.604" curve="-90"/>
<vertex x="39.0489" y="22.4676"/>
<vertex x="39.0489" y="22.7724"/>
<vertex x="39.0743" y="22.9629"/>
<vertex x="39.1378" y="23.1534"/>
<vertex x="39.2902" y="23.3693"/>
<vertex x="39.341" y="23.3947"/>
<vertex x="39.595" y="23.3947" curve="90"/>
<vertex x="39.6966" y="23.4963"/>
<vertex x="39.1759" y="23.4963" curve="-90"/>
<vertex x="39.0489" y="23.6233"/>
<vertex x="39.0489" y="24.4742" curve="-90"/>
<vertex x="39.1759" y="24.6012"/>
<vertex x="40.6745" y="24.6012" curve="-90"/>
<vertex x="40.8015" y="24.4742"/>
<vertex x="40.8015" y="23.6233" curve="-90"/>
<vertex x="40.6745" y="23.4963"/>
<vertex x="40.1538" y="23.4963" curve="90"/>
<vertex x="40.2554" y="23.3947"/>
<vertex x="40.5094" y="23.3947"/>
<vertex x="40.5602" y="23.3693"/>
<vertex x="40.7126" y="23.1534"/>
<vertex x="40.8015" y="22.7724"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="38.2615" y="22.4676" curve="-90"/>
<vertex x="37.3979" y="21.604"/>
<vertex x="37.3725" y="21.604" curve="-90"/>
<vertex x="36.5089" y="22.4676"/>
<vertex x="36.5089" y="22.7724"/>
<vertex x="36.5343" y="22.9629"/>
<vertex x="36.5978" y="23.1534"/>
<vertex x="36.7502" y="23.3693"/>
<vertex x="36.801" y="23.3947"/>
<vertex x="37.055" y="23.3947" curve="90"/>
<vertex x="37.1566" y="23.4963"/>
<vertex x="36.6359" y="23.4963" curve="-90"/>
<vertex x="36.5089" y="23.6233"/>
<vertex x="36.5089" y="24.4742" curve="-90"/>
<vertex x="36.6359" y="24.6012"/>
<vertex x="38.1345" y="24.6012" curve="-90"/>
<vertex x="38.2615" y="24.4742"/>
<vertex x="38.2615" y="23.6233" curve="-90"/>
<vertex x="38.1345" y="23.4963"/>
<vertex x="37.6138" y="23.4963" curve="90"/>
<vertex x="37.7154" y="23.3947"/>
<vertex x="37.9694" y="23.3947"/>
<vertex x="38.0202" y="23.3693"/>
<vertex x="38.1726" y="23.1534"/>
<vertex x="38.2615" y="22.7724"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="38.2615" y="22.4676" curve="-90"/>
<vertex x="37.3979" y="21.604"/>
<vertex x="37.3725" y="21.604" curve="-90"/>
<vertex x="36.5089" y="22.4676"/>
<vertex x="36.5089" y="22.7724"/>
<vertex x="36.5343" y="22.9629"/>
<vertex x="36.5978" y="23.1534"/>
<vertex x="36.7502" y="23.3693"/>
<vertex x="36.801" y="23.3947"/>
<vertex x="37.055" y="23.3947" curve="90"/>
<vertex x="37.1566" y="23.4963"/>
<vertex x="36.6359" y="23.4963" curve="-90"/>
<vertex x="36.5089" y="23.6233"/>
<vertex x="36.5089" y="24.4742" curve="-90"/>
<vertex x="36.6359" y="24.6012"/>
<vertex x="38.1345" y="24.6012" curve="-90"/>
<vertex x="38.2615" y="24.4742"/>
<vertex x="38.2615" y="23.6233" curve="-90"/>
<vertex x="38.1345" y="23.4963"/>
<vertex x="37.6138" y="23.4963" curve="90"/>
<vertex x="37.7154" y="23.3947"/>
<vertex x="37.9694" y="23.3947"/>
<vertex x="38.0202" y="23.3693"/>
<vertex x="38.1726" y="23.1534"/>
<vertex x="38.2615" y="22.7724"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="35.7215" y="22.4676" curve="-90"/>
<vertex x="34.8579" y="21.604"/>
<vertex x="34.8325" y="21.604" curve="-90"/>
<vertex x="33.9689" y="22.4676"/>
<vertex x="33.9689" y="22.7724"/>
<vertex x="33.9943" y="22.9629"/>
<vertex x="34.0578" y="23.1534"/>
<vertex x="34.2102" y="23.3693"/>
<vertex x="34.261" y="23.3947"/>
<vertex x="34.515" y="23.3947" curve="90"/>
<vertex x="34.6166" y="23.4963"/>
<vertex x="34.0959" y="23.4963" curve="-90"/>
<vertex x="33.9689" y="23.6233"/>
<vertex x="33.9689" y="24.4742" curve="-90"/>
<vertex x="34.0959" y="24.6012"/>
<vertex x="35.5945" y="24.6012" curve="-90"/>
<vertex x="35.7215" y="24.4742"/>
<vertex x="35.7215" y="23.6233" curve="-90"/>
<vertex x="35.5945" y="23.4963"/>
<vertex x="35.0738" y="23.4963" curve="90"/>
<vertex x="35.1754" y="23.3947"/>
<vertex x="35.4294" y="23.3947"/>
<vertex x="35.4802" y="23.3693"/>
<vertex x="35.6326" y="23.1534"/>
<vertex x="35.7215" y="22.7724"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="35.7215" y="22.4676" curve="-90"/>
<vertex x="34.8579" y="21.604"/>
<vertex x="34.8325" y="21.604" curve="-90"/>
<vertex x="33.9689" y="22.4676"/>
<vertex x="33.9689" y="22.7724"/>
<vertex x="33.9943" y="22.9629"/>
<vertex x="34.0578" y="23.1534"/>
<vertex x="34.2102" y="23.3693"/>
<vertex x="34.261" y="23.3947"/>
<vertex x="34.515" y="23.3947" curve="90"/>
<vertex x="34.6166" y="23.4963"/>
<vertex x="34.0959" y="23.4963" curve="-90"/>
<vertex x="33.9689" y="23.6233"/>
<vertex x="33.9689" y="24.4742" curve="-90"/>
<vertex x="34.0959" y="24.6012"/>
<vertex x="35.5945" y="24.6012" curve="-90"/>
<vertex x="35.7215" y="24.4742"/>
<vertex x="35.7215" y="23.6233" curve="-90"/>
<vertex x="35.5945" y="23.4963"/>
<vertex x="35.0738" y="23.4963" curve="90"/>
<vertex x="35.1754" y="23.3947"/>
<vertex x="35.4294" y="23.3947"/>
<vertex x="35.4802" y="23.3693"/>
<vertex x="35.6326" y="23.1534"/>
<vertex x="35.7215" y="22.7724"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="33.1815" y="22.4676" curve="-90"/>
<vertex x="32.3179" y="21.604"/>
<vertex x="32.2925" y="21.604" curve="-90"/>
<vertex x="31.4289" y="22.4676"/>
<vertex x="31.4289" y="22.7724"/>
<vertex x="31.4543" y="22.9629"/>
<vertex x="31.5178" y="23.1534"/>
<vertex x="31.6702" y="23.3693"/>
<vertex x="31.721" y="23.3947"/>
<vertex x="31.975" y="23.3947" curve="90"/>
<vertex x="32.0766" y="23.4963"/>
<vertex x="31.5559" y="23.4963" curve="-90"/>
<vertex x="31.4289" y="23.6233"/>
<vertex x="31.4289" y="24.4742" curve="-90"/>
<vertex x="31.5559" y="24.6012"/>
<vertex x="33.0545" y="24.6012" curve="-90"/>
<vertex x="33.1815" y="24.4742"/>
<vertex x="33.1815" y="23.6233" curve="-90"/>
<vertex x="33.0545" y="23.4963"/>
<vertex x="32.5338" y="23.4963" curve="90"/>
<vertex x="32.6354" y="23.3947"/>
<vertex x="32.8894" y="23.3947"/>
<vertex x="32.9402" y="23.3693"/>
<vertex x="33.0926" y="23.1534"/>
<vertex x="33.1815" y="22.7724"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="33.1815" y="22.4676" curve="-90"/>
<vertex x="32.3179" y="21.604"/>
<vertex x="32.2925" y="21.604" curve="-90"/>
<vertex x="31.4289" y="22.4676"/>
<vertex x="31.4289" y="22.7724"/>
<vertex x="31.4543" y="22.9629"/>
<vertex x="31.5178" y="23.1534"/>
<vertex x="31.6702" y="23.3693"/>
<vertex x="31.721" y="23.3947"/>
<vertex x="31.975" y="23.3947" curve="90"/>
<vertex x="32.0766" y="23.4963"/>
<vertex x="31.5559" y="23.4963" curve="-90"/>
<vertex x="31.4289" y="23.6233"/>
<vertex x="31.4289" y="24.4742" curve="-90"/>
<vertex x="31.5559" y="24.6012"/>
<vertex x="33.0545" y="24.6012" curve="-90"/>
<vertex x="33.1815" y="24.4742"/>
<vertex x="33.1815" y="23.6233" curve="-90"/>
<vertex x="33.0545" y="23.4963"/>
<vertex x="32.5338" y="23.4963" curve="90"/>
<vertex x="32.6354" y="23.3947"/>
<vertex x="32.8894" y="23.3947"/>
<vertex x="32.9402" y="23.3693"/>
<vertex x="33.0926" y="23.1534"/>
<vertex x="33.1815" y="22.7724"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="30.6415" y="22.4676" curve="-90"/>
<vertex x="29.7779" y="21.604"/>
<vertex x="29.7525" y="21.604" curve="-90"/>
<vertex x="28.8889" y="22.4676"/>
<vertex x="28.8889" y="22.7724"/>
<vertex x="28.9143" y="22.9629"/>
<vertex x="28.9778" y="23.1534"/>
<vertex x="29.1302" y="23.3693"/>
<vertex x="29.181" y="23.3947"/>
<vertex x="29.435" y="23.3947" curve="90"/>
<vertex x="29.5366" y="23.4963"/>
<vertex x="29.0159" y="23.4963" curve="-90"/>
<vertex x="28.8889" y="23.6233"/>
<vertex x="28.8889" y="24.4742" curve="-90"/>
<vertex x="29.0159" y="24.6012"/>
<vertex x="30.5145" y="24.6012" curve="-90"/>
<vertex x="30.6415" y="24.4742"/>
<vertex x="30.6415" y="23.6233" curve="-90"/>
<vertex x="30.5145" y="23.4963"/>
<vertex x="29.9938" y="23.4963" curve="90"/>
<vertex x="30.0954" y="23.3947"/>
<vertex x="30.3494" y="23.3947"/>
<vertex x="30.4002" y="23.3693"/>
<vertex x="30.5526" y="23.1534"/>
<vertex x="30.6415" y="22.7724"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="30.6415" y="22.4676" curve="-90"/>
<vertex x="29.7779" y="21.604"/>
<vertex x="29.7525" y="21.604" curve="-90"/>
<vertex x="28.8889" y="22.4676"/>
<vertex x="28.8889" y="22.7724"/>
<vertex x="28.9143" y="22.9629"/>
<vertex x="28.9778" y="23.1534"/>
<vertex x="29.1302" y="23.3693"/>
<vertex x="29.181" y="23.3947"/>
<vertex x="29.435" y="23.3947" curve="90"/>
<vertex x="29.5366" y="23.4963"/>
<vertex x="29.0159" y="23.4963" curve="-90"/>
<vertex x="28.8889" y="23.6233"/>
<vertex x="28.8889" y="24.4742" curve="-90"/>
<vertex x="29.0159" y="24.6012"/>
<vertex x="30.5145" y="24.6012" curve="-90"/>
<vertex x="30.6415" y="24.4742"/>
<vertex x="30.6415" y="23.6233" curve="-90"/>
<vertex x="30.5145" y="23.4963"/>
<vertex x="29.9938" y="23.4963" curve="90"/>
<vertex x="30.0954" y="23.3947"/>
<vertex x="30.3494" y="23.3947"/>
<vertex x="30.4002" y="23.3693"/>
<vertex x="30.5526" y="23.1534"/>
<vertex x="30.6415" y="22.7724"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="28.1015" y="22.4676" curve="-90"/>
<vertex x="27.2379" y="21.604"/>
<vertex x="27.2125" y="21.604" curve="-90"/>
<vertex x="26.3489" y="22.4676"/>
<vertex x="26.3489" y="22.7724"/>
<vertex x="26.3743" y="22.9629"/>
<vertex x="26.4378" y="23.1534"/>
<vertex x="26.5902" y="23.3693"/>
<vertex x="26.641" y="23.3947"/>
<vertex x="26.895" y="23.3947" curve="90"/>
<vertex x="26.9966" y="23.4963"/>
<vertex x="26.4759" y="23.4963" curve="-90"/>
<vertex x="26.3489" y="23.6233"/>
<vertex x="26.3489" y="24.4742" curve="-90"/>
<vertex x="26.4759" y="24.6012"/>
<vertex x="27.9745" y="24.6012" curve="-90"/>
<vertex x="28.1015" y="24.4742"/>
<vertex x="28.1015" y="23.6233" curve="-90"/>
<vertex x="27.9745" y="23.4963"/>
<vertex x="27.4538" y="23.4963" curve="90"/>
<vertex x="27.5554" y="23.3947"/>
<vertex x="27.8094" y="23.3947"/>
<vertex x="27.8602" y="23.3693"/>
<vertex x="28.0126" y="23.1534"/>
<vertex x="28.1015" y="22.7724"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="28.1015" y="22.4676" curve="-90"/>
<vertex x="27.2379" y="21.604"/>
<vertex x="27.2125" y="21.604" curve="-90"/>
<vertex x="26.3489" y="22.4676"/>
<vertex x="26.3489" y="22.7724"/>
<vertex x="26.3743" y="22.9629"/>
<vertex x="26.4378" y="23.1534"/>
<vertex x="26.5902" y="23.3693"/>
<vertex x="26.641" y="23.3947"/>
<vertex x="26.895" y="23.3947" curve="90"/>
<vertex x="26.9966" y="23.4963"/>
<vertex x="26.4759" y="23.4963" curve="-90"/>
<vertex x="26.3489" y="23.6233"/>
<vertex x="26.3489" y="24.4742" curve="-90"/>
<vertex x="26.4759" y="24.6012"/>
<vertex x="27.9745" y="24.6012" curve="-90"/>
<vertex x="28.1015" y="24.4742"/>
<vertex x="28.1015" y="23.6233" curve="-90"/>
<vertex x="27.9745" y="23.4963"/>
<vertex x="27.4538" y="23.4963" curve="90"/>
<vertex x="27.5554" y="23.3947"/>
<vertex x="27.8094" y="23.3947"/>
<vertex x="27.8602" y="23.3693"/>
<vertex x="28.0126" y="23.1534"/>
<vertex x="28.1015" y="22.7724"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="25.5615" y="22.4676" curve="-90"/>
<vertex x="24.6979" y="21.604"/>
<vertex x="24.6725" y="21.604" curve="-90"/>
<vertex x="23.8089" y="22.4676"/>
<vertex x="23.8089" y="22.7724"/>
<vertex x="23.8343" y="22.9629"/>
<vertex x="23.8978" y="23.1534"/>
<vertex x="24.0502" y="23.3693"/>
<vertex x="24.101" y="23.3947"/>
<vertex x="24.355" y="23.3947" curve="90"/>
<vertex x="24.4566" y="23.4963"/>
<vertex x="23.9359" y="23.4963" curve="-90"/>
<vertex x="23.8089" y="23.6233"/>
<vertex x="23.8089" y="24.4742" curve="-90"/>
<vertex x="23.9359" y="24.6012"/>
<vertex x="25.4345" y="24.6012" curve="-90"/>
<vertex x="25.5615" y="24.4742"/>
<vertex x="25.5615" y="23.6233" curve="-90"/>
<vertex x="25.4345" y="23.4963"/>
<vertex x="24.9138" y="23.4963" curve="90"/>
<vertex x="25.0154" y="23.3947"/>
<vertex x="25.2694" y="23.3947"/>
<vertex x="25.3202" y="23.3693"/>
<vertex x="25.4726" y="23.1534"/>
<vertex x="25.5615" y="22.7724"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="25.5615" y="22.4676" curve="-90"/>
<vertex x="24.6979" y="21.604"/>
<vertex x="24.6725" y="21.604" curve="-90"/>
<vertex x="23.8089" y="22.4676"/>
<vertex x="23.8089" y="22.7724"/>
<vertex x="23.8343" y="22.9629"/>
<vertex x="23.8978" y="23.1534"/>
<vertex x="24.0502" y="23.3693"/>
<vertex x="24.101" y="23.3947"/>
<vertex x="24.355" y="23.3947" curve="90"/>
<vertex x="24.4566" y="23.4963"/>
<vertex x="23.9359" y="23.4963" curve="-90"/>
<vertex x="23.8089" y="23.6233"/>
<vertex x="23.8089" y="24.4742" curve="-90"/>
<vertex x="23.9359" y="24.6012"/>
<vertex x="25.4345" y="24.6012" curve="-90"/>
<vertex x="25.5615" y="24.4742"/>
<vertex x="25.5615" y="23.6233" curve="-90"/>
<vertex x="25.4345" y="23.4963"/>
<vertex x="24.9138" y="23.4963" curve="90"/>
<vertex x="25.0154" y="23.3947"/>
<vertex x="25.2694" y="23.3947"/>
<vertex x="25.3202" y="23.3693"/>
<vertex x="25.4726" y="23.1534"/>
<vertex x="25.5615" y="22.7724"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="23.0215" y="22.4676" curve="-90"/>
<vertex x="22.1579" y="21.604"/>
<vertex x="22.1325" y="21.604" curve="-90"/>
<vertex x="21.2689" y="22.4676"/>
<vertex x="21.2689" y="22.7724"/>
<vertex x="21.2943" y="22.9629"/>
<vertex x="21.3578" y="23.1534"/>
<vertex x="21.5102" y="23.3693"/>
<vertex x="21.561" y="23.3947"/>
<vertex x="21.815" y="23.3947" curve="90"/>
<vertex x="21.9166" y="23.4963"/>
<vertex x="21.3959" y="23.4963" curve="-90"/>
<vertex x="21.2689" y="23.6233"/>
<vertex x="21.2689" y="24.4742" curve="-90"/>
<vertex x="21.3959" y="24.6012"/>
<vertex x="22.8945" y="24.6012" curve="-90"/>
<vertex x="23.0215" y="24.4742"/>
<vertex x="23.0215" y="23.6233" curve="-90"/>
<vertex x="22.8945" y="23.4963"/>
<vertex x="22.3738" y="23.4963" curve="90"/>
<vertex x="22.4754" y="23.3947"/>
<vertex x="22.7294" y="23.3947"/>
<vertex x="22.7802" y="23.3693"/>
<vertex x="22.9326" y="23.1534"/>
<vertex x="23.0215" y="22.7724"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="23.0215" y="22.4676" curve="-90"/>
<vertex x="22.1579" y="21.604"/>
<vertex x="22.1325" y="21.604" curve="-90"/>
<vertex x="21.2689" y="22.4676"/>
<vertex x="21.2689" y="22.7724"/>
<vertex x="21.2943" y="22.9629"/>
<vertex x="21.3578" y="23.1534"/>
<vertex x="21.5102" y="23.3693"/>
<vertex x="21.561" y="23.3947"/>
<vertex x="21.815" y="23.3947" curve="90"/>
<vertex x="21.9166" y="23.4963"/>
<vertex x="21.3959" y="23.4963" curve="-90"/>
<vertex x="21.2689" y="23.6233"/>
<vertex x="21.2689" y="24.4742" curve="-90"/>
<vertex x="21.3959" y="24.6012"/>
<vertex x="22.8945" y="24.6012" curve="-90"/>
<vertex x="23.0215" y="24.4742"/>
<vertex x="23.0215" y="23.6233" curve="-90"/>
<vertex x="22.8945" y="23.4963"/>
<vertex x="22.3738" y="23.4963" curve="90"/>
<vertex x="22.4754" y="23.3947"/>
<vertex x="22.7294" y="23.3947"/>
<vertex x="22.7802" y="23.3693"/>
<vertex x="22.9326" y="23.1534"/>
<vertex x="23.0215" y="22.7724"/>
</polygon>
<polygon width="0.0254" layer="29">
<vertex x="53.5015" y="22.7597"/>
<vertex x="53.5015" y="22.493" curve="-90"/>
<vertex x="52.6252" y="21.604" curve="-90"/>
<vertex x="51.7489" y="22.493"/>
<vertex x="51.7489" y="22.7597" curve="-90"/>
<vertex x="52.6252" y="23.636" curve="-90"/>
</polygon>
<polygon width="0.0254" layer="29">
<vertex x="50.9615" y="22.7597"/>
<vertex x="50.9615" y="22.493" curve="-90"/>
<vertex x="50.0852" y="21.604" curve="-90"/>
<vertex x="49.2089" y="22.493"/>
<vertex x="49.2089" y="22.7597" curve="-90"/>
<vertex x="50.0852" y="23.636" curve="-90"/>
</polygon>
<polygon width="0.0254" layer="29">
<vertex x="48.4215" y="22.7597"/>
<vertex x="48.4215" y="22.493" curve="-90"/>
<vertex x="47.5452" y="21.604" curve="-90"/>
<vertex x="46.6689" y="22.493"/>
<vertex x="46.6689" y="22.7597" curve="-90"/>
<vertex x="47.5452" y="23.636" curve="-90"/>
</polygon>
<polygon width="0.0254" layer="29">
<vertex x="45.8815" y="22.7597"/>
<vertex x="45.8815" y="22.493" curve="-90"/>
<vertex x="45.0052" y="21.604" curve="-90"/>
<vertex x="44.1289" y="22.493"/>
<vertex x="44.1289" y="22.7597" curve="-90"/>
<vertex x="45.0052" y="23.636" curve="-90"/>
</polygon>
<polygon width="0.0254" layer="29">
<vertex x="43.3415" y="22.7597"/>
<vertex x="43.3415" y="22.493" curve="-90"/>
<vertex x="42.4652" y="21.604" curve="-90"/>
<vertex x="41.5889" y="22.493"/>
<vertex x="41.5889" y="22.7597" curve="-90"/>
<vertex x="42.4652" y="23.636" curve="-90"/>
</polygon>
<polygon width="0.0254" layer="29">
<vertex x="40.8015" y="22.7597"/>
<vertex x="40.8015" y="22.493" curve="-90"/>
<vertex x="39.9252" y="21.604" curve="-90"/>
<vertex x="39.0489" y="22.493"/>
<vertex x="39.0489" y="22.7597" curve="-90"/>
<vertex x="39.9252" y="23.636" curve="-90"/>
</polygon>
<polygon width="0.0254" layer="29">
<vertex x="38.2615" y="22.7597"/>
<vertex x="38.2615" y="22.493" curve="-90"/>
<vertex x="37.3852" y="21.604" curve="-90"/>
<vertex x="36.5089" y="22.493"/>
<vertex x="36.5089" y="22.7597" curve="-90"/>
<vertex x="37.3852" y="23.636" curve="-90"/>
</polygon>
<polygon width="0.0254" layer="29">
<vertex x="35.7215" y="22.7597"/>
<vertex x="35.7215" y="22.493" curve="-90"/>
<vertex x="34.8452" y="21.604" curve="-90"/>
<vertex x="33.9689" y="22.493"/>
<vertex x="33.9689" y="22.7597" curve="-90"/>
<vertex x="34.8452" y="23.636" curve="-90"/>
</polygon>
<polygon width="0.0254" layer="29">
<vertex x="33.1815" y="22.7597"/>
<vertex x="33.1815" y="22.493" curve="-90"/>
<vertex x="32.3052" y="21.604" curve="-90"/>
<vertex x="31.4289" y="22.493"/>
<vertex x="31.4289" y="22.7597" curve="-90"/>
<vertex x="32.3052" y="23.636" curve="-90"/>
</polygon>
<polygon width="0.0254" layer="29">
<vertex x="30.6415" y="22.7597"/>
<vertex x="30.6415" y="22.493" curve="-90"/>
<vertex x="29.7652" y="21.604" curve="-90"/>
<vertex x="28.8889" y="22.493"/>
<vertex x="28.8889" y="22.7597" curve="-90"/>
<vertex x="29.7652" y="23.636" curve="-90"/>
</polygon>
<polygon width="0.0254" layer="29">
<vertex x="28.1015" y="22.7597"/>
<vertex x="28.1015" y="22.493" curve="-90"/>
<vertex x="27.2252" y="21.604" curve="-90"/>
<vertex x="26.3489" y="22.493"/>
<vertex x="26.3489" y="22.7597" curve="-90"/>
<vertex x="27.2252" y="23.636" curve="-90"/>
</polygon>
<polygon width="0.0254" layer="29">
<vertex x="25.5615" y="22.7597"/>
<vertex x="25.5615" y="22.493" curve="-90"/>
<vertex x="24.6852" y="21.604" curve="-90"/>
<vertex x="23.8089" y="22.493"/>
<vertex x="23.8089" y="22.7597" curve="-90"/>
<vertex x="24.6852" y="23.636" curve="-90"/>
</polygon>
<polygon width="0.0254" layer="29">
<vertex x="23.0215" y="22.7597"/>
<vertex x="23.0215" y="22.493" curve="-90"/>
<vertex x="22.1452" y="21.604" curve="-90"/>
<vertex x="21.2689" y="22.493"/>
<vertex x="21.2689" y="22.7597" curve="-90"/>
<vertex x="22.1452" y="23.636" curve="-90"/>
</polygon>
<polygon width="0.127" layer="16">
<vertex x="52.6252" y="21.731" curve="90"/>
<vertex x="53.3745" y="22.4803"/>
<vertex x="53.3745" y="22.7851"/>
<vertex x="53.3491" y="22.9375"/>
<vertex x="53.2983" y="23.1026"/>
<vertex x="53.184" y="23.2677"/>
<vertex x="52.9427" y="23.2677" curve="-90"/>
<vertex x="52.7268" y="23.4836"/>
<vertex x="52.7268" y="23.6106" curve="-90"/>
<vertex x="52.7395" y="23.6233"/>
<vertex x="53.3745" y="23.6233"/>
<vertex x="53.3745" y="24.4742"/>
<vertex x="51.8759" y="24.4742"/>
<vertex x="51.8759" y="23.6233"/>
<vertex x="52.5236" y="23.6233"/>
<vertex x="52.5236" y="23.4836" curve="-90"/>
<vertex x="52.3077" y="23.2677"/>
<vertex x="52.0664" y="23.2677"/>
<vertex x="51.9521" y="23.1026"/>
<vertex x="51.9013" y="22.9375"/>
<vertex x="51.8759" y="22.7851"/>
<vertex x="51.8759" y="22.4803" curve="90"/>
</polygon>
<polygon width="0.127" layer="16">
<vertex x="22.1452" y="21.731" curve="90"/>
<vertex x="22.8945" y="22.4803"/>
<vertex x="22.8945" y="22.7851"/>
<vertex x="22.8691" y="22.9375"/>
<vertex x="22.8183" y="23.1026"/>
<vertex x="22.704" y="23.2677"/>
<vertex x="22.4627" y="23.2677" curve="-90"/>
<vertex x="22.2468" y="23.4836"/>
<vertex x="22.2468" y="23.6106" curve="-90"/>
<vertex x="22.2595" y="23.6233"/>
<vertex x="22.8945" y="23.6233"/>
<vertex x="22.8945" y="24.4742"/>
<vertex x="21.3959" y="24.4742"/>
<vertex x="21.3959" y="23.6233"/>
<vertex x="22.0436" y="23.6233"/>
<vertex x="22.0436" y="23.4836" curve="-90"/>
<vertex x="21.8277" y="23.2677"/>
<vertex x="21.5864" y="23.2677"/>
<vertex x="21.4721" y="23.1026"/>
<vertex x="21.4213" y="22.9375"/>
<vertex x="21.3959" y="22.7851"/>
<vertex x="21.3959" y="22.4803" curve="90"/>
</polygon>
<polygon width="0.127" layer="16">
<vertex x="50.0852" y="21.731" curve="90"/>
<vertex x="50.8345" y="22.4803"/>
<vertex x="50.8345" y="22.7851"/>
<vertex x="50.8091" y="22.9375"/>
<vertex x="50.7583" y="23.1026"/>
<vertex x="50.644" y="23.2677"/>
<vertex x="50.4027" y="23.2677" curve="-90"/>
<vertex x="50.1868" y="23.4836"/>
<vertex x="50.1868" y="23.6106" curve="-90"/>
<vertex x="50.1995" y="23.6233"/>
<vertex x="50.8345" y="23.6233"/>
<vertex x="50.8345" y="24.4742"/>
<vertex x="49.3359" y="24.4742"/>
<vertex x="49.3359" y="23.6233"/>
<vertex x="49.9836" y="23.6233"/>
<vertex x="49.9836" y="23.4836" curve="-90"/>
<vertex x="49.7677" y="23.2677"/>
<vertex x="49.5264" y="23.2677"/>
<vertex x="49.4121" y="23.1026"/>
<vertex x="49.3613" y="22.9375"/>
<vertex x="49.3359" y="22.7851"/>
<vertex x="49.3359" y="22.4803" curve="90"/>
</polygon>
<polygon width="0.127" layer="16">
<vertex x="47.5452" y="21.731" curve="90"/>
<vertex x="48.2945" y="22.4803"/>
<vertex x="48.2945" y="22.7851"/>
<vertex x="48.2691" y="22.9375"/>
<vertex x="48.2183" y="23.1026"/>
<vertex x="48.104" y="23.2677"/>
<vertex x="47.8627" y="23.2677" curve="-90"/>
<vertex x="47.6468" y="23.4836"/>
<vertex x="47.6468" y="23.6106" curve="-90"/>
<vertex x="47.6595" y="23.6233"/>
<vertex x="48.2945" y="23.6233"/>
<vertex x="48.2945" y="24.4742"/>
<vertex x="46.7959" y="24.4742"/>
<vertex x="46.7959" y="23.6233"/>
<vertex x="47.4436" y="23.6233"/>
<vertex x="47.4436" y="23.4836" curve="-90"/>
<vertex x="47.2277" y="23.2677"/>
<vertex x="46.9864" y="23.2677"/>
<vertex x="46.8721" y="23.1026"/>
<vertex x="46.8213" y="22.9375"/>
<vertex x="46.7959" y="22.7851"/>
<vertex x="46.7959" y="22.4803" curve="90"/>
</polygon>
<polygon width="0.127" layer="16">
<vertex x="45.0052" y="21.731" curve="90"/>
<vertex x="45.7545" y="22.4803"/>
<vertex x="45.7545" y="22.7851"/>
<vertex x="45.7291" y="22.9375"/>
<vertex x="45.6783" y="23.1026"/>
<vertex x="45.564" y="23.2677"/>
<vertex x="45.3227" y="23.2677" curve="-90"/>
<vertex x="45.1068" y="23.4836"/>
<vertex x="45.1068" y="23.6106" curve="-90"/>
<vertex x="45.1195" y="23.6233"/>
<vertex x="45.7545" y="23.6233"/>
<vertex x="45.7545" y="24.4742"/>
<vertex x="44.2559" y="24.4742"/>
<vertex x="44.2559" y="23.6233"/>
<vertex x="44.9036" y="23.6233"/>
<vertex x="44.9036" y="23.4836" curve="-90"/>
<vertex x="44.6877" y="23.2677"/>
<vertex x="44.4464" y="23.2677"/>
<vertex x="44.3321" y="23.1026"/>
<vertex x="44.2813" y="22.9375"/>
<vertex x="44.2559" y="22.7851"/>
<vertex x="44.2559" y="22.4803" curve="90"/>
</polygon>
<polygon width="0.127" layer="16">
<vertex x="42.4652" y="21.731" curve="90"/>
<vertex x="43.2145" y="22.4803"/>
<vertex x="43.2145" y="22.7851"/>
<vertex x="43.1891" y="22.9375"/>
<vertex x="43.1383" y="23.1026"/>
<vertex x="43.024" y="23.2677"/>
<vertex x="42.7827" y="23.2677" curve="-90"/>
<vertex x="42.5668" y="23.4836"/>
<vertex x="42.5668" y="23.6106" curve="-90"/>
<vertex x="42.5795" y="23.6233"/>
<vertex x="43.2145" y="23.6233"/>
<vertex x="43.2145" y="24.4742"/>
<vertex x="41.7159" y="24.4742"/>
<vertex x="41.7159" y="23.6233"/>
<vertex x="42.3636" y="23.6233"/>
<vertex x="42.3636" y="23.4836" curve="-90"/>
<vertex x="42.1477" y="23.2677"/>
<vertex x="41.9064" y="23.2677"/>
<vertex x="41.7921" y="23.1026"/>
<vertex x="41.7413" y="22.9375"/>
<vertex x="41.7159" y="22.7851"/>
<vertex x="41.7159" y="22.4803" curve="90"/>
</polygon>
<polygon width="0.127" layer="16">
<vertex x="39.9252" y="21.731" curve="90"/>
<vertex x="40.6745" y="22.4803"/>
<vertex x="40.6745" y="22.7851"/>
<vertex x="40.6491" y="22.9375"/>
<vertex x="40.5983" y="23.1026"/>
<vertex x="40.484" y="23.2677"/>
<vertex x="40.2427" y="23.2677" curve="-90"/>
<vertex x="40.0268" y="23.4836"/>
<vertex x="40.0268" y="23.6106" curve="-90"/>
<vertex x="40.0395" y="23.6233"/>
<vertex x="40.6745" y="23.6233"/>
<vertex x="40.6745" y="24.4742"/>
<vertex x="39.1759" y="24.4742"/>
<vertex x="39.1759" y="23.6233"/>
<vertex x="39.8236" y="23.6233"/>
<vertex x="39.8236" y="23.4836" curve="-90"/>
<vertex x="39.6077" y="23.2677"/>
<vertex x="39.3664" y="23.2677"/>
<vertex x="39.2521" y="23.1026"/>
<vertex x="39.2013" y="22.9375"/>
<vertex x="39.1759" y="22.7851"/>
<vertex x="39.1759" y="22.4803" curve="90"/>
</polygon>
<polygon width="0.127" layer="16">
<vertex x="37.3852" y="21.731" curve="90"/>
<vertex x="38.1345" y="22.4803"/>
<vertex x="38.1345" y="22.7851"/>
<vertex x="38.1091" y="22.9375"/>
<vertex x="38.0583" y="23.1026"/>
<vertex x="37.944" y="23.2677"/>
<vertex x="37.7027" y="23.2677" curve="-90"/>
<vertex x="37.4868" y="23.4836"/>
<vertex x="37.4868" y="23.6106" curve="-90"/>
<vertex x="37.4995" y="23.6233"/>
<vertex x="38.1345" y="23.6233"/>
<vertex x="38.1345" y="24.4742"/>
<vertex x="36.6359" y="24.4742"/>
<vertex x="36.6359" y="23.6233"/>
<vertex x="37.2836" y="23.6233"/>
<vertex x="37.2836" y="23.4836" curve="-90"/>
<vertex x="37.0677" y="23.2677"/>
<vertex x="36.8264" y="23.2677"/>
<vertex x="36.7121" y="23.1026"/>
<vertex x="36.6613" y="22.9375"/>
<vertex x="36.6359" y="22.7851"/>
<vertex x="36.6359" y="22.4803" curve="90"/>
</polygon>
<polygon width="0.127" layer="16">
<vertex x="34.8452" y="21.731" curve="90"/>
<vertex x="35.5945" y="22.4803"/>
<vertex x="35.5945" y="22.7851"/>
<vertex x="35.5691" y="22.9375"/>
<vertex x="35.5183" y="23.1026"/>
<vertex x="35.404" y="23.2677"/>
<vertex x="35.1627" y="23.2677" curve="-90"/>
<vertex x="34.9468" y="23.4836"/>
<vertex x="34.9468" y="23.6106" curve="-90"/>
<vertex x="34.9595" y="23.6233"/>
<vertex x="35.5945" y="23.6233"/>
<vertex x="35.5945" y="24.4742"/>
<vertex x="34.0959" y="24.4742"/>
<vertex x="34.0959" y="23.6233"/>
<vertex x="34.7436" y="23.6233"/>
<vertex x="34.7436" y="23.4836" curve="-90"/>
<vertex x="34.5277" y="23.2677"/>
<vertex x="34.2864" y="23.2677"/>
<vertex x="34.1721" y="23.1026"/>
<vertex x="34.1213" y="22.9375"/>
<vertex x="34.0959" y="22.7851"/>
<vertex x="34.0959" y="22.4803" curve="90"/>
</polygon>
<polygon width="0.127" layer="16">
<vertex x="29.7652" y="21.731" curve="90"/>
<vertex x="30.5145" y="22.4803"/>
<vertex x="30.5145" y="22.7851"/>
<vertex x="30.4891" y="22.9375"/>
<vertex x="30.4383" y="23.1026"/>
<vertex x="30.324" y="23.2677"/>
<vertex x="30.0827" y="23.2677" curve="-90"/>
<vertex x="29.8668" y="23.4836"/>
<vertex x="29.8668" y="23.6106" curve="-90"/>
<vertex x="29.8795" y="23.6233"/>
<vertex x="30.5145" y="23.6233"/>
<vertex x="30.5145" y="24.4742"/>
<vertex x="29.0159" y="24.4742"/>
<vertex x="29.0159" y="23.6233"/>
<vertex x="29.6636" y="23.6233"/>
<vertex x="29.6636" y="23.4836" curve="-90"/>
<vertex x="29.4477" y="23.2677"/>
<vertex x="29.2064" y="23.2677"/>
<vertex x="29.0921" y="23.1026"/>
<vertex x="29.0413" y="22.9375"/>
<vertex x="29.0159" y="22.7851"/>
<vertex x="29.0159" y="22.4803" curve="90"/>
</polygon>
<polygon width="0.127" layer="16">
<vertex x="27.2252" y="21.731" curve="90"/>
<vertex x="27.9745" y="22.4803"/>
<vertex x="27.9745" y="22.7851"/>
<vertex x="27.9491" y="22.9375"/>
<vertex x="27.8983" y="23.1026"/>
<vertex x="27.784" y="23.2677"/>
<vertex x="27.5427" y="23.2677" curve="-90"/>
<vertex x="27.3268" y="23.4836"/>
<vertex x="27.3268" y="23.6106" curve="-90"/>
<vertex x="27.3395" y="23.6233"/>
<vertex x="27.9745" y="23.6233"/>
<vertex x="27.9745" y="24.4742"/>
<vertex x="26.4759" y="24.4742"/>
<vertex x="26.4759" y="23.6233"/>
<vertex x="27.1236" y="23.6233"/>
<vertex x="27.1236" y="23.4836" curve="-90"/>
<vertex x="26.9077" y="23.2677"/>
<vertex x="26.6664" y="23.2677"/>
<vertex x="26.5521" y="23.1026"/>
<vertex x="26.5013" y="22.9375"/>
<vertex x="26.4759" y="22.7851"/>
<vertex x="26.4759" y="22.4803" curve="90"/>
</polygon>
<polygon width="0.127" layer="16">
<vertex x="24.6852" y="21.731" curve="90"/>
<vertex x="25.4345" y="22.4803"/>
<vertex x="25.4345" y="22.7851"/>
<vertex x="25.4091" y="22.9375"/>
<vertex x="25.3583" y="23.1026"/>
<vertex x="25.244" y="23.2677"/>
<vertex x="25.0027" y="23.2677" curve="-90"/>
<vertex x="24.7868" y="23.4836"/>
<vertex x="24.7868" y="23.6106" curve="-90"/>
<vertex x="24.7995" y="23.6233"/>
<vertex x="25.4345" y="23.6233"/>
<vertex x="25.4345" y="24.4742"/>
<vertex x="23.9359" y="24.4742"/>
<vertex x="23.9359" y="23.6233"/>
<vertex x="24.5836" y="23.6233"/>
<vertex x="24.5836" y="23.4836" curve="-90"/>
<vertex x="24.3677" y="23.2677"/>
<vertex x="24.1264" y="23.2677"/>
<vertex x="24.0121" y="23.1026"/>
<vertex x="23.9613" y="22.9375"/>
<vertex x="23.9359" y="22.7851"/>
<vertex x="23.9359" y="22.4803" curve="90"/>
</polygon>
<polygon width="0.127" layer="16">
<vertex x="32.3052" y="21.731" curve="90"/>
<vertex x="33.0545" y="22.4803"/>
<vertex x="33.0545" y="22.7851"/>
<vertex x="33.0291" y="22.9375"/>
<vertex x="32.9783" y="23.1026"/>
<vertex x="32.864" y="23.2677"/>
<vertex x="32.6227" y="23.2677" curve="-90"/>
<vertex x="32.4068" y="23.4836"/>
<vertex x="32.4068" y="23.6106" curve="-90"/>
<vertex x="32.4195" y="23.6233"/>
<vertex x="33.0545" y="23.6233"/>
<vertex x="33.0545" y="24.4742"/>
<vertex x="31.5559" y="24.4742"/>
<vertex x="31.5559" y="23.6233"/>
<vertex x="32.2036" y="23.6233"/>
<vertex x="32.2036" y="23.4836" curve="-90"/>
<vertex x="31.9877" y="23.2677"/>
<vertex x="31.7464" y="23.2677"/>
<vertex x="31.6321" y="23.1026"/>
<vertex x="31.5813" y="22.9375"/>
<vertex x="31.5559" y="22.7851"/>
<vertex x="31.5559" y="22.4803" curve="90"/>
</polygon>
<polygon width="0.127" layer="16">
<vertex x="55.9145" y="21.7183"/>
<vertex x="55.9145" y="23.2677"/>
<vertex x="55.4827" y="23.2677" curve="-90"/>
<vertex x="55.2668" y="23.4836"/>
<vertex x="55.2668" y="23.6106" curve="-90"/>
<vertex x="55.2795" y="23.6233"/>
<vertex x="55.9145" y="23.6233"/>
<vertex x="55.9145" y="24.4742"/>
<vertex x="54.4159" y="24.4742"/>
<vertex x="54.4159" y="23.6233"/>
<vertex x="55.0636" y="23.6233"/>
<vertex x="55.0636" y="23.4836" curve="-90"/>
<vertex x="54.8477" y="23.2677"/>
<vertex x="54.4159" y="23.2677"/>
<vertex x="54.4159" y="21.7183"/>
</polygon>
<polygon width="0.127" layer="1">
<vertex x="52.6252" y="21.731" curve="90"/>
<vertex x="53.3745" y="22.4803"/>
<vertex x="53.3745" y="22.8232" curve="90"/>
<vertex x="52.6252" y="23.5725" curve="90"/>
<vertex x="51.8759" y="22.8232"/>
<vertex x="51.8759" y="22.4803" curve="90"/>
</polygon>
<polygon width="0.127" layer="1">
<vertex x="54.4159" y="21.731"/>
<vertex x="55.9145" y="21.731"/>
<vertex x="55.9145" y="23.5217"/>
<vertex x="54.4159" y="23.5217"/>
</polygon>
<polygon width="0.127" layer="1">
<vertex x="50.0852" y="21.731" curve="90"/>
<vertex x="50.8345" y="22.4803"/>
<vertex x="50.8345" y="22.8232" curve="90"/>
<vertex x="50.0852" y="23.5725" curve="90"/>
<vertex x="49.3359" y="22.8232"/>
<vertex x="49.3359" y="22.4803" curve="90"/>
</polygon>
<polygon width="0.127" layer="1">
<vertex x="47.5452" y="21.731" curve="90"/>
<vertex x="48.2945" y="22.4803"/>
<vertex x="48.2945" y="22.8232" curve="90"/>
<vertex x="47.5452" y="23.5725" curve="90"/>
<vertex x="46.7959" y="22.8232"/>
<vertex x="46.7959" y="22.4803" curve="90"/>
</polygon>
<polygon width="0.127" layer="1">
<vertex x="45.0052" y="21.731" curve="90"/>
<vertex x="45.7545" y="22.4803"/>
<vertex x="45.7545" y="22.8232" curve="90"/>
<vertex x="45.0052" y="23.5725" curve="90"/>
<vertex x="44.2559" y="22.8232"/>
<vertex x="44.2559" y="22.4803" curve="90"/>
</polygon>
<polygon width="0.127" layer="1">
<vertex x="42.4652" y="21.731" curve="90"/>
<vertex x="43.2145" y="22.4803"/>
<vertex x="43.2145" y="22.8232" curve="90"/>
<vertex x="42.4652" y="23.5725" curve="90"/>
<vertex x="41.7159" y="22.8232"/>
<vertex x="41.7159" y="22.4803" curve="90"/>
</polygon>
<polygon width="0.127" layer="1">
<vertex x="39.9252" y="21.731" curve="90"/>
<vertex x="40.6745" y="22.4803"/>
<vertex x="40.6745" y="22.8232" curve="90"/>
<vertex x="39.9252" y="23.5725" curve="90"/>
<vertex x="39.1759" y="22.8232"/>
<vertex x="39.1759" y="22.4803" curve="90"/>
</polygon>
<polygon width="0.127" layer="1">
<vertex x="37.3852" y="21.731" curve="90"/>
<vertex x="38.1345" y="22.4803"/>
<vertex x="38.1345" y="22.8232" curve="90"/>
<vertex x="37.3852" y="23.5725" curve="90"/>
<vertex x="36.6359" y="22.8232"/>
<vertex x="36.6359" y="22.4803" curve="90"/>
</polygon>
<polygon width="0.127" layer="1">
<vertex x="34.8452" y="21.731" curve="90"/>
<vertex x="35.5945" y="22.4803"/>
<vertex x="35.5945" y="22.8232" curve="90"/>
<vertex x="34.8452" y="23.5725" curve="90"/>
<vertex x="34.0959" y="22.8232"/>
<vertex x="34.0959" y="22.4803" curve="90"/>
</polygon>
<polygon width="0.127" layer="1">
<vertex x="32.3052" y="21.731" curve="90"/>
<vertex x="33.0545" y="22.4803"/>
<vertex x="33.0545" y="22.8232" curve="90"/>
<vertex x="32.3052" y="23.5725" curve="90"/>
<vertex x="31.5559" y="22.8232"/>
<vertex x="31.5559" y="22.4803" curve="90"/>
</polygon>
<polygon width="0.127" layer="1">
<vertex x="29.7652" y="21.731" curve="90"/>
<vertex x="30.5145" y="22.4803"/>
<vertex x="30.5145" y="22.8232" curve="90"/>
<vertex x="29.7652" y="23.5725" curve="90"/>
<vertex x="29.0159" y="22.8232"/>
<vertex x="29.0159" y="22.4803" curve="90"/>
</polygon>
<polygon width="0.127" layer="1">
<vertex x="27.2252" y="21.731" curve="90"/>
<vertex x="27.9745" y="22.4803"/>
<vertex x="27.9745" y="22.8232" curve="90"/>
<vertex x="27.2252" y="23.5725" curve="90"/>
<vertex x="26.4759" y="22.8232"/>
<vertex x="26.4759" y="22.4803" curve="90"/>
</polygon>
<polygon width="0.127" layer="1">
<vertex x="24.6852" y="21.731" curve="90"/>
<vertex x="25.4345" y="22.4803"/>
<vertex x="25.4345" y="22.8232" curve="90"/>
<vertex x="24.6852" y="23.5725" curve="90"/>
<vertex x="23.9359" y="22.8232"/>
<vertex x="23.9359" y="22.4803" curve="90"/>
</polygon>
<polygon width="0.127" layer="1">
<vertex x="22.1452" y="21.731" curve="90"/>
<vertex x="22.8945" y="22.4803"/>
<vertex x="22.8945" y="22.8232" curve="90"/>
<vertex x="22.1452" y="23.5725" curve="90"/>
<vertex x="21.3959" y="22.8232"/>
<vertex x="21.3959" y="22.4803" curve="90"/>
</polygon>
<hole x="2.25" y="2.25" drill="2.25"/>
<hole x="2.25" y="22.75" drill="2.25"/>
<hole x="59.25" y="22.75" drill="2.25"/>
<hole x="59.25" y="2.25" drill="2.25"/>
<wire x1="2.25" y1="0" x2="59.25" y2="0" width="0.1" layer="21"/>
<wire x1="59.25" y1="0" x2="61.5" y2="2.25" width="0.1" layer="21" curve="90"/>
<wire x1="61.5" y1="2.25" x2="61.5" y2="22.75" width="0.1" layer="21"/>
<wire x1="61.5" y1="22.75" x2="59.25" y2="25" width="0.1" layer="21" curve="90"/>
<wire x1="59.25" y1="25" x2="2.25" y2="25" width="0.1" layer="21"/>
<wire x1="2.25" y1="25" x2="0" y2="22.75" width="0.1" layer="21" curve="90"/>
<wire x1="0" y1="22.75" x2="0" y2="2.25" width="0.1" layer="21"/>
<wire x1="0" y1="2.25" x2="2.25" y2="0" width="0.1" layer="21" curve="90"/>
</package>
<package name="MKRBOARD-SOLDER">
<wire x1="22.1348" y1="1.99" x2="22.1348" y2="2.69" width="0.05" layer="39"/>
<pad name="AREF" x="22.1348" y="2.34" drill="1" first="yes"/>
<pad name="DAC0/A0" x="24.6748" y="2.34" drill="1"/>
<pad name="A1" x="27.2148" y="2.34" drill="1"/>
<pad name="A2" x="29.7548" y="2.34" drill="1"/>
<pad name="A3" x="32.2948" y="2.34" drill="1"/>
<pad name="A4" x="34.8348" y="2.34" drill="1"/>
<pad name="A5" x="37.3748" y="2.34" drill="1"/>
<pad name="A6" x="39.9148" y="2.34" drill="1"/>
<pad name="0" x="42.4548" y="2.34" drill="1"/>
<pad name="1" x="44.9948" y="2.34" drill="1"/>
<pad name="2" x="47.5348" y="2.34" drill="1"/>
<pad name="3" x="50.0748" y="2.34" drill="1"/>
<pad name="4" x="52.6148" y="2.34" drill="1"/>
<pad name="5" x="55.1548" y="2.34" drill="1"/>
<polygon width="0.0254" layer="29">
<vertex x="21.2585" y="3.3687"/>
<vertex x="23.0111" y="3.3687"/>
<vertex x="23.0111" y="1.3113"/>
<vertex x="21.2585" y="1.3113"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="21.2585" y="3.3687"/>
<vertex x="21.2585" y="1.5653"/>
<vertex x="21.8046" y="1.5653" curve="-90"/>
<vertex x="21.9062" y="1.4637"/>
<vertex x="21.3855" y="1.4637" curve="90"/>
<vertex x="21.2585" y="1.3367"/>
<vertex x="21.2585" y="0.4858" curve="90"/>
<vertex x="21.3855" y="0.3588"/>
<vertex x="22.8841" y="0.3588" curve="90"/>
<vertex x="23.0111" y="0.4858"/>
<vertex x="23.0111" y="1.3367" curve="90"/>
<vertex x="22.8841" y="1.4637"/>
<vertex x="22.3634" y="1.4637" curve="-90"/>
<vertex x="22.465" y="1.5653"/>
<vertex x="23.0111" y="1.5653"/>
<vertex x="23.0111" y="3.3687"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="23.7985" y="2.4924" curve="-90"/>
<vertex x="24.6621" y="3.356"/>
<vertex x="24.6875" y="3.356" curve="-90"/>
<vertex x="25.5511" y="2.4924"/>
<vertex x="25.5511" y="2.1876"/>
<vertex x="25.5257" y="1.9971"/>
<vertex x="25.4622" y="1.8066"/>
<vertex x="25.3098" y="1.5907"/>
<vertex x="25.259" y="1.5653"/>
<vertex x="25.005" y="1.5653" curve="90"/>
<vertex x="24.9034" y="1.4637"/>
<vertex x="25.4241" y="1.4637" curve="-90"/>
<vertex x="25.5511" y="1.3367"/>
<vertex x="25.5511" y="0.4858" curve="-90"/>
<vertex x="25.4241" y="0.3588"/>
<vertex x="23.9255" y="0.3588" curve="-90"/>
<vertex x="23.7985" y="0.4858"/>
<vertex x="23.7985" y="1.3367" curve="-90"/>
<vertex x="23.9255" y="1.4637"/>
<vertex x="24.4462" y="1.4637" curve="90"/>
<vertex x="24.3446" y="1.5653"/>
<vertex x="24.0906" y="1.5653"/>
<vertex x="24.0398" y="1.5907"/>
<vertex x="23.8874" y="1.8066"/>
<vertex x="23.7985" y="2.1876"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="23.7985" y="2.4924" curve="-90"/>
<vertex x="24.6621" y="3.356"/>
<vertex x="24.6875" y="3.356" curve="-90"/>
<vertex x="25.5511" y="2.4924"/>
<vertex x="25.5511" y="2.1876"/>
<vertex x="25.5257" y="1.9971"/>
<vertex x="25.4622" y="1.8066"/>
<vertex x="25.3098" y="1.5907"/>
<vertex x="25.259" y="1.5653"/>
<vertex x="25.005" y="1.5653" curve="90"/>
<vertex x="24.9034" y="1.4637"/>
<vertex x="25.4241" y="1.4637" curve="-90"/>
<vertex x="25.5511" y="1.3367"/>
<vertex x="25.5511" y="0.4858" curve="-90"/>
<vertex x="25.4241" y="0.3588"/>
<vertex x="23.9255" y="0.3588" curve="-90"/>
<vertex x="23.7985" y="0.4858"/>
<vertex x="23.7985" y="1.3367" curve="-90"/>
<vertex x="23.9255" y="1.4637"/>
<vertex x="24.4462" y="1.4637" curve="90"/>
<vertex x="24.3446" y="1.5653"/>
<vertex x="24.0906" y="1.5653"/>
<vertex x="24.0398" y="1.5907"/>
<vertex x="23.8874" y="1.8066"/>
<vertex x="23.7985" y="2.1876"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="26.3385" y="2.4924" curve="-90"/>
<vertex x="27.2021" y="3.356"/>
<vertex x="27.2275" y="3.356" curve="-90"/>
<vertex x="28.0911" y="2.4924"/>
<vertex x="28.0911" y="2.1876"/>
<vertex x="28.0657" y="1.9971"/>
<vertex x="28.0022" y="1.8066"/>
<vertex x="27.8498" y="1.5907"/>
<vertex x="27.799" y="1.5653"/>
<vertex x="27.545" y="1.5653" curve="90"/>
<vertex x="27.4434" y="1.4637"/>
<vertex x="27.9641" y="1.4637" curve="-90"/>
<vertex x="28.0911" y="1.3367"/>
<vertex x="28.0911" y="0.4858" curve="-90"/>
<vertex x="27.9641" y="0.3588"/>
<vertex x="26.4655" y="0.3588" curve="-90"/>
<vertex x="26.3385" y="0.4858"/>
<vertex x="26.3385" y="1.3367" curve="-90"/>
<vertex x="26.4655" y="1.4637"/>
<vertex x="26.9862" y="1.4637" curve="90"/>
<vertex x="26.8846" y="1.5653"/>
<vertex x="26.6306" y="1.5653"/>
<vertex x="26.5798" y="1.5907"/>
<vertex x="26.4274" y="1.8066"/>
<vertex x="26.3385" y="2.1876"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="26.3385" y="2.4924" curve="-90"/>
<vertex x="27.2021" y="3.356"/>
<vertex x="27.2275" y="3.356" curve="-90"/>
<vertex x="28.0911" y="2.4924"/>
<vertex x="28.0911" y="2.1876"/>
<vertex x="28.0657" y="1.9971"/>
<vertex x="28.0022" y="1.8066"/>
<vertex x="27.8498" y="1.5907"/>
<vertex x="27.799" y="1.5653"/>
<vertex x="27.545" y="1.5653" curve="90"/>
<vertex x="27.4434" y="1.4637"/>
<vertex x="27.9641" y="1.4637" curve="-90"/>
<vertex x="28.0911" y="1.3367"/>
<vertex x="28.0911" y="0.4858" curve="-90"/>
<vertex x="27.9641" y="0.3588"/>
<vertex x="26.4655" y="0.3588" curve="-90"/>
<vertex x="26.3385" y="0.4858"/>
<vertex x="26.3385" y="1.3367" curve="-90"/>
<vertex x="26.4655" y="1.4637"/>
<vertex x="26.9862" y="1.4637" curve="90"/>
<vertex x="26.8846" y="1.5653"/>
<vertex x="26.6306" y="1.5653"/>
<vertex x="26.5798" y="1.5907"/>
<vertex x="26.4274" y="1.8066"/>
<vertex x="26.3385" y="2.1876"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="28.8785" y="2.4924" curve="-90"/>
<vertex x="29.7421" y="3.356"/>
<vertex x="29.7675" y="3.356" curve="-90"/>
<vertex x="30.6311" y="2.4924"/>
<vertex x="30.6311" y="2.1876"/>
<vertex x="30.6057" y="1.9971"/>
<vertex x="30.5422" y="1.8066"/>
<vertex x="30.3898" y="1.5907"/>
<vertex x="30.339" y="1.5653"/>
<vertex x="30.085" y="1.5653" curve="90"/>
<vertex x="29.9834" y="1.4637"/>
<vertex x="30.5041" y="1.4637" curve="-90"/>
<vertex x="30.6311" y="1.3367"/>
<vertex x="30.6311" y="0.4858" curve="-90"/>
<vertex x="30.5041" y="0.3588"/>
<vertex x="29.0055" y="0.3588" curve="-90"/>
<vertex x="28.8785" y="0.4858"/>
<vertex x="28.8785" y="1.3367" curve="-90"/>
<vertex x="29.0055" y="1.4637"/>
<vertex x="29.5262" y="1.4637" curve="90"/>
<vertex x="29.4246" y="1.5653"/>
<vertex x="29.1706" y="1.5653"/>
<vertex x="29.1198" y="1.5907"/>
<vertex x="28.9674" y="1.8066"/>
<vertex x="28.8785" y="2.1876"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="28.8785" y="2.4924" curve="-90"/>
<vertex x="29.7421" y="3.356"/>
<vertex x="29.7675" y="3.356" curve="-90"/>
<vertex x="30.6311" y="2.4924"/>
<vertex x="30.6311" y="2.1876"/>
<vertex x="30.6057" y="1.9971"/>
<vertex x="30.5422" y="1.8066"/>
<vertex x="30.3898" y="1.5907"/>
<vertex x="30.339" y="1.5653"/>
<vertex x="30.085" y="1.5653" curve="90"/>
<vertex x="29.9834" y="1.4637"/>
<vertex x="30.5041" y="1.4637" curve="-90"/>
<vertex x="30.6311" y="1.3367"/>
<vertex x="30.6311" y="0.4858" curve="-90"/>
<vertex x="30.5041" y="0.3588"/>
<vertex x="29.0055" y="0.3588" curve="-90"/>
<vertex x="28.8785" y="0.4858"/>
<vertex x="28.8785" y="1.3367" curve="-90"/>
<vertex x="29.0055" y="1.4637"/>
<vertex x="29.5262" y="1.4637" curve="90"/>
<vertex x="29.4246" y="1.5653"/>
<vertex x="29.1706" y="1.5653"/>
<vertex x="29.1198" y="1.5907"/>
<vertex x="28.9674" y="1.8066"/>
<vertex x="28.8785" y="2.1876"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="31.4185" y="2.4924" curve="-90"/>
<vertex x="32.2821" y="3.356"/>
<vertex x="32.3075" y="3.356" curve="-90"/>
<vertex x="33.1711" y="2.4924"/>
<vertex x="33.1711" y="2.1876"/>
<vertex x="33.1457" y="1.9971"/>
<vertex x="33.0822" y="1.8066"/>
<vertex x="32.9298" y="1.5907"/>
<vertex x="32.879" y="1.5653"/>
<vertex x="32.625" y="1.5653" curve="90"/>
<vertex x="32.5234" y="1.4637"/>
<vertex x="33.0441" y="1.4637" curve="-90"/>
<vertex x="33.1711" y="1.3367"/>
<vertex x="33.1711" y="0.4858" curve="-90"/>
<vertex x="33.0441" y="0.3588"/>
<vertex x="31.5455" y="0.3588" curve="-90"/>
<vertex x="31.4185" y="0.4858"/>
<vertex x="31.4185" y="1.3367" curve="-90"/>
<vertex x="31.5455" y="1.4637"/>
<vertex x="32.0662" y="1.4637" curve="90"/>
<vertex x="31.9646" y="1.5653"/>
<vertex x="31.7106" y="1.5653"/>
<vertex x="31.6598" y="1.5907"/>
<vertex x="31.5074" y="1.8066"/>
<vertex x="31.4185" y="2.1876"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="31.4185" y="2.4924" curve="-90"/>
<vertex x="32.2821" y="3.356"/>
<vertex x="32.3075" y="3.356" curve="-90"/>
<vertex x="33.1711" y="2.4924"/>
<vertex x="33.1711" y="2.1876"/>
<vertex x="33.1457" y="1.9971"/>
<vertex x="33.0822" y="1.8066"/>
<vertex x="32.9298" y="1.5907"/>
<vertex x="32.879" y="1.5653"/>
<vertex x="32.625" y="1.5653" curve="90"/>
<vertex x="32.5234" y="1.4637"/>
<vertex x="33.0441" y="1.4637" curve="-90"/>
<vertex x="33.1711" y="1.3367"/>
<vertex x="33.1711" y="0.4858" curve="-90"/>
<vertex x="33.0441" y="0.3588"/>
<vertex x="31.5455" y="0.3588" curve="-90"/>
<vertex x="31.4185" y="0.4858"/>
<vertex x="31.4185" y="1.3367" curve="-90"/>
<vertex x="31.5455" y="1.4637"/>
<vertex x="32.0662" y="1.4637" curve="90"/>
<vertex x="31.9646" y="1.5653"/>
<vertex x="31.7106" y="1.5653"/>
<vertex x="31.6598" y="1.5907"/>
<vertex x="31.5074" y="1.8066"/>
<vertex x="31.4185" y="2.1876"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="33.9585" y="2.4924" curve="-90"/>
<vertex x="34.8221" y="3.356"/>
<vertex x="34.8475" y="3.356" curve="-90"/>
<vertex x="35.7111" y="2.4924"/>
<vertex x="35.7111" y="2.1876"/>
<vertex x="35.6857" y="1.9971"/>
<vertex x="35.6222" y="1.8066"/>
<vertex x="35.4698" y="1.5907"/>
<vertex x="35.419" y="1.5653"/>
<vertex x="35.165" y="1.5653" curve="90"/>
<vertex x="35.0634" y="1.4637"/>
<vertex x="35.5841" y="1.4637" curve="-90"/>
<vertex x="35.7111" y="1.3367"/>
<vertex x="35.7111" y="0.4858" curve="-90"/>
<vertex x="35.5841" y="0.3588"/>
<vertex x="34.0855" y="0.3588" curve="-90"/>
<vertex x="33.9585" y="0.4858"/>
<vertex x="33.9585" y="1.3367" curve="-90"/>
<vertex x="34.0855" y="1.4637"/>
<vertex x="34.6062" y="1.4637" curve="90"/>
<vertex x="34.5046" y="1.5653"/>
<vertex x="34.2506" y="1.5653"/>
<vertex x="34.1998" y="1.5907"/>
<vertex x="34.0474" y="1.8066"/>
<vertex x="33.9585" y="2.1876"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="33.9585" y="2.4924" curve="-90"/>
<vertex x="34.8221" y="3.356"/>
<vertex x="34.8475" y="3.356" curve="-90"/>
<vertex x="35.7111" y="2.4924"/>
<vertex x="35.7111" y="2.1876"/>
<vertex x="35.6857" y="1.9971"/>
<vertex x="35.6222" y="1.8066"/>
<vertex x="35.4698" y="1.5907"/>
<vertex x="35.419" y="1.5653"/>
<vertex x="35.165" y="1.5653" curve="90"/>
<vertex x="35.0634" y="1.4637"/>
<vertex x="35.5841" y="1.4637" curve="-90"/>
<vertex x="35.7111" y="1.3367"/>
<vertex x="35.7111" y="0.4858" curve="-90"/>
<vertex x="35.5841" y="0.3588"/>
<vertex x="34.0855" y="0.3588" curve="-90"/>
<vertex x="33.9585" y="0.4858"/>
<vertex x="33.9585" y="1.3367" curve="-90"/>
<vertex x="34.0855" y="1.4637"/>
<vertex x="34.6062" y="1.4637" curve="90"/>
<vertex x="34.5046" y="1.5653"/>
<vertex x="34.2506" y="1.5653"/>
<vertex x="34.1998" y="1.5907"/>
<vertex x="34.0474" y="1.8066"/>
<vertex x="33.9585" y="2.1876"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="36.4985" y="2.4924" curve="-90"/>
<vertex x="37.3621" y="3.356"/>
<vertex x="37.3875" y="3.356" curve="-90"/>
<vertex x="38.2511" y="2.4924"/>
<vertex x="38.2511" y="2.1876"/>
<vertex x="38.2257" y="1.9971"/>
<vertex x="38.1622" y="1.8066"/>
<vertex x="38.0098" y="1.5907"/>
<vertex x="37.959" y="1.5653"/>
<vertex x="37.705" y="1.5653" curve="90"/>
<vertex x="37.6034" y="1.4637"/>
<vertex x="38.1241" y="1.4637" curve="-90"/>
<vertex x="38.2511" y="1.3367"/>
<vertex x="38.2511" y="0.4858" curve="-90"/>
<vertex x="38.1241" y="0.3588"/>
<vertex x="36.6255" y="0.3588" curve="-90"/>
<vertex x="36.4985" y="0.4858"/>
<vertex x="36.4985" y="1.3367" curve="-90"/>
<vertex x="36.6255" y="1.4637"/>
<vertex x="37.1462" y="1.4637" curve="90"/>
<vertex x="37.0446" y="1.5653"/>
<vertex x="36.7906" y="1.5653"/>
<vertex x="36.7398" y="1.5907"/>
<vertex x="36.5874" y="1.8066"/>
<vertex x="36.4985" y="2.1876"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="36.4985" y="2.4924" curve="-90"/>
<vertex x="37.3621" y="3.356"/>
<vertex x="37.3875" y="3.356" curve="-90"/>
<vertex x="38.2511" y="2.4924"/>
<vertex x="38.2511" y="2.1876"/>
<vertex x="38.2257" y="1.9971"/>
<vertex x="38.1622" y="1.8066"/>
<vertex x="38.0098" y="1.5907"/>
<vertex x="37.959" y="1.5653"/>
<vertex x="37.705" y="1.5653" curve="90"/>
<vertex x="37.6034" y="1.4637"/>
<vertex x="38.1241" y="1.4637" curve="-90"/>
<vertex x="38.2511" y="1.3367"/>
<vertex x="38.2511" y="0.4858" curve="-90"/>
<vertex x="38.1241" y="0.3588"/>
<vertex x="36.6255" y="0.3588" curve="-90"/>
<vertex x="36.4985" y="0.4858"/>
<vertex x="36.4985" y="1.3367" curve="-90"/>
<vertex x="36.6255" y="1.4637"/>
<vertex x="37.1462" y="1.4637" curve="90"/>
<vertex x="37.0446" y="1.5653"/>
<vertex x="36.7906" y="1.5653"/>
<vertex x="36.7398" y="1.5907"/>
<vertex x="36.5874" y="1.8066"/>
<vertex x="36.4985" y="2.1876"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="39.0385" y="2.4924" curve="-90"/>
<vertex x="39.9021" y="3.356"/>
<vertex x="39.9275" y="3.356" curve="-90"/>
<vertex x="40.7911" y="2.4924"/>
<vertex x="40.7911" y="2.1876"/>
<vertex x="40.7657" y="1.9971"/>
<vertex x="40.7022" y="1.8066"/>
<vertex x="40.5498" y="1.5907"/>
<vertex x="40.499" y="1.5653"/>
<vertex x="40.245" y="1.5653" curve="90"/>
<vertex x="40.1434" y="1.4637"/>
<vertex x="40.6641" y="1.4637" curve="-90"/>
<vertex x="40.7911" y="1.3367"/>
<vertex x="40.7911" y="0.4858" curve="-90"/>
<vertex x="40.6641" y="0.3588"/>
<vertex x="39.1655" y="0.3588" curve="-90"/>
<vertex x="39.0385" y="0.4858"/>
<vertex x="39.0385" y="1.3367" curve="-90"/>
<vertex x="39.1655" y="1.4637"/>
<vertex x="39.6862" y="1.4637" curve="90"/>
<vertex x="39.5846" y="1.5653"/>
<vertex x="39.3306" y="1.5653"/>
<vertex x="39.2798" y="1.5907"/>
<vertex x="39.1274" y="1.8066"/>
<vertex x="39.0385" y="2.1876"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="39.0385" y="2.4924" curve="-90"/>
<vertex x="39.9021" y="3.356"/>
<vertex x="39.9275" y="3.356" curve="-90"/>
<vertex x="40.7911" y="2.4924"/>
<vertex x="40.7911" y="2.1876"/>
<vertex x="40.7657" y="1.9971"/>
<vertex x="40.7022" y="1.8066"/>
<vertex x="40.5498" y="1.5907"/>
<vertex x="40.499" y="1.5653"/>
<vertex x="40.245" y="1.5653" curve="90"/>
<vertex x="40.1434" y="1.4637"/>
<vertex x="40.6641" y="1.4637" curve="-90"/>
<vertex x="40.7911" y="1.3367"/>
<vertex x="40.7911" y="0.4858" curve="-90"/>
<vertex x="40.6641" y="0.3588"/>
<vertex x="39.1655" y="0.3588" curve="-90"/>
<vertex x="39.0385" y="0.4858"/>
<vertex x="39.0385" y="1.3367" curve="-90"/>
<vertex x="39.1655" y="1.4637"/>
<vertex x="39.6862" y="1.4637" curve="90"/>
<vertex x="39.5846" y="1.5653"/>
<vertex x="39.3306" y="1.5653"/>
<vertex x="39.2798" y="1.5907"/>
<vertex x="39.1274" y="1.8066"/>
<vertex x="39.0385" y="2.1876"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="41.5785" y="2.4924" curve="-90"/>
<vertex x="42.4421" y="3.356"/>
<vertex x="42.4675" y="3.356" curve="-90"/>
<vertex x="43.3311" y="2.4924"/>
<vertex x="43.3311" y="2.1876"/>
<vertex x="43.3057" y="1.9971"/>
<vertex x="43.2422" y="1.8066"/>
<vertex x="43.0898" y="1.5907"/>
<vertex x="43.039" y="1.5653"/>
<vertex x="42.785" y="1.5653" curve="90"/>
<vertex x="42.6834" y="1.4637"/>
<vertex x="43.2041" y="1.4637" curve="-90"/>
<vertex x="43.3311" y="1.3367"/>
<vertex x="43.3311" y="0.4858" curve="-90"/>
<vertex x="43.2041" y="0.3588"/>
<vertex x="41.7055" y="0.3588" curve="-90"/>
<vertex x="41.5785" y="0.4858"/>
<vertex x="41.5785" y="1.3367" curve="-90"/>
<vertex x="41.7055" y="1.4637"/>
<vertex x="42.2262" y="1.4637" curve="90"/>
<vertex x="42.1246" y="1.5653"/>
<vertex x="41.8706" y="1.5653"/>
<vertex x="41.8198" y="1.5907"/>
<vertex x="41.6674" y="1.8066"/>
<vertex x="41.5785" y="2.1876"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="41.5785" y="2.4924" curve="-90"/>
<vertex x="42.4421" y="3.356"/>
<vertex x="42.4675" y="3.356" curve="-90"/>
<vertex x="43.3311" y="2.4924"/>
<vertex x="43.3311" y="2.1876"/>
<vertex x="43.3057" y="1.9971"/>
<vertex x="43.2422" y="1.8066"/>
<vertex x="43.0898" y="1.5907"/>
<vertex x="43.039" y="1.5653"/>
<vertex x="42.785" y="1.5653" curve="90"/>
<vertex x="42.6834" y="1.4637"/>
<vertex x="43.2041" y="1.4637" curve="-90"/>
<vertex x="43.3311" y="1.3367"/>
<vertex x="43.3311" y="0.4858" curve="-90"/>
<vertex x="43.2041" y="0.3588"/>
<vertex x="41.7055" y="0.3588" curve="-90"/>
<vertex x="41.5785" y="0.4858"/>
<vertex x="41.5785" y="1.3367" curve="-90"/>
<vertex x="41.7055" y="1.4637"/>
<vertex x="42.2262" y="1.4637" curve="90"/>
<vertex x="42.1246" y="1.5653"/>
<vertex x="41.8706" y="1.5653"/>
<vertex x="41.8198" y="1.5907"/>
<vertex x="41.6674" y="1.8066"/>
<vertex x="41.5785" y="2.1876"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="44.1185" y="2.4924" curve="-90"/>
<vertex x="44.9821" y="3.356"/>
<vertex x="45.0075" y="3.356" curve="-90"/>
<vertex x="45.8711" y="2.4924"/>
<vertex x="45.8711" y="2.1876"/>
<vertex x="45.8457" y="1.9971"/>
<vertex x="45.7822" y="1.8066"/>
<vertex x="45.6298" y="1.5907"/>
<vertex x="45.579" y="1.5653"/>
<vertex x="45.325" y="1.5653" curve="90"/>
<vertex x="45.2234" y="1.4637"/>
<vertex x="45.7441" y="1.4637" curve="-90"/>
<vertex x="45.8711" y="1.3367"/>
<vertex x="45.8711" y="0.4858" curve="-90"/>
<vertex x="45.7441" y="0.3588"/>
<vertex x="44.2455" y="0.3588" curve="-90"/>
<vertex x="44.1185" y="0.4858"/>
<vertex x="44.1185" y="1.3367" curve="-90"/>
<vertex x="44.2455" y="1.4637"/>
<vertex x="44.7662" y="1.4637" curve="90"/>
<vertex x="44.6646" y="1.5653"/>
<vertex x="44.4106" y="1.5653"/>
<vertex x="44.3598" y="1.5907"/>
<vertex x="44.2074" y="1.8066"/>
<vertex x="44.1185" y="2.1876"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="44.1185" y="2.4924" curve="-90"/>
<vertex x="44.9821" y="3.356"/>
<vertex x="45.0075" y="3.356" curve="-90"/>
<vertex x="45.8711" y="2.4924"/>
<vertex x="45.8711" y="2.1876"/>
<vertex x="45.8457" y="1.9971"/>
<vertex x="45.7822" y="1.8066"/>
<vertex x="45.6298" y="1.5907"/>
<vertex x="45.579" y="1.5653"/>
<vertex x="45.325" y="1.5653" curve="90"/>
<vertex x="45.2234" y="1.4637"/>
<vertex x="45.7441" y="1.4637" curve="-90"/>
<vertex x="45.8711" y="1.3367"/>
<vertex x="45.8711" y="0.4858" curve="-90"/>
<vertex x="45.7441" y="0.3588"/>
<vertex x="44.2455" y="0.3588" curve="-90"/>
<vertex x="44.1185" y="0.4858"/>
<vertex x="44.1185" y="1.3367" curve="-90"/>
<vertex x="44.2455" y="1.4637"/>
<vertex x="44.7662" y="1.4637" curve="90"/>
<vertex x="44.6646" y="1.5653"/>
<vertex x="44.4106" y="1.5653"/>
<vertex x="44.3598" y="1.5907"/>
<vertex x="44.2074" y="1.8066"/>
<vertex x="44.1185" y="2.1876"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="46.6585" y="2.4924" curve="-90"/>
<vertex x="47.5221" y="3.356"/>
<vertex x="47.5475" y="3.356" curve="-90"/>
<vertex x="48.4111" y="2.4924"/>
<vertex x="48.4111" y="2.1876"/>
<vertex x="48.3857" y="1.9971"/>
<vertex x="48.3222" y="1.8066"/>
<vertex x="48.1698" y="1.5907"/>
<vertex x="48.119" y="1.5653"/>
<vertex x="47.865" y="1.5653" curve="90"/>
<vertex x="47.7634" y="1.4637"/>
<vertex x="48.2841" y="1.4637" curve="-90"/>
<vertex x="48.4111" y="1.3367"/>
<vertex x="48.4111" y="0.4858" curve="-90"/>
<vertex x="48.2841" y="0.3588"/>
<vertex x="46.7855" y="0.3588" curve="-90"/>
<vertex x="46.6585" y="0.4858"/>
<vertex x="46.6585" y="1.3367" curve="-90"/>
<vertex x="46.7855" y="1.4637"/>
<vertex x="47.3062" y="1.4637" curve="90"/>
<vertex x="47.2046" y="1.5653"/>
<vertex x="46.9506" y="1.5653"/>
<vertex x="46.8998" y="1.5907"/>
<vertex x="46.7474" y="1.8066"/>
<vertex x="46.6585" y="2.1876"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="46.6585" y="2.4924" curve="-90"/>
<vertex x="47.5221" y="3.356"/>
<vertex x="47.5475" y="3.356" curve="-90"/>
<vertex x="48.4111" y="2.4924"/>
<vertex x="48.4111" y="2.1876"/>
<vertex x="48.3857" y="1.9971"/>
<vertex x="48.3222" y="1.8066"/>
<vertex x="48.1698" y="1.5907"/>
<vertex x="48.119" y="1.5653"/>
<vertex x="47.865" y="1.5653" curve="90"/>
<vertex x="47.7634" y="1.4637"/>
<vertex x="48.2841" y="1.4637" curve="-90"/>
<vertex x="48.4111" y="1.3367"/>
<vertex x="48.4111" y="0.4858" curve="-90"/>
<vertex x="48.2841" y="0.3588"/>
<vertex x="46.7855" y="0.3588" curve="-90"/>
<vertex x="46.6585" y="0.4858"/>
<vertex x="46.6585" y="1.3367" curve="-90"/>
<vertex x="46.7855" y="1.4637"/>
<vertex x="47.3062" y="1.4637" curve="90"/>
<vertex x="47.2046" y="1.5653"/>
<vertex x="46.9506" y="1.5653"/>
<vertex x="46.8998" y="1.5907"/>
<vertex x="46.7474" y="1.8066"/>
<vertex x="46.6585" y="2.1876"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="49.1985" y="2.4924" curve="-90"/>
<vertex x="50.0621" y="3.356"/>
<vertex x="50.0875" y="3.356" curve="-90"/>
<vertex x="50.9511" y="2.4924"/>
<vertex x="50.9511" y="2.1876"/>
<vertex x="50.9257" y="1.9971"/>
<vertex x="50.8622" y="1.8066"/>
<vertex x="50.7098" y="1.5907"/>
<vertex x="50.659" y="1.5653"/>
<vertex x="50.405" y="1.5653" curve="90"/>
<vertex x="50.3034" y="1.4637"/>
<vertex x="50.8241" y="1.4637" curve="-90"/>
<vertex x="50.9511" y="1.3367"/>
<vertex x="50.9511" y="0.4858" curve="-90"/>
<vertex x="50.8241" y="0.3588"/>
<vertex x="49.3255" y="0.3588" curve="-90"/>
<vertex x="49.1985" y="0.4858"/>
<vertex x="49.1985" y="1.3367" curve="-90"/>
<vertex x="49.3255" y="1.4637"/>
<vertex x="49.8462" y="1.4637" curve="90"/>
<vertex x="49.7446" y="1.5653"/>
<vertex x="49.4906" y="1.5653"/>
<vertex x="49.4398" y="1.5907"/>
<vertex x="49.2874" y="1.8066"/>
<vertex x="49.1985" y="2.1876"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="49.1985" y="2.4924" curve="-90"/>
<vertex x="50.0621" y="3.356"/>
<vertex x="50.0875" y="3.356" curve="-90"/>
<vertex x="50.9511" y="2.4924"/>
<vertex x="50.9511" y="2.1876"/>
<vertex x="50.9257" y="1.9971"/>
<vertex x="50.8622" y="1.8066"/>
<vertex x="50.7098" y="1.5907"/>
<vertex x="50.659" y="1.5653"/>
<vertex x="50.405" y="1.5653" curve="90"/>
<vertex x="50.3034" y="1.4637"/>
<vertex x="50.8241" y="1.4637" curve="-90"/>
<vertex x="50.9511" y="1.3367"/>
<vertex x="50.9511" y="0.4858" curve="-90"/>
<vertex x="50.8241" y="0.3588"/>
<vertex x="49.3255" y="0.3588" curve="-90"/>
<vertex x="49.1985" y="0.4858"/>
<vertex x="49.1985" y="1.3367" curve="-90"/>
<vertex x="49.3255" y="1.4637"/>
<vertex x="49.8462" y="1.4637" curve="90"/>
<vertex x="49.7446" y="1.5653"/>
<vertex x="49.4906" y="1.5653"/>
<vertex x="49.4398" y="1.5907"/>
<vertex x="49.2874" y="1.8066"/>
<vertex x="49.1985" y="2.1876"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="51.7385" y="2.4924" curve="-90"/>
<vertex x="52.6021" y="3.356"/>
<vertex x="52.6275" y="3.356" curve="-90"/>
<vertex x="53.4911" y="2.4924"/>
<vertex x="53.4911" y="2.1876"/>
<vertex x="53.4657" y="1.9971"/>
<vertex x="53.4022" y="1.8066"/>
<vertex x="53.2498" y="1.5907"/>
<vertex x="53.199" y="1.5653"/>
<vertex x="52.945" y="1.5653" curve="90"/>
<vertex x="52.8434" y="1.4637"/>
<vertex x="53.3641" y="1.4637" curve="-90"/>
<vertex x="53.4911" y="1.3367"/>
<vertex x="53.4911" y="0.4858" curve="-90"/>
<vertex x="53.3641" y="0.3588"/>
<vertex x="51.8655" y="0.3588" curve="-90"/>
<vertex x="51.7385" y="0.4858"/>
<vertex x="51.7385" y="1.3367" curve="-90"/>
<vertex x="51.8655" y="1.4637"/>
<vertex x="52.3862" y="1.4637" curve="90"/>
<vertex x="52.2846" y="1.5653"/>
<vertex x="52.0306" y="1.5653"/>
<vertex x="51.9798" y="1.5907"/>
<vertex x="51.8274" y="1.8066"/>
<vertex x="51.7385" y="2.1876"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="51.7385" y="2.4924" curve="-90"/>
<vertex x="52.6021" y="3.356"/>
<vertex x="52.6275" y="3.356" curve="-90"/>
<vertex x="53.4911" y="2.4924"/>
<vertex x="53.4911" y="2.1876"/>
<vertex x="53.4657" y="1.9971"/>
<vertex x="53.4022" y="1.8066"/>
<vertex x="53.2498" y="1.5907"/>
<vertex x="53.199" y="1.5653"/>
<vertex x="52.945" y="1.5653" curve="90"/>
<vertex x="52.8434" y="1.4637"/>
<vertex x="53.3641" y="1.4637" curve="-90"/>
<vertex x="53.4911" y="1.3367"/>
<vertex x="53.4911" y="0.4858" curve="-90"/>
<vertex x="53.3641" y="0.3588"/>
<vertex x="51.8655" y="0.3588" curve="-90"/>
<vertex x="51.7385" y="0.4858"/>
<vertex x="51.7385" y="1.3367" curve="-90"/>
<vertex x="51.8655" y="1.4637"/>
<vertex x="52.3862" y="1.4637" curve="90"/>
<vertex x="52.2846" y="1.5653"/>
<vertex x="52.0306" y="1.5653"/>
<vertex x="51.9798" y="1.5907"/>
<vertex x="51.8274" y="1.8066"/>
<vertex x="51.7385" y="2.1876"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="54.2785" y="2.4924" curve="-90"/>
<vertex x="55.1421" y="3.356"/>
<vertex x="55.1675" y="3.356" curve="-90"/>
<vertex x="56.0311" y="2.4924"/>
<vertex x="56.0311" y="2.1876"/>
<vertex x="56.0057" y="1.9971"/>
<vertex x="55.9422" y="1.8066"/>
<vertex x="55.7898" y="1.5907"/>
<vertex x="55.739" y="1.5653"/>
<vertex x="55.485" y="1.5653" curve="90"/>
<vertex x="55.3834" y="1.4637"/>
<vertex x="55.9041" y="1.4637" curve="-90"/>
<vertex x="56.0311" y="1.3367"/>
<vertex x="56.0311" y="0.4858" curve="-90"/>
<vertex x="55.9041" y="0.3588"/>
<vertex x="54.4055" y="0.3588" curve="-90"/>
<vertex x="54.2785" y="0.4858"/>
<vertex x="54.2785" y="1.3367" curve="-90"/>
<vertex x="54.4055" y="1.4637"/>
<vertex x="54.9262" y="1.4637" curve="90"/>
<vertex x="54.8246" y="1.5653"/>
<vertex x="54.5706" y="1.5653"/>
<vertex x="54.5198" y="1.5907"/>
<vertex x="54.3674" y="1.8066"/>
<vertex x="54.2785" y="2.1876"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="54.2785" y="2.4924" curve="-90"/>
<vertex x="55.1421" y="3.356"/>
<vertex x="55.1675" y="3.356" curve="-90"/>
<vertex x="56.0311" y="2.4924"/>
<vertex x="56.0311" y="2.1876"/>
<vertex x="56.0057" y="1.9971"/>
<vertex x="55.9422" y="1.8066"/>
<vertex x="55.7898" y="1.5907"/>
<vertex x="55.739" y="1.5653"/>
<vertex x="55.485" y="1.5653" curve="90"/>
<vertex x="55.3834" y="1.4637"/>
<vertex x="55.9041" y="1.4637" curve="-90"/>
<vertex x="56.0311" y="1.3367"/>
<vertex x="56.0311" y="0.4858" curve="-90"/>
<vertex x="55.9041" y="0.3588"/>
<vertex x="54.4055" y="0.3588" curve="-90"/>
<vertex x="54.2785" y="0.4858"/>
<vertex x="54.2785" y="1.3367" curve="-90"/>
<vertex x="54.4055" y="1.4637"/>
<vertex x="54.9262" y="1.4637" curve="90"/>
<vertex x="54.8246" y="1.5653"/>
<vertex x="54.5706" y="1.5653"/>
<vertex x="54.5198" y="1.5907"/>
<vertex x="54.3674" y="1.8066"/>
<vertex x="54.2785" y="2.1876"/>
</polygon>
<polygon width="0.0254" layer="29">
<vertex x="23.7985" y="2.2003"/>
<vertex x="23.7985" y="2.467" curve="-90"/>
<vertex x="24.6748" y="3.356" curve="-90"/>
<vertex x="25.5511" y="2.467"/>
<vertex x="25.5511" y="2.2003" curve="-90"/>
<vertex x="24.6748" y="1.324" curve="-90"/>
</polygon>
<polygon width="0.0254" layer="29">
<vertex x="26.3385" y="2.2003"/>
<vertex x="26.3385" y="2.467" curve="-90"/>
<vertex x="27.2148" y="3.356" curve="-90"/>
<vertex x="28.0911" y="2.467"/>
<vertex x="28.0911" y="2.2003" curve="-90"/>
<vertex x="27.2148" y="1.324" curve="-90"/>
</polygon>
<polygon width="0.0254" layer="29">
<vertex x="28.8785" y="2.2003"/>
<vertex x="28.8785" y="2.467" curve="-90"/>
<vertex x="29.7548" y="3.356" curve="-90"/>
<vertex x="30.6311" y="2.467"/>
<vertex x="30.6311" y="2.2003" curve="-90"/>
<vertex x="29.7548" y="1.324" curve="-90"/>
</polygon>
<polygon width="0.0254" layer="29">
<vertex x="31.4185" y="2.2003"/>
<vertex x="31.4185" y="2.467" curve="-90"/>
<vertex x="32.2948" y="3.356" curve="-90"/>
<vertex x="33.1711" y="2.467"/>
<vertex x="33.1711" y="2.2003" curve="-90"/>
<vertex x="32.2948" y="1.324" curve="-90"/>
</polygon>
<polygon width="0.0254" layer="29">
<vertex x="33.9585" y="2.2003"/>
<vertex x="33.9585" y="2.467" curve="-90"/>
<vertex x="34.8348" y="3.356" curve="-90"/>
<vertex x="35.7111" y="2.467"/>
<vertex x="35.7111" y="2.2003" curve="-90"/>
<vertex x="34.8348" y="1.324" curve="-90"/>
</polygon>
<polygon width="0.0254" layer="29">
<vertex x="36.4985" y="2.2003"/>
<vertex x="36.4985" y="2.467" curve="-90"/>
<vertex x="37.3748" y="3.356" curve="-90"/>
<vertex x="38.2511" y="2.467"/>
<vertex x="38.2511" y="2.2003" curve="-90"/>
<vertex x="37.3748" y="1.324" curve="-90"/>
</polygon>
<polygon width="0.0254" layer="29">
<vertex x="39.0385" y="2.2003"/>
<vertex x="39.0385" y="2.467" curve="-90"/>
<vertex x="39.9148" y="3.356" curve="-90"/>
<vertex x="40.7911" y="2.467"/>
<vertex x="40.7911" y="2.2003" curve="-90"/>
<vertex x="39.9148" y="1.324" curve="-90"/>
</polygon>
<polygon width="0.0254" layer="29">
<vertex x="41.5785" y="2.2003"/>
<vertex x="41.5785" y="2.467" curve="-90"/>
<vertex x="42.4548" y="3.356" curve="-90"/>
<vertex x="43.3311" y="2.467"/>
<vertex x="43.3311" y="2.2003" curve="-90"/>
<vertex x="42.4548" y="1.324" curve="-90"/>
</polygon>
<polygon width="0.0254" layer="29">
<vertex x="44.1185" y="2.2003"/>
<vertex x="44.1185" y="2.467" curve="-90"/>
<vertex x="44.9948" y="3.356" curve="-90"/>
<vertex x="45.8711" y="2.467"/>
<vertex x="45.8711" y="2.2003" curve="-90"/>
<vertex x="44.9948" y="1.324" curve="-90"/>
</polygon>
<polygon width="0.0254" layer="29">
<vertex x="46.6585" y="2.2003"/>
<vertex x="46.6585" y="2.467" curve="-90"/>
<vertex x="47.5348" y="3.356" curve="-90"/>
<vertex x="48.4111" y="2.467"/>
<vertex x="48.4111" y="2.2003" curve="-90"/>
<vertex x="47.5348" y="1.324" curve="-90"/>
</polygon>
<polygon width="0.0254" layer="29">
<vertex x="49.1985" y="2.2003"/>
<vertex x="49.1985" y="2.467" curve="-90"/>
<vertex x="50.0748" y="3.356" curve="-90"/>
<vertex x="50.9511" y="2.467"/>
<vertex x="50.9511" y="2.2003" curve="-90"/>
<vertex x="50.0748" y="1.324" curve="-90"/>
</polygon>
<polygon width="0.0254" layer="29">
<vertex x="51.7385" y="2.2003"/>
<vertex x="51.7385" y="2.467" curve="-90"/>
<vertex x="52.6148" y="3.356" curve="-90"/>
<vertex x="53.4911" y="2.467"/>
<vertex x="53.4911" y="2.2003" curve="-90"/>
<vertex x="52.6148" y="1.324" curve="-90"/>
</polygon>
<polygon width="0.0254" layer="29">
<vertex x="54.2785" y="2.2003"/>
<vertex x="54.2785" y="2.467" curve="-90"/>
<vertex x="55.1548" y="3.356" curve="-90"/>
<vertex x="56.0311" y="2.467"/>
<vertex x="56.0311" y="2.2003" curve="-90"/>
<vertex x="55.1548" y="1.324" curve="-90"/>
</polygon>
<polygon width="0.127" layer="1">
<vertex x="24.6748" y="3.229" curve="90"/>
<vertex x="23.9255" y="2.4797"/>
<vertex x="23.9255" y="2.1749"/>
<vertex x="23.9509" y="2.0225"/>
<vertex x="24.0017" y="1.8574"/>
<vertex x="24.116" y="1.6923"/>
<vertex x="24.3573" y="1.6923" curve="-90"/>
<vertex x="24.5732" y="1.4764"/>
<vertex x="24.5732" y="1.3494" curve="-90"/>
<vertex x="24.5605" y="1.3367"/>
<vertex x="23.9255" y="1.3367"/>
<vertex x="23.9255" y="0.4858"/>
<vertex x="25.4241" y="0.4858"/>
<vertex x="25.4241" y="1.3367"/>
<vertex x="24.7764" y="1.3367"/>
<vertex x="24.7764" y="1.4764" curve="-90"/>
<vertex x="24.9923" y="1.6923"/>
<vertex x="25.2336" y="1.6923"/>
<vertex x="25.3479" y="1.8574"/>
<vertex x="25.3987" y="2.0225"/>
<vertex x="25.4241" y="2.1749"/>
<vertex x="25.4241" y="2.4797" curve="90"/>
</polygon>
<polygon width="0.127" layer="1">
<vertex x="55.1548" y="3.229" curve="90"/>
<vertex x="54.4055" y="2.4797"/>
<vertex x="54.4055" y="2.1749"/>
<vertex x="54.4309" y="2.0225"/>
<vertex x="54.4817" y="1.8574"/>
<vertex x="54.596" y="1.6923"/>
<vertex x="54.8373" y="1.6923" curve="-90"/>
<vertex x="55.0532" y="1.4764"/>
<vertex x="55.0532" y="1.3494" curve="-90"/>
<vertex x="55.0405" y="1.3367"/>
<vertex x="54.4055" y="1.3367"/>
<vertex x="54.4055" y="0.4858"/>
<vertex x="55.9041" y="0.4858"/>
<vertex x="55.9041" y="1.3367"/>
<vertex x="55.2564" y="1.3367"/>
<vertex x="55.2564" y="1.4764" curve="-90"/>
<vertex x="55.4723" y="1.6923"/>
<vertex x="55.7136" y="1.6923"/>
<vertex x="55.8279" y="1.8574"/>
<vertex x="55.8787" y="2.0225"/>
<vertex x="55.9041" y="2.1749"/>
<vertex x="55.9041" y="2.4797" curve="90"/>
</polygon>
<polygon width="0.127" layer="1">
<vertex x="27.2148" y="3.229" curve="90"/>
<vertex x="26.4655" y="2.4797"/>
<vertex x="26.4655" y="2.1749"/>
<vertex x="26.4909" y="2.0225"/>
<vertex x="26.5417" y="1.8574"/>
<vertex x="26.656" y="1.6923"/>
<vertex x="26.8973" y="1.6923" curve="-90"/>
<vertex x="27.1132" y="1.4764"/>
<vertex x="27.1132" y="1.3494" curve="-90"/>
<vertex x="27.1005" y="1.3367"/>
<vertex x="26.4655" y="1.3367"/>
<vertex x="26.4655" y="0.4858"/>
<vertex x="27.9641" y="0.4858"/>
<vertex x="27.9641" y="1.3367"/>
<vertex x="27.3164" y="1.3367"/>
<vertex x="27.3164" y="1.4764" curve="-90"/>
<vertex x="27.5323" y="1.6923"/>
<vertex x="27.7736" y="1.6923"/>
<vertex x="27.8879" y="1.8574"/>
<vertex x="27.9387" y="2.0225"/>
<vertex x="27.9641" y="2.1749"/>
<vertex x="27.9641" y="2.4797" curve="90"/>
</polygon>
<polygon width="0.127" layer="1">
<vertex x="29.7548" y="3.229" curve="90"/>
<vertex x="29.0055" y="2.4797"/>
<vertex x="29.0055" y="2.1749"/>
<vertex x="29.0309" y="2.0225"/>
<vertex x="29.0817" y="1.8574"/>
<vertex x="29.196" y="1.6923"/>
<vertex x="29.4373" y="1.6923" curve="-90"/>
<vertex x="29.6532" y="1.4764"/>
<vertex x="29.6532" y="1.3494" curve="-90"/>
<vertex x="29.6405" y="1.3367"/>
<vertex x="29.0055" y="1.3367"/>
<vertex x="29.0055" y="0.4858"/>
<vertex x="30.5041" y="0.4858"/>
<vertex x="30.5041" y="1.3367"/>
<vertex x="29.8564" y="1.3367"/>
<vertex x="29.8564" y="1.4764" curve="-90"/>
<vertex x="30.0723" y="1.6923"/>
<vertex x="30.3136" y="1.6923"/>
<vertex x="30.4279" y="1.8574"/>
<vertex x="30.4787" y="2.0225"/>
<vertex x="30.5041" y="2.1749"/>
<vertex x="30.5041" y="2.4797" curve="90"/>
</polygon>
<polygon width="0.127" layer="1">
<vertex x="32.2948" y="3.229" curve="90"/>
<vertex x="31.5455" y="2.4797"/>
<vertex x="31.5455" y="2.1749"/>
<vertex x="31.5709" y="2.0225"/>
<vertex x="31.6217" y="1.8574"/>
<vertex x="31.736" y="1.6923"/>
<vertex x="31.9773" y="1.6923" curve="-90"/>
<vertex x="32.1932" y="1.4764"/>
<vertex x="32.1932" y="1.3494" curve="-90"/>
<vertex x="32.1805" y="1.3367"/>
<vertex x="31.5455" y="1.3367"/>
<vertex x="31.5455" y="0.4858"/>
<vertex x="33.0441" y="0.4858"/>
<vertex x="33.0441" y="1.3367"/>
<vertex x="32.3964" y="1.3367"/>
<vertex x="32.3964" y="1.4764" curve="-90"/>
<vertex x="32.6123" y="1.6923"/>
<vertex x="32.8536" y="1.6923"/>
<vertex x="32.9679" y="1.8574"/>
<vertex x="33.0187" y="2.0225"/>
<vertex x="33.0441" y="2.1749"/>
<vertex x="33.0441" y="2.4797" curve="90"/>
</polygon>
<polygon width="0.127" layer="1">
<vertex x="34.8348" y="3.229" curve="90"/>
<vertex x="34.0855" y="2.4797"/>
<vertex x="34.0855" y="2.1749"/>
<vertex x="34.1109" y="2.0225"/>
<vertex x="34.1617" y="1.8574"/>
<vertex x="34.276" y="1.6923"/>
<vertex x="34.5173" y="1.6923" curve="-90"/>
<vertex x="34.7332" y="1.4764"/>
<vertex x="34.7332" y="1.3494" curve="-90"/>
<vertex x="34.7205" y="1.3367"/>
<vertex x="34.0855" y="1.3367"/>
<vertex x="34.0855" y="0.4858"/>
<vertex x="35.5841" y="0.4858"/>
<vertex x="35.5841" y="1.3367"/>
<vertex x="34.9364" y="1.3367"/>
<vertex x="34.9364" y="1.4764" curve="-90"/>
<vertex x="35.1523" y="1.6923"/>
<vertex x="35.3936" y="1.6923"/>
<vertex x="35.5079" y="1.8574"/>
<vertex x="35.5587" y="2.0225"/>
<vertex x="35.5841" y="2.1749"/>
<vertex x="35.5841" y="2.4797" curve="90"/>
</polygon>
<polygon width="0.127" layer="1">
<vertex x="37.3748" y="3.229" curve="90"/>
<vertex x="36.6255" y="2.4797"/>
<vertex x="36.6255" y="2.1749"/>
<vertex x="36.6509" y="2.0225"/>
<vertex x="36.7017" y="1.8574"/>
<vertex x="36.816" y="1.6923"/>
<vertex x="37.0573" y="1.6923" curve="-90"/>
<vertex x="37.2732" y="1.4764"/>
<vertex x="37.2732" y="1.3494" curve="-90"/>
<vertex x="37.2605" y="1.3367"/>
<vertex x="36.6255" y="1.3367"/>
<vertex x="36.6255" y="0.4858"/>
<vertex x="38.1241" y="0.4858"/>
<vertex x="38.1241" y="1.3367"/>
<vertex x="37.4764" y="1.3367"/>
<vertex x="37.4764" y="1.4764" curve="-90"/>
<vertex x="37.6923" y="1.6923"/>
<vertex x="37.9336" y="1.6923"/>
<vertex x="38.0479" y="1.8574"/>
<vertex x="38.0987" y="2.0225"/>
<vertex x="38.1241" y="2.1749"/>
<vertex x="38.1241" y="2.4797" curve="90"/>
</polygon>
<polygon width="0.127" layer="1">
<vertex x="39.9148" y="3.229" curve="90"/>
<vertex x="39.1655" y="2.4797"/>
<vertex x="39.1655" y="2.1749"/>
<vertex x="39.1909" y="2.0225"/>
<vertex x="39.2417" y="1.8574"/>
<vertex x="39.356" y="1.6923"/>
<vertex x="39.5973" y="1.6923" curve="-90"/>
<vertex x="39.8132" y="1.4764"/>
<vertex x="39.8132" y="1.3494" curve="-90"/>
<vertex x="39.8005" y="1.3367"/>
<vertex x="39.1655" y="1.3367"/>
<vertex x="39.1655" y="0.4858"/>
<vertex x="40.6641" y="0.4858"/>
<vertex x="40.6641" y="1.3367"/>
<vertex x="40.0164" y="1.3367"/>
<vertex x="40.0164" y="1.4764" curve="-90"/>
<vertex x="40.2323" y="1.6923"/>
<vertex x="40.4736" y="1.6923"/>
<vertex x="40.5879" y="1.8574"/>
<vertex x="40.6387" y="2.0225"/>
<vertex x="40.6641" y="2.1749"/>
<vertex x="40.6641" y="2.4797" curve="90"/>
</polygon>
<polygon width="0.127" layer="1">
<vertex x="42.4548" y="3.229" curve="90"/>
<vertex x="41.7055" y="2.4797"/>
<vertex x="41.7055" y="2.1749"/>
<vertex x="41.7309" y="2.0225"/>
<vertex x="41.7817" y="1.8574"/>
<vertex x="41.896" y="1.6923"/>
<vertex x="42.1373" y="1.6923" curve="-90"/>
<vertex x="42.3532" y="1.4764"/>
<vertex x="42.3532" y="1.3494" curve="-90"/>
<vertex x="42.3405" y="1.3367"/>
<vertex x="41.7055" y="1.3367"/>
<vertex x="41.7055" y="0.4858"/>
<vertex x="43.2041" y="0.4858"/>
<vertex x="43.2041" y="1.3367"/>
<vertex x="42.5564" y="1.3367"/>
<vertex x="42.5564" y="1.4764" curve="-90"/>
<vertex x="42.7723" y="1.6923"/>
<vertex x="43.0136" y="1.6923"/>
<vertex x="43.1279" y="1.8574"/>
<vertex x="43.1787" y="2.0225"/>
<vertex x="43.2041" y="2.1749"/>
<vertex x="43.2041" y="2.4797" curve="90"/>
</polygon>
<polygon width="0.127" layer="1">
<vertex x="47.5348" y="3.229" curve="90"/>
<vertex x="46.7855" y="2.4797"/>
<vertex x="46.7855" y="2.1749"/>
<vertex x="46.8109" y="2.0225"/>
<vertex x="46.8617" y="1.8574"/>
<vertex x="46.976" y="1.6923"/>
<vertex x="47.2173" y="1.6923" curve="-90"/>
<vertex x="47.4332" y="1.4764"/>
<vertex x="47.4332" y="1.3494" curve="-90"/>
<vertex x="47.4205" y="1.3367"/>
<vertex x="46.7855" y="1.3367"/>
<vertex x="46.7855" y="0.4858"/>
<vertex x="48.2841" y="0.4858"/>
<vertex x="48.2841" y="1.3367"/>
<vertex x="47.6364" y="1.3367"/>
<vertex x="47.6364" y="1.4764" curve="-90"/>
<vertex x="47.8523" y="1.6923"/>
<vertex x="48.0936" y="1.6923"/>
<vertex x="48.2079" y="1.8574"/>
<vertex x="48.2587" y="2.0225"/>
<vertex x="48.2841" y="2.1749"/>
<vertex x="48.2841" y="2.4797" curve="90"/>
</polygon>
<polygon width="0.127" layer="1">
<vertex x="50.0748" y="3.229" curve="90"/>
<vertex x="49.3255" y="2.4797"/>
<vertex x="49.3255" y="2.1749"/>
<vertex x="49.3509" y="2.0225"/>
<vertex x="49.4017" y="1.8574"/>
<vertex x="49.516" y="1.6923"/>
<vertex x="49.7573" y="1.6923" curve="-90"/>
<vertex x="49.9732" y="1.4764"/>
<vertex x="49.9732" y="1.3494" curve="-90"/>
<vertex x="49.9605" y="1.3367"/>
<vertex x="49.3255" y="1.3367"/>
<vertex x="49.3255" y="0.4858"/>
<vertex x="50.8241" y="0.4858"/>
<vertex x="50.8241" y="1.3367"/>
<vertex x="50.1764" y="1.3367"/>
<vertex x="50.1764" y="1.4764" curve="-90"/>
<vertex x="50.3923" y="1.6923"/>
<vertex x="50.6336" y="1.6923"/>
<vertex x="50.7479" y="1.8574"/>
<vertex x="50.7987" y="2.0225"/>
<vertex x="50.8241" y="2.1749"/>
<vertex x="50.8241" y="2.4797" curve="90"/>
</polygon>
<polygon width="0.127" layer="1">
<vertex x="52.6148" y="3.229" curve="90"/>
<vertex x="51.8655" y="2.4797"/>
<vertex x="51.8655" y="2.1749"/>
<vertex x="51.8909" y="2.0225"/>
<vertex x="51.9417" y="1.8574"/>
<vertex x="52.056" y="1.6923"/>
<vertex x="52.2973" y="1.6923" curve="-90"/>
<vertex x="52.5132" y="1.4764"/>
<vertex x="52.5132" y="1.3494" curve="-90"/>
<vertex x="52.5005" y="1.3367"/>
<vertex x="51.8655" y="1.3367"/>
<vertex x="51.8655" y="0.4858"/>
<vertex x="53.3641" y="0.4858"/>
<vertex x="53.3641" y="1.3367"/>
<vertex x="52.7164" y="1.3367"/>
<vertex x="52.7164" y="1.4764" curve="-90"/>
<vertex x="52.9323" y="1.6923"/>
<vertex x="53.1736" y="1.6923"/>
<vertex x="53.2879" y="1.8574"/>
<vertex x="53.3387" y="2.0225"/>
<vertex x="53.3641" y="2.1749"/>
<vertex x="53.3641" y="2.4797" curve="90"/>
</polygon>
<polygon width="0.127" layer="1">
<vertex x="44.9948" y="3.229" curve="90"/>
<vertex x="44.2455" y="2.4797"/>
<vertex x="44.2455" y="2.1749"/>
<vertex x="44.2709" y="2.0225"/>
<vertex x="44.3217" y="1.8574"/>
<vertex x="44.436" y="1.6923"/>
<vertex x="44.6773" y="1.6923" curve="-90"/>
<vertex x="44.8932" y="1.4764"/>
<vertex x="44.8932" y="1.3494" curve="-90"/>
<vertex x="44.8805" y="1.3367"/>
<vertex x="44.2455" y="1.3367"/>
<vertex x="44.2455" y="0.4858"/>
<vertex x="45.7441" y="0.4858"/>
<vertex x="45.7441" y="1.3367"/>
<vertex x="45.0964" y="1.3367"/>
<vertex x="45.0964" y="1.4764" curve="-90"/>
<vertex x="45.3123" y="1.6923"/>
<vertex x="45.5536" y="1.6923"/>
<vertex x="45.6679" y="1.8574"/>
<vertex x="45.7187" y="2.0225"/>
<vertex x="45.7441" y="2.1749"/>
<vertex x="45.7441" y="2.4797" curve="90"/>
</polygon>
<polygon width="0.127" layer="1">
<vertex x="21.3855" y="3.2417"/>
<vertex x="21.3855" y="1.6923"/>
<vertex x="21.8173" y="1.6923" curve="-90"/>
<vertex x="22.0332" y="1.4764"/>
<vertex x="22.0332" y="1.3494" curve="-90"/>
<vertex x="22.0205" y="1.3367"/>
<vertex x="21.3855" y="1.3367"/>
<vertex x="21.3855" y="0.4858"/>
<vertex x="22.8841" y="0.4858"/>
<vertex x="22.8841" y="1.3367"/>
<vertex x="22.2364" y="1.3367"/>
<vertex x="22.2364" y="1.4764" curve="-90"/>
<vertex x="22.4523" y="1.6923"/>
<vertex x="22.8841" y="1.6923"/>
<vertex x="22.8841" y="3.2417"/>
</polygon>
<polygon width="0.127" layer="16">
<vertex x="24.6748" y="3.229" curve="90"/>
<vertex x="23.9255" y="2.4797"/>
<vertex x="23.9255" y="2.1368" curve="90"/>
<vertex x="24.6748" y="1.3875" curve="90"/>
<vertex x="25.4241" y="2.1368"/>
<vertex x="25.4241" y="2.4797" curve="90"/>
</polygon>
<polygon width="0.127" layer="16">
<vertex x="22.8841" y="3.229"/>
<vertex x="21.3855" y="3.229"/>
<vertex x="21.3855" y="1.4383"/>
<vertex x="22.8841" y="1.4383"/>
</polygon>
<polygon width="0.127" layer="16">
<vertex x="27.2148" y="3.229" curve="90"/>
<vertex x="26.4655" y="2.4797"/>
<vertex x="26.4655" y="2.1368" curve="90"/>
<vertex x="27.2148" y="1.3875" curve="90"/>
<vertex x="27.9641" y="2.1368"/>
<vertex x="27.9641" y="2.4797" curve="90"/>
</polygon>
<polygon width="0.127" layer="16">
<vertex x="29.7548" y="3.229" curve="90"/>
<vertex x="29.0055" y="2.4797"/>
<vertex x="29.0055" y="2.1368" curve="90"/>
<vertex x="29.7548" y="1.3875" curve="90"/>
<vertex x="30.5041" y="2.1368"/>
<vertex x="30.5041" y="2.4797" curve="90"/>
</polygon>
<polygon width="0.127" layer="16">
<vertex x="32.2948" y="3.229" curve="90"/>
<vertex x="31.5455" y="2.4797"/>
<vertex x="31.5455" y="2.1368" curve="90"/>
<vertex x="32.2948" y="1.3875" curve="90"/>
<vertex x="33.0441" y="2.1368"/>
<vertex x="33.0441" y="2.4797" curve="90"/>
</polygon>
<polygon width="0.127" layer="16">
<vertex x="34.8348" y="3.229" curve="90"/>
<vertex x="34.0855" y="2.4797"/>
<vertex x="34.0855" y="2.1368" curve="90"/>
<vertex x="34.8348" y="1.3875" curve="90"/>
<vertex x="35.5841" y="2.1368"/>
<vertex x="35.5841" y="2.4797" curve="90"/>
</polygon>
<polygon width="0.127" layer="16">
<vertex x="37.3748" y="3.229" curve="90"/>
<vertex x="36.6255" y="2.4797"/>
<vertex x="36.6255" y="2.1368" curve="90"/>
<vertex x="37.3748" y="1.3875" curve="90"/>
<vertex x="38.1241" y="2.1368"/>
<vertex x="38.1241" y="2.4797" curve="90"/>
</polygon>
<polygon width="0.127" layer="16">
<vertex x="39.9148" y="3.229" curve="90"/>
<vertex x="39.1655" y="2.4797"/>
<vertex x="39.1655" y="2.1368" curve="90"/>
<vertex x="39.9148" y="1.3875" curve="90"/>
<vertex x="40.6641" y="2.1368"/>
<vertex x="40.6641" y="2.4797" curve="90"/>
</polygon>
<polygon width="0.127" layer="16">
<vertex x="42.4548" y="3.229" curve="90"/>
<vertex x="41.7055" y="2.4797"/>
<vertex x="41.7055" y="2.1368" curve="90"/>
<vertex x="42.4548" y="1.3875" curve="90"/>
<vertex x="43.2041" y="2.1368"/>
<vertex x="43.2041" y="2.4797" curve="90"/>
</polygon>
<polygon width="0.127" layer="16">
<vertex x="44.9948" y="3.229" curve="90"/>
<vertex x="44.2455" y="2.4797"/>
<vertex x="44.2455" y="2.1368" curve="90"/>
<vertex x="44.9948" y="1.3875" curve="90"/>
<vertex x="45.7441" y="2.1368"/>
<vertex x="45.7441" y="2.4797" curve="90"/>
</polygon>
<polygon width="0.127" layer="16">
<vertex x="47.5348" y="3.229" curve="90"/>
<vertex x="46.7855" y="2.4797"/>
<vertex x="46.7855" y="2.1368" curve="90"/>
<vertex x="47.5348" y="1.3875" curve="90"/>
<vertex x="48.2841" y="2.1368"/>
<vertex x="48.2841" y="2.4797" curve="90"/>
</polygon>
<polygon width="0.127" layer="16">
<vertex x="50.0748" y="3.229" curve="90"/>
<vertex x="49.3255" y="2.4797"/>
<vertex x="49.3255" y="2.1368" curve="90"/>
<vertex x="50.0748" y="1.3875" curve="90"/>
<vertex x="50.8241" y="2.1368"/>
<vertex x="50.8241" y="2.4797" curve="90"/>
</polygon>
<polygon width="0.127" layer="16">
<vertex x="52.6148" y="3.229" curve="90"/>
<vertex x="51.8655" y="2.4797"/>
<vertex x="51.8655" y="2.1368" curve="90"/>
<vertex x="52.6148" y="1.3875" curve="90"/>
<vertex x="53.3641" y="2.1368"/>
<vertex x="53.3641" y="2.4797" curve="90"/>
</polygon>
<polygon width="0.127" layer="16">
<vertex x="55.1548" y="3.229" curve="90"/>
<vertex x="54.4055" y="2.4797"/>
<vertex x="54.4055" y="2.1368" curve="90"/>
<vertex x="55.1548" y="1.3875" curve="90"/>
<vertex x="55.9041" y="2.1368"/>
<vertex x="55.9041" y="2.4797" curve="90"/>
</polygon>
<wire x1="55.1652" y1="22.97" x2="55.1652" y2="22.27" width="0.05" layer="39"/>
<pad name="6" x="55.1652" y="22.62" drill="1" rot="R180" first="yes"/>
<pad name="7" x="52.6252" y="22.62" drill="1" rot="R180"/>
<pad name="8" x="50.0852" y="22.62" drill="1" rot="R180"/>
<pad name="9" x="47.5452" y="22.62" drill="1" rot="R180"/>
<pad name="10" x="45.0052" y="22.62" drill="1" rot="R180"/>
<pad name="11" x="42.4652" y="22.62" drill="1" rot="R180"/>
<pad name="12" x="39.9252" y="22.62" drill="1" rot="R180"/>
<pad name="13" x="37.3852" y="22.62" drill="1" rot="R180"/>
<pad name="14" x="34.8452" y="22.62" drill="1" rot="R180"/>
<pad name="RST" x="32.3052" y="22.62" drill="1" rot="R180"/>
<pad name="GND" x="29.7652" y="22.62" drill="1" rot="R180"/>
<pad name="VCC" x="27.2252" y="22.62" drill="1" rot="R180"/>
<pad name="5V" x="24.6852" y="22.62" drill="1" rot="R180"/>
<pad name="VIN" x="22.1452" y="22.62" drill="1" rot="R180"/>
<polygon width="0.0254" layer="29">
<vertex x="56.0415" y="21.5913"/>
<vertex x="54.2889" y="21.5913"/>
<vertex x="54.2889" y="23.6487"/>
<vertex x="56.0415" y="23.6487"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="56.0415" y="21.5913"/>
<vertex x="56.0415" y="23.3947"/>
<vertex x="55.4954" y="23.3947" curve="-90"/>
<vertex x="55.3938" y="23.4963"/>
<vertex x="55.9145" y="23.4963" curve="90"/>
<vertex x="56.0415" y="23.6233"/>
<vertex x="56.0415" y="24.4742" curve="90"/>
<vertex x="55.9145" y="24.6012"/>
<vertex x="54.4159" y="24.6012" curve="90"/>
<vertex x="54.2889" y="24.4742"/>
<vertex x="54.2889" y="23.6233" curve="90"/>
<vertex x="54.4159" y="23.4963"/>
<vertex x="54.9366" y="23.4963" curve="-90"/>
<vertex x="54.835" y="23.3947"/>
<vertex x="54.2889" y="23.3947"/>
<vertex x="54.2889" y="21.5913"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="53.5015" y="22.4676" curve="-90"/>
<vertex x="52.6379" y="21.604"/>
<vertex x="52.6125" y="21.604" curve="-90"/>
<vertex x="51.7489" y="22.4676"/>
<vertex x="51.7489" y="22.7724"/>
<vertex x="51.7743" y="22.9629"/>
<vertex x="51.8378" y="23.1534"/>
<vertex x="51.9902" y="23.3693"/>
<vertex x="52.041" y="23.3947"/>
<vertex x="52.295" y="23.3947" curve="90"/>
<vertex x="52.3966" y="23.4963"/>
<vertex x="51.8759" y="23.4963" curve="-90"/>
<vertex x="51.7489" y="23.6233"/>
<vertex x="51.7489" y="24.4742" curve="-90"/>
<vertex x="51.8759" y="24.6012"/>
<vertex x="53.3745" y="24.6012" curve="-90"/>
<vertex x="53.5015" y="24.4742"/>
<vertex x="53.5015" y="23.6233" curve="-90"/>
<vertex x="53.3745" y="23.4963"/>
<vertex x="52.8538" y="23.4963" curve="90"/>
<vertex x="52.9554" y="23.3947"/>
<vertex x="53.2094" y="23.3947"/>
<vertex x="53.2602" y="23.3693"/>
<vertex x="53.4126" y="23.1534"/>
<vertex x="53.5015" y="22.7724"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="53.5015" y="22.4676" curve="-90"/>
<vertex x="52.6379" y="21.604"/>
<vertex x="52.6125" y="21.604" curve="-90"/>
<vertex x="51.7489" y="22.4676"/>
<vertex x="51.7489" y="22.7724"/>
<vertex x="51.7743" y="22.9629"/>
<vertex x="51.8378" y="23.1534"/>
<vertex x="51.9902" y="23.3693"/>
<vertex x="52.041" y="23.3947"/>
<vertex x="52.295" y="23.3947" curve="90"/>
<vertex x="52.3966" y="23.4963"/>
<vertex x="51.8759" y="23.4963" curve="-90"/>
<vertex x="51.7489" y="23.6233"/>
<vertex x="51.7489" y="24.4742" curve="-90"/>
<vertex x="51.8759" y="24.6012"/>
<vertex x="53.3745" y="24.6012" curve="-90"/>
<vertex x="53.5015" y="24.4742"/>
<vertex x="53.5015" y="23.6233" curve="-90"/>
<vertex x="53.3745" y="23.4963"/>
<vertex x="52.8538" y="23.4963" curve="90"/>
<vertex x="52.9554" y="23.3947"/>
<vertex x="53.2094" y="23.3947"/>
<vertex x="53.2602" y="23.3693"/>
<vertex x="53.4126" y="23.1534"/>
<vertex x="53.5015" y="22.7724"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="50.9615" y="22.4676" curve="-90"/>
<vertex x="50.0979" y="21.604"/>
<vertex x="50.0725" y="21.604" curve="-90"/>
<vertex x="49.2089" y="22.4676"/>
<vertex x="49.2089" y="22.7724"/>
<vertex x="49.2343" y="22.9629"/>
<vertex x="49.2978" y="23.1534"/>
<vertex x="49.4502" y="23.3693"/>
<vertex x="49.501" y="23.3947"/>
<vertex x="49.755" y="23.3947" curve="90"/>
<vertex x="49.8566" y="23.4963"/>
<vertex x="49.3359" y="23.4963" curve="-90"/>
<vertex x="49.2089" y="23.6233"/>
<vertex x="49.2089" y="24.4742" curve="-90"/>
<vertex x="49.3359" y="24.6012"/>
<vertex x="50.8345" y="24.6012" curve="-90"/>
<vertex x="50.9615" y="24.4742"/>
<vertex x="50.9615" y="23.6233" curve="-90"/>
<vertex x="50.8345" y="23.4963"/>
<vertex x="50.3138" y="23.4963" curve="90"/>
<vertex x="50.4154" y="23.3947"/>
<vertex x="50.6694" y="23.3947"/>
<vertex x="50.7202" y="23.3693"/>
<vertex x="50.8726" y="23.1534"/>
<vertex x="50.9615" y="22.7724"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="50.9615" y="22.4676" curve="-90"/>
<vertex x="50.0979" y="21.604"/>
<vertex x="50.0725" y="21.604" curve="-90"/>
<vertex x="49.2089" y="22.4676"/>
<vertex x="49.2089" y="22.7724"/>
<vertex x="49.2343" y="22.9629"/>
<vertex x="49.2978" y="23.1534"/>
<vertex x="49.4502" y="23.3693"/>
<vertex x="49.501" y="23.3947"/>
<vertex x="49.755" y="23.3947" curve="90"/>
<vertex x="49.8566" y="23.4963"/>
<vertex x="49.3359" y="23.4963" curve="-90"/>
<vertex x="49.2089" y="23.6233"/>
<vertex x="49.2089" y="24.4742" curve="-90"/>
<vertex x="49.3359" y="24.6012"/>
<vertex x="50.8345" y="24.6012" curve="-90"/>
<vertex x="50.9615" y="24.4742"/>
<vertex x="50.9615" y="23.6233" curve="-90"/>
<vertex x="50.8345" y="23.4963"/>
<vertex x="50.3138" y="23.4963" curve="90"/>
<vertex x="50.4154" y="23.3947"/>
<vertex x="50.6694" y="23.3947"/>
<vertex x="50.7202" y="23.3693"/>
<vertex x="50.8726" y="23.1534"/>
<vertex x="50.9615" y="22.7724"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="48.4215" y="22.4676" curve="-90"/>
<vertex x="47.5579" y="21.604"/>
<vertex x="47.5325" y="21.604" curve="-90"/>
<vertex x="46.6689" y="22.4676"/>
<vertex x="46.6689" y="22.7724"/>
<vertex x="46.6943" y="22.9629"/>
<vertex x="46.7578" y="23.1534"/>
<vertex x="46.9102" y="23.3693"/>
<vertex x="46.961" y="23.3947"/>
<vertex x="47.215" y="23.3947" curve="90"/>
<vertex x="47.3166" y="23.4963"/>
<vertex x="46.7959" y="23.4963" curve="-90"/>
<vertex x="46.6689" y="23.6233"/>
<vertex x="46.6689" y="24.4742" curve="-90"/>
<vertex x="46.7959" y="24.6012"/>
<vertex x="48.2945" y="24.6012" curve="-90"/>
<vertex x="48.4215" y="24.4742"/>
<vertex x="48.4215" y="23.6233" curve="-90"/>
<vertex x="48.2945" y="23.4963"/>
<vertex x="47.7738" y="23.4963" curve="90"/>
<vertex x="47.8754" y="23.3947"/>
<vertex x="48.1294" y="23.3947"/>
<vertex x="48.1802" y="23.3693"/>
<vertex x="48.3326" y="23.1534"/>
<vertex x="48.4215" y="22.7724"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="48.4215" y="22.4676" curve="-90"/>
<vertex x="47.5579" y="21.604"/>
<vertex x="47.5325" y="21.604" curve="-90"/>
<vertex x="46.6689" y="22.4676"/>
<vertex x="46.6689" y="22.7724"/>
<vertex x="46.6943" y="22.9629"/>
<vertex x="46.7578" y="23.1534"/>
<vertex x="46.9102" y="23.3693"/>
<vertex x="46.961" y="23.3947"/>
<vertex x="47.215" y="23.3947" curve="90"/>
<vertex x="47.3166" y="23.4963"/>
<vertex x="46.7959" y="23.4963" curve="-90"/>
<vertex x="46.6689" y="23.6233"/>
<vertex x="46.6689" y="24.4742" curve="-90"/>
<vertex x="46.7959" y="24.6012"/>
<vertex x="48.2945" y="24.6012" curve="-90"/>
<vertex x="48.4215" y="24.4742"/>
<vertex x="48.4215" y="23.6233" curve="-90"/>
<vertex x="48.2945" y="23.4963"/>
<vertex x="47.7738" y="23.4963" curve="90"/>
<vertex x="47.8754" y="23.3947"/>
<vertex x="48.1294" y="23.3947"/>
<vertex x="48.1802" y="23.3693"/>
<vertex x="48.3326" y="23.1534"/>
<vertex x="48.4215" y="22.7724"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="45.8815" y="22.4676" curve="-90"/>
<vertex x="45.0179" y="21.604"/>
<vertex x="44.9925" y="21.604" curve="-90"/>
<vertex x="44.1289" y="22.4676"/>
<vertex x="44.1289" y="22.7724"/>
<vertex x="44.1543" y="22.9629"/>
<vertex x="44.2178" y="23.1534"/>
<vertex x="44.3702" y="23.3693"/>
<vertex x="44.421" y="23.3947"/>
<vertex x="44.675" y="23.3947" curve="90"/>
<vertex x="44.7766" y="23.4963"/>
<vertex x="44.2559" y="23.4963" curve="-90"/>
<vertex x="44.1289" y="23.6233"/>
<vertex x="44.1289" y="24.4742" curve="-90"/>
<vertex x="44.2559" y="24.6012"/>
<vertex x="45.7545" y="24.6012" curve="-90"/>
<vertex x="45.8815" y="24.4742"/>
<vertex x="45.8815" y="23.6233" curve="-90"/>
<vertex x="45.7545" y="23.4963"/>
<vertex x="45.2338" y="23.4963" curve="90"/>
<vertex x="45.3354" y="23.3947"/>
<vertex x="45.5894" y="23.3947"/>
<vertex x="45.6402" y="23.3693"/>
<vertex x="45.7926" y="23.1534"/>
<vertex x="45.8815" y="22.7724"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="45.8815" y="22.4676" curve="-90"/>
<vertex x="45.0179" y="21.604"/>
<vertex x="44.9925" y="21.604" curve="-90"/>
<vertex x="44.1289" y="22.4676"/>
<vertex x="44.1289" y="22.7724"/>
<vertex x="44.1543" y="22.9629"/>
<vertex x="44.2178" y="23.1534"/>
<vertex x="44.3702" y="23.3693"/>
<vertex x="44.421" y="23.3947"/>
<vertex x="44.675" y="23.3947" curve="90"/>
<vertex x="44.7766" y="23.4963"/>
<vertex x="44.2559" y="23.4963" curve="-90"/>
<vertex x="44.1289" y="23.6233"/>
<vertex x="44.1289" y="24.4742" curve="-90"/>
<vertex x="44.2559" y="24.6012"/>
<vertex x="45.7545" y="24.6012" curve="-90"/>
<vertex x="45.8815" y="24.4742"/>
<vertex x="45.8815" y="23.6233" curve="-90"/>
<vertex x="45.7545" y="23.4963"/>
<vertex x="45.2338" y="23.4963" curve="90"/>
<vertex x="45.3354" y="23.3947"/>
<vertex x="45.5894" y="23.3947"/>
<vertex x="45.6402" y="23.3693"/>
<vertex x="45.7926" y="23.1534"/>
<vertex x="45.8815" y="22.7724"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="43.3415" y="22.4676" curve="-90"/>
<vertex x="42.4779" y="21.604"/>
<vertex x="42.4525" y="21.604" curve="-90"/>
<vertex x="41.5889" y="22.4676"/>
<vertex x="41.5889" y="22.7724"/>
<vertex x="41.6143" y="22.9629"/>
<vertex x="41.6778" y="23.1534"/>
<vertex x="41.8302" y="23.3693"/>
<vertex x="41.881" y="23.3947"/>
<vertex x="42.135" y="23.3947" curve="90"/>
<vertex x="42.2366" y="23.4963"/>
<vertex x="41.7159" y="23.4963" curve="-90"/>
<vertex x="41.5889" y="23.6233"/>
<vertex x="41.5889" y="24.4742" curve="-90"/>
<vertex x="41.7159" y="24.6012"/>
<vertex x="43.2145" y="24.6012" curve="-90"/>
<vertex x="43.3415" y="24.4742"/>
<vertex x="43.3415" y="23.6233" curve="-90"/>
<vertex x="43.2145" y="23.4963"/>
<vertex x="42.6938" y="23.4963" curve="90"/>
<vertex x="42.7954" y="23.3947"/>
<vertex x="43.0494" y="23.3947"/>
<vertex x="43.1002" y="23.3693"/>
<vertex x="43.2526" y="23.1534"/>
<vertex x="43.3415" y="22.7724"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="43.3415" y="22.4676" curve="-90"/>
<vertex x="42.4779" y="21.604"/>
<vertex x="42.4525" y="21.604" curve="-90"/>
<vertex x="41.5889" y="22.4676"/>
<vertex x="41.5889" y="22.7724"/>
<vertex x="41.6143" y="22.9629"/>
<vertex x="41.6778" y="23.1534"/>
<vertex x="41.8302" y="23.3693"/>
<vertex x="41.881" y="23.3947"/>
<vertex x="42.135" y="23.3947" curve="90"/>
<vertex x="42.2366" y="23.4963"/>
<vertex x="41.7159" y="23.4963" curve="-90"/>
<vertex x="41.5889" y="23.6233"/>
<vertex x="41.5889" y="24.4742" curve="-90"/>
<vertex x="41.7159" y="24.6012"/>
<vertex x="43.2145" y="24.6012" curve="-90"/>
<vertex x="43.3415" y="24.4742"/>
<vertex x="43.3415" y="23.6233" curve="-90"/>
<vertex x="43.2145" y="23.4963"/>
<vertex x="42.6938" y="23.4963" curve="90"/>
<vertex x="42.7954" y="23.3947"/>
<vertex x="43.0494" y="23.3947"/>
<vertex x="43.1002" y="23.3693"/>
<vertex x="43.2526" y="23.1534"/>
<vertex x="43.3415" y="22.7724"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="40.8015" y="22.4676" curve="-90"/>
<vertex x="39.9379" y="21.604"/>
<vertex x="39.9125" y="21.604" curve="-90"/>
<vertex x="39.0489" y="22.4676"/>
<vertex x="39.0489" y="22.7724"/>
<vertex x="39.0743" y="22.9629"/>
<vertex x="39.1378" y="23.1534"/>
<vertex x="39.2902" y="23.3693"/>
<vertex x="39.341" y="23.3947"/>
<vertex x="39.595" y="23.3947" curve="90"/>
<vertex x="39.6966" y="23.4963"/>
<vertex x="39.1759" y="23.4963" curve="-90"/>
<vertex x="39.0489" y="23.6233"/>
<vertex x="39.0489" y="24.4742" curve="-90"/>
<vertex x="39.1759" y="24.6012"/>
<vertex x="40.6745" y="24.6012" curve="-90"/>
<vertex x="40.8015" y="24.4742"/>
<vertex x="40.8015" y="23.6233" curve="-90"/>
<vertex x="40.6745" y="23.4963"/>
<vertex x="40.1538" y="23.4963" curve="90"/>
<vertex x="40.2554" y="23.3947"/>
<vertex x="40.5094" y="23.3947"/>
<vertex x="40.5602" y="23.3693"/>
<vertex x="40.7126" y="23.1534"/>
<vertex x="40.8015" y="22.7724"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="40.8015" y="22.4676" curve="-90"/>
<vertex x="39.9379" y="21.604"/>
<vertex x="39.9125" y="21.604" curve="-90"/>
<vertex x="39.0489" y="22.4676"/>
<vertex x="39.0489" y="22.7724"/>
<vertex x="39.0743" y="22.9629"/>
<vertex x="39.1378" y="23.1534"/>
<vertex x="39.2902" y="23.3693"/>
<vertex x="39.341" y="23.3947"/>
<vertex x="39.595" y="23.3947" curve="90"/>
<vertex x="39.6966" y="23.4963"/>
<vertex x="39.1759" y="23.4963" curve="-90"/>
<vertex x="39.0489" y="23.6233"/>
<vertex x="39.0489" y="24.4742" curve="-90"/>
<vertex x="39.1759" y="24.6012"/>
<vertex x="40.6745" y="24.6012" curve="-90"/>
<vertex x="40.8015" y="24.4742"/>
<vertex x="40.8015" y="23.6233" curve="-90"/>
<vertex x="40.6745" y="23.4963"/>
<vertex x="40.1538" y="23.4963" curve="90"/>
<vertex x="40.2554" y="23.3947"/>
<vertex x="40.5094" y="23.3947"/>
<vertex x="40.5602" y="23.3693"/>
<vertex x="40.7126" y="23.1534"/>
<vertex x="40.8015" y="22.7724"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="38.2615" y="22.4676" curve="-90"/>
<vertex x="37.3979" y="21.604"/>
<vertex x="37.3725" y="21.604" curve="-90"/>
<vertex x="36.5089" y="22.4676"/>
<vertex x="36.5089" y="22.7724"/>
<vertex x="36.5343" y="22.9629"/>
<vertex x="36.5978" y="23.1534"/>
<vertex x="36.7502" y="23.3693"/>
<vertex x="36.801" y="23.3947"/>
<vertex x="37.055" y="23.3947" curve="90"/>
<vertex x="37.1566" y="23.4963"/>
<vertex x="36.6359" y="23.4963" curve="-90"/>
<vertex x="36.5089" y="23.6233"/>
<vertex x="36.5089" y="24.4742" curve="-90"/>
<vertex x="36.6359" y="24.6012"/>
<vertex x="38.1345" y="24.6012" curve="-90"/>
<vertex x="38.2615" y="24.4742"/>
<vertex x="38.2615" y="23.6233" curve="-90"/>
<vertex x="38.1345" y="23.4963"/>
<vertex x="37.6138" y="23.4963" curve="90"/>
<vertex x="37.7154" y="23.3947"/>
<vertex x="37.9694" y="23.3947"/>
<vertex x="38.0202" y="23.3693"/>
<vertex x="38.1726" y="23.1534"/>
<vertex x="38.2615" y="22.7724"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="38.2615" y="22.4676" curve="-90"/>
<vertex x="37.3979" y="21.604"/>
<vertex x="37.3725" y="21.604" curve="-90"/>
<vertex x="36.5089" y="22.4676"/>
<vertex x="36.5089" y="22.7724"/>
<vertex x="36.5343" y="22.9629"/>
<vertex x="36.5978" y="23.1534"/>
<vertex x="36.7502" y="23.3693"/>
<vertex x="36.801" y="23.3947"/>
<vertex x="37.055" y="23.3947" curve="90"/>
<vertex x="37.1566" y="23.4963"/>
<vertex x="36.6359" y="23.4963" curve="-90"/>
<vertex x="36.5089" y="23.6233"/>
<vertex x="36.5089" y="24.4742" curve="-90"/>
<vertex x="36.6359" y="24.6012"/>
<vertex x="38.1345" y="24.6012" curve="-90"/>
<vertex x="38.2615" y="24.4742"/>
<vertex x="38.2615" y="23.6233" curve="-90"/>
<vertex x="38.1345" y="23.4963"/>
<vertex x="37.6138" y="23.4963" curve="90"/>
<vertex x="37.7154" y="23.3947"/>
<vertex x="37.9694" y="23.3947"/>
<vertex x="38.0202" y="23.3693"/>
<vertex x="38.1726" y="23.1534"/>
<vertex x="38.2615" y="22.7724"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="35.7215" y="22.4676" curve="-90"/>
<vertex x="34.8579" y="21.604"/>
<vertex x="34.8325" y="21.604" curve="-90"/>
<vertex x="33.9689" y="22.4676"/>
<vertex x="33.9689" y="22.7724"/>
<vertex x="33.9943" y="22.9629"/>
<vertex x="34.0578" y="23.1534"/>
<vertex x="34.2102" y="23.3693"/>
<vertex x="34.261" y="23.3947"/>
<vertex x="34.515" y="23.3947" curve="90"/>
<vertex x="34.6166" y="23.4963"/>
<vertex x="34.0959" y="23.4963" curve="-90"/>
<vertex x="33.9689" y="23.6233"/>
<vertex x="33.9689" y="24.4742" curve="-90"/>
<vertex x="34.0959" y="24.6012"/>
<vertex x="35.5945" y="24.6012" curve="-90"/>
<vertex x="35.7215" y="24.4742"/>
<vertex x="35.7215" y="23.6233" curve="-90"/>
<vertex x="35.5945" y="23.4963"/>
<vertex x="35.0738" y="23.4963" curve="90"/>
<vertex x="35.1754" y="23.3947"/>
<vertex x="35.4294" y="23.3947"/>
<vertex x="35.4802" y="23.3693"/>
<vertex x="35.6326" y="23.1534"/>
<vertex x="35.7215" y="22.7724"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="35.7215" y="22.4676" curve="-90"/>
<vertex x="34.8579" y="21.604"/>
<vertex x="34.8325" y="21.604" curve="-90"/>
<vertex x="33.9689" y="22.4676"/>
<vertex x="33.9689" y="22.7724"/>
<vertex x="33.9943" y="22.9629"/>
<vertex x="34.0578" y="23.1534"/>
<vertex x="34.2102" y="23.3693"/>
<vertex x="34.261" y="23.3947"/>
<vertex x="34.515" y="23.3947" curve="90"/>
<vertex x="34.6166" y="23.4963"/>
<vertex x="34.0959" y="23.4963" curve="-90"/>
<vertex x="33.9689" y="23.6233"/>
<vertex x="33.9689" y="24.4742" curve="-90"/>
<vertex x="34.0959" y="24.6012"/>
<vertex x="35.5945" y="24.6012" curve="-90"/>
<vertex x="35.7215" y="24.4742"/>
<vertex x="35.7215" y="23.6233" curve="-90"/>
<vertex x="35.5945" y="23.4963"/>
<vertex x="35.0738" y="23.4963" curve="90"/>
<vertex x="35.1754" y="23.3947"/>
<vertex x="35.4294" y="23.3947"/>
<vertex x="35.4802" y="23.3693"/>
<vertex x="35.6326" y="23.1534"/>
<vertex x="35.7215" y="22.7724"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="33.1815" y="22.4676" curve="-90"/>
<vertex x="32.3179" y="21.604"/>
<vertex x="32.2925" y="21.604" curve="-90"/>
<vertex x="31.4289" y="22.4676"/>
<vertex x="31.4289" y="22.7724"/>
<vertex x="31.4543" y="22.9629"/>
<vertex x="31.5178" y="23.1534"/>
<vertex x="31.6702" y="23.3693"/>
<vertex x="31.721" y="23.3947"/>
<vertex x="31.975" y="23.3947" curve="90"/>
<vertex x="32.0766" y="23.4963"/>
<vertex x="31.5559" y="23.4963" curve="-90"/>
<vertex x="31.4289" y="23.6233"/>
<vertex x="31.4289" y="24.4742" curve="-90"/>
<vertex x="31.5559" y="24.6012"/>
<vertex x="33.0545" y="24.6012" curve="-90"/>
<vertex x="33.1815" y="24.4742"/>
<vertex x="33.1815" y="23.6233" curve="-90"/>
<vertex x="33.0545" y="23.4963"/>
<vertex x="32.5338" y="23.4963" curve="90"/>
<vertex x="32.6354" y="23.3947"/>
<vertex x="32.8894" y="23.3947"/>
<vertex x="32.9402" y="23.3693"/>
<vertex x="33.0926" y="23.1534"/>
<vertex x="33.1815" y="22.7724"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="33.1815" y="22.4676" curve="-90"/>
<vertex x="32.3179" y="21.604"/>
<vertex x="32.2925" y="21.604" curve="-90"/>
<vertex x="31.4289" y="22.4676"/>
<vertex x="31.4289" y="22.7724"/>
<vertex x="31.4543" y="22.9629"/>
<vertex x="31.5178" y="23.1534"/>
<vertex x="31.6702" y="23.3693"/>
<vertex x="31.721" y="23.3947"/>
<vertex x="31.975" y="23.3947" curve="90"/>
<vertex x="32.0766" y="23.4963"/>
<vertex x="31.5559" y="23.4963" curve="-90"/>
<vertex x="31.4289" y="23.6233"/>
<vertex x="31.4289" y="24.4742" curve="-90"/>
<vertex x="31.5559" y="24.6012"/>
<vertex x="33.0545" y="24.6012" curve="-90"/>
<vertex x="33.1815" y="24.4742"/>
<vertex x="33.1815" y="23.6233" curve="-90"/>
<vertex x="33.0545" y="23.4963"/>
<vertex x="32.5338" y="23.4963" curve="90"/>
<vertex x="32.6354" y="23.3947"/>
<vertex x="32.8894" y="23.3947"/>
<vertex x="32.9402" y="23.3693"/>
<vertex x="33.0926" y="23.1534"/>
<vertex x="33.1815" y="22.7724"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="30.6415" y="22.4676" curve="-90"/>
<vertex x="29.7779" y="21.604"/>
<vertex x="29.7525" y="21.604" curve="-90"/>
<vertex x="28.8889" y="22.4676"/>
<vertex x="28.8889" y="22.7724"/>
<vertex x="28.9143" y="22.9629"/>
<vertex x="28.9778" y="23.1534"/>
<vertex x="29.1302" y="23.3693"/>
<vertex x="29.181" y="23.3947"/>
<vertex x="29.435" y="23.3947" curve="90"/>
<vertex x="29.5366" y="23.4963"/>
<vertex x="29.0159" y="23.4963" curve="-90"/>
<vertex x="28.8889" y="23.6233"/>
<vertex x="28.8889" y="24.4742" curve="-90"/>
<vertex x="29.0159" y="24.6012"/>
<vertex x="30.5145" y="24.6012" curve="-90"/>
<vertex x="30.6415" y="24.4742"/>
<vertex x="30.6415" y="23.6233" curve="-90"/>
<vertex x="30.5145" y="23.4963"/>
<vertex x="29.9938" y="23.4963" curve="90"/>
<vertex x="30.0954" y="23.3947"/>
<vertex x="30.3494" y="23.3947"/>
<vertex x="30.4002" y="23.3693"/>
<vertex x="30.5526" y="23.1534"/>
<vertex x="30.6415" y="22.7724"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="30.6415" y="22.4676" curve="-90"/>
<vertex x="29.7779" y="21.604"/>
<vertex x="29.7525" y="21.604" curve="-90"/>
<vertex x="28.8889" y="22.4676"/>
<vertex x="28.8889" y="22.7724"/>
<vertex x="28.9143" y="22.9629"/>
<vertex x="28.9778" y="23.1534"/>
<vertex x="29.1302" y="23.3693"/>
<vertex x="29.181" y="23.3947"/>
<vertex x="29.435" y="23.3947" curve="90"/>
<vertex x="29.5366" y="23.4963"/>
<vertex x="29.0159" y="23.4963" curve="-90"/>
<vertex x="28.8889" y="23.6233"/>
<vertex x="28.8889" y="24.4742" curve="-90"/>
<vertex x="29.0159" y="24.6012"/>
<vertex x="30.5145" y="24.6012" curve="-90"/>
<vertex x="30.6415" y="24.4742"/>
<vertex x="30.6415" y="23.6233" curve="-90"/>
<vertex x="30.5145" y="23.4963"/>
<vertex x="29.9938" y="23.4963" curve="90"/>
<vertex x="30.0954" y="23.3947"/>
<vertex x="30.3494" y="23.3947"/>
<vertex x="30.4002" y="23.3693"/>
<vertex x="30.5526" y="23.1534"/>
<vertex x="30.6415" y="22.7724"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="28.1015" y="22.4676" curve="-90"/>
<vertex x="27.2379" y="21.604"/>
<vertex x="27.2125" y="21.604" curve="-90"/>
<vertex x="26.3489" y="22.4676"/>
<vertex x="26.3489" y="22.7724"/>
<vertex x="26.3743" y="22.9629"/>
<vertex x="26.4378" y="23.1534"/>
<vertex x="26.5902" y="23.3693"/>
<vertex x="26.641" y="23.3947"/>
<vertex x="26.895" y="23.3947" curve="90"/>
<vertex x="26.9966" y="23.4963"/>
<vertex x="26.4759" y="23.4963" curve="-90"/>
<vertex x="26.3489" y="23.6233"/>
<vertex x="26.3489" y="24.4742" curve="-90"/>
<vertex x="26.4759" y="24.6012"/>
<vertex x="27.9745" y="24.6012" curve="-90"/>
<vertex x="28.1015" y="24.4742"/>
<vertex x="28.1015" y="23.6233" curve="-90"/>
<vertex x="27.9745" y="23.4963"/>
<vertex x="27.4538" y="23.4963" curve="90"/>
<vertex x="27.5554" y="23.3947"/>
<vertex x="27.8094" y="23.3947"/>
<vertex x="27.8602" y="23.3693"/>
<vertex x="28.0126" y="23.1534"/>
<vertex x="28.1015" y="22.7724"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="28.1015" y="22.4676" curve="-90"/>
<vertex x="27.2379" y="21.604"/>
<vertex x="27.2125" y="21.604" curve="-90"/>
<vertex x="26.3489" y="22.4676"/>
<vertex x="26.3489" y="22.7724"/>
<vertex x="26.3743" y="22.9629"/>
<vertex x="26.4378" y="23.1534"/>
<vertex x="26.5902" y="23.3693"/>
<vertex x="26.641" y="23.3947"/>
<vertex x="26.895" y="23.3947" curve="90"/>
<vertex x="26.9966" y="23.4963"/>
<vertex x="26.4759" y="23.4963" curve="-90"/>
<vertex x="26.3489" y="23.6233"/>
<vertex x="26.3489" y="24.4742" curve="-90"/>
<vertex x="26.4759" y="24.6012"/>
<vertex x="27.9745" y="24.6012" curve="-90"/>
<vertex x="28.1015" y="24.4742"/>
<vertex x="28.1015" y="23.6233" curve="-90"/>
<vertex x="27.9745" y="23.4963"/>
<vertex x="27.4538" y="23.4963" curve="90"/>
<vertex x="27.5554" y="23.3947"/>
<vertex x="27.8094" y="23.3947"/>
<vertex x="27.8602" y="23.3693"/>
<vertex x="28.0126" y="23.1534"/>
<vertex x="28.1015" y="22.7724"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="25.5615" y="22.4676" curve="-90"/>
<vertex x="24.6979" y="21.604"/>
<vertex x="24.6725" y="21.604" curve="-90"/>
<vertex x="23.8089" y="22.4676"/>
<vertex x="23.8089" y="22.7724"/>
<vertex x="23.8343" y="22.9629"/>
<vertex x="23.8978" y="23.1534"/>
<vertex x="24.0502" y="23.3693"/>
<vertex x="24.101" y="23.3947"/>
<vertex x="24.355" y="23.3947" curve="90"/>
<vertex x="24.4566" y="23.4963"/>
<vertex x="23.9359" y="23.4963" curve="-90"/>
<vertex x="23.8089" y="23.6233"/>
<vertex x="23.8089" y="24.4742" curve="-90"/>
<vertex x="23.9359" y="24.6012"/>
<vertex x="25.4345" y="24.6012" curve="-90"/>
<vertex x="25.5615" y="24.4742"/>
<vertex x="25.5615" y="23.6233" curve="-90"/>
<vertex x="25.4345" y="23.4963"/>
<vertex x="24.9138" y="23.4963" curve="90"/>
<vertex x="25.0154" y="23.3947"/>
<vertex x="25.2694" y="23.3947"/>
<vertex x="25.3202" y="23.3693"/>
<vertex x="25.4726" y="23.1534"/>
<vertex x="25.5615" y="22.7724"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="25.5615" y="22.4676" curve="-90"/>
<vertex x="24.6979" y="21.604"/>
<vertex x="24.6725" y="21.604" curve="-90"/>
<vertex x="23.8089" y="22.4676"/>
<vertex x="23.8089" y="22.7724"/>
<vertex x="23.8343" y="22.9629"/>
<vertex x="23.8978" y="23.1534"/>
<vertex x="24.0502" y="23.3693"/>
<vertex x="24.101" y="23.3947"/>
<vertex x="24.355" y="23.3947" curve="90"/>
<vertex x="24.4566" y="23.4963"/>
<vertex x="23.9359" y="23.4963" curve="-90"/>
<vertex x="23.8089" y="23.6233"/>
<vertex x="23.8089" y="24.4742" curve="-90"/>
<vertex x="23.9359" y="24.6012"/>
<vertex x="25.4345" y="24.6012" curve="-90"/>
<vertex x="25.5615" y="24.4742"/>
<vertex x="25.5615" y="23.6233" curve="-90"/>
<vertex x="25.4345" y="23.4963"/>
<vertex x="24.9138" y="23.4963" curve="90"/>
<vertex x="25.0154" y="23.3947"/>
<vertex x="25.2694" y="23.3947"/>
<vertex x="25.3202" y="23.3693"/>
<vertex x="25.4726" y="23.1534"/>
<vertex x="25.5615" y="22.7724"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="23.0215" y="22.4676" curve="-90"/>
<vertex x="22.1579" y="21.604"/>
<vertex x="22.1325" y="21.604" curve="-90"/>
<vertex x="21.2689" y="22.4676"/>
<vertex x="21.2689" y="22.7724"/>
<vertex x="21.2943" y="22.9629"/>
<vertex x="21.3578" y="23.1534"/>
<vertex x="21.5102" y="23.3693"/>
<vertex x="21.561" y="23.3947"/>
<vertex x="21.815" y="23.3947" curve="90"/>
<vertex x="21.9166" y="23.4963"/>
<vertex x="21.3959" y="23.4963" curve="-90"/>
<vertex x="21.2689" y="23.6233"/>
<vertex x="21.2689" y="24.4742" curve="-90"/>
<vertex x="21.3959" y="24.6012"/>
<vertex x="22.8945" y="24.6012" curve="-90"/>
<vertex x="23.0215" y="24.4742"/>
<vertex x="23.0215" y="23.6233" curve="-90"/>
<vertex x="22.8945" y="23.4963"/>
<vertex x="22.3738" y="23.4963" curve="90"/>
<vertex x="22.4754" y="23.3947"/>
<vertex x="22.7294" y="23.3947"/>
<vertex x="22.7802" y="23.3693"/>
<vertex x="22.9326" y="23.1534"/>
<vertex x="23.0215" y="22.7724"/>
</polygon>
<polygon width="0.0254" layer="30">
<vertex x="23.0215" y="22.4676" curve="-90"/>
<vertex x="22.1579" y="21.604"/>
<vertex x="22.1325" y="21.604" curve="-90"/>
<vertex x="21.2689" y="22.4676"/>
<vertex x="21.2689" y="22.7724"/>
<vertex x="21.2943" y="22.9629"/>
<vertex x="21.3578" y="23.1534"/>
<vertex x="21.5102" y="23.3693"/>
<vertex x="21.561" y="23.3947"/>
<vertex x="21.815" y="23.3947" curve="90"/>
<vertex x="21.9166" y="23.4963"/>
<vertex x="21.3959" y="23.4963" curve="-90"/>
<vertex x="21.2689" y="23.6233"/>
<vertex x="21.2689" y="24.4742" curve="-90"/>
<vertex x="21.3959" y="24.6012"/>
<vertex x="22.8945" y="24.6012" curve="-90"/>
<vertex x="23.0215" y="24.4742"/>
<vertex x="23.0215" y="23.6233" curve="-90"/>
<vertex x="22.8945" y="23.4963"/>
<vertex x="22.3738" y="23.4963" curve="90"/>
<vertex x="22.4754" y="23.3947"/>
<vertex x="22.7294" y="23.3947"/>
<vertex x="22.7802" y="23.3693"/>
<vertex x="22.9326" y="23.1534"/>
<vertex x="23.0215" y="22.7724"/>
</polygon>
<polygon width="0.0254" layer="29">
<vertex x="53.5015" y="22.7597"/>
<vertex x="53.5015" y="22.493" curve="-90"/>
<vertex x="52.6252" y="21.604" curve="-90"/>
<vertex x="51.7489" y="22.493"/>
<vertex x="51.7489" y="22.7597" curve="-90"/>
<vertex x="52.6252" y="23.636" curve="-90"/>
</polygon>
<polygon width="0.0254" layer="29">
<vertex x="50.9615" y="22.7597"/>
<vertex x="50.9615" y="22.493" curve="-90"/>
<vertex x="50.0852" y="21.604" curve="-90"/>
<vertex x="49.2089" y="22.493"/>
<vertex x="49.2089" y="22.7597" curve="-90"/>
<vertex x="50.0852" y="23.636" curve="-90"/>
</polygon>
<polygon width="0.0254" layer="29">
<vertex x="48.4215" y="22.7597"/>
<vertex x="48.4215" y="22.493" curve="-90"/>
<vertex x="47.5452" y="21.604" curve="-90"/>
<vertex x="46.6689" y="22.493"/>
<vertex x="46.6689" y="22.7597" curve="-90"/>
<vertex x="47.5452" y="23.636" curve="-90"/>
</polygon>
<polygon width="0.0254" layer="29">
<vertex x="45.8815" y="22.7597"/>
<vertex x="45.8815" y="22.493" curve="-90"/>
<vertex x="45.0052" y="21.604" curve="-90"/>
<vertex x="44.1289" y="22.493"/>
<vertex x="44.1289" y="22.7597" curve="-90"/>
<vertex x="45.0052" y="23.636" curve="-90"/>
</polygon>
<polygon width="0.0254" layer="29">
<vertex x="43.3415" y="22.7597"/>
<vertex x="43.3415" y="22.493" curve="-90"/>
<vertex x="42.4652" y="21.604" curve="-90"/>
<vertex x="41.5889" y="22.493"/>
<vertex x="41.5889" y="22.7597" curve="-90"/>
<vertex x="42.4652" y="23.636" curve="-90"/>
</polygon>
<polygon width="0.0254" layer="29">
<vertex x="40.8015" y="22.7597"/>
<vertex x="40.8015" y="22.493" curve="-90"/>
<vertex x="39.9252" y="21.604" curve="-90"/>
<vertex x="39.0489" y="22.493"/>
<vertex x="39.0489" y="22.7597" curve="-90"/>
<vertex x="39.9252" y="23.636" curve="-90"/>
</polygon>
<polygon width="0.0254" layer="29">
<vertex x="38.2615" y="22.7597"/>
<vertex x="38.2615" y="22.493" curve="-90"/>
<vertex x="37.3852" y="21.604" curve="-90"/>
<vertex x="36.5089" y="22.493"/>
<vertex x="36.5089" y="22.7597" curve="-90"/>
<vertex x="37.3852" y="23.636" curve="-90"/>
</polygon>
<polygon width="0.0254" layer="29">
<vertex x="35.7215" y="22.7597"/>
<vertex x="35.7215" y="22.493" curve="-90"/>
<vertex x="34.8452" y="21.604" curve="-90"/>
<vertex x="33.9689" y="22.493"/>
<vertex x="33.9689" y="22.7597" curve="-90"/>
<vertex x="34.8452" y="23.636" curve="-90"/>
</polygon>
<polygon width="0.0254" layer="29">
<vertex x="33.1815" y="22.7597"/>
<vertex x="33.1815" y="22.493" curve="-90"/>
<vertex x="32.3052" y="21.604" curve="-90"/>
<vertex x="31.4289" y="22.493"/>
<vertex x="31.4289" y="22.7597" curve="-90"/>
<vertex x="32.3052" y="23.636" curve="-90"/>
</polygon>
<polygon width="0.0254" layer="29">
<vertex x="30.6415" y="22.7597"/>
<vertex x="30.6415" y="22.493" curve="-90"/>
<vertex x="29.7652" y="21.604" curve="-90"/>
<vertex x="28.8889" y="22.493"/>
<vertex x="28.8889" y="22.7597" curve="-90"/>
<vertex x="29.7652" y="23.636" curve="-90"/>
</polygon>
<polygon width="0.0254" layer="29">
<vertex x="28.1015" y="22.7597"/>
<vertex x="28.1015" y="22.493" curve="-90"/>
<vertex x="27.2252" y="21.604" curve="-90"/>
<vertex x="26.3489" y="22.493"/>
<vertex x="26.3489" y="22.7597" curve="-90"/>
<vertex x="27.2252" y="23.636" curve="-90"/>
</polygon>
<polygon width="0.0254" layer="29">
<vertex x="25.5615" y="22.7597"/>
<vertex x="25.5615" y="22.493" curve="-90"/>
<vertex x="24.6852" y="21.604" curve="-90"/>
<vertex x="23.8089" y="22.493"/>
<vertex x="23.8089" y="22.7597" curve="-90"/>
<vertex x="24.6852" y="23.636" curve="-90"/>
</polygon>
<polygon width="0.0254" layer="29">
<vertex x="23.0215" y="22.7597"/>
<vertex x="23.0215" y="22.493" curve="-90"/>
<vertex x="22.1452" y="21.604" curve="-90"/>
<vertex x="21.2689" y="22.493"/>
<vertex x="21.2689" y="22.7597" curve="-90"/>
<vertex x="22.1452" y="23.636" curve="-90"/>
</polygon>
<polygon width="0.127" layer="1">
<vertex x="52.6252" y="21.731" curve="90"/>
<vertex x="53.3745" y="22.4803"/>
<vertex x="53.3745" y="22.7851"/>
<vertex x="53.3491" y="22.9375"/>
<vertex x="53.2983" y="23.1026"/>
<vertex x="53.184" y="23.2677"/>
<vertex x="52.9427" y="23.2677" curve="-90"/>
<vertex x="52.7268" y="23.4836"/>
<vertex x="52.7268" y="23.6106" curve="-90"/>
<vertex x="52.7395" y="23.6233"/>
<vertex x="53.3745" y="23.6233"/>
<vertex x="53.3745" y="24.4742"/>
<vertex x="51.8759" y="24.4742"/>
<vertex x="51.8759" y="23.6233"/>
<vertex x="52.5236" y="23.6233"/>
<vertex x="52.5236" y="23.4836" curve="-90"/>
<vertex x="52.3077" y="23.2677"/>
<vertex x="52.0664" y="23.2677"/>
<vertex x="51.9521" y="23.1026"/>
<vertex x="51.9013" y="22.9375"/>
<vertex x="51.8759" y="22.7851"/>
<vertex x="51.8759" y="22.4803" curve="90"/>
</polygon>
<polygon width="0.127" layer="1">
<vertex x="22.1452" y="21.731" curve="90"/>
<vertex x="22.8945" y="22.4803"/>
<vertex x="22.8945" y="22.7851"/>
<vertex x="22.8691" y="22.9375"/>
<vertex x="22.8183" y="23.1026"/>
<vertex x="22.704" y="23.2677"/>
<vertex x="22.4627" y="23.2677" curve="-90"/>
<vertex x="22.2468" y="23.4836"/>
<vertex x="22.2468" y="23.6106" curve="-90"/>
<vertex x="22.2595" y="23.6233"/>
<vertex x="22.8945" y="23.6233"/>
<vertex x="22.8945" y="24.4742"/>
<vertex x="21.3959" y="24.4742"/>
<vertex x="21.3959" y="23.6233"/>
<vertex x="22.0436" y="23.6233"/>
<vertex x="22.0436" y="23.4836" curve="-90"/>
<vertex x="21.8277" y="23.2677"/>
<vertex x="21.5864" y="23.2677"/>
<vertex x="21.4721" y="23.1026"/>
<vertex x="21.4213" y="22.9375"/>
<vertex x="21.3959" y="22.7851"/>
<vertex x="21.3959" y="22.4803" curve="90"/>
</polygon>
<polygon width="0.127" layer="1">
<vertex x="50.0852" y="21.731" curve="90"/>
<vertex x="50.8345" y="22.4803"/>
<vertex x="50.8345" y="22.7851"/>
<vertex x="50.8091" y="22.9375"/>
<vertex x="50.7583" y="23.1026"/>
<vertex x="50.644" y="23.2677"/>
<vertex x="50.4027" y="23.2677" curve="-90"/>
<vertex x="50.1868" y="23.4836"/>
<vertex x="50.1868" y="23.6106" curve="-90"/>
<vertex x="50.1995" y="23.6233"/>
<vertex x="50.8345" y="23.6233"/>
<vertex x="50.8345" y="24.4742"/>
<vertex x="49.3359" y="24.4742"/>
<vertex x="49.3359" y="23.6233"/>
<vertex x="49.9836" y="23.6233"/>
<vertex x="49.9836" y="23.4836" curve="-90"/>
<vertex x="49.7677" y="23.2677"/>
<vertex x="49.5264" y="23.2677"/>
<vertex x="49.4121" y="23.1026"/>
<vertex x="49.3613" y="22.9375"/>
<vertex x="49.3359" y="22.7851"/>
<vertex x="49.3359" y="22.4803" curve="90"/>
</polygon>
<polygon width="0.127" layer="1">
<vertex x="47.5452" y="21.731" curve="90"/>
<vertex x="48.2945" y="22.4803"/>
<vertex x="48.2945" y="22.7851"/>
<vertex x="48.2691" y="22.9375"/>
<vertex x="48.2183" y="23.1026"/>
<vertex x="48.104" y="23.2677"/>
<vertex x="47.8627" y="23.2677" curve="-90"/>
<vertex x="47.6468" y="23.4836"/>
<vertex x="47.6468" y="23.6106" curve="-90"/>
<vertex x="47.6595" y="23.6233"/>
<vertex x="48.2945" y="23.6233"/>
<vertex x="48.2945" y="24.4742"/>
<vertex x="46.7959" y="24.4742"/>
<vertex x="46.7959" y="23.6233"/>
<vertex x="47.4436" y="23.6233"/>
<vertex x="47.4436" y="23.4836" curve="-90"/>
<vertex x="47.2277" y="23.2677"/>
<vertex x="46.9864" y="23.2677"/>
<vertex x="46.8721" y="23.1026"/>
<vertex x="46.8213" y="22.9375"/>
<vertex x="46.7959" y="22.7851"/>
<vertex x="46.7959" y="22.4803" curve="90"/>
</polygon>
<polygon width="0.127" layer="1">
<vertex x="45.0052" y="21.731" curve="90"/>
<vertex x="45.7545" y="22.4803"/>
<vertex x="45.7545" y="22.7851"/>
<vertex x="45.7291" y="22.9375"/>
<vertex x="45.6783" y="23.1026"/>
<vertex x="45.564" y="23.2677"/>
<vertex x="45.3227" y="23.2677" curve="-90"/>
<vertex x="45.1068" y="23.4836"/>
<vertex x="45.1068" y="23.6106" curve="-90"/>
<vertex x="45.1195" y="23.6233"/>
<vertex x="45.7545" y="23.6233"/>
<vertex x="45.7545" y="24.4742"/>
<vertex x="44.2559" y="24.4742"/>
<vertex x="44.2559" y="23.6233"/>
<vertex x="44.9036" y="23.6233"/>
<vertex x="44.9036" y="23.4836" curve="-90"/>
<vertex x="44.6877" y="23.2677"/>
<vertex x="44.4464" y="23.2677"/>
<vertex x="44.3321" y="23.1026"/>
<vertex x="44.2813" y="22.9375"/>
<vertex x="44.2559" y="22.7851"/>
<vertex x="44.2559" y="22.4803" curve="90"/>
</polygon>
<polygon width="0.127" layer="1">
<vertex x="42.4652" y="21.731" curve="90"/>
<vertex x="43.2145" y="22.4803"/>
<vertex x="43.2145" y="22.7851"/>
<vertex x="43.1891" y="22.9375"/>
<vertex x="43.1383" y="23.1026"/>
<vertex x="43.024" y="23.2677"/>
<vertex x="42.7827" y="23.2677" curve="-90"/>
<vertex x="42.5668" y="23.4836"/>
<vertex x="42.5668" y="23.6106" curve="-90"/>
<vertex x="42.5795" y="23.6233"/>
<vertex x="43.2145" y="23.6233"/>
<vertex x="43.2145" y="24.4742"/>
<vertex x="41.7159" y="24.4742"/>
<vertex x="41.7159" y="23.6233"/>
<vertex x="42.3636" y="23.6233"/>
<vertex x="42.3636" y="23.4836" curve="-90"/>
<vertex x="42.1477" y="23.2677"/>
<vertex x="41.9064" y="23.2677"/>
<vertex x="41.7921" y="23.1026"/>
<vertex x="41.7413" y="22.9375"/>
<vertex x="41.7159" y="22.7851"/>
<vertex x="41.7159" y="22.4803" curve="90"/>
</polygon>
<polygon width="0.127" layer="1">
<vertex x="39.9252" y="21.731" curve="90"/>
<vertex x="40.6745" y="22.4803"/>
<vertex x="40.6745" y="22.7851"/>
<vertex x="40.6491" y="22.9375"/>
<vertex x="40.5983" y="23.1026"/>
<vertex x="40.484" y="23.2677"/>
<vertex x="40.2427" y="23.2677" curve="-90"/>
<vertex x="40.0268" y="23.4836"/>
<vertex x="40.0268" y="23.6106" curve="-90"/>
<vertex x="40.0395" y="23.6233"/>
<vertex x="40.6745" y="23.6233"/>
<vertex x="40.6745" y="24.4742"/>
<vertex x="39.1759" y="24.4742"/>
<vertex x="39.1759" y="23.6233"/>
<vertex x="39.8236" y="23.6233"/>
<vertex x="39.8236" y="23.4836" curve="-90"/>
<vertex x="39.6077" y="23.2677"/>
<vertex x="39.3664" y="23.2677"/>
<vertex x="39.2521" y="23.1026"/>
<vertex x="39.2013" y="22.9375"/>
<vertex x="39.1759" y="22.7851"/>
<vertex x="39.1759" y="22.4803" curve="90"/>
</polygon>
<polygon width="0.127" layer="1">
<vertex x="37.3852" y="21.731" curve="90"/>
<vertex x="38.1345" y="22.4803"/>
<vertex x="38.1345" y="22.7851"/>
<vertex x="38.1091" y="22.9375"/>
<vertex x="38.0583" y="23.1026"/>
<vertex x="37.944" y="23.2677"/>
<vertex x="37.7027" y="23.2677" curve="-90"/>
<vertex x="37.4868" y="23.4836"/>
<vertex x="37.4868" y="23.6106" curve="-90"/>
<vertex x="37.4995" y="23.6233"/>
<vertex x="38.1345" y="23.6233"/>
<vertex x="38.1345" y="24.4742"/>
<vertex x="36.6359" y="24.4742"/>
<vertex x="36.6359" y="23.6233"/>
<vertex x="37.2836" y="23.6233"/>
<vertex x="37.2836" y="23.4836" curve="-90"/>
<vertex x="37.0677" y="23.2677"/>
<vertex x="36.8264" y="23.2677"/>
<vertex x="36.7121" y="23.1026"/>
<vertex x="36.6613" y="22.9375"/>
<vertex x="36.6359" y="22.7851"/>
<vertex x="36.6359" y="22.4803" curve="90"/>
</polygon>
<polygon width="0.127" layer="1">
<vertex x="34.8452" y="21.731" curve="90"/>
<vertex x="35.5945" y="22.4803"/>
<vertex x="35.5945" y="22.7851"/>
<vertex x="35.5691" y="22.9375"/>
<vertex x="35.5183" y="23.1026"/>
<vertex x="35.404" y="23.2677"/>
<vertex x="35.1627" y="23.2677" curve="-90"/>
<vertex x="34.9468" y="23.4836"/>
<vertex x="34.9468" y="23.6106" curve="-90"/>
<vertex x="34.9595" y="23.6233"/>
<vertex x="35.5945" y="23.6233"/>
<vertex x="35.5945" y="24.4742"/>
<vertex x="34.0959" y="24.4742"/>
<vertex x="34.0959" y="23.6233"/>
<vertex x="34.7436" y="23.6233"/>
<vertex x="34.7436" y="23.4836" curve="-90"/>
<vertex x="34.5277" y="23.2677"/>
<vertex x="34.2864" y="23.2677"/>
<vertex x="34.1721" y="23.1026"/>
<vertex x="34.1213" y="22.9375"/>
<vertex x="34.0959" y="22.7851"/>
<vertex x="34.0959" y="22.4803" curve="90"/>
</polygon>
<polygon width="0.127" layer="1">
<vertex x="29.7652" y="21.731" curve="90"/>
<vertex x="30.5145" y="22.4803"/>
<vertex x="30.5145" y="22.7851"/>
<vertex x="30.4891" y="22.9375"/>
<vertex x="30.4383" y="23.1026"/>
<vertex x="30.324" y="23.2677"/>
<vertex x="30.0827" y="23.2677" curve="-90"/>
<vertex x="29.8668" y="23.4836"/>
<vertex x="29.8668" y="23.6106" curve="-90"/>
<vertex x="29.8795" y="23.6233"/>
<vertex x="30.5145" y="23.6233"/>
<vertex x="30.5145" y="24.4742"/>
<vertex x="29.0159" y="24.4742"/>
<vertex x="29.0159" y="23.6233"/>
<vertex x="29.6636" y="23.6233"/>
<vertex x="29.6636" y="23.4836" curve="-90"/>
<vertex x="29.4477" y="23.2677"/>
<vertex x="29.2064" y="23.2677"/>
<vertex x="29.0921" y="23.1026"/>
<vertex x="29.0413" y="22.9375"/>
<vertex x="29.0159" y="22.7851"/>
<vertex x="29.0159" y="22.4803" curve="90"/>
</polygon>
<polygon width="0.127" layer="1">
<vertex x="27.2252" y="21.731" curve="90"/>
<vertex x="27.9745" y="22.4803"/>
<vertex x="27.9745" y="22.7851"/>
<vertex x="27.9491" y="22.9375"/>
<vertex x="27.8983" y="23.1026"/>
<vertex x="27.784" y="23.2677"/>
<vertex x="27.5427" y="23.2677" curve="-90"/>
<vertex x="27.3268" y="23.4836"/>
<vertex x="27.3268" y="23.6106" curve="-90"/>
<vertex x="27.3395" y="23.6233"/>
<vertex x="27.9745" y="23.6233"/>
<vertex x="27.9745" y="24.4742"/>
<vertex x="26.4759" y="24.4742"/>
<vertex x="26.4759" y="23.6233"/>
<vertex x="27.1236" y="23.6233"/>
<vertex x="27.1236" y="23.4836" curve="-90"/>
<vertex x="26.9077" y="23.2677"/>
<vertex x="26.6664" y="23.2677"/>
<vertex x="26.5521" y="23.1026"/>
<vertex x="26.5013" y="22.9375"/>
<vertex x="26.4759" y="22.7851"/>
<vertex x="26.4759" y="22.4803" curve="90"/>
</polygon>
<polygon width="0.127" layer="1">
<vertex x="24.6852" y="21.731" curve="90"/>
<vertex x="25.4345" y="22.4803"/>
<vertex x="25.4345" y="22.7851"/>
<vertex x="25.4091" y="22.9375"/>
<vertex x="25.3583" y="23.1026"/>
<vertex x="25.244" y="23.2677"/>
<vertex x="25.0027" y="23.2677" curve="-90"/>
<vertex x="24.7868" y="23.4836"/>
<vertex x="24.7868" y="23.6106" curve="-90"/>
<vertex x="24.7995" y="23.6233"/>
<vertex x="25.4345" y="23.6233"/>
<vertex x="25.4345" y="24.4742"/>
<vertex x="23.9359" y="24.4742"/>
<vertex x="23.9359" y="23.6233"/>
<vertex x="24.5836" y="23.6233"/>
<vertex x="24.5836" y="23.4836" curve="-90"/>
<vertex x="24.3677" y="23.2677"/>
<vertex x="24.1264" y="23.2677"/>
<vertex x="24.0121" y="23.1026"/>
<vertex x="23.9613" y="22.9375"/>
<vertex x="23.9359" y="22.7851"/>
<vertex x="23.9359" y="22.4803" curve="90"/>
</polygon>
<polygon width="0.127" layer="1">
<vertex x="32.3052" y="21.731" curve="90"/>
<vertex x="33.0545" y="22.4803"/>
<vertex x="33.0545" y="22.7851"/>
<vertex x="33.0291" y="22.9375"/>
<vertex x="32.9783" y="23.1026"/>
<vertex x="32.864" y="23.2677"/>
<vertex x="32.6227" y="23.2677" curve="-90"/>
<vertex x="32.4068" y="23.4836"/>
<vertex x="32.4068" y="23.6106" curve="-90"/>
<vertex x="32.4195" y="23.6233"/>
<vertex x="33.0545" y="23.6233"/>
<vertex x="33.0545" y="24.4742"/>
<vertex x="31.5559" y="24.4742"/>
<vertex x="31.5559" y="23.6233"/>
<vertex x="32.2036" y="23.6233"/>
<vertex x="32.2036" y="23.4836" curve="-90"/>
<vertex x="31.9877" y="23.2677"/>
<vertex x="31.7464" y="23.2677"/>
<vertex x="31.6321" y="23.1026"/>
<vertex x="31.5813" y="22.9375"/>
<vertex x="31.5559" y="22.7851"/>
<vertex x="31.5559" y="22.4803" curve="90"/>
</polygon>
<polygon width="0.127" layer="1">
<vertex x="55.9145" y="21.7183"/>
<vertex x="55.9145" y="23.2677"/>
<vertex x="55.4827" y="23.2677" curve="-90"/>
<vertex x="55.2668" y="23.4836"/>
<vertex x="55.2668" y="23.6106" curve="-90"/>
<vertex x="55.2795" y="23.6233"/>
<vertex x="55.9145" y="23.6233"/>
<vertex x="55.9145" y="24.4742"/>
<vertex x="54.4159" y="24.4742"/>
<vertex x="54.4159" y="23.6233"/>
<vertex x="55.0636" y="23.6233"/>
<vertex x="55.0636" y="23.4836" curve="-90"/>
<vertex x="54.8477" y="23.2677"/>
<vertex x="54.4159" y="23.2677"/>
<vertex x="54.4159" y="21.7183"/>
</polygon>
<polygon width="0.127" layer="16">
<vertex x="52.6252" y="21.731" curve="90"/>
<vertex x="53.3745" y="22.4803"/>
<vertex x="53.3745" y="22.8232" curve="90"/>
<vertex x="52.6252" y="23.5725" curve="90"/>
<vertex x="51.8759" y="22.8232"/>
<vertex x="51.8759" y="22.4803" curve="90"/>
</polygon>
<polygon width="0.127" layer="16">
<vertex x="54.4159" y="21.731"/>
<vertex x="55.9145" y="21.731"/>
<vertex x="55.9145" y="23.5217"/>
<vertex x="54.4159" y="23.5217"/>
</polygon>
<polygon width="0.127" layer="16">
<vertex x="50.0852" y="21.731" curve="90"/>
<vertex x="50.8345" y="22.4803"/>
<vertex x="50.8345" y="22.8232" curve="90"/>
<vertex x="50.0852" y="23.5725" curve="90"/>
<vertex x="49.3359" y="22.8232"/>
<vertex x="49.3359" y="22.4803" curve="90"/>
</polygon>
<polygon width="0.127" layer="16">
<vertex x="47.5452" y="21.731" curve="90"/>
<vertex x="48.2945" y="22.4803"/>
<vertex x="48.2945" y="22.8232" curve="90"/>
<vertex x="47.5452" y="23.5725" curve="90"/>
<vertex x="46.7959" y="22.8232"/>
<vertex x="46.7959" y="22.4803" curve="90"/>
</polygon>
<polygon width="0.127" layer="16">
<vertex x="45.0052" y="21.731" curve="90"/>
<vertex x="45.7545" y="22.4803"/>
<vertex x="45.7545" y="22.8232" curve="90"/>
<vertex x="45.0052" y="23.5725" curve="90"/>
<vertex x="44.2559" y="22.8232"/>
<vertex x="44.2559" y="22.4803" curve="90"/>
</polygon>
<polygon width="0.127" layer="16">
<vertex x="42.4652" y="21.731" curve="90"/>
<vertex x="43.2145" y="22.4803"/>
<vertex x="43.2145" y="22.8232" curve="90"/>
<vertex x="42.4652" y="23.5725" curve="90"/>
<vertex x="41.7159" y="22.8232"/>
<vertex x="41.7159" y="22.4803" curve="90"/>
</polygon>
<polygon width="0.127" layer="16">
<vertex x="39.9252" y="21.731" curve="90"/>
<vertex x="40.6745" y="22.4803"/>
<vertex x="40.6745" y="22.8232" curve="90"/>
<vertex x="39.9252" y="23.5725" curve="90"/>
<vertex x="39.1759" y="22.8232"/>
<vertex x="39.1759" y="22.4803" curve="90"/>
</polygon>
<polygon width="0.127" layer="16">
<vertex x="37.3852" y="21.731" curve="90"/>
<vertex x="38.1345" y="22.4803"/>
<vertex x="38.1345" y="22.8232" curve="90"/>
<vertex x="37.3852" y="23.5725" curve="90"/>
<vertex x="36.6359" y="22.8232"/>
<vertex x="36.6359" y="22.4803" curve="90"/>
</polygon>
<polygon width="0.127" layer="16">
<vertex x="34.8452" y="21.731" curve="90"/>
<vertex x="35.5945" y="22.4803"/>
<vertex x="35.5945" y="22.8232" curve="90"/>
<vertex x="34.8452" y="23.5725" curve="90"/>
<vertex x="34.0959" y="22.8232"/>
<vertex x="34.0959" y="22.4803" curve="90"/>
</polygon>
<polygon width="0.127" layer="16">
<vertex x="32.3052" y="21.731" curve="90"/>
<vertex x="33.0545" y="22.4803"/>
<vertex x="33.0545" y="22.8232" curve="90"/>
<vertex x="32.3052" y="23.5725" curve="90"/>
<vertex x="31.5559" y="22.8232"/>
<vertex x="31.5559" y="22.4803" curve="90"/>
</polygon>
<polygon width="0.127" layer="16">
<vertex x="29.7652" y="21.731" curve="90"/>
<vertex x="30.5145" y="22.4803"/>
<vertex x="30.5145" y="22.8232" curve="90"/>
<vertex x="29.7652" y="23.5725" curve="90"/>
<vertex x="29.0159" y="22.8232"/>
<vertex x="29.0159" y="22.4803" curve="90"/>
</polygon>
<polygon width="0.127" layer="16">
<vertex x="27.2252" y="21.731" curve="90"/>
<vertex x="27.9745" y="22.4803"/>
<vertex x="27.9745" y="22.8232" curve="90"/>
<vertex x="27.2252" y="23.5725" curve="90"/>
<vertex x="26.4759" y="22.8232"/>
<vertex x="26.4759" y="22.4803" curve="90"/>
</polygon>
<polygon width="0.127" layer="16">
<vertex x="24.6852" y="21.731" curve="90"/>
<vertex x="25.4345" y="22.4803"/>
<vertex x="25.4345" y="22.8232" curve="90"/>
<vertex x="24.6852" y="23.5725" curve="90"/>
<vertex x="23.9359" y="22.8232"/>
<vertex x="23.9359" y="22.4803" curve="90"/>
</polygon>
<polygon width="0.127" layer="16">
<vertex x="22.1452" y="21.731" curve="90"/>
<vertex x="22.8945" y="22.4803"/>
<vertex x="22.8945" y="22.8232" curve="90"/>
<vertex x="22.1452" y="23.5725" curve="90"/>
<vertex x="21.3959" y="22.8232"/>
<vertex x="21.3959" y="22.4803" curve="90"/>
</polygon>
<hole x="2.25" y="2.25" drill="2.25"/>
<hole x="2.25" y="22.75" drill="2.25"/>
<hole x="59.25" y="22.75" drill="2.25"/>
<hole x="59.25" y="2.25" drill="2.25"/>
<wire x1="2.25" y1="0" x2="59.25" y2="0" width="0.1" layer="21"/>
<wire x1="59.25" y1="0" x2="61.5" y2="2.25" width="0.1" layer="21" curve="90"/>
<wire x1="61.5" y1="2.25" x2="61.5" y2="22.75" width="0.1" layer="21"/>
<wire x1="61.5" y1="22.75" x2="59.25" y2="25" width="0.1" layer="21" curve="90"/>
<wire x1="59.25" y1="25" x2="2.25" y2="25" width="0.1" layer="21"/>
<wire x1="2.25" y1="25" x2="0" y2="22.75" width="0.1" layer="21" curve="90"/>
<wire x1="0" y1="22.75" x2="0" y2="2.25" width="0.1" layer="21"/>
<wire x1="0" y1="2.25" x2="2.25" y2="0" width="0.1" layer="21" curve="90"/>
</package>
<package name="MKRBOARD-DIM">
<wire x1="0" y1="2.25" x2="2.25" y2="0" width="0.1" layer="20" curve="90"/>
<wire x1="2.25" y1="0" x2="59.25" y2="0" width="0.1" layer="20"/>
<wire x1="59.25" y1="0" x2="61.5" y2="2.25" width="0.1" layer="20" curve="90"/>
<wire x1="61.5" y1="2.25" x2="61.5" y2="22.75" width="0.1" layer="20"/>
<wire x1="61.5" y1="22.75" x2="59.25" y2="25" width="0.1" layer="20" curve="90"/>
<wire x1="59.25" y1="25" x2="2.25" y2="25" width="0.1" layer="20"/>
<wire x1="2.25" y1="25" x2="0" y2="22.75" width="0.1" layer="20" curve="90"/>
<wire x1="0" y1="22.75" x2="0" y2="2.25" width="0.1" layer="20"/>
<pad name="6" x="55.1548" y="22.66" drill="0.8" diameter="1.524" shape="square"/>
<pad name="AREF" x="22.1348" y="2.34" drill="0.8" diameter="1.524" shape="square"/>
<pad name="7" x="52.6148" y="22.66" drill="0.8" diameter="1.524"/>
<pad name="8" x="50.0748" y="22.66" drill="0.8" diameter="1.524"/>
<pad name="9" x="47.5348" y="22.66" drill="0.8" diameter="1.524"/>
<pad name="10" x="44.9948" y="22.66" drill="0.8" diameter="1.524"/>
<pad name="11" x="42.4548" y="22.66" drill="0.8" diameter="1.524"/>
<pad name="12" x="39.9148" y="22.66" drill="0.8" diameter="1.524"/>
<pad name="13" x="37.3748" y="22.66" drill="0.8" diameter="1.524"/>
<pad name="14" x="34.8348" y="22.66" drill="0.8" diameter="1.524"/>
<pad name="RST" x="32.2948" y="22.66" drill="0.8" diameter="1.524"/>
<pad name="GND" x="29.7548" y="22.66" drill="0.8" diameter="1.524"/>
<pad name="VCC" x="27.2148" y="22.66" drill="0.8" diameter="1.524"/>
<pad name="VIN" x="24.6748" y="22.66" drill="0.8" diameter="1.524"/>
<pad name="5V" x="22.1348" y="22.66" drill="0.8" diameter="1.524"/>
<pad name="A0" x="24.6748" y="2.34" drill="0.8" diameter="1.524"/>
<pad name="A1" x="27.2148" y="2.34" drill="0.8" diameter="1.524"/>
<pad name="A2" x="29.7548" y="2.34" drill="0.8" diameter="1.524"/>
<pad name="A3" x="32.2948" y="2.34" drill="0.8" diameter="1.524"/>
<pad name="A4" x="34.8348" y="2.34" drill="0.8" diameter="1.524"/>
<pad name="A5" x="37.3748" y="2.34" drill="0.8" diameter="1.524"/>
<pad name="A6" x="39.9148" y="2.34" drill="0.8" diameter="1.524"/>
<pad name="0" x="42.4548" y="2.34" drill="0.8" diameter="1.524"/>
<pad name="1" x="44.9948" y="2.34" drill="0.8" diameter="1.524"/>
<pad name="2" x="47.5348" y="2.34" drill="0.8" diameter="1.524"/>
<pad name="3" x="50.0748" y="2.34" drill="0.8" diameter="1.524"/>
<pad name="4" x="52.6148" y="2.34" drill="0.8" diameter="1.524"/>
<pad name="5" x="55.1548" y="2.34" drill="0.8" diameter="1.524"/>
<hole x="59.25" y="2.25" drill="2.25"/>
<hole x="59.25" y="22.75" drill="2.25"/>
<hole x="2.25" y="2.25" drill="2.25"/>
<hole x="2.25" y="22.75" drill="2.25"/>
<text x="22.86" y="3.41" size="0.6096" layer="21" rot="R90">AREF</text>
<text x="25.4" y="3.41" size="0.6096" layer="21" rot="R90">DAC0/A0</text>
<text x="27.94" y="3.41" size="0.6096" layer="21" rot="R90">A1</text>
<text x="30.48" y="3.41" size="0.6096" layer="21" rot="R90">A2</text>
<text x="33.02" y="3.41" size="0.6096" layer="21" rot="R90">A3</text>
<text x="35.56" y="3.41" size="0.6096" layer="21" rot="R90">A4</text>
<text x="38.1" y="3.41" size="0.6096" layer="21" rot="R90">A5</text>
<text x="40.64" y="3.41" size="0.6096" layer="21" rot="R90">A6</text>
<text x="43.18" y="3.41" size="0.6096" layer="21" rot="R90">0</text>
<text x="45.72" y="3.41" size="0.6096" layer="21" rot="R90">1</text>
<text x="48.26" y="3.41" size="0.6096" layer="21" rot="R90">2</text>
<text x="50.8" y="3.41" size="0.6096" layer="21" rot="R90">3</text>
<text x="53.34" y="3.41" size="0.6096" layer="21" rot="R90">4</text>
<text x="55.88" y="3.41" size="0.6096" layer="21" rot="R90">5</text>
<text x="22.86" y="21.59" size="0.6096" layer="21" rot="R270" align="top-left">5V</text>
<text x="25.4" y="21.59" size="0.6096" layer="21" rot="R270" align="top-left">VIN</text>
<text x="27.94" y="21.59" size="0.6096" layer="21" rot="R270" align="top-left">VCC</text>
<text x="30.48" y="21.59" size="0.6096" layer="21" rot="R270" align="top-left">GND</text>
<text x="33.02" y="21.59" size="0.6096" layer="21" rot="R270" align="top-left">RST</text>
<text x="35.56" y="21.59" size="0.6096" layer="21" rot="R270" align="top-left">14/TX</text>
<text x="38.1" y="21.59" size="0.6096" layer="21" rot="R270" align="top-left">13/RX</text>
<text x="40.64" y="21.59" size="0.6096" layer="21" rot="R270" align="top-left">12/SCL</text>
<text x="43.18" y="21.59" size="0.6096" layer="21" rot="R270" align="top-left">11/SDA</text>
<text x="45.72" y="21.59" size="0.6096" layer="21" rot="R270" align="top-left">10/MISO</text>
<text x="48.26" y="21.59" size="0.6096" layer="21" rot="R270" align="top-left">9/SCK</text>
<text x="50.8" y="21.59" size="0.6096" layer="21" rot="R270" align="top-left">8/MOSI</text>
<text x="53.34" y="21.59" size="0.6096" layer="21" rot="R270" align="top-left">7</text>
<text x="55.88" y="21.59" size="0.6096" layer="21" rot="R270" align="top-left">6</text>
<circle x="2.25" y="22.75" radius="2.25" width="0.1" layer="42"/>
<circle x="2.25" y="2.25" radius="2.25" width="0.1" layer="42"/>
<circle x="59.25" y="2.25" radius="2.25" width="0.1" layer="42"/>
<circle x="59.25" y="22.75" radius="2.25" width="0.1" layer="42"/>
<wire x1="2.25" y1="20.5" x2="0" y2="20.5" width="0.1" layer="42"/>
<wire x1="4.5" y1="22.75" x2="4.5" y2="25" width="0.1" layer="42"/>
<wire x1="57" y1="22.75" x2="57" y2="25" width="0.1" layer="42"/>
<wire x1="61.5" y1="20.5" x2="59.25" y2="20.5" width="0.1" layer="42"/>
<wire x1="2.25" y1="4.5" x2="0" y2="4.5" width="0.1" layer="42"/>
<wire x1="4.5" y1="0" x2="4.5" y2="2.25" width="0.1" layer="42"/>
<wire x1="61.5" y1="4.5" x2="59.25" y2="4.5" width="0.1" layer="42"/>
<wire x1="57" y1="0" x2="57" y2="2.25" width="0.1" layer="42"/>
<circle x="2.25" y="22.75" radius="2.25" width="0.1" layer="41"/>
<circle x="2.25" y="2.25" radius="2.25" width="0.1" layer="41"/>
<circle x="59.25" y="2.25" radius="2.25" width="0.1" layer="41"/>
<circle x="59.25" y="22.75" radius="2.25" width="0.1" layer="41"/>
<wire x1="2.25" y1="20.5" x2="0" y2="20.5" width="0.1" layer="41"/>
<wire x1="4.5" y1="22.75" x2="4.5" y2="25" width="0.1" layer="41"/>
<wire x1="57" y1="22.75" x2="57" y2="25" width="0.1" layer="41"/>
<wire x1="61.5" y1="20.5" x2="59.25" y2="20.5" width="0.1" layer="41"/>
<wire x1="2.25" y1="4.5" x2="0" y2="4.5" width="0.1" layer="41"/>
<wire x1="4.5" y1="0" x2="4.5" y2="2.25" width="0.1" layer="41"/>
<wire x1="61.5" y1="4.5" x2="59.25" y2="4.5" width="0.1" layer="41"/>
<wire x1="57" y1="0" x2="57" y2="2.25" width="0.1" layer="41"/>
</package>
<package name="MKRBOARD-DIM-L68MM">
<wire x1="-2.763934375" y1="0" x2="60.763934375" y2="0" width="0.1" layer="20"/>
<wire x1="60.763934375" y1="0" x2="63" y2="2.236065625" width="0.1" layer="20" curve="90"/>
<wire x1="63" y1="2.236065625" x2="63" y2="22.763934375" width="0.1" layer="20"/>
<wire x1="63" y1="22.763934375" x2="60.763934375" y2="25" width="0.1" layer="20" curve="90"/>
<wire x1="60.763934375" y1="25" x2="-2.763934375" y2="25" width="0.1" layer="20"/>
<wire x1="-2.763934375" y1="25" x2="-5" y2="22.763934375" width="0.1" layer="20" curve="90"/>
<wire x1="-5" y1="22.763934375" x2="-5" y2="2.236065625" width="0.1" layer="20"/>
<wire x1="-5" y1="2.236065625" x2="-2.763934375" y2="0" width="0.1" layer="20" curve="90"/>
<pad name="6" x="55.1548" y="22.66" drill="0.8" diameter="1.524" shape="square"/>
<pad name="AREF" x="22.1348" y="2.34" drill="0.8" diameter="1.524" shape="square"/>
<pad name="7" x="52.6148" y="22.66" drill="0.8" diameter="1.524"/>
<pad name="8" x="50.0748" y="22.66" drill="0.8" diameter="1.524"/>
<pad name="9" x="47.5348" y="22.66" drill="0.8" diameter="1.524"/>
<pad name="10" x="44.9948" y="22.66" drill="0.8" diameter="1.524"/>
<pad name="11" x="42.4548" y="22.66" drill="0.8" diameter="1.524"/>
<pad name="12" x="39.9148" y="22.66" drill="0.8" diameter="1.524"/>
<pad name="13" x="37.3748" y="22.66" drill="0.8" diameter="1.524"/>
<pad name="14" x="34.8348" y="22.66" drill="0.8" diameter="1.524"/>
<pad name="RST" x="32.2948" y="22.66" drill="0.8" diameter="1.524"/>
<pad name="GND" x="29.7548" y="22.66" drill="0.8" diameter="1.524"/>
<pad name="VCC" x="27.2148" y="22.66" drill="0.8" diameter="1.524"/>
<pad name="VIN" x="24.6748" y="22.66" drill="0.8" diameter="1.524"/>
<pad name="5V" x="22.1348" y="22.66" drill="0.8" diameter="1.524"/>
<pad name="A0" x="24.6748" y="2.34" drill="0.8" diameter="1.524"/>
<pad name="A1" x="27.2148" y="2.34" drill="0.8" diameter="1.524"/>
<pad name="A2" x="29.7548" y="2.34" drill="0.8" diameter="1.524"/>
<pad name="A3" x="32.2948" y="2.34" drill="0.8" diameter="1.524"/>
<pad name="A4" x="34.8348" y="2.34" drill="0.8" diameter="1.524"/>
<pad name="A5" x="37.3748" y="2.34" drill="0.8" diameter="1.524"/>
<pad name="A6" x="39.9148" y="2.34" drill="0.8" diameter="1.524"/>
<pad name="0" x="42.4548" y="2.34" drill="0.8" diameter="1.524"/>
<pad name="1" x="44.9948" y="2.34" drill="0.8" diameter="1.524"/>
<pad name="2" x="47.5348" y="2.34" drill="0.8" diameter="1.524"/>
<pad name="3" x="50.0748" y="2.34" drill="0.8" diameter="1.524"/>
<pad name="4" x="52.6148" y="2.34" drill="0.8" diameter="1.524"/>
<pad name="5" x="55.1548" y="2.34" drill="0.8" diameter="1.524"/>
<text x="22.86" y="3.41" size="0.6096" layer="21" rot="R90">AREF</text>
<text x="25.4" y="3.41" size="0.6096" layer="21" rot="R90">DAC0/A0</text>
<text x="27.94" y="3.41" size="0.6096" layer="21" rot="R90">A1</text>
<text x="30.48" y="3.41" size="0.6096" layer="21" rot="R90">A2</text>
<text x="33.02" y="3.41" size="0.6096" layer="21" rot="R90">A3</text>
<text x="35.56" y="3.41" size="0.6096" layer="21" rot="R90">A4</text>
<text x="38.1" y="3.41" size="0.6096" layer="21" rot="R90">A5</text>
<text x="40.64" y="3.41" size="0.6096" layer="21" rot="R90">A6</text>
<text x="43.18" y="3.41" size="0.6096" layer="21" rot="R90">0</text>
<text x="45.72" y="3.41" size="0.6096" layer="21" rot="R90">1</text>
<text x="48.26" y="3.41" size="0.6096" layer="21" rot="R90">2</text>
<text x="50.8" y="3.41" size="0.6096" layer="21" rot="R90">3</text>
<text x="53.34" y="3.41" size="0.6096" layer="21" rot="R90">4</text>
<text x="55.88" y="3.41" size="0.6096" layer="21" rot="R90">5</text>
<text x="22.86" y="21.59" size="0.6096" layer="21" rot="R270" align="top-left">5V</text>
<text x="25.4" y="21.59" size="0.6096" layer="21" rot="R270" align="top-left">VIN</text>
<text x="27.94" y="21.59" size="0.6096" layer="21" rot="R270" align="top-left">VCC</text>
<text x="30.48" y="21.59" size="0.6096" layer="21" rot="R270" align="top-left">GND</text>
<text x="33.02" y="21.59" size="0.6096" layer="21" rot="R270" align="top-left">RST</text>
<text x="35.56" y="21.59" size="0.6096" layer="21" rot="R270" align="top-left">14/TX</text>
<text x="38.1" y="21.59" size="0.6096" layer="21" rot="R270" align="top-left">13/RX</text>
<text x="40.64" y="21.59" size="0.6096" layer="21" rot="R270" align="top-left">12/SCL</text>
<text x="43.18" y="21.59" size="0.6096" layer="21" rot="R270" align="top-left">11/SDA</text>
<text x="45.72" y="21.59" size="0.6096" layer="21" rot="R270" align="top-left">10/MISO</text>
<text x="48.26" y="21.59" size="0.6096" layer="21" rot="R270" align="top-left">9/SCK</text>
<text x="50.8" y="21.59" size="0.6096" layer="21" rot="R270" align="top-left">8/MOSI</text>
<text x="53.34" y="21.59" size="0.6096" layer="21" rot="R270" align="top-left">7</text>
<text x="55.88" y="21.59" size="0.6096" layer="21" rot="R270" align="top-left">6</text>
<circle x="2.25" y="22.75" radius="2.25" width="0.1" layer="42"/>
<circle x="2.25" y="2.25" radius="2.25" width="0.1" layer="42"/>
<circle x="59.25" y="2.25" radius="2.25" width="0.1" layer="42"/>
<circle x="59.25" y="22.75" radius="2.25" width="0.1" layer="42"/>
<wire x1="0" y1="22.75" x2="0" y2="25" width="0.1" layer="42"/>
<wire x1="57" y1="22.75" x2="57" y2="25" width="0.1" layer="42"/>
<wire x1="4.5" y1="0" x2="4.5" y2="2.25" width="0.1" layer="42"/>
<wire x1="57" y1="0" x2="57" y2="2.25" width="0.1" layer="42"/>
<circle x="2.25" y="22.75" radius="2.25" width="0.1" layer="41"/>
<circle x="2.25" y="2.25" radius="2.25" width="0.1" layer="41"/>
<circle x="59.25" y="2.25" radius="2.25" width="0.1" layer="41"/>
<circle x="59.25" y="22.75" radius="2.25" width="0.1" layer="41"/>
<wire x1="4.5" y1="22.75" x2="4.5" y2="25" width="0.1" layer="41"/>
<wire x1="57" y1="22.75" x2="57" y2="25" width="0.1" layer="41"/>
<wire x1="4.5" y1="0" x2="4.5" y2="2.25" width="0.1" layer="41"/>
<wire x1="57" y1="0" x2="57" y2="2.25" width="0.1" layer="41"/>
<dimension x1="-5" y1="-5" x2="63" y2="-5" x3="29" y3="-7" textsize="1.27" layer="49"/>
<hole x="59.25" y="2.25" drill="2.25"/>
<hole x="59.25" y="22.75" drill="2.25"/>
<hole x="2.25" y="2.25" drill="2.25"/>
<hole x="2.25" y="22.75" drill="2.25"/>
<wire x1="0" y1="22.75" x2="0" y2="25" width="0.1" layer="41"/>
<wire x1="0" y1="-0.01" x2="0" y2="2.24" width="0.1" layer="42"/>
<wire x1="0" y1="-0.01" x2="0" y2="2.24" width="0.1" layer="41"/>
<wire x1="61.5" y1="0.14" x2="61.5" y2="2.39" width="0.1" layer="42"/>
<wire x1="61.5" y1="0.14" x2="61.5" y2="2.39" width="0.1" layer="41"/>
<wire x1="61.5" y1="22.62" x2="61.5" y2="24.87" width="0.1" layer="42"/>
<wire x1="61.5" y1="22.62" x2="61.5" y2="24.87" width="0.1" layer="41"/>
</package>
<package name="MKRBOARD-NOOUTLINE2">
<pad name="6" x="33.5648" y="22.66" drill="0.8" shape="square"/>
<pad name="AREF" x="0.5448" y="2.34" drill="0.8" shape="square"/>
<pad name="7" x="31.0248" y="22.66" drill="0.8"/>
<pad name="8" x="28.4848" y="22.66" drill="0.8"/>
<pad name="9" x="25.9448" y="22.66" drill="0.8"/>
<pad name="10" x="23.4048" y="22.66" drill="0.8"/>
<pad name="11" x="20.8648" y="22.66" drill="0.8"/>
<pad name="12" x="18.3248" y="22.66" drill="0.8"/>
<pad name="13" x="15.7848" y="22.66" drill="0.8"/>
<pad name="14" x="13.2448" y="22.66" drill="0.8"/>
<pad name="RST" x="10.7048" y="22.66" drill="0.8"/>
<pad name="GND" x="8.1648" y="22.66" drill="0.8"/>
<pad name="VCC" x="5.6248" y="22.66" drill="0.8"/>
<pad name="VIN" x="3.0848" y="22.66" drill="0.8"/>
<pad name="5V" x="0.5448" y="22.66" drill="0.8"/>
<pad name="A0" x="3.0848" y="2.34" drill="0.8"/>
<pad name="A1" x="5.6248" y="2.34" drill="0.8"/>
<pad name="A2" x="8.1648" y="2.34" drill="0.8"/>
<pad name="A3" x="10.7048" y="2.34" drill="0.8"/>
<pad name="A4" x="13.2448" y="2.34" drill="0.8"/>
<pad name="A5" x="15.7848" y="2.34" drill="0.8"/>
<pad name="A6" x="18.3248" y="2.34" drill="0.8"/>
<pad name="0" x="20.8648" y="2.34" drill="0.8"/>
<pad name="1" x="23.4048" y="2.34" drill="0.8"/>
<pad name="2" x="25.9448" y="2.34" drill="0.8"/>
<pad name="3" x="28.4848" y="2.34" drill="0.8"/>
<pad name="4" x="31.0248" y="2.34" drill="0.8"/>
<pad name="5" x="33.5648" y="2.34" drill="0.8"/>
<text x="1.17" y="3.51" size="0.6096" layer="21" rot="R90">AREF</text>
<text x="3.71" y="3.51" size="0.6096" layer="21" rot="R90">DAC0/A0</text>
<text x="6.25" y="3.51" size="0.6096" layer="21" rot="R90">A1</text>
<text x="8.79" y="3.51" size="0.6096" layer="21" rot="R90">A2</text>
<text x="11.33" y="3.51" size="0.6096" layer="21" rot="R90">A3</text>
<text x="13.87" y="3.51" size="0.6096" layer="21" rot="R90">A4</text>
<text x="16.41" y="3.51" size="0.6096" layer="21" rot="R90">A5</text>
<text x="18.95" y="3.51" size="0.6096" layer="21" rot="R90">A6</text>
<text x="21.49" y="3.51" size="0.6096" layer="21" rot="R90">0</text>
<text x="24.03" y="3.51" size="0.6096" layer="21" rot="R90">1</text>
<text x="26.57" y="3.51" size="0.6096" layer="21" rot="R90">2</text>
<text x="29.11" y="3.51" size="0.6096" layer="21" rot="R90">3</text>
<text x="31.65" y="3.51" size="0.6096" layer="21" rot="R90">4</text>
<text x="34.19" y="3.51" size="0.6096" layer="21" rot="R90">5</text>
<text x="1.27" y="21.59" size="0.6096" layer="21" rot="R270" align="top-left">5V</text>
<text x="3.81" y="21.59" size="0.6096" layer="21" rot="R270" align="top-left">VIN</text>
<text x="6.35" y="21.59" size="0.6096" layer="21" rot="R270" align="top-left">VCC</text>
<text x="8.89" y="21.59" size="0.6096" layer="21" rot="R270" align="top-left">GND</text>
<text x="11.43" y="21.59" size="0.6096" layer="21" rot="R270" align="top-left">RST</text>
<text x="13.97" y="21.59" size="0.6096" layer="21" rot="R270" align="top-left">14/TX</text>
<text x="16.51" y="21.59" size="0.6096" layer="21" rot="R270" align="top-left">13/RX</text>
<text x="19.05" y="21.59" size="0.6096" layer="21" rot="R270" align="top-left">12/SCL</text>
<text x="21.59" y="21.59" size="0.6096" layer="21" rot="R270" align="top-left">11/SDA</text>
<text x="24.13" y="21.59" size="0.6096" layer="21" rot="R270" align="top-left">10/MISO</text>
<text x="26.67" y="21.59" size="0.6096" layer="21" rot="R270" align="top-left">9/SCK</text>
<text x="29.21" y="21.59" size="0.6096" layer="21" rot="R270" align="top-left">8/MOSI</text>
<text x="31.75" y="21.59" size="0.6096" layer="21" rot="R270" align="top-left">7</text>
<text x="34.29" y="21.59" size="0.6096" layer="21" rot="R270" align="top-left">6</text>
</package>
<package name="MKRBOARD-4CARRIER">
<wire x1="0" y1="2.25" x2="2.25" y2="0" width="0.1" layer="21" curve="90"/>
<wire x1="2.25" y1="0" x2="59.25" y2="0" width="0.1" layer="21"/>
<wire x1="59.25" y1="0" x2="61.5" y2="2.25" width="0.1" layer="21" curve="90"/>
<wire x1="61.5" y1="2.25" x2="61.5" y2="22.75" width="0.1" layer="21"/>
<wire x1="61.5" y1="22.75" x2="59.25" y2="25" width="0.1" layer="21" curve="90"/>
<wire x1="59.25" y1="25" x2="2.25" y2="25" width="0.1" layer="21"/>
<wire x1="2.25" y1="25" x2="0" y2="22.75" width="0.1" layer="21" curve="90"/>
<wire x1="0" y1="22.75" x2="0" y2="2.25" width="0.1" layer="21"/>
<pad name="6" x="55.1548" y="22.66" drill="0.8" diameter="1.524" shape="square"/>
<pad name="AREF" x="22.1348" y="2.34" drill="0.8" diameter="1.524" shape="square"/>
<pad name="7" x="52.6148" y="22.66" drill="0.8" diameter="1.524"/>
<pad name="8" x="50.0748" y="22.66" drill="0.8" diameter="1.524"/>
<pad name="9" x="47.5348" y="22.66" drill="0.8" diameter="1.524"/>
<pad name="10" x="44.9948" y="22.66" drill="0.8" diameter="1.524"/>
<pad name="11" x="42.4548" y="22.66" drill="0.8" diameter="1.524"/>
<pad name="12" x="39.9148" y="22.66" drill="0.8" diameter="1.524"/>
<pad name="13" x="37.3748" y="22.66" drill="0.8" diameter="1.524"/>
<pad name="14" x="34.8348" y="22.66" drill="0.8" diameter="1.524"/>
<pad name="RST" x="32.2948" y="22.66" drill="0.8" diameter="1.524"/>
<pad name="GND" x="29.7548" y="22.66" drill="0.8" diameter="1.524"/>
<pad name="VCC" x="27.2148" y="22.66" drill="0.8" diameter="1.524"/>
<pad name="VIN" x="24.6748" y="22.66" drill="0.8" diameter="1.524"/>
<pad name="5V" x="22.1348" y="22.66" drill="0.8" diameter="1.524"/>
<pad name="A0" x="24.6748" y="2.34" drill="0.8" diameter="1.524"/>
<pad name="A1" x="27.2148" y="2.34" drill="0.8" diameter="1.524"/>
<pad name="A2" x="29.7548" y="2.34" drill="0.8" diameter="1.524"/>
<pad name="A3" x="32.2948" y="2.34" drill="0.8" diameter="1.524"/>
<pad name="A4" x="34.8348" y="2.34" drill="0.8" diameter="1.524"/>
<pad name="A5" x="37.3748" y="2.34" drill="0.8" diameter="1.524"/>
<pad name="A6" x="39.9148" y="2.34" drill="0.8" diameter="1.524"/>
<pad name="0" x="42.4548" y="2.34" drill="0.8" diameter="1.524"/>
<pad name="1" x="44.9948" y="2.34" drill="0.8" diameter="1.524"/>
<pad name="2" x="47.5348" y="2.34" drill="0.8" diameter="1.524"/>
<pad name="3" x="50.0748" y="2.34" drill="0.8" diameter="1.524"/>
<pad name="4" x="52.6148" y="2.34" drill="0.8" diameter="1.524"/>
<pad name="5" x="55.1548" y="2.34" drill="0.8" diameter="1.524"/>
<hole x="59.25" y="2.25" drill="2.25"/>
<hole x="59.25" y="22.75" drill="2.25"/>
<text x="21.59" y="-0.635" size="1.016" layer="51" rot="R270">AREF</text>
<text x="24.13" y="-0.635" size="1.016" layer="51" rot="R270">DAC0/A0</text>
<text x="26.67" y="-0.635" size="1.016" layer="51" rot="R270">A1</text>
<text x="29.21" y="-0.635" size="1.016" layer="51" rot="R270">A2</text>
<text x="31.75" y="-0.635" size="1.016" layer="51" rot="R270">A3</text>
<text x="34.29" y="-0.635" size="1.016" layer="51" rot="R270">A4</text>
<text x="36.83" y="-0.635" size="1.016" layer="51" rot="R270">A5</text>
<text x="39.37" y="-0.635" size="1.016" layer="51" rot="R270">A6</text>
<text x="41.91" y="-0.635" size="1.016" layer="51" rot="R270">0</text>
<text x="44.45" y="-0.635" size="1.016" layer="51" rot="R270">1</text>
<text x="46.99" y="-0.635" size="1.016" layer="51" rot="R270">2</text>
<text x="49.53" y="-0.635" size="1.016" layer="51" rot="R270">3</text>
<text x="52.07" y="-0.635" size="1.016" layer="51" rot="R270">4</text>
<text x="54.61" y="-0.635" size="1.016" layer="51" rot="R270">5</text>
<text x="21.59" y="25.4" size="1.016" layer="51" rot="R90" align="top-left">5V</text>
<text x="24.13" y="25.4" size="1.016" layer="51" rot="R90" align="top-left">VIN</text>
<text x="26.67" y="25.4" size="1.016" layer="51" rot="R90" align="top-left">VCC</text>
<text x="29.21" y="25.4" size="1.016" layer="51" rot="R90" align="top-left">GND</text>
<text x="31.75" y="25.4" size="1.016" layer="51" rot="R90" align="top-left">RST</text>
<text x="34.29" y="25.4" size="1.016" layer="51" rot="R90" align="top-left">14/TX</text>
<text x="36.83" y="25.4" size="1.016" layer="51" rot="R90" align="top-left">13/RX</text>
<text x="39.37" y="25.4" size="1.016" layer="51" rot="R90" align="top-left">12/SCL</text>
<text x="41.91" y="25.4" size="1.016" layer="51" rot="R90" align="top-left">11/SDA</text>
<text x="44.45" y="25.4" size="1.016" layer="51" rot="R90" align="top-left">10/MISO</text>
<text x="46.99" y="25.4" size="1.016" layer="51" rot="R90" align="top-left">9/SCK</text>
<text x="49.53" y="25.4" size="1.016" layer="51" rot="R90" align="top-left">8/MOSI</text>
<text x="52.07" y="25.4" size="1.016" layer="51" rot="R90" align="top-left">7</text>
<text x="54.61" y="25.4" size="1.016" layer="51" rot="R90" align="top-left">6</text>
<hole x="2.25" y="22.75" drill="2.25"/>
<hole x="2.25" y="2.25" drill="2.25"/>
</package>
</packages>
<symbols>
<symbol name="MKRBOARD">
<pin name="A5" x="-15.24" y="2.54" visible="pin" length="middle"/>
<pin name="A4" x="-15.24" y="5.08" visible="pin" length="middle"/>
<pin name="A3" x="-15.24" y="7.62" visible="pin" length="middle"/>
<pin name="A2" x="-15.24" y="10.16" visible="pin" length="middle"/>
<pin name="A1" x="-15.24" y="12.7" visible="pin" length="middle"/>
<pin name="DAC0/A0" x="-15.24" y="15.24" visible="pin" length="middle"/>
<pin name="AREF" x="-15.24" y="17.78" visible="pin" length="middle"/>
<pin name="A6" x="-15.24" y="0" visible="pin" length="middle"/>
<pin name="0" x="-15.24" y="-2.54" visible="pin" length="middle"/>
<pin name="1" x="-15.24" y="-5.08" visible="pin" length="middle"/>
<pin name="2" x="-15.24" y="-7.62" visible="pin" length="middle"/>
<pin name="3" x="-15.24" y="-10.16" visible="pin" length="middle"/>
<pin name="4" x="-15.24" y="-12.7" visible="pin" length="middle"/>
<pin name="5" x="-15.24" y="-15.24" visible="pin" length="middle"/>
<pin name="5V" x="15.24" y="17.78" visible="pin" length="middle" rot="R180"/>
<pin name="VIN" x="15.24" y="15.24" visible="pin" length="middle" rot="R180"/>
<pin name="VCC" x="15.24" y="12.7" visible="pin" length="middle" rot="R180"/>
<pin name="GND" x="15.24" y="10.16" visible="pin" length="middle" rot="R180"/>
<pin name="RST" x="15.24" y="7.62" visible="pin" length="middle" rot="R180"/>
<pin name="14/TX" x="15.24" y="5.08" visible="pin" length="middle" rot="R180"/>
<pin name="13/RX" x="15.24" y="2.54" visible="pin" length="middle" rot="R180"/>
<pin name="12/SCL" x="15.24" y="0" visible="pin" length="middle" rot="R180"/>
<pin name="11/SDA" x="15.24" y="-2.54" visible="pin" length="middle" rot="R180"/>
<pin name="10/MISO" x="15.24" y="-5.08" visible="pin" length="middle" rot="R180"/>
<pin name="9/SCK" x="15.24" y="-7.62" visible="pin" length="middle" rot="R180"/>
<pin name="8/MOSI" x="15.24" y="-10.16" visible="pin" length="middle" rot="R180"/>
<pin name="7" x="15.24" y="-12.7" visible="pin" length="middle" rot="R180"/>
<pin name="6" x="15.24" y="-15.24" visible="pin" length="middle" rot="R180"/>
<wire x1="-10.16" y1="20.32" x2="-10.16" y2="-17.78" width="0.1524" layer="94"/>
<wire x1="-10.16" y1="-17.78" x2="-7.62" y2="-20.32" width="0.1524" layer="94" curve="90"/>
<wire x1="-7.62" y1="-20.32" x2="7.62" y2="-20.32" width="0.1524" layer="94"/>
<wire x1="7.62" y1="-20.32" x2="10.16" y2="-17.78" width="0.1524" layer="94" curve="90"/>
<wire x1="10.16" y1="-17.78" x2="10.16" y2="20.32" width="0.1524" layer="94"/>
<wire x1="10.16" y1="20.32" x2="7.62" y2="22.86" width="0.1524" layer="94" curve="90"/>
<wire x1="7.62" y1="22.86" x2="-7.62" y2="22.86" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="22.86" x2="-10.16" y2="20.32" width="0.1524" layer="94" curve="90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MKRBOARD">
<gates>
<gate name="G$1" symbol="MKRBOARD" x="0" y="-2.54"/>
</gates>
<devices>
<device name="OUTLINE" package="MKRBOARD">
<connects>
<connect gate="G$1" pin="0" pad="0"/>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10/MISO" pad="10"/>
<connect gate="G$1" pin="11/SDA" pad="11"/>
<connect gate="G$1" pin="12/SCL" pad="12"/>
<connect gate="G$1" pin="13/RX" pad="13"/>
<connect gate="G$1" pin="14/TX" pad="14"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="5V" pad="5V"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8/MOSI" pad="8"/>
<connect gate="G$1" pin="9/SCK" pad="9"/>
<connect gate="G$1" pin="A1" pad="A1"/>
<connect gate="G$1" pin="A2" pad="A2"/>
<connect gate="G$1" pin="A3" pad="A3"/>
<connect gate="G$1" pin="A4" pad="A4"/>
<connect gate="G$1" pin="A5" pad="A5"/>
<connect gate="G$1" pin="A6" pad="A6"/>
<connect gate="G$1" pin="AREF" pad="AREF"/>
<connect gate="G$1" pin="DAC0/A0" pad="A0"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="RST" pad="RST"/>
<connect gate="G$1" pin="VCC" pad="VCC"/>
<connect gate="G$1" pin="VIN" pad="VIN"/>
</connects>
<technologies>
<technology name="">
<attribute name="DATASHEET" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="DNP" constant="no"/>
<attribute name="MANUFACTURER" value="DNP" constant="no"/>
<attribute name="MANUFACTURER-PN" value="DNP" constant="no"/>
<attribute name="VALUE" value="DNP" constant="no"/>
</technology>
</technologies>
</device>
<device name="NO-OUTLINE" package="MKRBOARD-NOOUTLINE">
<connects>
<connect gate="G$1" pin="0" pad="0"/>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10/MISO" pad="10"/>
<connect gate="G$1" pin="11/SDA" pad="11"/>
<connect gate="G$1" pin="12/SCL" pad="12"/>
<connect gate="G$1" pin="13/RX" pad="13"/>
<connect gate="G$1" pin="14/TX" pad="14"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="5V" pad="5V"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8/MOSI" pad="8"/>
<connect gate="G$1" pin="9/SCK" pad="9"/>
<connect gate="G$1" pin="A1" pad="A1"/>
<connect gate="G$1" pin="A2" pad="A2"/>
<connect gate="G$1" pin="A3" pad="A3"/>
<connect gate="G$1" pin="A4" pad="A4"/>
<connect gate="G$1" pin="A5" pad="A5"/>
<connect gate="G$1" pin="A6" pad="A6"/>
<connect gate="G$1" pin="AREF" pad="AREF"/>
<connect gate="G$1" pin="DAC0/A0" pad="A0"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="RST" pad="RST"/>
<connect gate="G$1" pin="VCC" pad="VCC"/>
<connect gate="G$1" pin="VIN" pad="VIN"/>
</connects>
<technologies>
<technology name="">
<attribute name="DATASHEET" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="DNP" constant="no"/>
<attribute name="MANUFACTURER" value="DNP" constant="no"/>
<attribute name="MANUFACTURER-PN" value="DNP" constant="no"/>
<attribute name="VALUE" value="DNP" constant="no"/>
</technology>
</technologies>
</device>
<device name="W-SMD" package="MKRBOARD-W-SMD">
<connects>
<connect gate="G$1" pin="0" pad="0"/>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10/MISO" pad="10"/>
<connect gate="G$1" pin="11/SDA" pad="11"/>
<connect gate="G$1" pin="12/SCL" pad="12"/>
<connect gate="G$1" pin="13/RX" pad="13"/>
<connect gate="G$1" pin="14/TX" pad="14"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="5V" pad="5V"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8/MOSI" pad="8"/>
<connect gate="G$1" pin="9/SCK" pad="9"/>
<connect gate="G$1" pin="A1" pad="A1"/>
<connect gate="G$1" pin="A2" pad="A2"/>
<connect gate="G$1" pin="A3" pad="A3"/>
<connect gate="G$1" pin="A4" pad="A4"/>
<connect gate="G$1" pin="A5" pad="A5"/>
<connect gate="G$1" pin="A6" pad="A6"/>
<connect gate="G$1" pin="AREF" pad="AREF"/>
<connect gate="G$1" pin="DAC0/A0" pad="DAC0/A0"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="RST" pad="RST"/>
<connect gate="G$1" pin="VCC" pad="VCC"/>
<connect gate="G$1" pin="VIN" pad="VIN"/>
</connects>
<technologies>
<technology name="">
<attribute name="DATASHEET" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="DNP" constant="no"/>
<attribute name="MANUFACTURER" value="DNP" constant="no"/>
<attribute name="MANUFACTURER-PN" value="DNP" constant="no"/>
<attribute name="VALUE" value="DNP" constant="no"/>
</technology>
</technologies>
</device>
<device name="SOLDER" package="MKRBOARD-SOLDER">
<connects>
<connect gate="G$1" pin="0" pad="0"/>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10/MISO" pad="10"/>
<connect gate="G$1" pin="11/SDA" pad="11"/>
<connect gate="G$1" pin="12/SCL" pad="12"/>
<connect gate="G$1" pin="13/RX" pad="13"/>
<connect gate="G$1" pin="14/TX" pad="14"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="5V" pad="5V"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8/MOSI" pad="8"/>
<connect gate="G$1" pin="9/SCK" pad="9"/>
<connect gate="G$1" pin="A1" pad="A1"/>
<connect gate="G$1" pin="A2" pad="A2"/>
<connect gate="G$1" pin="A3" pad="A3"/>
<connect gate="G$1" pin="A4" pad="A4"/>
<connect gate="G$1" pin="A5" pad="A5"/>
<connect gate="G$1" pin="A6" pad="A6"/>
<connect gate="G$1" pin="AREF" pad="AREF"/>
<connect gate="G$1" pin="DAC0/A0" pad="DAC0/A0"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="RST" pad="RST"/>
<connect gate="G$1" pin="VCC" pad="VCC"/>
<connect gate="G$1" pin="VIN" pad="VIN"/>
</connects>
<technologies>
<technology name="">
<attribute name="DATASHEET" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="DNP" constant="no"/>
<attribute name="MANUFACTURER" value="DNP" constant="no"/>
<attribute name="MANUFACTURER-PN" value="DNP" constant="no"/>
<attribute name="VALUE" value="DNP" constant="no"/>
</technology>
</technologies>
</device>
<device name="-DIM" package="MKRBOARD-DIM">
<connects>
<connect gate="G$1" pin="0" pad="0"/>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10/MISO" pad="10"/>
<connect gate="G$1" pin="11/SDA" pad="11"/>
<connect gate="G$1" pin="12/SCL" pad="12"/>
<connect gate="G$1" pin="13/RX" pad="13"/>
<connect gate="G$1" pin="14/TX" pad="14"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="5V" pad="5V"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8/MOSI" pad="8"/>
<connect gate="G$1" pin="9/SCK" pad="9"/>
<connect gate="G$1" pin="A1" pad="A1"/>
<connect gate="G$1" pin="A2" pad="A2"/>
<connect gate="G$1" pin="A3" pad="A3"/>
<connect gate="G$1" pin="A4" pad="A4"/>
<connect gate="G$1" pin="A5" pad="A5"/>
<connect gate="G$1" pin="A6" pad="A6"/>
<connect gate="G$1" pin="AREF" pad="AREF"/>
<connect gate="G$1" pin="DAC0/A0" pad="A0"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="RST" pad="RST"/>
<connect gate="G$1" pin="VCC" pad="VCC"/>
<connect gate="G$1" pin="VIN" pad="VIN"/>
</connects>
<technologies>
<technology name="-DNP">
<attribute name="DATASHEET" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="DNP" constant="no"/>
<attribute name="MANUFACTURER" value="DNP" constant="no"/>
<attribute name="MANUFACTURER-PN" value="DNP" constant="no"/>
<attribute name="VALUE" value="DNP" constant="no"/>
</technology>
<technology name="-MALE">
<attribute name="DATASHEET" value="-" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="-" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="-" constant="no"/>
<attribute name="MANUFACTURER" value="ELDECO" constant="no"/>
<attribute name="MANUFACTURER-PN" value="PH254-104DF113A00V" constant="no"/>
<attribute name="VALUE" value="-" constant="no"/>
</technology>
</technologies>
</device>
<device name="MKRBOARD-DIM-L68MM" package="MKRBOARD-DIM-L68MM">
<connects>
<connect gate="G$1" pin="0" pad="0"/>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10/MISO" pad="10"/>
<connect gate="G$1" pin="11/SDA" pad="11"/>
<connect gate="G$1" pin="12/SCL" pad="12"/>
<connect gate="G$1" pin="13/RX" pad="13"/>
<connect gate="G$1" pin="14/TX" pad="14"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="5V" pad="5V"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8/MOSI" pad="8"/>
<connect gate="G$1" pin="9/SCK" pad="9"/>
<connect gate="G$1" pin="A1" pad="A1"/>
<connect gate="G$1" pin="A2" pad="A2"/>
<connect gate="G$1" pin="A3" pad="A3"/>
<connect gate="G$1" pin="A4" pad="A4"/>
<connect gate="G$1" pin="A5" pad="A5"/>
<connect gate="G$1" pin="A6" pad="A6"/>
<connect gate="G$1" pin="AREF" pad="AREF"/>
<connect gate="G$1" pin="DAC0/A0" pad="A0"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="RST" pad="RST"/>
<connect gate="G$1" pin="VCC" pad="VCC"/>
<connect gate="G$1" pin="VIN" pad="VIN"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="NO-OUTLINE2" package="MKRBOARD-NOOUTLINE2">
<connects>
<connect gate="G$1" pin="0" pad="0"/>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10/MISO" pad="10"/>
<connect gate="G$1" pin="11/SDA" pad="11"/>
<connect gate="G$1" pin="12/SCL" pad="12"/>
<connect gate="G$1" pin="13/RX" pad="13"/>
<connect gate="G$1" pin="14/TX" pad="14"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="5V" pad="5V"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8/MOSI" pad="8"/>
<connect gate="G$1" pin="9/SCK" pad="9"/>
<connect gate="G$1" pin="A1" pad="A1"/>
<connect gate="G$1" pin="A2" pad="A2"/>
<connect gate="G$1" pin="A3" pad="A3"/>
<connect gate="G$1" pin="A4" pad="A4"/>
<connect gate="G$1" pin="A5" pad="A5"/>
<connect gate="G$1" pin="A6" pad="A6"/>
<connect gate="G$1" pin="AREF" pad="AREF"/>
<connect gate="G$1" pin="DAC0/A0" pad="A0"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="RST" pad="RST"/>
<connect gate="G$1" pin="VCC" pad="VCC"/>
<connect gate="G$1" pin="VIN" pad="VIN"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4CARRIER" package="MKRBOARD-4CARRIER">
<connects>
<connect gate="G$1" pin="0" pad="0"/>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10/MISO" pad="10"/>
<connect gate="G$1" pin="11/SDA" pad="11"/>
<connect gate="G$1" pin="12/SCL" pad="12"/>
<connect gate="G$1" pin="13/RX" pad="13"/>
<connect gate="G$1" pin="14/TX" pad="14"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="5V" pad="5V"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8/MOSI" pad="8"/>
<connect gate="G$1" pin="9/SCK" pad="9"/>
<connect gate="G$1" pin="A1" pad="A1"/>
<connect gate="G$1" pin="A2" pad="A2"/>
<connect gate="G$1" pin="A3" pad="A3"/>
<connect gate="G$1" pin="A4" pad="A4"/>
<connect gate="G$1" pin="A5" pad="A5"/>
<connect gate="G$1" pin="A6" pad="A6"/>
<connect gate="G$1" pin="AREF" pad="AREF"/>
<connect gate="G$1" pin="DAC0/A0" pad="A0"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="RST" pad="RST"/>
<connect gate="G$1" pin="VCC" pad="VCC"/>
<connect gate="G$1" pin="VIN" pad="VIN"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Arduino-ICs">
<packages>
<package name="TSSOP20">
<circle x="-2.2756" y="-1.2192" radius="0.4572" width="0.1524" layer="21"/>
<wire x1="-3.1646" y1="-2.2828" x2="3.1646" y2="-2.2828" width="0.1524" layer="51"/>
<wire x1="3.1646" y1="2.2828" x2="3.1646" y2="-2.2828" width="0.1524" layer="21"/>
<wire x1="3.1646" y1="2.2828" x2="-3.1646" y2="2.2828" width="0.1524" layer="51"/>
<wire x1="-3.1646" y1="-2.2828" x2="-3.1646" y2="2.2828" width="0.1524" layer="21"/>
<rectangle x1="-3.0266" y1="-3.121" x2="-2.8234" y2="-2.2828" layer="51"/>
<rectangle x1="-2.3766" y1="-3.121" x2="-2.1734" y2="-2.2828" layer="51"/>
<rectangle x1="-1.7266" y1="-3.121" x2="-1.5234" y2="-2.2828" layer="51"/>
<rectangle x1="-1.0766" y1="-3.121" x2="-0.8734" y2="-2.2828" layer="51"/>
<rectangle x1="-0.4266" y1="-3.121" x2="-0.2234" y2="-2.2828" layer="51"/>
<rectangle x1="0.2234" y1="-3.121" x2="0.4266" y2="-2.2828" layer="51"/>
<rectangle x1="0.8734" y1="-3.121" x2="1.0766" y2="-2.2828" layer="51"/>
<rectangle x1="1.5234" y1="-3.121" x2="1.7266" y2="-2.2828" layer="51"/>
<rectangle x1="1.5234" y1="2.2828" x2="1.7266" y2="3.121" layer="51"/>
<rectangle x1="0.8734" y1="2.2828" x2="1.0766" y2="3.121" layer="51"/>
<rectangle x1="0.2234" y1="2.2828" x2="0.4266" y2="3.121" layer="51"/>
<rectangle x1="-0.4266" y1="2.2828" x2="-0.2234" y2="3.121" layer="51"/>
<rectangle x1="-1.0766" y1="2.2828" x2="-0.8734" y2="3.121" layer="51"/>
<rectangle x1="-1.7266" y1="2.2828" x2="-1.5234" y2="3.121" layer="51"/>
<rectangle x1="-2.3766" y1="2.2828" x2="-2.1734" y2="3.121" layer="51"/>
<rectangle x1="-3.0266" y1="2.2828" x2="-2.8234" y2="3.121" layer="51"/>
<rectangle x1="2.1734" y1="-3.121" x2="2.3766" y2="-2.2828" layer="51"/>
<rectangle x1="2.8234" y1="-3.121" x2="3.0266" y2="-2.2828" layer="51"/>
<rectangle x1="2.1734" y1="2.2828" x2="2.3766" y2="3.121" layer="51"/>
<rectangle x1="2.8234" y1="2.2828" x2="3.0266" y2="3.121" layer="51"/>
<smd name="1" x="-2.925" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="2" x="-2.275" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="3" x="-1.625" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="4" x="-0.975" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="5" x="-0.325" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="6" x="0.325" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="7" x="0.975" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="8" x="1.625" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="9" x="2.275" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="10" x="2.925" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="11" x="2.925" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="12" x="2.275" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="13" x="1.625" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="14" x="0.975" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="15" x="0.325" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="16" x="-0.325" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="17" x="-0.975" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="18" x="-1.625" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="19" x="-2.275" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="20" x="-2.925" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<text x="-3.5456" y="-2.0828" size="1.016" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="4.5362" y="-2.0828" size="1.016" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
</package>
<package name="QFN16">
<description>&lt;b&gt;RTE (S-PQFP-N16)&lt;/b&gt; 3x3 mm&lt;p&gt;
Source: www.ti.com .. tpa6132a2 direct path stereo $.65.pdf</description>
<wire x1="-1.75" y1="1.75" x2="1.75" y2="1.75" width="0.1016" layer="51"/>
<wire x1="1.75" y1="1.75" x2="1.75" y2="-1.75" width="0.1016" layer="51"/>
<wire x1="1.75" y1="-1.75" x2="-1.75" y2="-1.75" width="0.1016" layer="51"/>
<wire x1="-1.75" y1="-1.75" x2="-1.75" y2="1.75" width="0.1016" layer="51"/>
<wire x1="1.75" y1="1.75" x2="1.75" y2="1.025" width="0.1016" layer="21"/>
<wire x1="1.025" y1="1.75" x2="1.75" y2="1.75" width="0.1016" layer="21"/>
<wire x1="-1.5" y1="1.5" x2="-1.05" y2="1.5" width="0.1016" layer="21"/>
<wire x1="-1.5" y1="1.05" x2="-1.5" y2="1.5" width="0.1016" layer="21"/>
<wire x1="-1.75" y1="-1.75" x2="-1.75" y2="-1.025" width="0.1016" layer="21"/>
<wire x1="-1.025" y1="-1.75" x2="-1.75" y2="-1.75" width="0.1016" layer="21"/>
<wire x1="1.75" y1="-1.75" x2="1.025" y2="-1.75" width="0.1016" layer="21"/>
<wire x1="1.75" y1="-1.025" x2="1.75" y2="-1.75" width="0.1016" layer="21"/>
<circle x="-1.2" y="0.75" radius="0.125" width="0" layer="31"/>
<circle x="-1.2" y="0.75" radius="0.2" width="0" layer="29"/>
<circle x="-1.2" y="0.25" radius="0.125" width="0" layer="31"/>
<circle x="-1.2" y="0.25" radius="0.2" width="0" layer="29"/>
<circle x="-1.2" y="-0.25" radius="0.125" width="0" layer="31"/>
<circle x="-1.2" y="-0.75" radius="0.125" width="0" layer="31"/>
<circle x="-0.75" y="-1.2" radius="0.125" width="0" layer="31"/>
<circle x="-0.25" y="-1.2" radius="0.125" width="0" layer="31"/>
<circle x="0.25" y="-1.2" radius="0.125" width="0" layer="31"/>
<circle x="0.75" y="-1.2" radius="0.125" width="0" layer="31"/>
<circle x="1.2" y="-0.75" radius="0.125" width="0" layer="31"/>
<circle x="1.2" y="-0.25" radius="0.125" width="0" layer="31"/>
<circle x="1.2" y="0.25" radius="0.125" width="0" layer="31"/>
<circle x="1.2" y="0.75" radius="0.125" width="0" layer="31"/>
<circle x="0.75" y="1.2" radius="0.125" width="0" layer="31"/>
<circle x="0.25" y="1.2" radius="0.125" width="0" layer="31"/>
<circle x="-0.25" y="1.2" radius="0.125" width="0" layer="31"/>
<circle x="-0.75" y="1.2" radius="0.125" width="0" layer="31"/>
<circle x="-1.2" y="-0.25" radius="0.2" width="0" layer="29"/>
<circle x="-1.2" y="-0.75" radius="0.2" width="0" layer="29"/>
<circle x="-0.75" y="-1.2" radius="0.2" width="0" layer="29"/>
<circle x="-0.25" y="-1.2" radius="0.2" width="0" layer="29"/>
<circle x="0.25" y="-1.2" radius="0.2" width="0" layer="29"/>
<circle x="0.75" y="-1.2" radius="0.2" width="0" layer="29"/>
<circle x="1.2" y="-0.75" radius="0.2" width="0" layer="29"/>
<circle x="1.2" y="-0.25" radius="0.2" width="0" layer="29"/>
<circle x="1.2" y="0.25" radius="0.2" width="0" layer="29"/>
<circle x="1.2" y="0.75" radius="0.2" width="0" layer="29"/>
<circle x="0.75" y="1.2" radius="0.2" width="0" layer="29"/>
<circle x="0.25" y="1.2" radius="0.2" width="0" layer="29"/>
<circle x="-0.25" y="1.2" radius="0.2" width="0" layer="29"/>
<circle x="-0.75" y="1.2" radius="0.2" width="0" layer="29"/>
<smd name="1" x="-1.655" y="0.75" dx="0.85" dy="0.3" layer="1" roundness="75" stop="no" cream="no"/>
<smd name="2" x="-1.655" y="0.25" dx="0.85" dy="0.3" layer="1" roundness="75" stop="no" cream="no"/>
<smd name="3" x="-1.655" y="-0.25" dx="0.85" dy="0.3" layer="1" roundness="75" stop="no" cream="no"/>
<smd name="4" x="-1.655" y="-0.75" dx="0.85" dy="0.3" layer="1" roundness="75" stop="no" cream="no"/>
<smd name="5" x="-0.75" y="-1.655" dx="0.85" dy="0.3" layer="1" roundness="75" rot="R90" stop="no" cream="no"/>
<smd name="6" x="-0.25" y="-1.655" dx="0.85" dy="0.3" layer="1" roundness="75" rot="R90" stop="no" cream="no"/>
<smd name="7" x="0.25" y="-1.655" dx="0.85" dy="0.3" layer="1" roundness="75" rot="R90" stop="no" cream="no"/>
<smd name="8" x="0.75" y="-1.655" dx="0.85" dy="0.3" layer="1" roundness="75" rot="R90" stop="no" cream="no"/>
<smd name="9" x="1.655" y="-0.75" dx="0.85" dy="0.3" layer="1" roundness="75" rot="R180" stop="no" cream="no"/>
<smd name="10" x="1.655" y="-0.25" dx="0.85" dy="0.3" layer="1" roundness="75" rot="R180" stop="no" cream="no"/>
<smd name="11" x="1.655" y="0.25" dx="0.85" dy="0.3" layer="1" roundness="75" rot="R180" stop="no" cream="no"/>
<smd name="12" x="1.655" y="0.75" dx="0.85" dy="0.3" layer="1" roundness="75" rot="R180" stop="no" cream="no"/>
<smd name="13" x="0.75" y="1.655" dx="0.85" dy="0.3" layer="1" roundness="75" rot="R270" stop="no" cream="no"/>
<smd name="14" x="0.25" y="1.655" dx="0.85" dy="0.3" layer="1" roundness="75" rot="R270" stop="no" cream="no"/>
<smd name="15" x="-0.25" y="1.655" dx="0.85" dy="0.3" layer="1" roundness="75" rot="R270" stop="no" cream="no"/>
<smd name="16" x="-0.75" y="1.655" dx="0.85" dy="0.3" layer="1" roundness="75" rot="R270" stop="no" cream="no"/>
<text x="-2" y="2" size="1.27" layer="25">&gt;NAME</text>
<text x="-2" y="-3.5" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.875" y1="-0.875" x2="0.875" y2="0.875" layer="29"/>
<rectangle x1="-0.8" y1="0.1" x2="-0.1" y2="0.8" layer="31" rot="R90"/>
<rectangle x1="-1.9" y1="0.55" x2="-1.2" y2="0.95" layer="29"/>
<rectangle x1="-1.85" y1="0.625" x2="-1.2" y2="0.875" layer="31"/>
<rectangle x1="-1.75" y1="1" x2="-1" y2="1.75" layer="21"/>
<rectangle x1="-1.75" y1="0" x2="0" y2="1.75" layer="51"/>
<rectangle x1="-1.9" y1="0.05" x2="-1.2" y2="0.45" layer="29"/>
<rectangle x1="-1.85" y1="0.125" x2="-1.2" y2="0.375" layer="31"/>
<rectangle x1="-1.85" y1="-0.375" x2="-1.2" y2="-0.125" layer="31"/>
<rectangle x1="-1.85" y1="-0.875" x2="-1.2" y2="-0.625" layer="31"/>
<rectangle x1="-1.075" y1="-1.65" x2="-0.425" y2="-1.4" layer="31" rot="R90"/>
<rectangle x1="-0.575" y1="-1.65" x2="0.075" y2="-1.4" layer="31" rot="R90"/>
<rectangle x1="-0.075" y1="-1.65" x2="0.575" y2="-1.4" layer="31" rot="R90"/>
<rectangle x1="0.425" y1="-1.65" x2="1.075" y2="-1.4" layer="31" rot="R90"/>
<rectangle x1="1.2" y1="-0.875" x2="1.85" y2="-0.625" layer="31" rot="R180"/>
<rectangle x1="1.2" y1="-0.375" x2="1.85" y2="-0.125" layer="31" rot="R180"/>
<rectangle x1="1.2" y1="0.125" x2="1.85" y2="0.375" layer="31" rot="R180"/>
<rectangle x1="1.2" y1="0.625" x2="1.85" y2="0.875" layer="31" rot="R180"/>
<rectangle x1="0.425" y1="1.4" x2="1.075" y2="1.65" layer="31" rot="R270"/>
<rectangle x1="-0.075" y1="1.4" x2="0.575" y2="1.65" layer="31" rot="R270"/>
<rectangle x1="-0.575" y1="1.4" x2="0.075" y2="1.65" layer="31" rot="R270"/>
<rectangle x1="-1.075" y1="1.4" x2="-0.425" y2="1.65" layer="31" rot="R270"/>
<rectangle x1="-0.8" y1="-0.8" x2="-0.1" y2="-0.1" layer="31" rot="R180"/>
<rectangle x1="0.1" y1="-0.8" x2="0.8" y2="-0.1" layer="31" rot="R270"/>
<rectangle x1="0.1" y1="0.1" x2="0.8" y2="0.8" layer="31"/>
<rectangle x1="-1.9" y1="-0.45" x2="-1.2" y2="-0.05" layer="29"/>
<rectangle x1="-1.9" y1="-0.95" x2="-1.2" y2="-0.55" layer="29"/>
<rectangle x1="-1.1" y1="-1.75" x2="-0.4" y2="-1.35" layer="29" rot="R90"/>
<rectangle x1="-0.6" y1="-1.75" x2="0.1" y2="-1.35" layer="29" rot="R90"/>
<rectangle x1="-0.1" y1="-1.75" x2="0.6" y2="-1.35" layer="29" rot="R90"/>
<rectangle x1="0.4" y1="-1.75" x2="1.1" y2="-1.35" layer="29" rot="R90"/>
<rectangle x1="1.2" y1="-0.95" x2="1.9" y2="-0.55" layer="29" rot="R180"/>
<rectangle x1="1.2" y1="-0.45" x2="1.9" y2="-0.05" layer="29" rot="R180"/>
<rectangle x1="1.2" y1="0.05" x2="1.9" y2="0.45" layer="29" rot="R180"/>
<rectangle x1="1.2" y1="0.55" x2="1.9" y2="0.95" layer="29" rot="R180"/>
<rectangle x1="0.4" y1="1.35" x2="1.1" y2="1.75" layer="29" rot="R270"/>
<rectangle x1="-0.1" y1="1.35" x2="0.6" y2="1.75" layer="29" rot="R270"/>
<rectangle x1="-0.6" y1="1.35" x2="0.1" y2="1.75" layer="29" rot="R270"/>
<rectangle x1="-1.1" y1="1.35" x2="-0.4" y2="1.75" layer="29" rot="R270"/>
<smd name="17" x="0" y="0" dx="1.905" dy="1.905" layer="1" rot="R90"/>
</package>
</packages>
<symbols>
<symbol name="TXB0108PWR">
<wire x1="-12.7" y1="17.78" x2="12.7" y2="17.78" width="0.254" layer="94"/>
<wire x1="12.7" y1="17.78" x2="12.7" y2="-20.32" width="0.254" layer="94"/>
<wire x1="12.7" y1="-20.32" x2="-12.7" y2="-20.32" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-20.32" x2="-12.7" y2="17.78" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-17.78" x2="-2.54" y2="-17.78" width="0.127" layer="97"/>
<wire x1="2.54" y1="-17.78" x2="12.7" y2="-17.78" width="0.127" layer="97"/>
<wire x1="-12.7" y1="7.62" x2="-2.54" y2="7.62" width="0.127" layer="97"/>
<wire x1="-2.54" y1="-17.78" x2="-2.54" y2="7.62" width="0.127" layer="97"/>
<wire x1="12.7" y1="7.62" x2="2.54" y2="7.62" width="0.127" layer="97"/>
<wire x1="2.54" y1="7.62" x2="2.54" y2="-17.78" width="0.127" layer="97"/>
<pin name="A1" x="-15.24" y="5.08" length="short"/>
<pin name="A2" x="-15.24" y="2.54" length="short"/>
<pin name="A3" x="-15.24" y="0" length="short"/>
<pin name="A4" x="-15.24" y="-2.54" length="short"/>
<pin name="A5" x="-15.24" y="-5.08" length="short"/>
<pin name="A6" x="-15.24" y="-7.62" length="short"/>
<pin name="A7" x="-15.24" y="-10.16" length="short"/>
<pin name="A8" x="-15.24" y="-12.7" length="short"/>
<pin name="B1" x="15.24" y="5.08" length="short" rot="R180"/>
<pin name="B2" x="15.24" y="2.54" length="short" rot="R180"/>
<pin name="B3" x="15.24" y="0" length="short" rot="R180"/>
<pin name="B4" x="15.24" y="-2.54" length="short" rot="R180"/>
<pin name="B5" x="15.24" y="-5.08" length="short" rot="R180"/>
<pin name="B6" x="15.24" y="-7.62" length="short" rot="R180"/>
<pin name="B7" x="15.24" y="-10.16" length="short" rot="R180"/>
<pin name="B8" x="15.24" y="-12.7" length="short" rot="R180"/>
<pin name="GND" x="0" y="-22.86" length="short" rot="R90"/>
<pin name="OE" x="-15.24" y="12.7" length="short"/>
<pin name="VCCA" x="-2.54" y="20.32" length="short" rot="R270"/>
<pin name="VCCB" x="2.54" y="20.32" length="short" rot="R270"/>
<text x="-12.7" y="20.32" size="1.27" layer="95">&gt;NAME</text>
<text x="-12.7" y="-22.86" size="1.27" layer="96">&gt;VALUE</text>
<text x="-11.43" y="-16.51" size="1.27" layer="97">1.2-3.6V</text>
<text x="3.175" y="-16.51" size="1.27" layer="97">1.65-5.5V</text>
</symbol>
<symbol name="TS3001X-1">
<wire x1="-15.24" y1="20.32" x2="-15.24" y2="12.7" width="0.254" layer="94"/>
<wire x1="-15.24" y1="12.7" x2="-15.24" y2="-15.24" width="0.254" layer="94"/>
<wire x1="-15.24" y1="-15.24" x2="15.24" y2="-15.24" width="0.254" layer="94"/>
<wire x1="15.24" y1="-15.24" x2="15.24" y2="12.7" width="0.254" layer="94"/>
<pin name="VCC" x="-20.32" y="10.16" length="middle"/>
<pin name="EN" x="-20.32" y="-10.16" length="middle"/>
<pin name="BST" x="20.32" y="10.16" length="middle" rot="R180"/>
<pin name="GND" x="-5.08" y="-20.32" length="middle" rot="R90"/>
<pin name="PGND" x="5.08" y="-20.32" length="middle" rot="R90"/>
<pin name="VSW" x="20.32" y="5.08" length="middle" rot="R180"/>
<pin name="PG" x="20.32" y="-10.16" length="middle" rot="R180"/>
<pin name="FB" x="20.32" y="-5.08" length="middle" rot="R180"/>
<text x="-15.24" y="-17.78" size="1.27" layer="96">&gt;VALUE</text>
<text x="-15.24" y="20.32" size="1.27" layer="95">&gt;NAME</text>
<wire x1="15.24" y1="12.7" x2="15.24" y2="20.32" width="0.254" layer="94"/>
<wire x1="-15.24" y1="20.32" x2="15.24" y2="20.32" width="0.254" layer="94"/>
<text x="-12.7" y="12.7" size="1.778" layer="94">IC REG BUCK 1/2/3A
</text>
<wire x1="-15.24" y1="12.7" x2="15.24" y2="12.7" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="LSF0108PWR" prefix="U" uservalue="yes">
<description>Voltage Level Translator Bidirectional 1 Circuit 8 Channel 100Mbps</description>
<gates>
<gate name="G$1" symbol="TXB0108PWR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TSSOP20">
<connects>
<connect gate="G$1" pin="A1" pad="3"/>
<connect gate="G$1" pin="A2" pad="4"/>
<connect gate="G$1" pin="A3" pad="5"/>
<connect gate="G$1" pin="A4" pad="6"/>
<connect gate="G$1" pin="A5" pad="7"/>
<connect gate="G$1" pin="A6" pad="8"/>
<connect gate="G$1" pin="A7" pad="9"/>
<connect gate="G$1" pin="A8" pad="10"/>
<connect gate="G$1" pin="B1" pad="18"/>
<connect gate="G$1" pin="B2" pad="17"/>
<connect gate="G$1" pin="B3" pad="16"/>
<connect gate="G$1" pin="B4" pad="15"/>
<connect gate="G$1" pin="B5" pad="14"/>
<connect gate="G$1" pin="B6" pad="13"/>
<connect gate="G$1" pin="B7" pad="12"/>
<connect gate="G$1" pin="B8" pad="11"/>
<connect gate="G$1" pin="GND" pad="1"/>
<connect gate="G$1" pin="OE" pad="20"/>
<connect gate="G$1" pin="VCCA" pad="2"/>
<connect gate="G$1" pin="VCCB" pad="19"/>
</connects>
<technologies>
<technology name="">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-TK2" value="-" constant="no"/>
<attribute name="DATASHEET" value="http://www.ti.com/lit/ds/symlink/lsf0108.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/texas-instruments/LSF0108PWR/296-37828-1-ND/4876357" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="296-37828-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Texas Instruments " constant="no"/>
<attribute name="MANUFACTURER-PN" value="LSF0108PWR" constant="no"/>
<attribute name="VALUE" value="LSF0108" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TS3001X" prefix="U">
<description>&lt;b&gt;TS30011/12/13&lt;/b&gt;
&lt;br&gt;Buck Switching Regulator IC Positive Fixed/adjustable 5V 1 Output 1A/2A/3A 16-VFQFN Exposed Pad</description>
<gates>
<gate name="G$1" symbol="TS3001X-1" x="0" y="0"/>
</gates>
<devices>
<device name="TS30012" package="QFN16">
<connects>
<connect gate="G$1" pin="BST" pad="10"/>
<connect gate="G$1" pin="EN" pad="9"/>
<connect gate="G$1" pin="FB" pad="5"/>
<connect gate="G$1" pin="GND" pad="4"/>
<connect gate="G$1" pin="PG" pad="8"/>
<connect gate="G$1" pin="PGND" pad="14 15 17"/>
<connect gate="G$1" pin="VCC" pad="2 3 11"/>
<connect gate="G$1" pin="VSW" pad="1 12 13 16"/>
</connects>
<technologies>
<technology name="">
<attribute name="DATASHEET" value="http://www.semtech.com/images/datasheet/ts30011_12_13.pdf" constant="no"/>
<attribute name="DISTRIBUTOR" value="Digikey" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.se/products/en?keywords=S30012-M050QFNRCT-ND" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="TS30012-M050QFNRCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Semtech Corp." constant="no"/>
<attribute name="MANUFACTURER-PN" value="TS30012-M050QFNR" constant="no"/>
<attribute name="VALUE" value="TS30012" constant="no"/>
</technology>
</technologies>
</device>
<device name="TS30013" package="QFN16">
<connects>
<connect gate="G$1" pin="BST" pad="10"/>
<connect gate="G$1" pin="EN" pad="9"/>
<connect gate="G$1" pin="FB" pad="5"/>
<connect gate="G$1" pin="GND" pad="4"/>
<connect gate="G$1" pin="PG" pad="8"/>
<connect gate="G$1" pin="PGND" pad="14 15 17"/>
<connect gate="G$1" pin="VCC" pad="2 3 11"/>
<connect gate="G$1" pin="VSW" pad="1 12 13 16"/>
</connects>
<technologies>
<technology name="">
<attribute name="DATASHEET" value="http://www.semtech.com/images/datasheet/ts30011_12_13.pdf" constant="no"/>
<attribute name="DISTRIBUTOR" value="Digikey" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.se/products/en?keywords=TS30013-M050QFNRCT-ND" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="TS30013-M050QFNRCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Semtech Corporation" constant="no"/>
<attribute name="MANUFACTURER-PN" value="TS30013-M050QFNR" constant="no"/>
<attribute name="VALUE" value="TS30013" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="BCMI-supply">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
Please keep in mind, that these devices are necessary for the
automatic wiring of the supply signals.&lt;p&gt;
The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="+3.3V">
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="0.635" x2="0" y2="1.905" width="0.1524" layer="94"/>
<circle x="0" y="1.27" radius="1.27" width="0.254" layer="94"/>
<text x="-1.905" y="3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="+3.3V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="VM">
<wire x1="-0.635" y1="3.81" x2="0.635" y2="3.81" width="0.1524" layer="94"/>
<wire x1="0" y1="3.175" x2="0" y2="4.445" width="0.1524" layer="94"/>
<circle x="0" y="3.81" radius="1.27" width="0.254" layer="94"/>
<text x="-3.175" y="5.715" size="1.778" layer="96">&gt;VALUE</text>
<pin name="+VM" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="+3.3V">
<gates>
<gate name="G$1" symbol="+3.3V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+VM">
<gates>
<gate name="G$1" symbol="VM" x="0" y="-2.54"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="BCMI-connectors">
<description>Copyright (c) 2016-2017 Arduino LLC.  All right reserved.</description>
<packages>
<package name="OSTTE/4">
<pad name="1" x="-5.25" y="0" drill="1.2"/>
<pad name="2" x="-1.75" y="0" drill="1.2"/>
<pad name="3" x="1.75" y="0" drill="1.2"/>
<pad name="4" x="5.25" y="0" drill="1.2"/>
<wire x1="-5.25" y1="-3.1" x2="-5.45" y2="-3.4" width="0.127" layer="21"/>
<wire x1="-5.45" y1="-3.4" x2="-5.25" y2="-3.4" width="0.127" layer="21"/>
<wire x1="-5.25" y1="-3.4" x2="-5.05" y2="-3.4" width="0.127" layer="21"/>
<wire x1="-5.05" y1="-3.4" x2="-5.25" y2="-3.1" width="0.127" layer="21"/>
<wire x1="-5.25" y1="-3.1" x2="-5.25" y2="-3.4" width="0.127" layer="21"/>
<wire x1="-5.25" y1="-3.4" x2="-5.15" y2="-3.3" width="0.127" layer="21"/>
<wire x1="-5.15" y1="-3.3" x2="-5.35" y2="-3.3" width="0.127" layer="21"/>
<circle x="3.5" y="-1.75" radius="0.282840625" width="0.2" layer="21"/>
<circle x="-3.5" y="-1.75" radius="0.282840625" width="0.2" layer="21"/>
<circle x="0" y="-1.75" radius="0.282840625" width="0.2" layer="21"/>
<wire x1="-7" y1="-3" x2="-7" y2="-3.6" width="0.127" layer="21"/>
<wire x1="7" y1="3.4" x2="7" y2="2.25" width="0.127" layer="21"/>
<wire x1="7" y1="2.25" x2="7" y2="-3" width="0.127" layer="21"/>
<wire x1="7" y1="-3" x2="7" y2="-3.6" width="0.127" layer="21"/>
<wire x1="-7" y1="3.4" x2="7" y2="3.4" width="0.127" layer="21"/>
<circle x="-5.25" y="0" radius="1.5" width="0.127" layer="21"/>
<wire x1="-7" y1="-3.6" x2="7" y2="-3.6" width="0.127" layer="21"/>
<wire x1="-6.25" y1="-1" x2="-4.25" y2="1" width="0.127" layer="21"/>
<circle x="-1.75" y="0" radius="1.5" width="0.127" layer="21"/>
<wire x1="-2.75" y1="-1" x2="-0.75" y2="1" width="0.127" layer="21"/>
<circle x="1.75" y="0" radius="1.5" width="0.127" layer="21"/>
<wire x1="0.75" y1="-1" x2="2.75" y2="1" width="0.127" layer="21"/>
<circle x="5.25" y="0" radius="1.5" width="0.127" layer="21"/>
<wire x1="4.25" y1="-1" x2="6.25" y2="1" width="0.127" layer="21"/>
<wire x1="-7" y1="-3" x2="7" y2="-3" width="0.127" layer="21"/>
<wire x1="-6.45" y1="2.4" x2="-6.45" y2="1.9" width="0.127" layer="21"/>
<wire x1="-6.45" y1="1.9" x2="-3.75" y2="1.9" width="0.127" layer="21"/>
<wire x1="-3.75" y1="1.9" x2="-3.75" y2="2.4" width="0.127" layer="21"/>
<wire x1="-3.75" y1="2.4" x2="-6.45" y2="2.4" width="0.127" layer="21"/>
<wire x1="-3.25" y1="2.4" x2="-3.25" y2="1.9" width="0.127" layer="21"/>
<wire x1="-3.25" y1="1.9" x2="-0.25" y2="1.9" width="0.127" layer="21"/>
<wire x1="-0.25" y1="1.9" x2="-0.25" y2="2.4" width="0.127" layer="21"/>
<wire x1="-0.25" y1="2.4" x2="-3.25" y2="2.4" width="0.127" layer="21"/>
<wire x1="0.25" y1="2.4" x2="0.25" y2="1.9" width="0.127" layer="21"/>
<wire x1="0.25" y1="1.9" x2="3.25" y2="1.9" width="0.127" layer="21"/>
<wire x1="3.25" y1="1.9" x2="3.25" y2="2.4" width="0.127" layer="21"/>
<wire x1="3.25" y1="2.4" x2="0.25" y2="2.4" width="0.127" layer="21"/>
<wire x1="3.75" y1="2.4" x2="3.75" y2="1.9" width="0.127" layer="21"/>
<wire x1="3.75" y1="1.9" x2="6.65" y2="1.9" width="0.127" layer="21"/>
<wire x1="6.65" y1="1.9" x2="6.65" y2="2.4" width="0.127" layer="21"/>
<wire x1="6.65" y1="2.4" x2="3.75" y2="2.4" width="0.127" layer="21"/>
<wire x1="-7" y1="3.4" x2="-7" y2="-1.75" width="0.127" layer="21"/>
<wire x1="-7" y1="-1.75" x2="-7" y2="-3" width="0.127" layer="21"/>
<wire x1="-7.5" y1="-2" x2="-7.5" y2="-1.5" width="0.127" layer="21"/>
<wire x1="7.5" y1="2.5" x2="7.5" y2="2" width="0.127" layer="21"/>
<wire x1="-7.5" y1="-2" x2="-7" y2="-1.75" width="0.127" layer="21"/>
<wire x1="-7" y1="-1.75" x2="-7.5" y2="-1.5" width="0.127" layer="21"/>
<wire x1="7.5" y1="2.5" x2="7" y2="2.25" width="0.127" layer="21"/>
<wire x1="7" y1="2.25" x2="7.5" y2="2" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="SCHRAUBKLEMMEV">
<wire x1="-1.27" y1="0.635" x2="-0.254" y2="-0.381" width="0.1524" layer="94"/>
<wire x1="-0.508" y1="-0.635" x2="-1.524" y2="0.381" width="0.1524" layer="94"/>
<wire x1="-2.286" y1="1.27" x2="0.508" y2="1.27" width="0.1524" layer="94"/>
<wire x1="0.508" y1="1.27" x2="0.508" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="0.508" y1="-1.27" x2="-2.286" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="-2.286" y1="-1.27" x2="-2.286" y2="1.27" width="0.1524" layer="94"/>
<circle x="-0.889" y="0" radius="0.889" width="0.254" layer="94"/>
<text x="-3.175" y="0.762" size="1.524" layer="95" rot="R180">&gt;NAME</text>
<text x="1.27" y="3.81" size="1.778" layer="96" rot="R180">&gt;VALUE</text>
<pin name="1" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="SCHRAUBKLEMME">
<wire x1="-1.27" y1="0.635" x2="-0.254" y2="-0.381" width="0.1524" layer="94"/>
<wire x1="-0.508" y1="-0.635" x2="-1.524" y2="0.381" width="0.1524" layer="94"/>
<wire x1="-2.286" y1="1.27" x2="0.508" y2="1.27" width="0.1524" layer="94"/>
<wire x1="0.508" y1="1.27" x2="0.508" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="0.508" y1="-1.27" x2="-2.286" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="-2.286" y1="-1.27" x2="-2.286" y2="1.27" width="0.1524" layer="94"/>
<circle x="-0.889" y="0" radius="0.889" width="0.254" layer="94"/>
<text x="-3.175" y="0.762" size="1.524" layer="95" rot="R180">&gt;NAME</text>
<pin name="1" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="OSTTE/4" prefix="X">
<description>TERM BLOCK 3.5MM VERT 4POS PCB &lt;br&gt;0.138" (3.50mm) Through Hole&lt;br&gt;10A 125V&lt;br&gt;16-26 AWG&lt;br&gt;Blue&lt;br&gt;Screw M2</description>
<gates>
<gate name="-1" symbol="SCHRAUBKLEMMEV" x="7.62" y="2.54" addlevel="always"/>
<gate name="-2" symbol="SCHRAUBKLEMME" x="7.62" y="0" addlevel="always"/>
<gate name="-3" symbol="SCHRAUBKLEMME" x="7.62" y="-2.54" addlevel="always"/>
<gate name="-4" symbol="SCHRAUBKLEMME" x="7.62" y="-5.08" addlevel="always"/>
</gates>
<devices>
<device name="" package="OSTTE/4">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="DATASHEET" value="http://www.on-shore.com/wp-content/uploads/2015/09/osttexx0161.pdf" constant="no"/>
<attribute name="DISTRIBUTOR" value="Digikey" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.se/product-detail/en/on-shore-technology-inc/OSTTE040161/ED2637-ND/614586" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="ED2637-ND" constant="no"/>
<attribute name="MANUFACTURER" value="On Shore Technologies" constant="no"/>
<attribute name="MANUFACTURER-PN" value="OSTTE040161" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="BCMI-rcl">
<description>Copyright (c) 2016-2017 Arduino LLC.  All right reserved.</description>
<packages>
<package name="0402-1005X55N">
<circle x="0" y="0" radius="0.25" width="0.05" layer="39"/>
<wire x1="-0.5" y1="-0.25" x2="0.5" y2="-0.25" width="0.025" layer="21"/>
<wire x1="0.5" y1="-0.25" x2="0.5" y2="0.25" width="0.025" layer="21"/>
<wire x1="0.5" y1="0.25" x2="-0.5" y2="0.25" width="0.025" layer="21"/>
<wire x1="-0.5" y1="0.25" x2="-0.5" y2="-0.25" width="0.025" layer="21"/>
<wire x1="-0.35" y1="0" x2="0.35" y2="0" width="0.05" layer="39"/>
<wire x1="0" y1="-0.35" x2="0" y2="0.35" width="0.05" layer="39"/>
<wire x1="-0.95" y1="0.45" x2="0.95" y2="0.45" width="0.05" layer="39"/>
<wire x1="0.95" y1="0.45" x2="0.95" y2="-0.45" width="0.05" layer="39"/>
<wire x1="0.95" y1="-0.45" x2="-0.95" y2="-0.45" width="0.05" layer="39"/>
<wire x1="-0.95" y1="-0.45" x2="-0.95" y2="0.45" width="0.05" layer="39"/>
<smd name="1" x="-0.45" y="0" dx="0.62" dy="0.62" layer="1" roundness="3"/>
<smd name="2" x="0.45" y="0" dx="0.62" dy="0.62" layer="1" roundness="3"/>
<text x="-0.9" y="0.5" size="0.4064" layer="51" font="vector" ratio="10">&gt;NAME</text>
</package>
<package name="0805-R2013X50N">
<circle x="0" y="0" radius="0.25" width="0.05" layer="39"/>
<wire x1="1" y1="-0.625" x2="1" y2="0.625" width="0.025" layer="51"/>
<wire x1="1" y1="0.625" x2="-1" y2="0.625" width="0.025" layer="51"/>
<wire x1="-1" y1="0.625" x2="-1" y2="-0.625" width="0.025" layer="51"/>
<wire x1="-1" y1="-0.625" x2="1" y2="-0.625" width="0.025" layer="51"/>
<wire x1="0" y1="-0.35" x2="0" y2="0.35" width="0.05" layer="39"/>
<wire x1="0.35" y1="0" x2="-0.35" y2="0" width="0.05" layer="39"/>
<wire x1="-1.75" y1="1" x2="1.75" y2="1" width="0.05" layer="39"/>
<wire x1="1.75" y1="1" x2="1.75" y2="-1" width="0.05" layer="39"/>
<wire x1="1.75" y1="-1" x2="-1.75" y2="-1" width="0.05" layer="39"/>
<wire x1="-1.75" y1="-1" x2="-1.75" y2="1" width="0.05" layer="39"/>
<smd name="1" x="-1" y="0" dx="0.95" dy="1.45" layer="1" roundness="3"/>
<smd name="2" x="1" y="0" dx="0.95" dy="1.45" layer="1" roundness="3"/>
<text x="0" y="0" size="1.2" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="0" y="0" size="1.2" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<text x="0" y="0" size="0.92" layer="51" font="vector" ratio="10">&gt;NAME</text>
</package>
<package name="0204V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 2.5 mm</description>
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.508" layer="51"/>
<wire x1="-0.127" y1="0" x2="0.127" y2="0" width="0.508" layer="21"/>
<circle x="-1.27" y="0" radius="0.889" width="0.1524" layer="51"/>
<circle x="-1.27" y="0" radius="0.635" width="0.0508" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="square"/>
<text x="-2.1336" y="1.1684" size="1.27" layer="25" ratio="12">&gt;NAME</text>
<text x="-2.1336" y="-2.3114" size="1.27" layer="27" ratio="12">&gt;VALUE</text>
</package>
<package name="PWR">
<description>Multilayer SMD</description>
<wire x1="-2.8004" y1="4.375" x2="2.8004" y2="4.375" width="0.127" layer="21"/>
<wire x1="2.8004" y1="-4.375" x2="-2.8004" y2="-4.375" width="0.127" layer="21"/>
<smd name="1" x="-5.4" y="0" dx="5.2" dy="8.75" layer="1"/>
<smd name="2" x="5.4" y="0" dx="5.2" dy="8.75" layer="1"/>
<text x="0" y="0" size="0.6096" layer="25" align="center">&gt;NAME</text>
</package>
<package name="C0201">
<smd name="1" x="0" y="0.35" dx="0.45" dy="0.45" layer="1" roundness="20"/>
<smd name="2" x="0" y="-0.33" dx="0.45" dy="0.45" layer="1" roundness="20"/>
<rectangle x1="-0.3" y1="-0.15" x2="0.3" y2="0.15" layer="21" rot="R270"/>
<text x="-0.9525" y="0" size="0.6096" layer="25" font="vector" ratio="10" rot="R270" align="center">&gt;NAME</text>
</package>
<package name="0603-1608X90N">
<circle x="0" y="0" radius="0.25" width="0.05" layer="39"/>
<wire x1="-0.814" y1="-0.4" x2="0.786" y2="-0.4" width="0.025" layer="51"/>
<wire x1="0.786" y1="-0.4" x2="0.786" y2="0.4" width="0.025" layer="51"/>
<wire x1="0.786" y1="0.4" x2="-0.814" y2="0.4" width="0.025" layer="51"/>
<wire x1="-0.814" y1="0.4" x2="-0.814" y2="-0.4" width="0.025" layer="51"/>
<wire x1="-0.35" y1="0" x2="0.35" y2="0" width="0.05" layer="39"/>
<wire x1="0" y1="-0.35" x2="0" y2="0.35" width="0.05" layer="39"/>
<wire x1="-1.6" y1="0.8" x2="-1.6" y2="-0.8" width="0.05" layer="39"/>
<wire x1="-1.6" y1="-0.8" x2="1.6" y2="-0.8" width="0.05" layer="39"/>
<wire x1="1.6" y1="-0.8" x2="1.6" y2="0.8" width="0.05" layer="39"/>
<wire x1="1.6" y1="0.8" x2="-1.6" y2="0.8" width="0.05" layer="39"/>
<smd name="1" x="-0.8" y="0" dx="0.95" dy="1" layer="1" roundness="3"/>
<smd name="2" x="0.8" y="0" dx="0.95" dy="1" layer="1" roundness="3"/>
<text x="-1.27" y="0.9525" size="0.6096" layer="25" font="vector" ratio="10">&gt;NAME</text>
</package>
<package name="C1206">
<smd name="1" x="0" y="1.4" dx="1.8" dy="1.45" layer="1" roundness="20"/>
<smd name="2" x="0" y="-1.4" dx="1.8" dy="1.45" layer="1" roundness="20"/>
<wire x1="-0.85" y1="1.6" x2="-0.85" y2="1" width="0.127" layer="21"/>
<wire x1="-0.85" y1="1" x2="-0.85" y2="-1" width="0.127" layer="21"/>
<wire x1="-0.85" y1="-1" x2="-0.85" y2="-1.6" width="0.127" layer="21"/>
<wire x1="0.85" y1="1.6" x2="0.85" y2="1" width="0.127" layer="21"/>
<wire x1="0.85" y1="1" x2="0.85" y2="-1" width="0.127" layer="21"/>
<wire x1="0.85" y1="-1" x2="0.85" y2="-1.6" width="0.127" layer="21"/>
<wire x1="0.85" y1="1.6" x2="-0.85" y2="1.6" width="0.127" layer="21"/>
<wire x1="0.85" y1="-1.6" x2="-0.85" y2="-1.6" width="0.127" layer="21"/>
<wire x1="0.85" y1="1" x2="-0.85" y2="1" width="0.127" layer="21"/>
<wire x1="0.85" y1="-1" x2="-0.85" y2="-1" width="0.127" layer="21"/>
<text x="-1.5875" y="0" size="0.8128" layer="25" font="vector" ratio="10" rot="R270" align="center">&gt;NAME</text>
</package>
<package name="TANT">
<wire x1="-5.11" y1="-2.8" x2="5.11" y2="-2.8" width="0.1" layer="39"/>
<wire x1="5.11" y1="2.8" x2="-5.11" y2="2.8" width="0.1" layer="39"/>
<wire x1="-5.11" y1="-2.8" x2="-5.11" y2="2.8" width="0.1" layer="39"/>
<wire x1="5.11" y1="-2.8" x2="5.11" y2="2.8" width="0.1" layer="39"/>
<smd name="+" x="-3.4" y="0" dx="2.6" dy="2.77" layer="1" rot="R90"/>
<smd name="-" x="3.4" y="0" dx="2.6" dy="2.77" layer="1" rot="R90"/>
<wire x1="-4.5" y1="2" x2="-4.5" y2="-2" width="0.1" layer="21"/>
<wire x1="-4.5" y1="-2" x2="4.5" y2="-2" width="0.1" layer="21"/>
<wire x1="4.5" y1="-2" x2="4.5" y2="2" width="0.1" layer="21"/>
<wire x1="4.5" y1="2" x2="-4.5" y2="2" width="0.1" layer="21"/>
<text x="-5.1" y="2" size="0.8128" layer="21">+</text>
<text x="0" y="0" size="0.8128" layer="25" align="center">&gt;NAME</text>
</package>
<package name="CAPAE530X610N">
<smd name="1" x="-1.9812" y="0" dx="2.8702" dy="0.762" layer="1"/>
<smd name="2" x="1.9812" y="0" dx="2.8702" dy="0.762" layer="1"/>
<wire x1="3.6576" y1="-2.9972" x2="-3.6576" y2="-2.9972" width="0.1524" layer="39"/>
<wire x1="-3.6576" y1="-2.9972" x2="-3.6576" y2="2.9972" width="0.1524" layer="39"/>
<wire x1="-3.6576" y1="2.9972" x2="3.6576" y2="2.9972" width="0.1524" layer="39"/>
<wire x1="3.6576" y1="2.9972" x2="3.6576" y2="-2.9972" width="0.1524" layer="39"/>
<wire x1="2.7432" y1="0.7112" x2="2.7432" y2="2.7432" width="0.1524" layer="21"/>
<wire x1="-2.7432" y1="-0.7112" x2="-2.7432" y2="-1.3716" width="0.1524" layer="21"/>
<wire x1="-2.7432" y1="-1.3716" x2="-2.7432" y2="-2.7432" width="0.1524" layer="21"/>
<wire x1="-1.6002" y1="1.27" x2="-2.3622" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.9812" y1="1.651" x2="-1.9812" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.7432" y1="1.3716" x2="-1.3716" y2="2.7432" width="0.1524" layer="21"/>
<wire x1="-2.7432" y1="-1.3716" x2="-1.3716" y2="-2.7432" width="0.1524" layer="21"/>
<wire x1="-2.7432" y1="-2.7432" x2="2.7432" y2="-2.7432" width="0.1524" layer="21"/>
<wire x1="2.7432" y1="-2.7432" x2="2.7432" y2="-0.7112" width="0.1524" layer="21"/>
<wire x1="2.7432" y1="2.7432" x2="-2.7432" y2="2.7432" width="0.1524" layer="21"/>
<wire x1="-2.7432" y1="2.7432" x2="-2.7432" y2="0.7112" width="0.1524" layer="21"/>
<wire x1="-1.6002" y1="1.27" x2="-2.3622" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-1.9812" y1="1.651" x2="-1.9812" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-2.7432" y1="1.3716" x2="-1.3716" y2="2.7432" width="0.1524" layer="51"/>
<wire x1="-2.7432" y1="-1.3716" x2="-1.3716" y2="-2.7432" width="0.1524" layer="51"/>
<wire x1="-2.7432" y1="-2.7432" x2="2.7432" y2="-2.7432" width="0.1524" layer="51"/>
<wire x1="2.7432" y1="-2.7432" x2="2.7432" y2="2.7432" width="0.1524" layer="51"/>
<wire x1="2.7432" y1="2.7432" x2="-2.7432" y2="2.7432" width="0.1524" layer="51"/>
<wire x1="-2.7432" y1="2.7432" x2="-2.7432" y2="-2.7432" width="0.1524" layer="51"/>
<text x="-4.7498" y="3.1242" size="2.0828" layer="25" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-5.7658" y="-5.1562" size="2.0828" layer="27" ratio="10" rot="SR0">&gt;VALUE</text>
</package>
<package name="SMC_B">
<description>&lt;b&gt;Chip Capacitor &lt;/b&gt; Polar tantalum capacitors with solid electrolyte&lt;p&gt;
Siemens Matsushita Components B 45 194, B 45 197, B 45 198&lt;br&gt;
Source: www.farnell.com/datasheets/247.pdf</description>
<wire x1="-1.6" y1="1.35" x2="1.6" y2="1.35" width="0.1016" layer="21"/>
<wire x1="1.6" y1="1.35" x2="1.6" y2="-1.35" width="0.1016" layer="21"/>
<wire x1="1.6" y1="-1.35" x2="-1.6" y2="-1.35" width="0.1016" layer="21"/>
<wire x1="-1.6" y1="-1.35" x2="-1.6" y2="1.35" width="0.1016" layer="21"/>
<smd name="+" x="-1.5" y="0" dx="1.6" dy="2.4" layer="1"/>
<smd name="-" x="1.5" y="0" dx="1.6" dy="2.4" layer="1" rot="R180"/>
<text x="-1.905" y="1.905" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.75" y1="-1.1" x2="-1.55" y2="1.1" layer="21"/>
<rectangle x1="1.55" y1="-1.1" x2="1.75" y2="1.1" layer="21" rot="R180"/>
<rectangle x1="-1.6" y1="-1.35" x2="-0.95" y2="1.35" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="R">
<description>Resistor</description>
<wire x1="-2.286" y1="0.762" x2="2.286" y2="0.762" width="0.2794" layer="94"/>
<wire x1="-2.286" y1="-0.762" x2="2.286" y2="-0.762" width="0.2794" layer="94"/>
<wire x1="-2.286" y1="-0.762" x2="-2.286" y2="0" width="0.2794" layer="94"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="0.762" width="0.2794" layer="94"/>
<wire x1="2.286" y1="0.762" x2="2.286" y2="0" width="0.2794" layer="94"/>
<wire x1="2.286" y1="0" x2="2.286" y2="-0.762" width="0.2794" layer="94"/>
<wire x1="2.286" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-2.286" y2="0" width="0.1524" layer="94"/>
<pin name="1" x="-2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
<pin name="2" x="2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
<text x="0" y="1.27" size="1.778" layer="95" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.27" size="1.778" layer="96" align="top-center">&gt;VALUE</text>
</symbol>
<symbol name="C">
<rectangle x1="-2.032" y1="-0.762" x2="2.032" y2="-0.254" layer="94"/>
<rectangle x1="-2.032" y1="0.254" x2="2.032" y2="0.762" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="0.762" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-0.762" width="0.1524" layer="94"/>
<pin name="1" x="0" y="2.54" visible="off" length="point" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="point" direction="pas" swaplevel="1" rot="R90"/>
<text x="1.524" y="1.651" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-3.429" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="C+">
<rectangle x1="-2.032" y1="-0.762" x2="2.032" y2="-0.254" layer="94"/>
<rectangle x1="-2.032" y1="0.254" x2="2.032" y2="0.762" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="0.762" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-0.762" width="0.1524" layer="94"/>
<pin name="+" x="0" y="2.54" visible="off" length="point" direction="pas" swaplevel="1" rot="R270"/>
<pin name="-" x="0" y="-2.54" visible="off" length="point" direction="pas" swaplevel="1" rot="R90"/>
<text x="1.524" y="1.651" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-3.429" size="1.778" layer="96">&gt;VALUE</text>
<text x="-1.905" y="0.889" size="1.4224" layer="94">+</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="R" prefix="R" uservalue="yes">
<description>SMD Resistor
&lt;br&gt; RC0603FR-071KP :1k Ohm ±1% 0.1W, 1/10W Chip Resistor 0603 (1608 Metric) Moisture Resistant Thick Film</description>
<gates>
<gate name="G$1" symbol="R" x="0" y="0"/>
</gates>
<devices>
<device name="-0402" package="0402-1005X55N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DATASHEET" value="" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MANUFACTURER-PN" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
<technology name="-140R">
<attribute name="DATASHEET" value="www.yageo.com.tw/exep/pages/download/literatures/PYu-R_INT-thick_7.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.se/products/en/resistors/chip-resistor-surface-mount/52?k=resistor&amp;k=&amp;pkeyword=resistor&amp;pv7=2&amp;FV=fffc000d%2C400ad%2C400004%2C1f140000%2Cffe00034%2Cc0001&amp;mnonly=0&amp;ColumnSort=0&amp;page=1&amp;stock=1&amp;quantity=0&amp;ptm=0&amp;fid=0&amp;pageSize=500" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="311-140LRCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER-PN" value="RC0402FR-07140RL" constant="no"/>
<attribute name="VALUE" value="140R" constant="no"/>
</technology>
<technology name="-1K">
<attribute name="DATASHEET" value="http://www.yageo.com/NewPortal/yageodocoutput?fileName=/pdf/R-Chip/PYu-AC_51_RoHS_L_6.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.se/product-detail/en/yageo/AC0402FR-071KL/311-1KLBCT-ND/2828122" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="311-1KLBCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER-PN" value="AC0402FR-071KL" constant="no"/>
<attribute name="VALUE" value="1K" constant="no"/>
</technology>
<technology name="-1M">
<attribute name="DATASHEET" value="http://www.yageo.com/NewPortal/yageodocoutput?fileName=/pdf/R-Chip/PYu-AC_51_RoHS_L_6.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.se/product-detail/en/yageo/AC0402FR-071ML/YAG3450CT-ND/6006299" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="YAG3450CT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER-PN" value="AC0402FR-071ML" constant="no"/>
<attribute name="VALUE" value="1M" constant="no"/>
</technology>
<technology name="-200K">
<attribute name="DATASHEET" value="http://www.yageo.com/NewPortal/yageodocoutput?fileName=/pdf/R-Chip/PYu-AC_51_RoHS_L_6.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.se/product-detail/en/yageo/AC0402FR-07200KL/YAG3452CT-ND/6006301" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="YAG3452CT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER-PN" value="AC0402FR-07200KL" constant="no"/>
<attribute name="VALUE" value="200K" constant="no"/>
</technology>
<technology name="-220K">
<attribute name="DATASHEET" value="www.yageo.com.tw/exep/pages/download/literatures/PYu-R_INT-thick_7.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/yageo/RC0402JR-07220KL/311-220KJRCT-ND/729390" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="311-220KJRCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER-PN" value="RC0402JR-07220KL" constant="no"/>
<attribute name="VALUE" value="220k" constant="no"/>
</technology>
<technology name="-39R">
<attribute name="DATASHEET" value="www.yageo.com.tw/exep/pages/download/literatures/PYu-R_INT-thick_7.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="http://www.digikey.com/product-detail/en/yageo/RC0402FR-0739RL/311-39.0LRCT-ND/729549" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="311-39.0LRCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo " constant="no"/>
<attribute name="MANUFACTURER-PN" value="RC0402FR-0739RL " constant="no"/>
<attribute name="VALUE" value="39R" constant="no"/>
</technology>
<technology name="-43K">
<attribute name="DATASHEET" value="www.yageo.com.tw/exep/pages/download/literatures/PYu-R_INT-thick_7.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/yageo/RC0402FR-0743KL/311-43.0KLRCT-ND/729556" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="311-43.0KLRCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER-PN" value="RC0402FR-0743KL" constant="no"/>
<attribute name="VALUE" value="43k" constant="no"/>
</technology>
<technology name="-47K">
<attribute name="DATASHEET" value="www.yageo.com.tw/exep/pages/download/literatures/PYu-R_INT-thick_7.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="http://www.digikey.com/product-detail/en/yageo/RC0402JR-0747KL/311-47KJRCT-ND/729432" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="311-47KJRCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER-PN" value="RC0402JR-0747KL" constant="no"/>
<attribute name="VALUE" value="47k" constant="no"/>
</technology>
<technology name="-4K7">
<attribute name="DATASHEET" value="www.yageo.com.tw/exep/pages/download/literatures/PYu-R_INT-thick_7.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="http://www.digikey.com/product-detail/en/yageo/RC0402FR-074K7L/311-4.7KLRCT-ND/2827881" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="311-4.7KLRCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo " constant="no"/>
<attribute name="MANUFACTURER-PN" value="RC0402FR-074K7L " constant="no"/>
<attribute name="VALUE" value="4k7" constant="no"/>
</technology>
<technology name="-5K49">
<attribute name="DATASHEET" value="www.yageo.com.tw/exep/pages/download/literatures/PYu-R_INT-thick_7.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/yageo/RC0402FR-075K49L/311-5.49KLRCT-ND/729573" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="311-5.49KLRCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER-PN" value="RC0402FR-075K49L" constant="no"/>
<attribute name="VALUE" value="5k49" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0805" package="0805-R2013X50N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="-0.002R">
<attribute name="DATASHEET" value="http://www.rohm.com/web/global/datasheet/PMR18EZPFV/pmr-e" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.se/products/en/resistors/chip-resistor-surface-mount/52?k=chip+resistor&amp;k=&amp;pkeyword=chip+resistor&amp;pv7=2&amp;FV=4031b%2C142c040d%2C1f140000%2Cffe00034%2Cc0001&amp;mnonly=0&amp;ColumnSort=0&amp;page=1&amp;stock=1&amp;quantity=0&amp;ptm=0&amp;fid=0&amp;pageSize=500" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="RHM.002AJCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Rohm Semiconductor" constant="no"/>
<attribute name="MANUFACTURER-PN" value="PMR10EZPFV2L00" constant="no"/>
<attribute name="VALUE" value="0.002R" constant="no"/>
</technology>
<technology name="-0.012R">
<attribute name="DATASHEET" value="https://industrial.panasonic.com/cdbs/www-data/pdf/RDA0000/AOA0000C295.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="http://www.digikey.com/product-detail/en/panasonic-electronic-components/ERJ-6CWFR012V/P19198CT-ND/6004553" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="P19198CT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Panasonic" constant="no"/>
<attribute name="MANUFACTURER-PN" value="ERJ-6CWFR012V" constant="no"/>
<attribute name="VALUE" value="0.012R" constant="no"/>
</technology>
<technology name="-100R">
<attribute name="DATASHEET" value="http://www.yageo.com.tw/exep/pages/download/literatures/PYu-R_INT-thick_7.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="http://www.digikey.de/product-detail/en/yageo/RC0805FR-07100RL/311-100CRTR-ND/727543?cur=EUR&amp;lang=en " constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="311-100CRTR-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER-PN" value="RC0805FR-07100RL" constant="no"/>
<attribute name="VALUE" value="100R" constant="no"/>
</technology>
<technology name="-DNP">
<attribute name="DATASHEET" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="DNP" constant="no"/>
<attribute name="MANUFACTURER" value="DNP" constant="no"/>
<attribute name="MANUFACTURER-PN" value="DNP" constant="no"/>
<attribute name="VALUE" value="DNP" constant="no"/>
</technology>
</technologies>
</device>
<device name="TH-VERTICAL" package="0204V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DATASHEET" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="DNP" constant="no"/>
<attribute name="MANUFACTURER" value="DNP" constant="no"/>
<attribute name="MANUFACTURER-PN" value="DNP" constant="no"/>
<attribute name="VALUE" value="DNP" constant="no"/>
</technology>
<technology name="-1M">
<attribute name="DATASHEET" value="" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MANUFACTURER-PN" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="PWR" package="PWR">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="-.03MR">
<attribute name="DATASHEET" value="http://www.vishay.com/docs/30176/wslp3921.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/products/en?keywords=%20WSLPD-.0003CT-ND%20" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="WSLPD-.0003CT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MANUFCATURER-PN" value="WSLP5931L3000FEB" constant="no"/>
<attribute name="VALUE" value="0.3mR" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0201" package="C0201">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="-0R">
<attribute name="DATASHEET" value="http://www.yageo.com.tw/exep/pages/download/literatures/PYu-R_INT-thick_7.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/yageo/RC0201JR-070RL/311-0.0NCT-ND/1949004" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="311-0.0NCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER-PN" value="RC0201JR-070RL" constant="no"/>
<attribute name="VALUE" value="0R" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0603" package="0603-1608X90N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DATASHEET" value="" constant="no"/>
<attribute name="DISTRIBUTOR" value="" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MANUFACTURER-PN" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
<technology name="-1K">
<attribute name="DATASHEET" value="http://www.yageo.com.tw/exep/pages/download/literatures/PYu-R_INT-thick_7.pdf" constant="no"/>
<attribute name="DISTRIBUTOR" value="Digikey" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.se/product-detail/en/yageo/RC0603FR-071KP/YAG1294CT-ND/4935407" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="YAG1294CT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER-PN" value="RC0603FR-071KP" constant="no"/>
<attribute name="VALUE" value="1K" constant="no"/>
</technology>
<technology name="-330R">
<attribute name="DATASHEET" value="http://www.yageo.com.tw/exep/pages/download/literatures/PYu-R_INT-thick_7.pdf" constant="no"/>
<attribute name="DISTRIBUTOR" value="Digikey" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.se/product-detail/en/yageo/RC0603JR-07330RL/311-330GRCT-ND/729716" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="311-330GRCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER-PN" value="RC0603JR-07330RL" constant="no"/>
<attribute name="VALUE" value="330R" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="C" prefix="C" uservalue="yes">
<description>&lt;b&gt;SMD Capacitor&lt;/b&gt;&lt;br&gt;

CGA5L3X5R1V106K160AB: CAP CER 10UF 35V X5R 1206</description>
<gates>
<gate name="G$1" symbol="C" x="0" y="0"/>
</gates>
<devices>
<device name="-0402" package="0402-1005X55N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="-100NF">
<attribute name="DATASHEET" value="http://www.tdk.com/pdf/TDKMLCCCapRange.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="http://www.digikey.com/product-detail/en/tdk-corporation/C1005X5R1H104K050BB/445-5942-1-ND/2443982" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="445-5942-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="TDK" constant="no"/>
<attribute name="MANUFACTURER-PN" value="C1005X5R1H104K050BB" constant="no"/>
<attribute name="VALUE" value="100n" constant="no"/>
</technology>
<technology name="-10NF">
<attribute name="DATASHEET" value="http://search.murata.co.jp/Ceramy/image/img/A01X/partnumbering_e_01.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="http://www.digikey.com/product-detail/en/murata-electronics-north-america/GRM155R71H103KA88J/490-6351-1-ND/3845548" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="490-6351-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Murata " constant="no"/>
<attribute name="MANUFACTURER-PN" value="GRM155R71H103KA88J " constant="no"/>
<attribute name="VALUE" value="10n" constant="no"/>
</technology>
<technology name="-110P">
<attribute name="DATASHEET" value="http://search.murata.co.jp/Ceramy/image/img/A01X/G101/ENG/GRM1555C1H111GA01-01.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/murata-electronics-north-america/GRM1555C1H111GA01D/490-6195-1-ND/3845392" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="490-6195-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Murata" constant="no"/>
<attribute name="MANUFACTURER-PN" value="GRM1555C1H111GA01D" constant="no"/>
<attribute name="VALUE" value="110p" constant="no"/>
</technology>
<technology name="-15PF">
<attribute name="DATASHEET" value="http://search.murata.co.jp/Ceramy/image/img/A01X/partnumbering_e_01.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/murata-electronics-north-america/GRM1555C1H150JA01D/490-5888-1-ND/3315234" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="490-5888-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Murata" constant="no"/>
<attribute name="MANUFACTURER-PN" value="GRM1555C1H150JA01D" constant="no"/>
<attribute name="VALUE" value="15p" constant="no"/>
</technology>
<technology name="-1NF">
<attribute name="DATASHEET" value="http://www.samsungsem.com/kr/support/product-search/mlcc/__icsFiles/afieldfile/2016/08/18/S_CL05B102KB5NNNC.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="http://www.digikey.com/product-detail/en/samsung-electro-mechanics-america-inc/CL05B102KB5NNNC/1276-1032-1-ND/3889118" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="1276-1032-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Samsung" constant="no"/>
<attribute name="MANUFACTURER-PN" value="CL05B102KB5NNNC " constant="no"/>
<attribute name="VALUE" value="1n" constant="no"/>
</technology>
<technology name="-1UF">
<attribute name="DATASHEET" value="http://search.murata.co.jp/Ceramy/image/img/A01X/partnumbering_e_01.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="http://www.digikey.com/product-detail/en/murata-electronics-north-america/GRM155R61E105KA12D/490-10017-1-ND/5026367" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="490-10017-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Murata" constant="no"/>
<attribute name="MANUFACTURER-PN" value="GRM155R61E105KA12D " constant="no"/>
<attribute name="VALUE" value="1u" constant="no"/>
</technology>
<technology name="-2.2UF">
<attribute name="DATASHEET" value="http://search.murata.co.jp/Ceramy/image/img/A01X/partnumbering_e_01.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/murata-electronics-north-america/GRM155R61A225KE95D/490-10451-1-ND/5026361" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="490-10451-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Murata" constant="no"/>
<attribute name="MANUFACTURER-PN" value="GRM155R61A225KE95D" constant="no"/>
<attribute name="VALUE" value="2u2" constant="no"/>
</technology>
<technology name="-20PF">
<attribute name="DATASHEET" value="http://www.kemet.com/docfinder?Partnumber=CBR04C200J5GAC" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="http://www.digikey.com/product-detail/en/kemet/CBR04C200J5GAC/399-6173-1-ND/2732143" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="399-6173-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Kemet" constant="no"/>
<attribute name="MANUFACTURER-PN" value="CBR04C200J5GAC " constant="no"/>
<attribute name="VALUE" value="20p" constant="no"/>
</technology>
<technology name="-22N">
<attribute name="DATASHEET" value="http://www.murata.com/~/media/webrenewal/support/library/catalog/products/capacitor/mlcc/c02e.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="http://www.digikey.com/product-detail/en/murata-electronics-north-america/GRM155R71H223KA12D/490-3884-1-ND/965926" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="490-3884-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Murata" constant="no"/>
<attribute name="MANUFACTURER-PN" value="GRM155R71H223KA12D" constant="no"/>
<attribute name="VALUE" value="22n" constant="no"/>
</technology>
<technology name="-33PF">
<attribute name="DATASHEET" value="http://www.murata.com/~/media/webrenewal/support/library/catalog/products/capacitor/mlcc/c02e.pdf " constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="http://www.digikey.com/product-search/en?keywords=490-5936-1-ND" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="490-5936-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Murata" constant="no"/>
<attribute name="MANUFACTURER-PN" value="GRM1555C1H330JA01D" constant="no"/>
<attribute name="VALUE" value="33p" constant="no"/>
</technology>
<technology name="-4.7UF">
<attribute name="DATASHEET" value="http://search.murata.co.jp/Ceramy/image/img/A01X/partnumbering_e_01.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="http://www.digikey.com/product-detail/en/murata-electronics-north-america/GRM155R60J475ME47D/490-5915-1-ND/3719860" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="490-5915-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Murata" constant="no"/>
<attribute name="MANUFACTURER-PN" value="GRM155R60J475ME47D " constant="no"/>
<attribute name="VALUE" value="4u7" constant="no"/>
</technology>
<technology name="-47N">
<attribute name="DATASHEET" value="http://www.murata.com/~/media/webrenewal/support/library/catalog/products/capacitor/mlcc/c02e.pdf " constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/murata-electronics-north-america/GRM155R71E473KA88D/490-3254-1-ND/702795" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="490-3254-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Murata" constant="no"/>
<attribute name="MANUFACTURER-PN" value="GRM155R71E473KA88D" constant="no"/>
<attribute name="VALUE" value="47n" constant="no"/>
</technology>
<technology name="-4N7">
<attribute name="DATASHEET" value="http://search.murata.co.jp/Ceramy/image/img/A01X/partnumbering_e_01.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/murata-electronics-north-america/GRM155R71H472KA01J/490-8259-1-ND/4380553" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="490-8259-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Murata" constant="no"/>
<attribute name="MANUFACTURER-PN" value="GRM155R71H472KA01J" constant="no"/>
<attribute name="VALUE" value="4n7" constant="no"/>
</technology>
<technology name="-56P">
<attribute name="DATASHEET" value="http://www.mouser.com/ds/2/281/c02e-2905.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="http://www.mouser.it/ProductDetail/Murata-Electronics/GRM1555C1E560JA01D/?qs=sGAEpiMZZMs0AnBnWHyRQA3eyUP2RfX77sTVhhB4SFb8UyJFwKUI8g%3d%3d" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="81-GRM1555C1E560JA1D" constant="no"/>
<attribute name="MANUFACTURER" value="Murata" constant="no"/>
<attribute name="MANUFACTURER-PN" value=" GRM1555C1E560JA01D " constant="no"/>
<attribute name="VALUE" value="56p" constant="no"/>
</technology>
<technology name="-6N8">
<attribute name="DATASHEET" value="http://search.murata.co.jp/Ceramy/image/img/A01X/G101/ENG/GRM155R71H682KA88-01.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="http://www.digikey.com/product-detail/en/murata-electronics-north-america/GRM155R71H682KA88D/490-4515-1-ND/1033274" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="490-4515-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Murata" constant="no"/>
<attribute name="MANUFACTURER-PN" value="GRM155R71H682KA88D" constant="no"/>
<attribute name="VALUE" value="6n8" constant="no"/>
</technology>
<technology name="-6PF">
<attribute name="DATASHEET" value="http://www.samsungsem.com/kr/support/product-search/mlcc/__icsFiles/afieldfile/2016/08/18/S_CL05C060DB5NNNC.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.se/product-detail/en/samsung-electro-mechanics-america-inc/CL05C060DB5NNNC/1276-1609-1-ND/3889695" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="1276-1609-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Samsung" constant="no"/>
<attribute name="MANUFACTURER-PN" value="CL05C060DB5NNNC" constant="no"/>
<attribute name="VALUE" value="6pF" constant="no"/>
</technology>
<technology name="-8N2">
<attribute name="DATASHEET" value="http://www.yageo.com/documents/recent/UPY-GPHC_X7R_6.3V-to-50V_18.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/yageo/CC0402KRX7R7BB822/311-1041-1-ND/302958" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="311-1041-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER-PN" value="CC0402KRX7R7BB822" constant="no"/>
<attribute name="VALUE" value="8n2" constant="no"/>
</technology>
<technology name="DNP">
<attribute name="DATASHEET" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="DNP" constant="no"/>
<attribute name="MANUFACTURER" value="DNP" constant="no"/>
<attribute name="MANUFACTURER-PN" value="DNP" constant="no"/>
<attribute name="VALUE" value="DNP" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0805" package="0805-R2013X50N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="-22U">
<attribute name="DATASHEET" value="https://media.digikey.com/pdf/Data%20Sheets/Samsung%20PDFs/CL21A226MQCLQNC_character.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/samsung-electro-mechanics-america-inc/CL21A226MQCLQNC/1276-2412-1-ND/3890498" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="1276-2412-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Samsung" constant="no"/>
<attribute name="MANUFACTURER-PN" value="CL21A226MQCLQNC" constant="no"/>
<attribute name="VALUE" value="22u" constant="no"/>
</technology>
<technology name="4.7NF">
<attribute name="DATASHEET" value="http://www.kemet.com/Lists/ProductCatalog/Attachments/40/KEM_C1010_X7R_HV_SMD.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="www.kemet.com/docfinder?Partnumber=C0805C472KDRACTU" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="399-6738-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Kemet" constant="no"/>
<attribute name="MANUFACTURER-PN" value="C0805C472KDRACTU" constant="no"/>
<attribute name="VALUE" value="4n7" constant="no"/>
</technology>
<technology name="DNP">
<attribute name="DATASHEET" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="DNP" constant="no"/>
<attribute name="MANUFACTURER" value="DNP" constant="no"/>
<attribute name="MANUFACTURER-PN" value="DNP" constant="no"/>
<attribute name="VALUE" value="DNP" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0603" package="0603-1608X90N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="-10UF">
<attribute name="DATASHEET" value="http://www.murata.com/~/media/webrenewal/support/library/catalog/products/capacitor/mlcc/c02e.pdf " constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="http://www.digikey.com/product-search/en?keywords=490-10728-1-ND " constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="490-10728-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Murata" constant="no"/>
<attribute name="MANUFACTURER-PN" value="GRM188R61C106KAALD" constant="no"/>
<attribute name="VALUE" value="10u" constant="no"/>
</technology>
<technology name="-4.7UF">
<attribute name="DATASHEET" value="http://www.murata.com/~/media/webrenewal/support/library/catalog/products/capacitor/mlcc/c02e.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="http://www.digikey.com/product-search/en?keywords=490-7203-1-ND" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="490-7203-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Murata" constant="no"/>
<attribute name="MANUFACTURER-PN" value="GRM188R61E475KE11D" constant="no"/>
<attribute name="VALUE" value="4u7" constant="no"/>
</technology>
<technology name="-DNP">
<attribute name="DATASHEET" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="DNP" constant="no"/>
<attribute name="MANUFACTURER" value="DNP" constant="no"/>
<attribute name="MANUFACTURER-PN" value="DNP" constant="no"/>
<attribute name="VALUE" value="DNP" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1206" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="-100UF">
<attribute name="DATASHEET" value="http://search.murata.co.jp/Ceramy/image/img/A01X/partnumbering_e_01.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/murata-electronics-north-america/GRM31CD80J107ME39L/490-10525-1-ND/5026461" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="490-10525-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Murata" constant="no"/>
<attribute name="MANUFACTURER-PN" value="GRM31CD80J107ME39L" constant="no"/>
<attribute name="VALUE" value="100uF" constant="no"/>
</technology>
<technology name="-10UF">
<attribute name="DATASHEET" value="https://product.tdk.com/info/en/documents/chara_sheet/CGA5L3X5R1V106K160AB.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.se/products/en/capacitors/ceramic-capacitors/60?k=CGA5L3X5R1V106K160AB&amp;k=&amp;pkeyword=CGA5L3X5R1V106K160AB&amp;pv7=2&amp;mnonly=0&amp;ColumnSort=0&amp;page=1&amp;quantity=0&amp;ptm=0&amp;fid=0&amp;pageSize=500" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="445-7887-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="TDK" constant="no"/>
<attribute name="MANUFACTURER-PN" value="  CGA5L3X5R1V106K160AB" constant="no"/>
<attribute name="VALUE" value="10uF" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="C+" prefix="C">
<description>TCJB106M025R0090:        10µF ±20% Molded Tantalum Polymer Capacitor 25V 1411 (3528 Metric) 90 mOhm&lt;br&gt;
TCNT476M016R0200A:   47µF Molded Tantalum Polymer Capacitor 16V 1411 (3528 Metric), 1210 200 mOhm</description>
<gates>
<gate name="G$1" symbol="C+" x="0" y="0"/>
</gates>
<devices>
<device name="KEMET-D" package="TANT">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="DATASHEET" value="http://www.kemet.com/Lists/ProductCatalog/Attachments/684/KEM_T2076_T52X-530.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/product-detail/en/kemet/T520D337M006ATE045/399-4055-2-ND/818916" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="399-4055-2-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Kemet" constant="no"/>
<attribute name="MANUFACTURER-PN" value="T520D337M006ATE045" constant="no"/>
<attribute name="VALUE" value="330u" constant="no"/>
</technology>
</technologies>
</device>
<device name="CASE-C" package="CAPAE530X610N">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name="100UF-">
<attribute name="DATASHEET" value="https://industrial.panasonic.com/cdbs/www-data/pdf/RDE0000/ABA0000C1145.pdf" constant="no"/>
<attribute name="DISTRIBUTOR" value="Digikey" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.se/product-detail/en/panasonic-electronic-components/EEE-1AA101WR/PCE3867CT-ND/766243" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="PCE3867CT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Panasonic" constant="no"/>
<attribute name="MANUFACTURER-PN" value="EEE1AA101WR" constant="no"/>
<attribute name="VALUE" value="100uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="AVX-TCJ" package="SMC_B">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="-47UF">
<attribute name="DATASHEET" value="http://catalogs.avx.com/TantalumNiobium.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.se/products/en?keywords=%09478-9595-1-ND" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="478-9595-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="AVX" constant="no"/>
<attribute name="MANUFACTURER-PN" value="TCNT476M016R0200A" constant="no"/>
<attribute name="VALUE" value="47uF" constant="no"/>
</technology>
<technology name="10UF-">
<attribute name="DATASHEET" value="http://datasheets.avx.com/TCJ.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.se/product-detail/en/avx-corporation/TCJB106M025R0090/478-9407-1-ND/5001620" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="478-9407-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="AVX" constant="no"/>
<attribute name="MANUFACTURER-PN" value="TCJB106M025R0090" constant="no"/>
<attribute name="VALUE" value="10uF" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="BCMI-diodes">
<description>Copyright (c) 2016-2017 Arduino LLC.  All right reserved.</description>
<packages>
<package name="LEDC1608X80N-AK">
<circle x="0" y="0" radius="0.25" width="0.05" layer="39"/>
<wire x1="-0.645" y1="-0.395" x2="-0.355" y2="-0.395" width="0.01" layer="51"/>
<wire x1="-0.355" y1="-0.395" x2="-0.355" y2="0.395" width="0.01" layer="21"/>
<wire x1="-0.355" y1="0.395" x2="-0.645" y2="0.395" width="0.01" layer="51"/>
<wire x1="-0.645" y1="0.395" x2="-0.645" y2="-0.395" width="0.01" layer="21"/>
<wire x1="-0.64" y1="-0.39" x2="-0.36" y2="-0.39" width="0.01" layer="21"/>
<wire x1="-0.36" y1="-0.39" x2="-0.36" y2="0.39" width="0.01" layer="21"/>
<wire x1="-0.36" y1="0.39" x2="-0.64" y2="0.39" width="0.01" layer="21"/>
<wire x1="-0.64" y1="0.39" x2="-0.64" y2="-0.39" width="0.01" layer="21"/>
<wire x1="-0.53" y1="-0.28" x2="-0.47" y2="-0.28" width="0.2" layer="51"/>
<wire x1="-0.47" y1="-0.28" x2="-0.47" y2="0.28" width="0.2" layer="51"/>
<wire x1="-0.47" y1="0.28" x2="-0.53" y2="0.28" width="0.2" layer="51"/>
<wire x1="-0.53" y1="0.28" x2="-0.53" y2="-0.28" width="0.2" layer="21"/>
<wire x1="-0.58" y1="-0.33" x2="-0.42" y2="-0.33" width="0.1" layer="21"/>
<wire x1="-0.42" y1="-0.33" x2="-0.42" y2="0.33" width="0.1" layer="21"/>
<wire x1="-0.42" y1="0.33" x2="-0.58" y2="0.33" width="0.1" layer="51"/>
<wire x1="-0.58" y1="0.33" x2="-0.58" y2="-0.33" width="0.1" layer="51"/>
<wire x1="-0.8" y1="-0.4" x2="0.8" y2="-0.4" width="0.025" layer="21"/>
<wire x1="0.8" y1="-0.4" x2="0.8" y2="0.4" width="0.025" layer="21"/>
<wire x1="0.8" y1="0.4" x2="-0.8" y2="0.4" width="0.025" layer="21"/>
<wire x1="-0.8" y1="0.4" x2="-0.8" y2="-0.4" width="0.025" layer="21"/>
<wire x1="-0.63" y1="-0.38" x2="-0.37" y2="-0.38" width="0.025" layer="21"/>
<wire x1="-0.37" y1="-0.38" x2="-0.37" y2="0.38" width="0.025" layer="21"/>
<wire x1="-0.37" y1="0.38" x2="-0.63" y2="0.38" width="0.025" layer="21"/>
<wire x1="-0.63" y1="0.38" x2="-0.63" y2="-0.38" width="0.025" layer="21"/>
<wire x1="-0.618" y1="-0.367" x2="-0.383" y2="-0.367" width="0.025" layer="21"/>
<wire x1="-0.383" y1="-0.367" x2="-0.383" y2="0.368" width="0.025" layer="21"/>
<wire x1="-0.383" y1="0.368" x2="-0.618" y2="0.368" width="0.025" layer="51"/>
<wire x1="-0.618" y1="0.368" x2="-0.618" y2="-0.367" width="0.025" layer="21"/>
<wire x1="-0.605" y1="-0.355" x2="-0.395" y2="-0.355" width="0.05" layer="21"/>
<wire x1="-0.395" y1="-0.355" x2="-0.395" y2="0.355" width="0.05" layer="51"/>
<wire x1="-0.395" y1="0.355" x2="-0.605" y2="0.355" width="0.05" layer="21"/>
<wire x1="-0.605" y1="0.355" x2="-0.605" y2="-0.355" width="0.05" layer="51"/>
<wire x1="-0.35" y1="0" x2="0.35" y2="0" width="0.05" layer="39"/>
<wire x1="0" y1="-0.35" x2="0" y2="0.35" width="0.05" layer="39"/>
<wire x1="-1.55" y1="0.75" x2="-1.55" y2="-0.75" width="0.05" layer="39"/>
<wire x1="-1.55" y1="-0.75" x2="1.55" y2="-0.75" width="0.05" layer="39"/>
<wire x1="1.55" y1="-0.75" x2="1.55" y2="0.75" width="0.05" layer="39"/>
<wire x1="1.55" y1="0.75" x2="-1.55" y2="0.75" width="0.05" layer="39"/>
<smd name="A" x="0.8" y="0" dx="0.9" dy="0.95" layer="1" roundness="3"/>
<smd name="C" x="-0.8" y="0" dx="0.9" dy="0.95" layer="1" roundness="3"/>
<text x="-1.27" y="-2.54" size="0.6096" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<text x="0" y="-1.27" size="0.6096" layer="51" font="vector" ratio="10" align="center">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="LED">
<wire x1="1.27" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="-0.762" x2="-3.429" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-3.302" y2="-3.302" width="0.1524" layer="94"/>
<pin name="A" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="C" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<text x="3.556" y="-4.572" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="-4.572" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="-2.159"/>
<vertex x="-3.048" y="-1.27"/>
<vertex x="-2.54" y="-1.778"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-3.302"/>
<vertex x="-2.921" y="-2.413"/>
<vertex x="-2.413" y="-2.921"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="LED" prefix="DL" uservalue="yes">
<description>LED</description>
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="-0603" package="LEDC1608X80N-AK">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="-DNP">
<attribute name="DATASHEET" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="DNP" constant="no"/>
<attribute name="MANUFACTURER" value="DNP" constant="no"/>
<attribute name="MANUFACTURER-PN" value="DNP" constant="no"/>
<attribute name="VALUE" value="DNP" constant="no"/>
</technology>
<technology name="-GREEN">
<attribute name="DATASHEET" value="http://media.digikey.com/pdf/Data%20Sheets/Avago%20PDFs/HSMz-Czzz.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="http://www.digikey.com/product-detail/en/broadcom-limited/HSMG-C190/516-1425-1-ND/637749" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="516-1425-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Avago " constant="no"/>
<attribute name="MANUFACTURER-PN" value="HSMG-C190 " constant="no"/>
<attribute name="VALUE" value="GREEN" constant="no"/>
</technology>
<technology name="-ORANGE">
<attribute name="DATASHEET" value="http://media.digikey.com/pdf/Data%20Sheets/Avago%20PDFs/HSMz-Czzz.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="http://www.digikey.com/product-detail/en/broadcom-limited/HSMD-C190/516-2489-1-ND/2744915" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="516-2489-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Avago " constant="no"/>
<attribute name="MANUFACTURER-PN" value="HSMD-C190 " constant="no"/>
<attribute name="VALUE" value="ORANGE" constant="no"/>
</technology>
<technology name="-YELLOW">
<attribute name="DATASHEET" value="http://media.digikey.com/pdf/Data%20Sheets/Avago%20PDFs/HSMz-Czzz.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="http://www.digikey.com/product-detail/en/broadcom-limited/HSMY-C190/516-1424-1-ND/637748" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="516-1424-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Avago" constant="no"/>
<attribute name="MANUFACTURER-PN" value="HSMY-C190" constant="no"/>
<attribute name="VALUE" value="YELLOW" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="BCMI-transistors">
<description>Copyright (c) 2016-2017 Arduino LLC.  All right reserved.</description>
<packages>
<package name="DFN3X3_8L">
<smd name="P$1" x="-1.4" y="1.05" dx="0.5" dy="0.35" layer="1"/>
<smd name="P$2" x="-1.4" y="0.4" dx="0.5" dy="0.35" layer="1"/>
<smd name="P$3" x="-1.4" y="-0.25" dx="0.5" dy="0.35" layer="1"/>
<smd name="P$4" x="-1.4" y="-0.9" dx="0.5" dy="0.35" layer="1"/>
<smd name="P$5" x="0.51" y="0.055" dx="2.28" dy="2.45" layer="1"/>
<wire x1="-1.51" y1="-1.44" x2="1.49" y2="-1.44" width="0.127" layer="21"/>
<wire x1="1.49" y1="-1.44" x2="1.49" y2="1.56" width="0.127" layer="21"/>
<wire x1="1.49" y1="1.56" x2="-1.51" y2="1.56" width="0.127" layer="21"/>
<wire x1="-1.51" y1="1.56" x2="-1.51" y2="-1.44" width="0.127" layer="21"/>
<rectangle x1="-1.49" y1="1.07" x2="-0.95" y2="1.54" layer="21"/>
<smd name="P$6" x="0" y="1.47" dx="0.43" dy="0.38" layer="1"/>
<smd name="P$7" x="0" y="-1.36" dx="0.43" dy="0.38" layer="1"/>
</package>
</packages>
<symbols>
<symbol name="P-MOS">
<wire x1="0" y1="0" x2="-1.016" y2="0.381" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="0.381" x2="-1.016" y2="-0.381" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="-0.381" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="2.032" x2="0" y2="2.794" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="0" x2="-0.508" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.508" y1="0" x2="-0.381" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="0" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-3.048" x2="1.27" y2="-3.048" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-3.048" x2="1.27" y2="-0.254" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-0.254" x2="1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="2.794" width="0.1524" layer="94"/>
<wire x1="1.27" y1="2.794" x2="0" y2="2.794" width="0.1524" layer="94"/>
<wire x1="0.762" y1="-0.762" x2="1.778" y2="-0.762" width="0.1524" layer="94"/>
<wire x1="1.778" y1="-0.762" x2="1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0" x2="0.762" y2="-0.762" width="0.1524" layer="94"/>
<wire x1="0.762" y1="0" x2="1.778" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-2.032" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.032" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="2.032" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="0" x2="-0.889" y2="-0.127" width="0.254" layer="94"/>
<wire x1="-0.889" y1="-0.127" x2="-0.889" y2="0.127" width="0.254" layer="94"/>
<wire x1="-0.889" y1="0.127" x2="-0.508" y2="0" width="0.254" layer="94"/>
<wire x1="1.016" y1="-0.635" x2="1.524" y2="-0.635" width="0.254" layer="94"/>
<wire x1="1.524" y1="-0.635" x2="1.27" y2="-0.254" width="0.254" layer="94"/>
<wire x1="1.27" y1="-0.254" x2="1.016" y2="-0.635" width="0.254" layer="94"/>
<circle x="0" y="2.794" radius="0.3592" width="0" layer="94"/>
<circle x="0" y="2.032" radius="0.3592" width="0" layer="94"/>
<circle x="0" y="-3.048" radius="0.3592" width="0" layer="94"/>
<text x="2.54" y="0" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-2.54" x2="-1.524" y2="-1.27" layer="94"/>
<rectangle x1="-2.032" y1="1.27" x2="-1.524" y2="2.54" layer="94"/>
<rectangle x1="-2.032" y1="-0.762" x2="-1.524" y2="0.762" layer="94"/>
<pin name="G" x="-5.08" y="2.54" visible="off" length="short" direction="pas"/>
<pin name="D" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="S" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="AON7407" prefix="Q">
<description>MOSFET P-CH 20V 14.5A 8DFN

&lt;b&gt;P-Channel Mosfet&lt;/b&gt;
&lt;p&gt;&lt;b&gt;LEGEND&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;
&lt;b&gt;VDS&lt;/b&gt;: Voltage Drain-Source&lt;br/&gt;
&lt;b&gt;ID&lt;/b&gt;: Drain Current&lt;br/&gt;
&lt;b&gt;RDS(ON)&lt;/b&gt;: Drain-Source On-State Resistance&lt;br/&gt;
&lt;b&gt;VGS(TH)&lt;/b&gt;: Gate-Source Threshold Voltage&lt;br/&gt;
&lt;b&gt;CISS&lt;/b&gt;: Drain-Source Input Capacitance
&lt;/p&gt;
&lt;p&gt;
&lt;b&gt;SOT-23&lt;/b&gt;
&lt;table border="0" width="90%" cellspacing="0" cellpadding="5"&gt;
&lt;tr bgcolor="#DDDDDD"&gt;
&lt;td&gt;Name&lt;/td&gt;
&lt;td&gt;VDS&lt;/td&gt;
&lt;td&gt;ID&lt;/td&gt;
&lt;td&gt;RDS(ON)&lt;/td&gt;
&lt;td&gt;VGS(TH)&lt;/td&gt;
&lt;td&gt;CISS&lt;/td&gt;
&lt;td&gt;Package&lt;/td&gt;
&lt;td&gt;Order Number&lt;/td&gt;
&lt;/tr&gt;

&lt;tr&gt;
&lt;td&gt;AON7407&lt;/td&gt;
&lt;td&gt;20V&lt;/td&gt;
&lt;td&gt;14.5A&lt;/td&gt;
&lt;td&gt;mOhm&lt;/td&gt;
&lt;td&gt;--&lt;/td&gt;
&lt;td&gt;pF @ 25V&lt;/td&gt;
&lt;td&gt;8DFN_3x3&lt;/td&gt;
&lt;td&gt;Digikey&lt;/td&gt;
&lt;/tr&gt;</description>
<gates>
<gate name="G$1" symbol="P-MOS" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DFN3X3_8L">
<connects>
<connect gate="G$1" pin="D" pad="P$5 P$6 P$7"/>
<connect gate="G$1" pin="G" pad="P$4"/>
<connect gate="G$1" pin="S" pad="P$1 P$2 P$3"/>
</connects>
<technologies>
<technology name="">
<attribute name="DATASHEET" value="http://aosmd.com/res/data_sheets/AON7407.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.se/products/en?keywords=785-1306-1-ND" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="785-1306-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Alpha&amp;Omega" constant="no"/>
<attribute name="MANUFACTURER-PN" value="AON7407" constant="no"/>
<attribute name="VALUE" value="AON7407" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="BCMI-Switches">
<packages>
<package name="CL-SB-22A">
<smd name="P$1" x="-2.5" y="-1.29" dx="1.2" dy="1.6" layer="1"/>
<smd name="P$2" x="0" y="-1.29" dx="1.2" dy="1.6" layer="1"/>
<smd name="P$3" x="2.5" y="-1.29" dx="1.2" dy="1.6" layer="1"/>
<smd name="P$4" x="-2.5" y="1.29" dx="1.2" dy="1.6" layer="1"/>
<smd name="P$5" x="0" y="1.29" dx="1.2" dy="1.6" layer="1"/>
<smd name="P$6" x="2.5" y="1.29" dx="1.2" dy="1.6" layer="1"/>
<wire x1="-3.5" y1="1.75" x2="-3.5" y2="-1.75" width="0.127" layer="21"/>
<wire x1="3.5" y1="1.75" x2="3.5" y2="-1.75" width="0.127" layer="21"/>
<wire x1="-3.5" y1="1.75" x2="3.5" y2="1.75" width="0.127" layer="21"/>
<wire x1="-3.5" y1="-1.75" x2="3.5" y2="-1.75" width="0.127" layer="21"/>
<wire x1="-1.38" y1="0.6" x2="1.5" y2="0.6" width="0.127" layer="51"/>
<wire x1="1.5" y1="0.6" x2="1.5" y2="-0.58" width="0.127" layer="51"/>
<wire x1="1.5" y1="-0.58" x2="-1.38" y2="-0.58" width="0.127" layer="51"/>
<wire x1="-1.38" y1="-0.58" x2="-1.38" y2="0.59" width="0.127" layer="51"/>
<wire x1="-1.38" y1="0.59" x2="-0.44" y2="0.59" width="0.127" layer="51"/>
<wire x1="-0.44" y1="0.59" x2="-0.44" y2="-0.57" width="0.127" layer="51"/>
<wire x1="-0.44" y1="-0.57" x2="0.58" y2="-0.57" width="0.127" layer="51"/>
<wire x1="0.58" y1="-0.57" x2="0.58" y2="0.56" width="0.127" layer="51"/>
<wire x1="-3.46" y1="-1.31" x2="-3.05" y2="-1.73" width="0.127" layer="51"/>
<wire x1="3" y1="-1.75" x2="3.49" y2="-1.27" width="0.127" layer="51"/>
<wire x1="2.77" y1="1.76" x2="3.47" y2="1.24" width="0.127" layer="51"/>
<wire x1="-3.46" y1="1.21" x2="-2.97" y2="1.73" width="0.127" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="SWITCH-DPDT">
<description>&lt;h3&gt;Double-Pole, Double-Throw (DPDT) Switch&lt;/h3&gt;
&lt;p&gt;Double-pole, double-throw switches.&lt;/p&gt;</description>
<wire x1="1.27" y1="5.08" x2="-2.286" y2="2.794" width="0.254" layer="94"/>
<wire x1="-2.286" y1="-4.826" x2="1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="6.35" x2="2.54" y2="7.62" width="0.254" layer="94"/>
<wire x1="2.54" y1="7.62" x2="-2.54" y2="7.62" width="0.254" layer="94"/>
<wire x1="-2.54" y1="7.62" x2="-2.54" y2="3.81" width="0.254" layer="94"/>
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="-3.81" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-6.35" x2="-2.54" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-10.16" x2="2.54" y2="-10.16" width="0.254" layer="94"/>
<wire x1="2.54" y1="-10.16" x2="2.54" y2="-8.89" width="0.254" layer="94"/>
<wire x1="2.54" y1="-6.35" x2="2.54" y2="-3.81" width="0.254" layer="94"/>
<wire x1="2.54" y1="3.81" x2="2.54" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="4.064" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-3.302" width="0.254" layer="94"/>
<circle x="-2.54" y="2.54" radius="0.3592" width="0.254" layer="94"/>
<circle x="2.54" y="5.08" radius="0.3592" width="0.254" layer="94"/>
<circle x="2.54" y="0" radius="0.3592" width="0.254" layer="94"/>
<circle x="2.54" y="-2.54" radius="0.3592" width="0.254" layer="94"/>
<circle x="2.54" y="-7.62" radius="0.3592" width="0.254" layer="94"/>
<circle x="-2.54" y="-5.08" radius="0.3592" width="0.254" layer="94"/>
<text x="0" y="7.874" size="1.778" layer="95" font="vector" align="bottom-center">&gt;NAME</text>
<text x="0" y="-10.414" size="1.778" layer="96" font="vector" align="top-center">&gt;VALUE</text>
<pin name="1" x="5.08" y="5.08" visible="off" length="short" rot="R180"/>
<pin name="2" x="-5.08" y="2.54" visible="off" length="short"/>
<pin name="3" x="5.08" y="0" visible="off" length="short" rot="R180"/>
<pin name="4" x="5.08" y="-2.54" visible="off" length="short" rot="R180"/>
<pin name="5" x="-5.08" y="-5.08" visible="off" length="short"/>
<pin name="6" x="5.08" y="-7.62" visible="off" length="short" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CL-SB-22A-02T">
<description>Slide Switch DPDT Surface Mount</description>
<gates>
<gate name="G$1" symbol="SWITCH-DPDT" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CL-SB-22A">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
<connect gate="G$1" pin="3" pad="P$3"/>
<connect gate="G$1" pin="4" pad="P$4"/>
<connect gate="G$1" pin="5" pad="P$5"/>
<connect gate="G$1" pin="6" pad="P$6"/>
</connects>
<technologies>
<technology name="">
<attribute name="DATASHEET" value="https://www.nidec-copal-electronics.com/e/catalog/switch/cl-sb.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.se/products/en?keywords=563-1339-1-ND" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="563-1339-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Copal Electronics" constant="no"/>
<attribute name="MANUFACTURER-PN" value="CL-SB-22A-02T" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Arduino-diodes">
<description>Copyright (c) 2016-2017 Arduino LLC.  All right reserved.</description>
<packages>
<package name="SMA_DO214AC">
<smd name="C" x="0" y="1.7" dx="1.7" dy="1.7" layer="1" roundness="20"/>
<smd name="A" x="0" y="-1.7" dx="1.7" dy="1.7" layer="1" roundness="20"/>
<wire x1="1.45" y1="2.3" x2="1.45" y2="1.11" width="0.127" layer="21"/>
<wire x1="1.45" y1="1.11" x2="1.45" y2="0.99" width="0.127" layer="21"/>
<wire x1="1.45" y1="0.99" x2="1.45" y2="-2.3" width="0.127" layer="21"/>
<wire x1="-1.45" y1="2.3" x2="-1.45" y2="1.11" width="0.127" layer="21"/>
<wire x1="-1.45" y1="1.11" x2="-1.45" y2="0.99" width="0.127" layer="21"/>
<wire x1="-1.45" y1="0.99" x2="-1.45" y2="-2.3" width="0.127" layer="21"/>
<wire x1="1.45" y1="2.3" x2="-1.45" y2="2.3" width="0.127" layer="21"/>
<wire x1="1.45" y1="-2.3" x2="-1.45" y2="-2.3" width="0.127" layer="21"/>
<text x="1.905" y="0" size="0.8128" layer="25" font="vector" ratio="10" rot="R270" align="center">&gt;NAME</text>
<text x="-1.905" y="0" size="0.8128" layer="27" font="vector" rot="R90" align="center">&gt;VALUE</text>
<wire x1="1.45" y1="1.11" x2="-1.45" y2="1.11" width="0.127" layer="21"/>
<wire x1="1.45" y1="0.99" x2="-1.45" y2="0.99" width="0.127" layer="21"/>
<wire x1="0.6" y1="-0.627" x2="0" y2="0.373" width="0.127" layer="21"/>
<wire x1="0" y1="0.373" x2="-0.6" y2="-0.627" width="0.127" layer="21"/>
<wire x1="-0.6" y1="-0.627" x2="0.6" y2="-0.627" width="0.127" layer="21"/>
<rectangle x1="-0.3048" y1="-0.2286" x2="0.3048" y2="1.3462" layer="21" rot="R270"/>
</package>
<package name="SOD106">
<wire x1="0.7874" y1="-1.8208" x2="0.7874" y2="1.8208" width="0.127" layer="21"/>
<wire x1="-0.7874" y1="-1.8208" x2="-0.7874" y2="1.8208" width="0.127" layer="21"/>
<wire x1="0.6" y1="-0.28575" x2="0" y2="0.71425" width="0.2032" layer="21"/>
<wire x1="0" y1="0.71425" x2="-0.6" y2="-0.28575" width="0.2032" layer="21"/>
<wire x1="-0.6" y1="-0.28575" x2="0.6" y2="-0.28575" width="0.2032" layer="21"/>
<smd name="C" x="0" y="2.25" dx="2.1" dy="1.6" layer="1" roundness="20"/>
<smd name="A" x="0" y="-2.25" dx="2.1" dy="1.6" layer="1" roundness="20"/>
<text x="1.905" y="0" size="0.8128" layer="25" font="vector" ratio="10" rot="R270" align="center">&gt;NAME</text>
<text x="-1.905" y="0" size="0.8128" layer="27" font="vector" ratio="10" rot="R270" align="center">&gt;VALUE</text>
<rectangle x1="-0.2794" y1="1.2112" x2="0.2794" y2="2.9384" layer="21" rot="R270"/>
<rectangle x1="-0.2794" y1="-2.9384" x2="0.2794" y2="-1.2112" layer="21" rot="R270"/>
<rectangle x1="-0.3048" y1="0.11265" x2="0.3048" y2="1.68745" layer="21" rot="R270"/>
</package>
<package name="SOD110">
<wire x1="0.5" y1="1" x2="0.5" y2="-1.05" width="0.127" layer="21"/>
<wire x1="0.5" y1="-1.05" x2="-0.5" y2="-1.05" width="0.127" layer="21"/>
<wire x1="-0.5" y1="-1.05" x2="-0.5" y2="1" width="0.127" layer="21"/>
<wire x1="-0.5" y1="1" x2="0.5" y2="1" width="0.127" layer="21"/>
<smd name="C" x="0" y="0.95" dx="1" dy="0.8" layer="1" roundness="20"/>
<smd name="A" x="0" y="-0.95" dx="1" dy="0.8" layer="1" roundness="20"/>
<text x="-1.27" y="0" size="0.8128" layer="25" font="vector" ratio="10" rot="R270" align="center">&gt;NAME</text>
<text x="1.27" y="0" size="0.8128" layer="27" font="vector" ratio="10" rot="R270" align="center">&gt;VALUE</text>
<rectangle x1="-0.125" y1="-0.08375" x2="0.125" y2="0.81625" layer="21" rot="R270"/>
<wire x1="0.44125" y1="-0.3095" x2="0" y2="0.373" width="0.127" layer="21"/>
<wire x1="0" y1="0.373" x2="-0.44125" y2="-0.3095" width="0.127" layer="21"/>
<wire x1="-0.44125" y1="-0.3095" x2="0.44125" y2="-0.3095" width="0.127" layer="21"/>
</package>
<package name="SOD80">
<wire x1="0.7874" y1="-1.3208" x2="0.7874" y2="1.3208" width="0.127" layer="21"/>
<wire x1="-0.7874" y1="-1.3208" x2="-0.7874" y2="1.3208" width="0.127" layer="21"/>
<wire x1="0.6" y1="-0.627" x2="0" y2="0.373" width="0.127" layer="21"/>
<wire x1="0" y1="0.373" x2="-0.6" y2="-0.627" width="0.127" layer="21"/>
<wire x1="-0.6" y1="-0.627" x2="0.6" y2="-0.627" width="0.127" layer="21"/>
<smd name="C" x="0" y="1.65" dx="1.9" dy="1.2" layer="1" roundness="20"/>
<smd name="A" x="0" y="-1.65" dx="1.9" dy="1.2" layer="1" roundness="20"/>
<text x="-1.905" y="0" size="0.8128" layer="25" font="vector" rot="R270" align="center">&gt;NAME</text>
<text x="1.905" y="0" size="0.8128" layer="27" font="vector" ratio="10" rot="R90" align="center">&gt;VALUE</text>
<rectangle x1="-0.2794" y1="0.7112" x2="0.2794" y2="2.4384" layer="21" rot="R270"/>
<rectangle x1="-0.2794" y1="-2.4384" x2="0.2794" y2="-0.7112" layer="21" rot="R270"/>
<rectangle x1="-0.3048" y1="-0.2286" x2="0.3048" y2="1.3462" layer="21" rot="R270"/>
</package>
<package name="SOD882">
<wire x1="0.25" y1="0.45" x2="0.25" y2="-0.2" width="0.127" layer="21"/>
<wire x1="0.25" y1="-0.2" x2="0.25" y2="-0.45" width="0.127" layer="21"/>
<wire x1="0.25" y1="-0.45" x2="-0.25" y2="-0.45" width="0.127" layer="21"/>
<wire x1="-0.25" y1="-0.45" x2="-0.25" y2="-0.2" width="0.127" layer="21"/>
<wire x1="-0.25" y1="-0.2" x2="-0.25" y2="0.45" width="0.127" layer="21"/>
<wire x1="-0.25" y1="0.45" x2="0.25" y2="0.45" width="0.127" layer="21"/>
<smd name="C" x="0" y="0.35" dx="0.65" dy="0.4" layer="1" roundness="20"/>
<smd name="A" x="0" y="-0.35" dx="0.65" dy="0.4" layer="1" roundness="20"/>
<text x="-1.27" y="0" size="0.8128" layer="25" font="vector" ratio="10" rot="R270" align="center">&gt;NAME</text>
<text x="1.27" y="0" size="0.8128" layer="27" font="vector" ratio="10" rot="R270" align="center">&gt;VALUE</text>
<rectangle x1="-0.079375" y1="0.020625" x2="0.079375" y2="0.620625" layer="21" rot="R270"/>
<wire x1="0.25" y1="-0.2" x2="0" y2="0.3" width="0.127" layer="21"/>
<wire x1="0" y1="0.3" x2="-0.25" y2="-0.2" width="0.127" layer="21"/>
<wire x1="-0.25" y1="-0.2" x2="0.25" y2="-0.2" width="0.127" layer="21"/>
</package>
<package name="SMB_DO214AA">
<smd name="C" x="0" y="2" dx="2.5" dy="2.2" layer="1" roundness="20"/>
<smd name="A" x="0" y="-2" dx="2.5" dy="2.2" layer="1" roundness="20"/>
<wire x1="1.9" y1="2.35" x2="1.9" y2="1.11" width="0.127" layer="21"/>
<wire x1="1.9" y1="1.11" x2="1.9" y2="0.99" width="0.127" layer="21"/>
<wire x1="1.9" y1="0.99" x2="1.9" y2="-2.35" width="0.127" layer="21"/>
<wire x1="-1.9" y1="2.35" x2="-1.9" y2="1.11" width="0.127" layer="21"/>
<wire x1="-1.9" y1="1.11" x2="-1.9" y2="0.99" width="0.127" layer="21"/>
<wire x1="-1.9" y1="0.99" x2="-1.9" y2="-2.35" width="0.127" layer="21"/>
<wire x1="1.9" y1="2.35" x2="-1.9" y2="2.35" width="0.127" layer="21"/>
<wire x1="1.9" y1="-2.35" x2="-1.9" y2="-2.35" width="0.127" layer="21"/>
<text x="2.54" y="0" size="0.8128" layer="25" font="vector" ratio="10" rot="R270" align="center">&gt;NAME</text>
<text x="-2.54" y="0" size="0.8128" layer="27" font="vector" rot="R90" align="center">&gt;VALUE</text>
<wire x1="1.9" y1="1.11" x2="-1.9" y2="1.11" width="0.127" layer="21"/>
<wire x1="1.9" y1="0.99" x2="-1.9" y2="0.99" width="0.127" layer="21"/>
<wire x1="0.6" y1="-0.627" x2="0" y2="0.373" width="0.127" layer="21"/>
<wire x1="0" y1="0.373" x2="-0.6" y2="-0.627" width="0.127" layer="21"/>
<wire x1="-0.6" y1="-0.627" x2="0.6" y2="-0.627" width="0.127" layer="21"/>
<rectangle x1="-0.3048" y1="-0.2286" x2="0.3048" y2="1.3462" layer="21" rot="R270"/>
</package>
<package name="SMC_DO214AB">
<smd name="C" x="0" y="3.3" dx="3.5" dy="2.5" layer="1" roundness="20"/>
<smd name="A" x="0" y="-3.3" dx="3.5" dy="2.5" layer="1" roundness="20"/>
<wire x1="3.1" y1="3.55" x2="3.1" y2="2.1" width="0.127" layer="21"/>
<wire x1="3.1" y1="2.1" x2="3.1" y2="2" width="0.127" layer="21"/>
<wire x1="3.1" y1="2" x2="3.1" y2="-3.55" width="0.127" layer="21"/>
<wire x1="-3.1" y1="3.55" x2="-3.1" y2="2.1" width="0.127" layer="21"/>
<wire x1="-3.1" y1="2.1" x2="-3.1" y2="2" width="0.127" layer="21"/>
<wire x1="-3.1" y1="2" x2="-3.1" y2="-3.55" width="0.127" layer="21"/>
<wire x1="3.1" y1="3.55" x2="-3.1" y2="3.55" width="0.127" layer="21"/>
<wire x1="3.1" y1="-3.55" x2="-3.1" y2="-3.55" width="0.127" layer="21"/>
<text x="2.54" y="0" size="0.8128" layer="25" font="vector" ratio="10" rot="R270" align="center">&gt;NAME</text>
<text x="-2.54" y="0" size="0.8128" layer="27" font="vector" rot="R90" align="center">&gt;VALUE</text>
<wire x1="3.1" y1="2.1" x2="-3.1" y2="2.1" width="0.127" layer="21"/>
<wire x1="3.1" y1="2" x2="-3.1" y2="2" width="0.127" layer="21"/>
<wire x1="0.6" y1="-0.627" x2="0" y2="0.373" width="0.127" layer="21"/>
<wire x1="0" y1="0.373" x2="-0.6" y2="-0.627" width="0.127" layer="21"/>
<wire x1="-0.6" y1="-0.627" x2="0.6" y2="-0.627" width="0.127" layer="21"/>
<rectangle x1="-0.3048" y1="-0.2286" x2="0.3048" y2="1.3462" layer="21" rot="R270"/>
</package>
<package name="SMA_DO123">
<smd name="C" x="0" y="1.4" dx="1.2" dy="1.2" layer="1" roundness="20"/>
<smd name="A" x="0" y="-1.4" dx="1.2" dy="1.2" layer="1" roundness="20"/>
<wire x1="0.85" y1="2" x2="0.85" y2="1.11" width="0.127" layer="21"/>
<wire x1="0.85" y1="1.11" x2="0.85" y2="0.99" width="0.127" layer="21"/>
<wire x1="0.85" y1="0.99" x2="0.85" y2="-2" width="0.127" layer="21"/>
<wire x1="-0.85" y1="2" x2="-0.85" y2="1.11" width="0.127" layer="21"/>
<wire x1="-0.85" y1="1.11" x2="-0.85" y2="0.99" width="0.127" layer="21"/>
<wire x1="-0.85" y1="0.99" x2="-0.85" y2="-2" width="0.127" layer="21"/>
<wire x1="0.85" y1="2" x2="-0.85" y2="2" width="0.127" layer="21"/>
<wire x1="0.85" y1="-2" x2="-0.85" y2="-2" width="0.127" layer="21"/>
<text x="1.405" y="0" size="0.8128" layer="25" font="vector" ratio="10" rot="R270" align="center">&gt;NAME</text>
<text x="-1.405" y="0" size="0.8128" layer="27" font="vector" rot="R90" align="center">&gt;VALUE</text>
<wire x1="0.85" y1="1.11" x2="-0.85" y2="1.11" width="0.127" layer="21"/>
<wire x1="0.85" y1="0.99" x2="-0.85" y2="0.99" width="0.127" layer="21"/>
<wire x1="0.6" y1="-0.627" x2="0" y2="0.373" width="0.127" layer="21"/>
<wire x1="0" y1="0.373" x2="-0.6" y2="-0.627" width="0.127" layer="21"/>
<wire x1="-0.6" y1="-0.627" x2="0.6" y2="-0.627" width="0.127" layer="21"/>
<rectangle x1="-0.3048" y1="-0.2286" x2="0.3048" y2="1.3462" layer="21" rot="R270"/>
</package>
<package name="SOD-128">
<smd name="P$1" x="-2.2" y="0" dx="1.2" dy="1.9" layer="1"/>
<smd name="P$2" x="2.2" y="0" dx="1.2" dy="1.9" layer="1"/>
<wire x1="2.1" y1="1.7" x2="-1.3" y2="1.7" width="0.127" layer="21"/>
<wire x1="-1.4" y1="1.7" x2="-1.5" y2="1.7" width="0.127" layer="21"/>
<wire x1="-1.5" y1="1.7" x2="-2.1" y2="1.7" width="0.127" layer="21"/>
<wire x1="-2.1" y1="-1.7" x2="-1.5" y2="-1.7" width="0.127" layer="21"/>
<wire x1="-1.4" y1="-1.7" x2="-1.3" y2="-1.7" width="0.127" layer="21"/>
<wire x1="-1.3" y1="-1.7" x2="2.1" y2="-1.7" width="0.127" layer="21"/>
<wire x1="2.1" y1="-1.7" x2="2.1" y2="-1.1" width="0.127" layer="21"/>
<wire x1="2.1" y1="-1.1" x2="2.9" y2="-1.1" width="0.127" layer="21"/>
<wire x1="2.9" y1="-1.1" x2="2.9" y2="1.1" width="0.127" layer="21"/>
<wire x1="2.9" y1="1.1" x2="2.1" y2="1.1" width="0.127" layer="21"/>
<wire x1="2.1" y1="1.1" x2="2.1" y2="1.7" width="0.127" layer="21"/>
<wire x1="-2.1" y1="-1.7" x2="-2.1" y2="-1.1" width="0.127" layer="21"/>
<wire x1="-2.1" y1="-1.1" x2="-2.9" y2="-1.1" width="0.127" layer="21"/>
<wire x1="-2.9" y1="-1.1" x2="-2.9" y2="1.1" width="0.127" layer="21"/>
<wire x1="-2.9" y1="1.1" x2="-2.1" y2="1.1" width="0.127" layer="21"/>
<wire x1="-2.1" y1="1.1" x2="-2.1" y2="1.7" width="0.127" layer="21"/>
<wire x1="0.627" y1="0.6" x2="-0.373" y2="0" width="0.127" layer="21"/>
<wire x1="-0.373" y1="0" x2="0.627" y2="-0.6" width="0.127" layer="21"/>
<wire x1="0.627" y1="-0.6" x2="0.627" y2="0.6" width="0.127" layer="21"/>
<rectangle x1="-0.8636" y1="-0.7874" x2="-0.254" y2="0.7874" layer="21"/>
<wire x1="-1.5" y1="1.7" x2="-1.5" y2="-1.7" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-1.7" x2="-1.4" y2="-1.7" width="0.127" layer="21"/>
<wire x1="-1.4" y1="-1.7" x2="-1.4" y2="1.7" width="0.127" layer="21"/>
<wire x1="-1.4" y1="1.7" x2="-1.3" y2="1.7" width="0.127" layer="21"/>
<wire x1="-1.3" y1="1.7" x2="-1.3" y2="-1.7" width="0.127" layer="21"/>
</package>
<package name="LEDC1608X80N-AK">
<circle x="0" y="0" radius="0.25" width="0.05" layer="39"/>
<wire x1="-0.645" y1="-0.395" x2="-0.355" y2="-0.395" width="0.01" layer="51"/>
<wire x1="-0.355" y1="-0.395" x2="-0.355" y2="0.395" width="0.01" layer="51"/>
<wire x1="-0.355" y1="0.395" x2="-0.645" y2="0.395" width="0.01" layer="51"/>
<wire x1="-0.645" y1="0.395" x2="-0.645" y2="-0.395" width="0.01" layer="51"/>
<wire x1="-0.64" y1="-0.39" x2="-0.36" y2="-0.39" width="0.01" layer="51"/>
<wire x1="-0.36" y1="-0.39" x2="-0.36" y2="0.39" width="0.01" layer="51"/>
<wire x1="-0.36" y1="0.39" x2="-0.64" y2="0.39" width="0.01" layer="51"/>
<wire x1="-0.64" y1="0.39" x2="-0.64" y2="-0.39" width="0.01" layer="51"/>
<wire x1="-0.53" y1="-0.28" x2="-0.47" y2="-0.28" width="0.2" layer="51"/>
<wire x1="-0.47" y1="-0.28" x2="-0.47" y2="0.28" width="0.2" layer="51"/>
<wire x1="-0.47" y1="0.28" x2="-0.53" y2="0.28" width="0.2" layer="51"/>
<wire x1="-0.53" y1="0.28" x2="-0.53" y2="-0.28" width="0.2" layer="51"/>
<wire x1="-0.58" y1="-0.33" x2="-0.42" y2="-0.33" width="0.1" layer="51"/>
<wire x1="-0.42" y1="-0.33" x2="-0.42" y2="0.33" width="0.1" layer="51"/>
<wire x1="-0.42" y1="0.33" x2="-0.58" y2="0.33" width="0.1" layer="51"/>
<wire x1="-0.58" y1="0.33" x2="-0.58" y2="-0.33" width="0.1" layer="51"/>
<wire x1="-0.8" y1="-0.4" x2="0.8" y2="-0.4" width="0.025" layer="51"/>
<wire x1="0.8" y1="-0.4" x2="0.8" y2="0.4" width="0.025" layer="51"/>
<wire x1="0.8" y1="0.4" x2="-0.8" y2="0.4" width="0.025" layer="51"/>
<wire x1="-0.8" y1="0.4" x2="-0.8" y2="-0.4" width="0.025" layer="51"/>
<wire x1="-0.63" y1="-0.38" x2="-0.37" y2="-0.38" width="0.025" layer="51"/>
<wire x1="-0.37" y1="-0.38" x2="-0.37" y2="0.38" width="0.025" layer="51"/>
<wire x1="-0.37" y1="0.38" x2="-0.63" y2="0.38" width="0.025" layer="51"/>
<wire x1="-0.63" y1="0.38" x2="-0.63" y2="-0.38" width="0.025" layer="51"/>
<wire x1="-0.618" y1="-0.367" x2="-0.383" y2="-0.367" width="0.025" layer="51"/>
<wire x1="-0.383" y1="-0.367" x2="-0.383" y2="0.368" width="0.025" layer="51"/>
<wire x1="-0.383" y1="0.368" x2="-0.618" y2="0.368" width="0.025" layer="51"/>
<wire x1="-0.618" y1="0.368" x2="-0.618" y2="-0.367" width="0.025" layer="51"/>
<wire x1="-0.605" y1="-0.355" x2="-0.395" y2="-0.355" width="0.05" layer="51"/>
<wire x1="-0.395" y1="-0.355" x2="-0.395" y2="0.355" width="0.05" layer="51"/>
<wire x1="-0.395" y1="0.355" x2="-0.605" y2="0.355" width="0.05" layer="51"/>
<wire x1="-0.605" y1="0.355" x2="-0.605" y2="-0.355" width="0.05" layer="51"/>
<wire x1="-0.35" y1="0" x2="0.35" y2="0" width="0.05" layer="39"/>
<wire x1="0" y1="-0.35" x2="0" y2="0.35" width="0.05" layer="39"/>
<wire x1="-1.55" y1="0.75" x2="-1.55" y2="-0.75" width="0.05" layer="39"/>
<wire x1="-1.55" y1="-0.75" x2="1.55" y2="-0.75" width="0.05" layer="39"/>
<wire x1="1.55" y1="-0.75" x2="1.55" y2="0.75" width="0.05" layer="39"/>
<wire x1="1.55" y1="0.75" x2="-1.55" y2="0.75" width="0.05" layer="39"/>
<smd name="A" x="0.8" y="0" dx="0.9" dy="0.95" layer="1" roundness="3"/>
<smd name="C" x="-0.8" y="0" dx="0.9" dy="0.95" layer="1" roundness="3"/>
<text x="0" y="0" size="1.2" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="0" y="0" size="1.2" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<text x="0" y="0" size="0.4064" layer="51" font="vector" ratio="10" align="center">&gt;NAME</text>
</package>
<package name="0402-LED">
<circle x="0" y="0" radius="0.25" width="0.05" layer="39"/>
<wire x1="-0.5" y1="-0.25" x2="0.5" y2="-0.25" width="0.025" layer="51"/>
<wire x1="0.5" y1="-0.25" x2="0.5" y2="0.25" width="0.025" layer="51"/>
<wire x1="0.5" y1="0.25" x2="-0.5" y2="0.25" width="0.025" layer="51"/>
<wire x1="-0.5" y1="0.25" x2="-0.5" y2="-0.25" width="0.025" layer="51"/>
<wire x1="-0.35" y1="0" x2="0.35" y2="0" width="0.05" layer="39"/>
<wire x1="0" y1="-0.35" x2="0" y2="0.35" width="0.05" layer="39"/>
<wire x1="-0.95" y1="0.45" x2="0.95" y2="0.45" width="0.05" layer="39"/>
<wire x1="0.95" y1="0.45" x2="0.95" y2="-0.45" width="0.05" layer="39"/>
<wire x1="0.95" y1="-0.45" x2="-0.95" y2="-0.45" width="0.05" layer="39"/>
<wire x1="-0.95" y1="-0.45" x2="-0.95" y2="0.45" width="0.05" layer="39"/>
<smd name="A" x="-0.45" y="0" dx="0.62" dy="0.62" layer="1" roundness="3"/>
<smd name="C" x="0.45" y="0" dx="0.62" dy="0.62" layer="1" roundness="3"/>
<text x="-0.9" y="0.5" size="0.4064" layer="51" font="vector" ratio="10">&gt;NAME</text>
<wire x1="0.26" y1="0.24" x2="0.26" y2="-0.24" width="0.0254" layer="51"/>
</package>
<package name="0402-1005X55N">
<circle x="0" y="0" radius="0.25" width="0.05" layer="39"/>
<wire x1="-0.5" y1="-0.25" x2="0.5" y2="-0.25" width="0.025" layer="51"/>
<wire x1="0.5" y1="-0.25" x2="0.5" y2="0.25" width="0.025" layer="51"/>
<wire x1="0.5" y1="0.25" x2="-0.5" y2="0.25" width="0.025" layer="51"/>
<wire x1="-0.5" y1="0.25" x2="-0.5" y2="-0.25" width="0.025" layer="51"/>
<wire x1="-0.35" y1="0" x2="0.35" y2="0" width="0.05" layer="39"/>
<wire x1="0" y1="-0.35" x2="0" y2="0.35" width="0.05" layer="39"/>
<wire x1="-0.95" y1="0.45" x2="0.95" y2="0.45" width="0.05" layer="39"/>
<wire x1="0.95" y1="0.45" x2="0.95" y2="-0.45" width="0.05" layer="39"/>
<wire x1="0.95" y1="-0.45" x2="-0.95" y2="-0.45" width="0.05" layer="39"/>
<wire x1="-0.95" y1="-0.45" x2="-0.95" y2="0.45" width="0.05" layer="39"/>
<smd name="1" x="-0.45" y="0" dx="0.62" dy="0.62" layer="1" roundness="3"/>
<smd name="2" x="0.45" y="0" dx="0.62" dy="0.62" layer="1" roundness="3"/>
<text x="-0.9" y="0.5" size="0.4064" layer="51" font="vector" ratio="10">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="SCHOTTKY">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.905" y1="1.27" x2="1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.905" y1="1.27" x2="1.905" y2="1.016" width="0.254" layer="94"/>
<wire x1="1.27" y1="-1.27" x2="0.635" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0.635" y1="-1.016" x2="0.635" y2="-1.27" width="0.254" layer="94"/>
<text x="-2.286" y="1.905" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.286" y="-3.429" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="LED">
<wire x1="1.27" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="-0.762" x2="-3.429" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-3.302" y2="-3.302" width="0.1524" layer="94"/>
<pin name="A" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="C" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<text x="3.556" y="-4.572" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="-4.572" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="-2.159"/>
<vertex x="-3.048" y="-1.27"/>
<vertex x="-2.54" y="-1.778"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-3.302"/>
<vertex x="-2.921" y="-2.413"/>
<vertex x="-2.413" y="-2.921"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="D_SCHOTTKY" prefix="D" uservalue="yes">
<description>Schottky Diodes</description>
<gates>
<gate name="D" symbol="SCHOTTKY" x="0" y="0"/>
</gates>
<devices>
<device name="-SMA" package="SMA_DO214AC">
<connects>
<connect gate="D" pin="A" pad="A"/>
<connect gate="D" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SOD106" package="SOD106">
<connects>
<connect gate="D" pin="A" pad="A"/>
<connect gate="D" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SOD110" package="SOD110">
<connects>
<connect gate="D" pin="A" pad="A"/>
<connect gate="D" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SOD80" package="SOD80">
<connects>
<connect gate="D" pin="A" pad="A"/>
<connect gate="D" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SOD882" package="SOD882">
<connects>
<connect gate="D" pin="A" pad="A"/>
<connect gate="D" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SMB" package="SMB_DO214AA">
<connects>
<connect gate="D" pin="A" pad="A"/>
<connect gate="D" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SMC" package="SMC_DO214AB">
<connects>
<connect gate="D" pin="A" pad="A"/>
<connect gate="D" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SO123" package="SMA_DO123">
<connects>
<connect gate="D" pin="A" pad="A"/>
<connect gate="D" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DATASHEET" value="http://www.mccsemi.com/up_pdf/MBR0520~MBR0580%28SOD123%29.pdf " constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="http://www.digikey.com/product-detail/en/micro-commercial-co/MBR0560-TP/MBR0560TPMSCT-ND/717368 " constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="MBR0560TPMSCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Micro Comm " constant="no"/>
<attribute name="MANUFACTURER-PN" value="MBR0560-TP" constant="no"/>
<attribute name="VALUE" value="MBR0560" constant="no"/>
</technology>
</technologies>
</device>
<device name="-PMEG4050EP" package="SOD-128">
<connects>
<connect gate="D" pin="A" pad="P$2"/>
<connect gate="D" pin="C" pad="P$1"/>
</connects>
<technologies>
<technology name="">
<attribute name="DATASHEET" value="https://assets.nexperia.com/documents/data-sheet/PMEG4050EP.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.se/products/en?keywords=1727-5842-1-ND" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="1727-5842-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Nexperia USA Inc." constant="no"/>
<attribute name="MANUFACTURER-PN" value="PMEG4050EP,115" constant="no"/>
<attribute name="VALUE" value="40V/5A" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LED" prefix="DL" uservalue="yes">
<description>LED</description>
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="-0603" package="LEDC1608X80N-AK">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="-DNP">
<attribute name="DATASHEET" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="DNP" constant="no"/>
<attribute name="MANUFACTURER" value="DNP" constant="no"/>
<attribute name="MANUFACTURER-PN" value="DNP" constant="no"/>
<attribute name="VALUE" value="DNP" constant="no"/>
</technology>
<technology name="-GREEN">
<attribute name="DATASHEET" value="http://media.digikey.com/pdf/Data%20Sheets/Avago%20PDFs/HSMz-Czzz.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="http://www.digikey.com/product-detail/en/broadcom-limited/HSMG-C190/516-1425-1-ND/637749" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="516-1425-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Avago " constant="no"/>
<attribute name="MANUFACTURER-PN" value="HSMG-C190 " constant="no"/>
<attribute name="VALUE" value="GREEN" constant="no"/>
</technology>
<technology name="-ORANGE">
<attribute name="DATASHEET" value="http://media.digikey.com/pdf/Data%20Sheets/Avago%20PDFs/HSMz-Czzz.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="http://www.digikey.com/product-detail/en/broadcom-limited/HSMD-C190/516-2489-1-ND/2744915" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="516-2489-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Avago " constant="no"/>
<attribute name="MANUFACTURER-PN" value="HSMD-C190 " constant="no"/>
<attribute name="VALUE" value="ORANGE" constant="no"/>
</technology>
<technology name="-YELLOW">
<attribute name="DATASHEET" value="http://media.digikey.com/pdf/Data%20Sheets/Avago%20PDFs/HSMz-Czzz.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="http://www.digikey.com/product-detail/en/broadcom-limited/HSMY-C190/516-1424-1-ND/637748" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="516-1424-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Avago" constant="no"/>
<attribute name="MANUFACTURER-PN" value="HSMY-C190" constant="no"/>
<attribute name="VALUE" value="YELLOW" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0402" package="0402-LED">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DATASHEET" value="http://www.kingbrightusa.com/images/catalog/SPEC/APHHS1005LSYCK-J3-PF.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.com/products/en?keywords=754-2125-1-ND" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="754-2125-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Kingbright" constant="no"/>
<attribute name="MANUFACTURER-PN" value="APHHS1005LSYCK/J3-PF" constant="no"/>
<attribute name="VALUE" value="Yellow/20mcd" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0805" package="0402-1005X55N">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<technologies>
<technology name="-DNP">
<attribute name="CODE-BCE" value="DNP" constant="no"/>
<attribute name="CODE-BCMI" value="DNP" constant="no"/>
<attribute name="CODE-TK2" value="DNP" constant="no"/>
<attribute name="DATASHEET" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="DNP" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="DNP" constant="no"/>
<attribute name="MANUFACTURER" value="DNP" constant="no"/>
<attribute name="MANUFACTURER-PN" value="DNP" constant="no"/>
<attribute name="VALUE" value="DNP" constant="no"/>
</technology>
<technology name="-GREEN">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="0043" constant="no"/>
<attribute name="CODE-TK2" value="KPT-2012SGC" constant="no"/>
<attribute name="DATASHEET" value="https://media.digikey.com/pdf/Data%20Sheets/Lite-On%20PDFs/LTST-C170GKT.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.it/product-detail/it/lite-on-inc/LTST-C170GKT/160-1179-1-ND/269251" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="160-1179-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Lite-On Inc." constant="no"/>
<attribute name="MANUFACTURER-PN" value="LTST-C170GKT" constant="no"/>
<attribute name="VALUE" value="GREEN" constant="no"/>
</technology>
<technology name="-YELLOW">
<attribute name="CODE-BCE" value="-" constant="no"/>
<attribute name="CODE-BCMI" value="0044" constant="no"/>
<attribute name="CODE-TK2" value="KPT-2012YC" constant="no"/>
<attribute name="DATASHEET" value="https://media.digikey.com/pdf/Data%20Sheets/Lite-On%20PDFs/LTST-C170YKT.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.it/product-detail/it/lite-on-inc/LTST-C170YKT/160-1175-1-ND/269247" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="160-1175-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Lite-On Inc." constant="no"/>
<attribute name="MANUFACTURER-PN" value="LTST-C170YKT" constant="no"/>
<attribute name="VALUE" value="YELLOW" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SOIC127P600X170-8N">
<packages>
<package name="SOIC127P600X170-8N">
<rectangle x1="-0.66699375" y1="-0.892428125" x2="0.645" y2="0.863" layer="31"/>
<text x="-4.10851875" y="-2.77041875" size="1.3143" layer="27" align="top-left">&gt;VALUE</text>
<text x="-4.112109375" y="2.77281875" size="1.31546875" layer="25">&gt;NAME</text>
<circle x="-4.445" y="2.505" radius="0.2" width="0" layer="21"/>
<circle x="-4.445" y="2.505" radius="0.2" width="0" layer="51"/>
<wire x1="-1.95" y1="2.45" x2="1.95" y2="2.45" width="0.127" layer="51"/>
<wire x1="-1.95" y1="-2.45" x2="1.95" y2="-2.45" width="0.127" layer="51"/>
<wire x1="-1.95" y1="2.45" x2="1.95" y2="2.45" width="0.127" layer="21"/>
<wire x1="-1.95" y1="-2.45" x2="1.95" y2="-2.45" width="0.127" layer="21"/>
<wire x1="-1.95" y1="2.45" x2="-1.95" y2="-2.45" width="0.127" layer="51"/>
<wire x1="1.95" y1="2.45" x2="1.95" y2="-2.45" width="0.127" layer="51"/>
<wire x1="-3.71" y1="2.7" x2="3.71" y2="2.7" width="0.05" layer="39"/>
<wire x1="-3.71" y1="-2.7" x2="3.71" y2="-2.7" width="0.05" layer="39"/>
<wire x1="-3.71" y1="2.7" x2="-3.71" y2="-2.7" width="0.05" layer="39"/>
<wire x1="3.71" y1="2.7" x2="3.71" y2="-2.7" width="0.05" layer="39"/>
<smd name="1" x="-2.475" y="1.905" dx="1.97" dy="0.6" layer="1" roundness="25"/>
<smd name="2" x="-2.475" y="0.635" dx="1.97" dy="0.6" layer="1" roundness="25"/>
<smd name="3" x="-2.475" y="-0.635" dx="1.97" dy="0.6" layer="1" roundness="25"/>
<smd name="4" x="-2.475" y="-1.905" dx="1.97" dy="0.6" layer="1" roundness="25"/>
<smd name="5" x="2.475" y="-1.905" dx="1.97" dy="0.6" layer="1" roundness="25"/>
<smd name="6" x="2.475" y="-0.635" dx="1.97" dy="0.6" layer="1" roundness="25"/>
<smd name="7" x="2.475" y="0.635" dx="1.97" dy="0.6" layer="1" roundness="25"/>
<smd name="8" x="2.475" y="1.905" dx="1.97" dy="0.6" layer="1" roundness="25"/>
<smd name="9" x="0" y="0" dx="2.58" dy="3.45" layer="1" cream="no"/>
</package>
</packages>
<symbols>
<symbol name="DRV8871">
<wire x1="-10.16" y1="7.62" x2="-10.16" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-12.7" x2="10.16" y2="-12.7" width="0.254" layer="94"/>
<wire x1="10.16" y1="-12.7" x2="10.16" y2="7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="7.62" x2="-10.16" y2="7.62" width="0.254" layer="94"/>
<pin name="GND" x="-15.24" y="5.08" length="middle"/>
<pin name="IN2" x="-15.24" y="0" length="middle"/>
<pin name="IN1" x="-15.24" y="-5.08" length="middle"/>
<pin name="ILIM" x="-15.24" y="-10.16" length="middle"/>
<pin name="VM" x="15.24" y="-10.16" length="middle" rot="R180"/>
<pin name="OUT1" x="15.24" y="-5.08" length="middle" rot="R180"/>
<pin name="PGND" x="15.24" y="0" length="middle" rot="R180"/>
<pin name="OUT2" x="15.24" y="5.08" length="middle" rot="R180"/>
<pin name="PPAD" x="0" y="-15.24" length="short" rot="R90"/>
<text x="-10.16" y="-15.24" size="1.778" layer="94">&gt;NAME</text>
<text x="-10.16" y="7.62" size="1.778" layer="94">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="DRV8871" prefix="U">
<gates>
<gate name="G$1" symbol="DRV8871" x="0" y="2.54"/>
</gates>
<devices>
<device name="" package="SOIC127P600X170-8N">
<connects>
<connect gate="G$1" pin="GND" pad="1"/>
<connect gate="G$1" pin="ILIM" pad="4"/>
<connect gate="G$1" pin="IN1" pad="3"/>
<connect gate="G$1" pin="IN2" pad="2"/>
<connect gate="G$1" pin="OUT1" pad="6"/>
<connect gate="G$1" pin="OUT2" pad="8"/>
<connect gate="G$1" pin="PGND" pad="7"/>
<connect gate="G$1" pin="PPAD" pad="9"/>
<connect gate="G$1" pin="VM" pad="5"/>
</connects>
<technologies>
<technology name="">
<attribute name="DATASHEET" value="http://www.ti.com/lit/ds/symlink/drv8871.pdf" constant="no"/>
<attribute name="DESCRIPTION" value="Motor Driver 3.6A 6.5-45v" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.se/products/en?keywords=296-43024-1-ND" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="296-43024-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Texas Instruments" constant="no"/>
<attribute name="MANUFACTURER-PN" value="DRV8871DDAR" constant="no"/>
<attribute name="VALUE" value="DRV8871" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="MyLibrary">
<packages>
<package name="1X02">
<wire x1="-0.635" y1="1.27" x2="3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.81" y1="0.635" x2="3.81" y2="-0.635" width="0.1524" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="octagon" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" shape="octagon" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="MOLEX-1X2">
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.048" x2="3.81" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="3.81" y1="-2.54" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
</package>
<package name="SCREWTERMINAL-3.5MM-2">
<wire x1="-1.75" y1="3.4" x2="5.25" y2="3.4" width="0.2032" layer="21"/>
<wire x1="5.25" y1="3.4" x2="5.25" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-2.8" x2="5.25" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-3.6" x2="-1.75" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-3.6" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-2.8" x2="-1.75" y2="3.4" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-2.8" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-1.35" x2="-2.15" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-1.35" x2="-2.15" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-2.35" x2="-1.75" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="5.25" y1="3.15" x2="5.65" y2="3.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="3.15" x2="5.65" y2="2.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="2.15" x2="5.25" y2="2.15" width="0.2032" layer="51"/>
<circle x="2" y="3" radius="0.2828" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="JST-2-SMD">
<description>2mm SMD side-entry connector. tDocu layer indicates the actual physical plastic housing. +/- indicate SparkFun standard batteries and wiring.</description>
<wire x1="-4" y1="-1" x2="-4" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="-4" y1="-4.5" x2="-3.2" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="-3.2" y1="-4.5" x2="-3.2" y2="-2" width="0.2032" layer="21"/>
<wire x1="-3.2" y1="-2" x2="-2" y2="-2" width="0.2032" layer="21"/>
<wire x1="2" y1="-2" x2="3.2" y2="-2" width="0.2032" layer="21"/>
<wire x1="3.2" y1="-2" x2="3.2" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="3.2" y1="-4.5" x2="4" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="4" y1="-4.5" x2="4" y2="-1" width="0.2032" layer="21"/>
<wire x1="2" y1="3" x2="-2" y2="3" width="0.2032" layer="21"/>
<smd name="1" x="-1" y="-3.7" dx="1" dy="4.6" layer="1"/>
<smd name="2" x="1" y="-3.7" dx="1" dy="4.6" layer="1"/>
<smd name="NC1" x="-3.4" y="1.5" dx="3.4" dy="1.6" layer="1" rot="R90"/>
<smd name="NC2" x="3.4" y="1.5" dx="3.4" dy="1.6" layer="1" rot="R90"/>
<text x="-1.27" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.27" y="0" size="0.4064" layer="27">&gt;Value</text>
<text x="2.159" y="-4.445" size="1.27" layer="51">+</text>
<text x="-2.921" y="-4.445" size="1.27" layer="51">-</text>
</package>
<package name="1X02_BIG">
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="5.08" y2="-1.27" width="0.127" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.127" layer="21"/>
<wire x1="5.08" y1="1.27" x2="-1.27" y2="1.27" width="0.127" layer="21"/>
<pad name="P$1" x="0" y="0" drill="1.0668"/>
<pad name="P$2" x="3.81" y="0" drill="1.0668"/>
</package>
<package name="JST-2-SMD-VERT">
<wire x1="-4.1" y1="2.97" x2="4.2" y2="2.97" width="0.2032" layer="51"/>
<wire x1="4.2" y1="2.97" x2="4.2" y2="-2.13" width="0.2032" layer="51"/>
<wire x1="4.2" y1="-2.13" x2="-4.1" y2="-2.13" width="0.2032" layer="51"/>
<wire x1="-4.1" y1="-2.13" x2="-4.1" y2="2.97" width="0.2032" layer="51"/>
<wire x1="-4.1" y1="3" x2="4.2" y2="3" width="0.2032" layer="21"/>
<wire x1="4.2" y1="3" x2="4.2" y2="2.3" width="0.2032" layer="21"/>
<wire x1="-4.1" y1="3" x2="-4.1" y2="2.3" width="0.2032" layer="21"/>
<wire x1="2" y1="-2.1" x2="4.2" y2="-2.1" width="0.2032" layer="21"/>
<wire x1="4.2" y1="-2.1" x2="4.2" y2="-1.7" width="0.2032" layer="21"/>
<wire x1="-2" y1="-2.1" x2="-4.1" y2="-2.1" width="0.2032" layer="21"/>
<wire x1="-4.1" y1="-2.1" x2="-4.1" y2="-1.8" width="0.2032" layer="21"/>
<smd name="P$1" x="-3.4" y="0.27" dx="3" dy="1.6" layer="1" rot="R90"/>
<smd name="P$2" x="3.4" y="0.27" dx="3" dy="1.6" layer="1" rot="R90"/>
<smd name="VCC" x="-1" y="-2" dx="1" dy="5.5" layer="1"/>
<smd name="GND" x="1" y="-2" dx="1" dy="5.5" layer="1"/>
<text x="2.54" y="-5.08" size="1.27" layer="25">&gt;Name</text>
<text x="2.24" y="3.48" size="1.27" layer="27">&gt;Value</text>
</package>
<package name="R_SW_TH">
<wire x1="-1.651" y1="19.2532" x2="-1.651" y2="-1.3716" width="0.2032" layer="21"/>
<wire x1="-1.651" y1="-1.3716" x2="-1.651" y2="-2.2352" width="0.2032" layer="21"/>
<wire x1="-1.651" y1="19.2532" x2="13.589" y2="19.2532" width="0.2032" layer="21"/>
<wire x1="13.589" y1="19.2532" x2="13.589" y2="-2.2352" width="0.2032" layer="21"/>
<wire x1="13.589" y1="-2.2352" x2="-1.651" y2="-2.2352" width="0.2032" layer="21"/>
<pad name="P$1" x="0" y="0" drill="1.6002"/>
<pad name="P$2" x="0" y="16.9926" drill="1.6002"/>
<pad name="P$3" x="12.0904" y="15.494" drill="1.6002"/>
<pad name="P$4" x="12.0904" y="8.4582" drill="1.6002"/>
</package>
<package name="SCREWTERMINAL-5MM-2">
<wire x1="-3.1" y1="4.2" x2="8.1" y2="4.2" width="0.2032" layer="21"/>
<wire x1="8.1" y1="4.2" x2="8.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="8.1" y1="-2.3" x2="8.1" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="8.1" y1="-3.3" x2="-3.1" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-3.3" x2="-3.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-2.3" x2="-3.1" y2="4.2" width="0.2032" layer="21"/>
<wire x1="8.1" y1="-2.3" x2="-3.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-1.35" x2="-3.7" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-3.7" y1="-1.35" x2="-3.7" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-3.7" y1="-2.35" x2="-3.1" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="8.1" y1="4" x2="8.7" y2="4" width="0.2032" layer="51"/>
<wire x1="8.7" y1="4" x2="8.7" y2="3" width="0.2032" layer="51"/>
<wire x1="8.7" y1="3" x2="8.1" y2="3" width="0.2032" layer="51"/>
<circle x="2.5" y="3.7" radius="0.2828" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="1.3" diameter="2.032" shape="square"/>
<pad name="2" x="5" y="0" drill="1.3" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X02_LOCK">
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="-0.1778" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.7178" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2921" y1="-0.2921" x2="0.2921" y2="0.2921" layer="51"/>
<rectangle x1="2.2479" y1="-0.2921" x2="2.8321" y2="0.2921" layer="51"/>
</package>
<package name="MOLEX-1X2_LOCK">
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.048" x2="3.81" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="3.81" y1="-2.54" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="-0.127" y="0" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.667" y="0" drill="1.016" diameter="1.8796"/>
<rectangle x1="-0.2921" y1="-0.2921" x2="0.2921" y2="0.2921" layer="51"/>
<rectangle x1="2.2479" y1="-0.2921" x2="2.8321" y2="0.2921" layer="51"/>
</package>
<package name="1X02_LOCK_LONGPADS">
<description>This footprint was designed to help hold the alignment of a through-hole component (i.e.  6-pin header) while soldering it into place.  
You may notice that each hole has been shifted either up or down by 0.005 of an inch from it's more standard position (which is a perfectly straight line).  
This slight alteration caused the pins (the squares in the middle) to touch the edges of the holes.  Because they are alternating, it causes a "brace" 
to hold the component in place.  0.005 has proven to be the perfect amount of "off-center" position when using our standard breakaway headers.
Although looks a little odd when you look at the bare footprint, once you have a header in there, the alteration is very hard to notice.  Also,
if you push a header all the way into place, it is covered up entirely on the bottom side.  This idea of altering the position of holes to aid alignment 
will be further integrated into the Sparkfun Library for other footprints.  It can help hold any component with 3 or more connection pins.</description>
<wire x1="1.651" y1="0" x2="0.889" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.016" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="0.9906" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.9906" x2="-0.9906" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-0.9906" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.9906" x2="-0.9906" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0" x2="3.556" y2="0" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0" x2="3.81" y2="-0.9906" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.9906" x2="3.5306" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0" x2="3.81" y2="0.9906" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.9906" x2="3.5306" y2="1.27" width="0.2032" layer="21"/>
<pad name="1" x="-0.127" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="2.667" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-1.27" y="1.778" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-1.27" y="-3.302" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.2921" y1="-0.2921" x2="0.2921" y2="0.2921" layer="51"/>
<rectangle x1="2.2479" y1="-0.2921" x2="2.8321" y2="0.2921" layer="51"/>
</package>
<package name="SCREWTERMINAL-3.5MM-2_LOCK">
<wire x1="-1.75" y1="3.4" x2="5.25" y2="3.4" width="0.2032" layer="21"/>
<wire x1="5.25" y1="3.4" x2="5.25" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-2.8" x2="5.25" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-3.6" x2="-1.75" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-3.6" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-2.8" x2="-1.75" y2="3.4" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-2.8" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-1.35" x2="-2.15" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-1.35" x2="-2.15" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-2.35" x2="-1.75" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="5.25" y1="3.15" x2="5.65" y2="3.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="3.15" x2="5.65" y2="2.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="2.15" x2="5.25" y2="2.15" width="0.2032" layer="51"/>
<circle x="2" y="3" radius="0.2828" width="0.127" layer="51"/>
<circle x="0" y="0" radius="0.4318" width="0.0254" layer="51"/>
<circle x="3.5" y="0" radius="0.4318" width="0.0254" layer="51"/>
<pad name="1" x="-0.1778" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.6778" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X02_LONGPADS">
<pad name="1" x="0" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
</package>
<package name="1X02_NO_SILK">
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="JST-2-PTH">
<wire x1="-2" y1="0" x2="-2" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="-2" y1="-1.8" x2="-3" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="-3" y1="-1.8" x2="-3" y2="6" width="0.2032" layer="21"/>
<wire x1="-3" y1="6" x2="3" y2="6" width="0.2032" layer="21"/>
<wire x1="3" y1="6" x2="3" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3" y1="-1.8" x2="2" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="2" y1="-1.8" x2="2" y2="0" width="0.2032" layer="21"/>
<pad name="1" x="-1" y="0" drill="0.7" diameter="1.4478"/>
<pad name="2" x="1" y="0" drill="0.7" diameter="1.4478"/>
<text x="-1.27" y="5.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.27" y="4" size="0.4064" layer="27">&gt;Value</text>
<text x="0.6" y="0.7" size="1.27" layer="51">+</text>
<text x="-1.4" y="0.7" size="1.27" layer="51">-</text>
</package>
<package name="1X02_XTRA_BIG">
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-2.54" x2="5.08" y2="-2.54" width="0.127" layer="21"/>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="2.54" width="0.127" layer="21"/>
<wire x1="5.08" y1="2.54" x2="-5.08" y2="2.54" width="0.127" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="2.0574" diameter="3.556"/>
<pad name="2" x="2.54" y="0" drill="2.0574" diameter="3.556"/>
</package>
<package name="1X02_PP_HOLES_ONLY">
<circle x="0" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="2.54" y="0" radius="0.635" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<hole x="0" y="0" drill="1.4732"/>
<hole x="2.54" y="0" drill="1.4732"/>
</package>
<package name="SCREWTERMINAL-3.5MM-2-NS">
<wire x1="-1.75" y1="3.4" x2="5.25" y2="3.4" width="0.2032" layer="51"/>
<wire x1="5.25" y1="3.4" x2="5.25" y2="-2.8" width="0.2032" layer="51"/>
<wire x1="5.25" y1="-2.8" x2="5.25" y2="-3.6" width="0.2032" layer="51"/>
<wire x1="5.25" y1="-3.6" x2="-1.75" y2="-3.6" width="0.2032" layer="51"/>
<wire x1="-1.75" y1="-3.6" x2="-1.75" y2="-2.8" width="0.2032" layer="51"/>
<wire x1="-1.75" y1="-2.8" x2="-1.75" y2="3.4" width="0.2032" layer="51"/>
<wire x1="5.25" y1="-2.8" x2="-1.75" y2="-2.8" width="0.2032" layer="51"/>
<wire x1="-1.75" y1="-1.35" x2="-2.15" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-1.35" x2="-2.15" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-2.35" x2="-1.75" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="5.25" y1="3.15" x2="5.65" y2="3.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="3.15" x2="5.65" y2="2.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="2.15" x2="5.25" y2="2.15" width="0.2032" layer="51"/>
<circle x="2" y="3" radius="0.2828" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="JST-2-PTH-NS">
<wire x1="-2" y1="0" x2="-2" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-2" y1="-1.8" x2="-3" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3" y1="-1.8" x2="-3" y2="6" width="0.2032" layer="51"/>
<wire x1="-3" y1="6" x2="3" y2="6" width="0.2032" layer="51"/>
<wire x1="3" y1="6" x2="3" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="3" y1="-1.8" x2="2" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="2" y1="-1.8" x2="2" y2="0" width="0.2032" layer="51"/>
<pad name="1" x="-1" y="0" drill="0.7" diameter="1.4478"/>
<pad name="2" x="1" y="0" drill="0.7" diameter="1.4478"/>
<text x="-1.27" y="5.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.27" y="4" size="0.4064" layer="27">&gt;Value</text>
<text x="0.6" y="0.7" size="1.27" layer="51">+</text>
<text x="-1.4" y="0.7" size="1.27" layer="51">-</text>
</package>
<package name="JST-2-PTH-KIT">
<description>&lt;H3&gt;JST-2-PTH-KIT&lt;/h3&gt;
2-Pin JST, through-hole connector&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of this package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.</description>
<wire x1="-2" y1="0" x2="-2" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-2" y1="-1.8" x2="-3" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3" y1="-1.8" x2="-3" y2="6" width="0.2032" layer="51"/>
<wire x1="-3" y1="6" x2="3" y2="6" width="0.2032" layer="51"/>
<wire x1="3" y1="6" x2="3" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="3" y1="-1.8" x2="2" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="2" y1="-1.8" x2="2" y2="0" width="0.2032" layer="51"/>
<pad name="1" x="-1" y="0" drill="0.7" diameter="1.4478"/>
<pad name="2" x="1" y="0" drill="0.7" diameter="1.4478"/>
<text x="-1.27" y="5.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.27" y="4" size="0.4064" layer="27">&gt;Value</text>
<text x="0.6" y="0.7" size="1.27" layer="51">+</text>
<text x="-1.4" y="0.7" size="1.27" layer="51">-</text>
<polygon width="0.127" layer="30">
<vertex x="-0.9975" y="-0.6604" curve="-90.025935"/>
<vertex x="-1.6604" y="0" curve="-90.017354"/>
<vertex x="-1" y="0.6604" curve="-90"/>
<vertex x="-0.3396" y="0" curve="-90.078137"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-1" y="-0.2865" curve="-90.08005"/>
<vertex x="-1.2865" y="0" curve="-90.040011"/>
<vertex x="-1" y="0.2865" curve="-90"/>
<vertex x="-0.7135" y="0" curve="-90"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="1.0025" y="-0.6604" curve="-90.025935"/>
<vertex x="0.3396" y="0" curve="-90.017354"/>
<vertex x="1" y="0.6604" curve="-90"/>
<vertex x="1.6604" y="0" curve="-90.078137"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="1" y="-0.2865" curve="-90.08005"/>
<vertex x="0.7135" y="0" curve="-90.040011"/>
<vertex x="1" y="0.2865" curve="-90"/>
<vertex x="1.2865" y="0" curve="-90"/>
</polygon>
</package>
<package name="SPRINGTERMINAL-2.54MM-2">
<wire x1="-4.2" y1="7.88" x2="-4.2" y2="-2.8" width="0.254" layer="21"/>
<wire x1="-4.2" y1="-2.8" x2="-4.2" y2="-4.72" width="0.254" layer="51"/>
<wire x1="-4.2" y1="-4.72" x2="3.44" y2="-4.72" width="0.254" layer="51"/>
<wire x1="3.44" y1="-4.72" x2="3.44" y2="-2.8" width="0.254" layer="51"/>
<wire x1="3.44" y1="7.88" x2="-4.2" y2="7.88" width="0.254" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.254" layer="1"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.254" layer="16"/>
<wire x1="2.54" y1="0" x2="2.54" y2="5.08" width="0.254" layer="16"/>
<wire x1="2.54" y1="0" x2="2.54" y2="5.08" width="0.254" layer="1"/>
<wire x1="-4.2" y1="-2.8" x2="3.44" y2="-2.8" width="0.254" layer="21"/>
<wire x1="3.44" y1="4" x2="3.44" y2="1" width="0.254" layer="21"/>
<wire x1="3.44" y1="7.88" x2="3.44" y2="6" width="0.254" layer="21"/>
<wire x1="3.44" y1="-0.9" x2="3.44" y2="-2.8" width="0.254" layer="21"/>
<pad name="1" x="0" y="0" drill="1.1" diameter="1.9"/>
<pad name="P$2" x="0" y="5.08" drill="1.1" diameter="1.9"/>
<pad name="P$3" x="2.54" y="5.08" drill="1.1" diameter="1.9"/>
<pad name="2" x="2.54" y="0" drill="1.1" diameter="1.9"/>
</package>
<package name="1X02_2.54_SCREWTERM">
<pad name="P2" x="0" y="0" drill="1.016" shape="square"/>
<pad name="P1" x="2.54" y="0" drill="1.016" shape="square"/>
<wire x1="-1.5" y1="3.25" x2="4" y2="3.25" width="0.127" layer="21"/>
<wire x1="4" y1="3.25" x2="4" y2="2.5" width="0.127" layer="21"/>
<wire x1="4" y1="2.5" x2="4" y2="-3.25" width="0.127" layer="21"/>
<wire x1="4" y1="-3.25" x2="-1.5" y2="-3.25" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-3.25" x2="-1.5" y2="2.5" width="0.127" layer="21"/>
<wire x1="-1.5" y1="2.5" x2="-1.5" y2="3.25" width="0.127" layer="21"/>
<wire x1="-1.5" y1="2.5" x2="4" y2="2.5" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="M02">
<wire x1="3.81" y1="-2.54" x2="-2.54" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-2.54" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.54" y="5.842" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="0" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="2" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="M02" prefix="JP">
<description>Standard 2-pin 0.1" header. Use with &lt;br&gt;
- straight break away headers ( PRT-00116)&lt;br&gt;
- right angle break away headers (PRT-00553)&lt;br&gt;
- swiss pins (PRT-00743)&lt;br&gt;
- machine pins (PRT-00117)&lt;br&gt;
- female headers (PRT-00115)&lt;br&gt;&lt;br&gt;

 Molex polarized connector foot print use with: PRT-08233 with associated crimp pins and housings.&lt;br&gt;&lt;br&gt;

2.54_SCREWTERM for use with  PRT-10571.&lt;br&gt;&lt;br&gt;

3.5mm Screw Terminal footprints for  PRT-08084&lt;br&gt;&lt;br&gt;

5mm Screw Terminal footprints for use with PRT-08433</description>
<gates>
<gate name="G$1" symbol="M02" x="-2.54" y="0"/>
</gates>
<devices>
<device name="PTH" package="1X02">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POLAR" package="MOLEX-1X2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.5MM" package="SCREWTERMINAL-3.5MM-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-JST-2MM-SMT" package="JST-2-SMD">
<connects>
<connect gate="G$1" pin="1" pad="2"/>
<connect gate="G$1" pin="2" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH2" package="1X02_BIG">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4UCON-15767" package="JST-2-SMD-VERT">
<connects>
<connect gate="G$1" pin="1" pad="GND"/>
<connect gate="G$1" pin="2" pad="VCC"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="ROCKER" package="R_SW_TH">
<connects>
<connect gate="G$1" pin="1" pad="P$3"/>
<connect gate="G$1" pin="2" pad="P$4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5MM" package="SCREWTERMINAL-5MM-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK" package="1X02_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POLAR_LOCK" package="MOLEX-1X2_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK_LONGPADS" package="1X02_LOCK_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.5MM_LOCK" package="SCREWTERMINAL-3.5MM-2_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH3" package="1X02_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1X02_NO_SILK" package="1X02_NO_SILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH-2" package="JST-2-PTH">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH4" package="1X02_XTRA_BIG">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POGO_PIN_HOLES_ONLY" package="1X02_PP_HOLES_ONLY">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.5MM-NO_SILK" package="SCREWTERMINAL-3.5MM-2-NS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-JST-2-PTH-NO_SILK" package="JST-2-PTH-NS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH-2-KIT" package="JST-2-PTH-KIT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SPRING-2.54-RA" package="SPRINGTERMINAL-2.54MM-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2.54MM_SCREWTERM" package="1X02_2.54_SCREWTERM">
<connects>
<connect gate="G$1" pin="1" pad="P1"/>
<connect gate="G$1" pin="2" pad="P2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="ATSAMD11D14A-MNT">
<description>&lt;ARM Microcontrollers - MCU Cortex-M0+, 16KB FLASH,4KB SRAM, USB - 24 QFN,105C, GREEN,1.6-3.6V,48MHz,USB&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="QFN50P400X400X90-25N">
<description>&lt;b&gt;24-pin QFN, 4x4mm Body, 0.5mm Pitch_3&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-2" y="1.25" dx="0.8" dy="0.3" layer="1"/>
<smd name="2" x="-2" y="0.75" dx="0.8" dy="0.3" layer="1"/>
<smd name="3" x="-2" y="0.25" dx="0.8" dy="0.3" layer="1"/>
<smd name="4" x="-2" y="-0.25" dx="0.8" dy="0.3" layer="1"/>
<smd name="5" x="-2" y="-0.75" dx="0.8" dy="0.3" layer="1"/>
<smd name="6" x="-2" y="-1.25" dx="0.8" dy="0.3" layer="1"/>
<smd name="7" x="-1.25" y="-2" dx="0.8" dy="0.3" layer="1" rot="R90"/>
<smd name="8" x="-0.75" y="-2" dx="0.8" dy="0.3" layer="1" rot="R90"/>
<smd name="9" x="-0.25" y="-2" dx="0.8" dy="0.3" layer="1" rot="R90"/>
<smd name="10" x="0.25" y="-2" dx="0.8" dy="0.3" layer="1" rot="R90"/>
<smd name="11" x="0.75" y="-2" dx="0.8" dy="0.3" layer="1" rot="R90"/>
<smd name="12" x="1.25" y="-2" dx="0.8" dy="0.3" layer="1" rot="R90"/>
<smd name="13" x="2" y="-1.25" dx="0.8" dy="0.3" layer="1"/>
<smd name="14" x="2" y="-0.75" dx="0.8" dy="0.3" layer="1"/>
<smd name="15" x="2" y="-0.25" dx="0.8" dy="0.3" layer="1"/>
<smd name="16" x="2" y="0.25" dx="0.8" dy="0.3" layer="1"/>
<smd name="17" x="2" y="0.75" dx="0.8" dy="0.3" layer="1"/>
<smd name="18" x="2" y="1.25" dx="0.8" dy="0.3" layer="1"/>
<smd name="19" x="1.25" y="2" dx="0.8" dy="0.3" layer="1" rot="R90"/>
<smd name="20" x="0.75" y="2" dx="0.8" dy="0.3" layer="1" rot="R90"/>
<smd name="21" x="0.25" y="2" dx="0.8" dy="0.3" layer="1" rot="R90"/>
<smd name="22" x="-0.25" y="2" dx="0.8" dy="0.3" layer="1" rot="R90"/>
<smd name="23" x="-0.75" y="2" dx="0.8" dy="0.3" layer="1" rot="R90"/>
<smd name="24" x="-1.25" y="2" dx="0.8" dy="0.3" layer="1" rot="R90"/>
<smd name="25" x="0" y="0" dx="2.75" dy="2.75" layer="1" rot="R90"/>
<text x="0" y="0" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="51" align="center">&gt;VALUE</text>
<wire x1="-2.625" y1="2.625" x2="2.625" y2="2.625" width="0.05" layer="21"/>
<wire x1="2.625" y1="2.625" x2="2.625" y2="-2.625" width="0.05" layer="21"/>
<wire x1="2.625" y1="-2.625" x2="-2.625" y2="-2.625" width="0.05" layer="21"/>
<wire x1="-2.625" y1="-2.625" x2="-2.625" y2="2.625" width="0.05" layer="21"/>
<wire x1="-2" y1="2" x2="2" y2="2" width="0.1" layer="51"/>
<wire x1="2" y1="2" x2="2" y2="-2" width="0.1" layer="51"/>
<wire x1="2" y1="-2" x2="-2" y2="-2" width="0.1" layer="51"/>
<wire x1="-2" y1="-2" x2="-2" y2="2" width="0.1" layer="51"/>
<wire x1="-2" y1="1.5" x2="-1.5" y2="2" width="0.1" layer="51"/>
<circle x="-2.4" y="2" radius="0.125" width="0.25" layer="25"/>
</package>
</packages>
<symbols>
<symbol name="ATSAMD11D14A-MNT">
<wire x1="-17.78" y1="17.78" x2="20.32" y2="17.78" width="0.254" layer="94"/>
<wire x1="20.32" y1="17.78" x2="20.32" y2="-20.32" width="0.254" layer="94"/>
<wire x1="20.32" y1="-20.32" x2="-17.78" y2="-20.32" width="0.254" layer="94"/>
<wire x1="-17.78" y1="-20.32" x2="-17.78" y2="17.78" width="0.254" layer="94"/>
<text x="13.97" y="19.05" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="13.97" y="-21.59" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="PA02" x="-22.86" y="0" length="middle"/>
<pin name="PA03" x="-22.86" y="-2.54" length="middle"/>
<pin name="PA04" x="-22.86" y="-5.08" length="middle"/>
<pin name="PA05" x="-22.86" y="-7.62" length="middle"/>
<pin name="PA06" x="-22.86" y="-10.16" length="middle"/>
<pin name="PA07" x="-22.86" y="-12.7" length="middle"/>
<pin name="PA08" x="-5.08" y="-25.4" length="middle" rot="R90"/>
<pin name="PA09" x="-2.54" y="-25.4" length="middle" rot="R90"/>
<pin name="PA10" x="0" y="-25.4" length="middle" rot="R90"/>
<pin name="PA11" x="2.54" y="-25.4" length="middle" rot="R90"/>
<pin name="PA14" x="5.08" y="-25.4" length="middle" rot="R90"/>
<pin name="PA15" x="7.62" y="-25.4" length="middle" rot="R90"/>
<pin name="PA28/!RESET" x="25.4" y="0" length="middle" rot="R180"/>
<pin name="PA27" x="25.4" y="-2.54" length="middle" rot="R180"/>
<pin name="PA23" x="25.4" y="-5.08" length="middle" rot="R180"/>
<pin name="PA22" x="25.4" y="-7.62" length="middle" rot="R180"/>
<pin name="PA17" x="25.4" y="-10.16" length="middle" rot="R180"/>
<pin name="PA16" x="25.4" y="-12.7" length="middle" rot="R180"/>
<pin name="GND(2)" x="-7.62" y="22.86" length="middle" direction="pwr" rot="R270"/>
<pin name="VDD" x="-5.08" y="22.86" length="middle" rot="R270"/>
<pin name="GND(1)" x="-2.54" y="22.86" length="middle" direction="pwr" rot="R270"/>
<pin name="PA25" x="0" y="22.86" length="middle" rot="R270"/>
<pin name="PA24" x="2.54" y="22.86" length="middle" rot="R270"/>
<pin name="SWDIO/PA31" x="5.08" y="22.86" length="middle" rot="R270"/>
<pin name="SWCLK/PA30" x="7.62" y="22.86" length="middle" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ATSAMD11D14A-MNT" prefix="IC">
<description>&lt;b&gt;ARM Microcontrollers - MCU Cortex-M0+, 16KB FLASH,4KB SRAM, USB - 24 QFN,105C, GREEN,1.6-3.6V,48MHz,USB&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="http://ww1.microchip.com/downloads/en/devicedoc/atmel-42363-sam-d11_datasheet.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="ATSAMD11D14A-MNT" x="0" y="0"/>
</gates>
<devices>
<device name="" package="QFN50P400X400X90-25N">
<connects>
<connect gate="G$1" pin="GND(1)" pad="23"/>
<connect gate="G$1" pin="GND(2)" pad="25"/>
<connect gate="G$1" pin="PA02" pad="1"/>
<connect gate="G$1" pin="PA03" pad="2"/>
<connect gate="G$1" pin="PA04" pad="3"/>
<connect gate="G$1" pin="PA05" pad="4"/>
<connect gate="G$1" pin="PA06" pad="5"/>
<connect gate="G$1" pin="PA07" pad="6"/>
<connect gate="G$1" pin="PA08" pad="7"/>
<connect gate="G$1" pin="PA09" pad="8"/>
<connect gate="G$1" pin="PA10" pad="9"/>
<connect gate="G$1" pin="PA11" pad="10"/>
<connect gate="G$1" pin="PA14" pad="11"/>
<connect gate="G$1" pin="PA15" pad="12"/>
<connect gate="G$1" pin="PA16" pad="13"/>
<connect gate="G$1" pin="PA17" pad="14"/>
<connect gate="G$1" pin="PA22" pad="15"/>
<connect gate="G$1" pin="PA23" pad="16"/>
<connect gate="G$1" pin="PA24" pad="21"/>
<connect gate="G$1" pin="PA25" pad="22"/>
<connect gate="G$1" pin="PA27" pad="17"/>
<connect gate="G$1" pin="PA28/!RESET" pad="18"/>
<connect gate="G$1" pin="SWCLK/PA30" pad="19"/>
<connect gate="G$1" pin="SWDIO/PA31" pad="20"/>
<connect gate="G$1" pin="VDD" pad="24"/>
</connects>
<technologies>
<technology name="">
<attribute name="DATASHEEET" value="http://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-42363-SAM-D11_Datasheet.pdf" constant="no"/>
<attribute name="DESCRIPTION" value="ARM Microcontrollers - MCU Cortex-M0+, 16KB FLASH,4KB SRAM, USB - 24 QFN,105C, GREEN,1.6-3.6V,48MHz,USB" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.se/products/en?keywords=ATSAMD11D14A-MUTCT-ND" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="ATSAMD11D14A-MUTCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Microchip" constant="no"/>
<attribute name="MANUFACTURER-PN" value="ATSAMD11D14A-MNT" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="MC33926PNB-newSymbol">
<description>&lt;Motor / Motion / Ignition Controllers &amp; Drivers THROTTLE CTRL H-BRIDGE&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="QFN80P800X800X220-33N">
<description>&lt;b&gt;32-PIN PQFN&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-3.85" y="2.8" dx="1.05" dy="0.6" layer="1"/>
<smd name="2" x="-3.85" y="2" dx="1.05" dy="0.6" layer="1"/>
<smd name="3" x="-3.85" y="1.2" dx="1.05" dy="0.6" layer="1"/>
<smd name="4" x="-3.85" y="0.4" dx="1.05" dy="0.6" layer="1"/>
<smd name="5" x="-3.85" y="-0.4" dx="1.05" dy="0.6" layer="1"/>
<smd name="6" x="-3.85" y="-1.2" dx="1.05" dy="0.6" layer="1"/>
<smd name="7" x="-3.85" y="-2" dx="1.05" dy="0.6" layer="1"/>
<smd name="8" x="-3.85" y="-2.8" dx="1.05" dy="0.6" layer="1"/>
<smd name="9" x="-2.8" y="-3.85" dx="1.05" dy="0.6" layer="1" rot="R90"/>
<smd name="10" x="-2" y="-3.85" dx="1.05" dy="0.6" layer="1" rot="R90"/>
<smd name="11" x="-1.2" y="-3.85" dx="1.05" dy="0.6" layer="1" rot="R90"/>
<smd name="12" x="-0.4" y="-3.85" dx="1.05" dy="0.6" layer="1" rot="R90"/>
<smd name="13" x="0.4" y="-3.85" dx="1.05" dy="0.6" layer="1" rot="R90"/>
<smd name="14" x="1.2" y="-3.85" dx="1.05" dy="0.6" layer="1" rot="R90"/>
<smd name="15" x="2" y="-3.85" dx="1.05" dy="0.6" layer="1" rot="R90"/>
<smd name="16" x="2.8" y="-3.85" dx="1.05" dy="0.6" layer="1" rot="R90"/>
<smd name="17" x="3.85" y="-2.8" dx="1.05" dy="0.6" layer="1"/>
<smd name="18" x="3.85" y="-2" dx="1.05" dy="0.6" layer="1"/>
<smd name="19" x="3.85" y="-1.2" dx="1.05" dy="0.6" layer="1"/>
<smd name="20" x="3.85" y="-0.4" dx="1.05" dy="0.6" layer="1"/>
<smd name="21" x="3.85" y="0.4" dx="1.05" dy="0.6" layer="1"/>
<smd name="22" x="3.85" y="1.2" dx="1.05" dy="0.6" layer="1"/>
<smd name="23" x="3.85" y="2" dx="1.05" dy="0.6" layer="1"/>
<smd name="24" x="3.85" y="2.8" dx="1.05" dy="0.6" layer="1"/>
<smd name="25" x="2.8" y="3.85" dx="1.05" dy="0.6" layer="1" rot="R90"/>
<smd name="26" x="2" y="3.85" dx="1.05" dy="0.6" layer="1" rot="R90"/>
<smd name="27" x="1.2" y="3.85" dx="1.05" dy="0.6" layer="1" rot="R90"/>
<smd name="28" x="0.4" y="3.85" dx="1.05" dy="0.6" layer="1" rot="R90"/>
<smd name="29" x="-0.4" y="3.85" dx="1.05" dy="0.6" layer="1" rot="R90"/>
<smd name="30" x="-1.2" y="3.85" dx="1.05" dy="0.6" layer="1" rot="R90"/>
<smd name="31" x="-2" y="3.85" dx="1.05" dy="0.6" layer="1" rot="R90"/>
<smd name="32" x="-2.8" y="3.85" dx="1.05" dy="0.6" layer="1" rot="R90"/>
<smd name="EPAD" x="0" y="0" dx="5.2" dy="5.2" layer="1" rot="R90"/>
<text x="0" y="0" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="51" align="center">&gt;VALUE</text>
<wire x1="-4.6" y1="4.6" x2="4.6" y2="4.6" width="0.05" layer="21"/>
<wire x1="4.6" y1="4.6" x2="4.6" y2="-4.6" width="0.05" layer="21"/>
<wire x1="4.6" y1="-4.6" x2="-4.6" y2="-4.6" width="0.05" layer="21"/>
<wire x1="-4.6" y1="-4.6" x2="-4.6" y2="4.6" width="0.05" layer="21"/>
<wire x1="-4" y1="4" x2="4" y2="4" width="0.1" layer="51"/>
<wire x1="4" y1="4" x2="4" y2="-4" width="0.1" layer="51"/>
<wire x1="4" y1="-4" x2="-4" y2="-4" width="0.1" layer="51"/>
<wire x1="-4" y1="-4" x2="-4" y2="4" width="0.1" layer="51"/>
<wire x1="-4" y1="3.2" x2="-3.2" y2="4" width="0.1" layer="51"/>
<circle x="-4.375" y="4" radius="0.2" width="0.4" layer="25"/>
</package>
<package name="32-PQFN(8X8)">
<wire x1="4" y1="4" x2="4" y2="-4" width="0.127" layer="21"/>
<wire x1="4" y1="-4" x2="-4" y2="-4" width="0.127" layer="21"/>
<wire x1="-4" y1="-4" x2="-4" y2="4" width="0.127" layer="21"/>
<wire x1="-4" y1="4" x2="4" y2="4" width="0.127" layer="21"/>
<smd name="EPAD" x="0" y="0" dx="5.2" dy="5.2" layer="1"/>
<wire x1="-2.62" y1="-2.56" x2="-2.62" y2="1.74" width="0.127" layer="51"/>
<wire x1="-2.62" y1="1.74" x2="-1.77" y2="2.59" width="0.127" layer="51"/>
<wire x1="-1.77" y1="2.59" x2="2.56" y2="2.59" width="0.127" layer="51"/>
<wire x1="2.56" y1="2.59" x2="2.58" y2="2.57" width="0.127" layer="51"/>
<wire x1="2.58" y1="2.57" x2="2.58" y2="-2.56" width="0.127" layer="51"/>
<wire x1="2.58" y1="-2.56" x2="-2.62" y2="-2.56" width="0.127" layer="51"/>
<smd name="1" x="-3.44" y="3.46" dx="1.05" dy="1.05" layer="1" rot="R270"/>
<smd name="29" x="0" y="3.46" dx="1.05" dy="0.54" layer="1" rot="R90"/>
<smd name="28" x="0.8" y="3.46" dx="1.05" dy="0.54" layer="1" rot="R90"/>
<smd name="27" x="1.6" y="3.46" dx="1.05" dy="0.54" layer="1" rot="R90"/>
<smd name="26" x="2.4" y="3.46" dx="1.05" dy="0.54" layer="1" rot="R90"/>
<smd name="30" x="-0.8" y="3.46" dx="1.05" dy="0.54" layer="1" rot="R90"/>
<smd name="31" x="-1.6" y="3.46" dx="1.05" dy="0.54" layer="1" rot="R90"/>
<smd name="32" x="-2.4" y="3.46" dx="1.05" dy="0.54" layer="1" rot="R90"/>
<smd name="13" x="0" y="-3.46" dx="1.05" dy="0.54" layer="1" rot="R270"/>
<smd name="14" x="0.8" y="-3.46" dx="1.05" dy="0.54" layer="1" rot="R270"/>
<smd name="15" x="1.6" y="-3.46" dx="1.05" dy="0.54" layer="1" rot="R270"/>
<smd name="16" x="2.4" y="-3.46" dx="1.05" dy="0.54" layer="1" rot="R270"/>
<smd name="12" x="-0.8" y="-3.46" dx="1.05" dy="0.54" layer="1" rot="R270"/>
<smd name="11" x="-1.6" y="-3.46" dx="1.05" dy="0.54" layer="1" rot="R270"/>
<smd name="10" x="-2.4" y="-3.46" dx="1.05" dy="0.54" layer="1" rot="R270"/>
<smd name="21" x="3.46" y="0" dx="1.05" dy="0.54" layer="1"/>
<smd name="20" x="3.46" y="-0.8" dx="1.05" dy="0.54" layer="1"/>
<smd name="19" x="3.46" y="-1.6" dx="1.05" dy="0.54" layer="1"/>
<smd name="18" x="3.46" y="-2.4" dx="1.05" dy="0.54" layer="1"/>
<smd name="22" x="3.46" y="0.8" dx="1.05" dy="0.54" layer="1"/>
<smd name="23" x="3.46" y="1.6" dx="1.05" dy="0.54" layer="1"/>
<smd name="24" x="3.46" y="2.4" dx="1.05" dy="0.54" layer="1"/>
<smd name="8" x="-3.46" y="-2.4" dx="1.05" dy="0.54" layer="1" rot="R180"/>
<smd name="7" x="-3.46" y="-1.6" dx="1.05" dy="0.54" layer="1" rot="R180"/>
<smd name="6" x="-3.46" y="-0.8" dx="1.05" dy="0.54" layer="1" rot="R180"/>
<smd name="5" x="-3.46" y="0" dx="1.05" dy="0.54" layer="1" rot="R180"/>
<smd name="4" x="-3.46" y="0.8" dx="1.05" dy="0.54" layer="1" rot="R180"/>
<smd name="3" x="-3.46" y="1.6" dx="1.05" dy="0.54" layer="1" rot="R180"/>
<smd name="2" x="-3.46" y="2.4" dx="1.05" dy="0.54" layer="1" rot="R180"/>
<smd name="25" x="3.46" y="3.46" dx="1.05" dy="1.05" layer="1" rot="R270"/>
<smd name="17" x="3.46" y="-3.46" dx="1.05" dy="1.05" layer="1" rot="R180"/>
<smd name="9" x="-3.46" y="-3.46" dx="1.05" dy="1.05" layer="1" rot="R90"/>
<circle x="-2.8" y="2.8" radius="0.14141875" width="0" layer="49"/>
</package>
</packages>
<symbols>
<symbol name="MC33926PNB">
<wire x1="-15.24" y1="15.24" x2="15.24" y2="15.24" width="0.254" layer="94"/>
<wire x1="15.24" y1="15.24" x2="15.24" y2="-15.24" width="0.254" layer="94"/>
<wire x1="15.24" y1="-15.24" x2="-15.24" y2="-15.24" width="0.254" layer="94"/>
<wire x1="-15.24" y1="-15.24" x2="-15.24" y2="15.24" width="0.254" layer="94"/>
<text x="-13.97" y="16.51" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="-13.97" y="-16.51" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="IN2" x="-20.32" y="2.54" length="middle" direction="in"/>
<pin name="IN1" x="-20.32" y="5.08" length="middle" direction="in"/>
<pin name="SLEW" x="-20.32" y="-5.08" length="middle" direction="pas"/>
<pin name="VPWR" x="20.32" y="12.7" length="middle" direction="pas" rot="R180"/>
<pin name="AGND" x="20.32" y="-12.7" length="middle" direction="pwr" rot="R180"/>
<pin name="INV" x="-20.32" y="-2.54" length="middle" direction="in"/>
<pin name="FB" x="-20.32" y="10.16" length="middle" direction="pas"/>
<pin name="EN" x="-20.32" y="-12.7" length="middle" direction="pas"/>
<pin name="OUT1" x="20.32" y="2.54" length="middle" direction="out" rot="R180"/>
<pin name="!D2" x="-20.32" y="-10.16" length="middle" direction="pas"/>
<pin name="!SF" x="-20.32" y="12.7" length="middle" direction="pas"/>
<pin name="PGND" x="20.32" y="-10.16" length="middle" direction="pwr" rot="R180"/>
<pin name="CCP" x="20.32" y="7.62" length="middle" direction="pas" rot="R180"/>
<pin name="OUT2" x="20.32" y="0" length="middle" direction="out" rot="R180"/>
<pin name="D1" x="-20.32" y="-7.62" length="middle" direction="pas"/>
<pin name="NC" x="20.32" y="-5.08" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MC33926PNB" prefix="U">
<description>&lt;b&gt;Motor / Motion / Ignition Controllers &amp; Drivers THROTTLE CTRL H-BRIDGE&lt;/b&gt;&lt;p&gt;
&lt;/b&gt;&lt;br&gt;&lt;a href="&lt;br&gt;&lt;a href="https://componentsearchengine.com/Images/1/MC33926PNB.jpg" title="Image"&gt;
&lt;img src="https://componentsearchengine.com/Images/1/MC33926PNB.jpg" width="150"&gt;&lt;/a&gt;&lt;p&gt;
Source: &lt;a href="http://cache.nxp.com/files/analog/doc/data_sheet/MC33926.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="MC33926PNB" x="0" y="0"/>
</gates>
<devices>
<device name="DIGIKEY" package="QFN80P800X800X220-33N">
<connects>
<connect gate="G$1" pin="!D2" pad="16"/>
<connect gate="G$1" pin="!SF" pad="21"/>
<connect gate="G$1" pin="AGND" pad="5 EPAD"/>
<connect gate="G$1" pin="CCP" pad="32"/>
<connect gate="G$1" pin="D1" pad="26"/>
<connect gate="G$1" pin="EN" pad="10"/>
<connect gate="G$1" pin="FB" pad="8"/>
<connect gate="G$1" pin="IN1" pad="2"/>
<connect gate="G$1" pin="IN2" pad="1"/>
<connect gate="G$1" pin="INV" pad="7"/>
<connect gate="G$1" pin="NC" pad="9 17 25"/>
<connect gate="G$1" pin="OUT1" pad="12 13 14 15"/>
<connect gate="G$1" pin="OUT2" pad="27 28 29 30"/>
<connect gate="G$1" pin="PGND" pad="18 19 20 22 23 24"/>
<connect gate="G$1" pin="SLEW" pad="3"/>
<connect gate="G$1" pin="VPWR" pad="4 6 11 31"/>
</connects>
<technologies>
<technology name="">
<attribute name="3D_PACKAGE" value="" constant="no"/>
<attribute name="ALLIED_NUMBER" value="" constant="no"/>
<attribute name="DESCRIPTION" value="Motor / Motion / Ignition Controllers &amp; Drivers THROTTLE CTRL H-BRIDGE" constant="no"/>
<attribute name="HEIGHT" value="2.2mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Nexperia" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="MC33926PNB" constant="no"/>
<attribute name="OTHER_PART_NUMBER" value="" constant="no"/>
<attribute name="RS_PART_NUMBER" value="" constant="no"/>
<attribute name="SUPPLIER_NAME" value="RS" constant="no"/>
</technology>
</technologies>
</device>
<device name="" package="32-PQFN(8X8)">
<connects>
<connect gate="G$1" pin="!D2" pad="16"/>
<connect gate="G$1" pin="!SF" pad="21"/>
<connect gate="G$1" pin="AGND" pad="5 EPAD"/>
<connect gate="G$1" pin="CCP" pad="32"/>
<connect gate="G$1" pin="D1" pad="26"/>
<connect gate="G$1" pin="EN" pad="10"/>
<connect gate="G$1" pin="FB" pad="8"/>
<connect gate="G$1" pin="IN1" pad="2"/>
<connect gate="G$1" pin="IN2" pad="1"/>
<connect gate="G$1" pin="INV" pad="7"/>
<connect gate="G$1" pin="NC" pad="9 17 25"/>
<connect gate="G$1" pin="OUT1" pad="12 13 14 15"/>
<connect gate="G$1" pin="OUT2" pad="27 28 29 30"/>
<connect gate="G$1" pin="PGND" pad="18 19 20 22 23 24"/>
<connect gate="G$1" pin="SLEW" pad="3"/>
<connect gate="G$1" pin="VPWR" pad="4 6 11 31"/>
</connects>
<technologies>
<technology name="">
<attribute name="DATASHEET" value="https://www.nxp.com/docs/en/data-sheet/MC33926.pdf" constant="no"/>
<attribute name="DISTRIBUTOR-LINK" value="https://www.digikey.se/products/en?keywords=MC33926PNBR2CT-ND" constant="no"/>
<attribute name="DISTRIBUTOR-PN" value="MC33926PNBR2CT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="NXP" constant="no"/>
<attribute name="MANUFACTURER-PN" value="MC33926PNBR2" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="GND12" library="supply1" deviceset="GND" device=""/>
<part name="GND13" library="supply1" deviceset="GND" device=""/>
<part name="GND17" library="supply1" deviceset="GND" device=""/>
<part name="SUPPLY2" library="supply2" deviceset="+5V" device=""/>
<part name="GND11" library="supply1" deviceset="GND" device=""/>
<part name="GND15" library="supply1" deviceset="GND" device=""/>
<part name="GND16" library="supply1" deviceset="GND" device=""/>
<part name="GND24" library="supply1" deviceset="GND" device=""/>
<part name="SUPPLY1" library="supply2" deviceset="+5V" device=""/>
<part name="SUPPLY3" library="supply2" deviceset="+5V" device=""/>
<part name="SUPPLY4" library="supply2" deviceset="+5V" device=""/>
<part name="SUPPLY5" library="supply2" deviceset="+5V" device=""/>
<part name="GND25" library="supply1" deviceset="GND" device=""/>
<part name="GND26" library="supply1" deviceset="GND" device=""/>
<part name="GND27" library="supply1" deviceset="GND" device=""/>
<part name="GND29" library="supply1" deviceset="GND" device=""/>
<part name="GND30" library="supply1" deviceset="GND" device=""/>
<part name="SUPPLY6" library="supply2" deviceset="+5V" device=""/>
<part name="GND9" library="supply1" deviceset="GND" device=""/>
<part name="GND32" library="supply1" deviceset="GND" device=""/>
<part name="U$1" library="BCMI-utility" deviceset="MKRBOARD" device="4CARRIER"/>
<part name="SUPPLY13" library="supply2" deviceset="+5V" device=""/>
<part name="U$4" library="supply2" deviceset="PWRIN" device=""/>
<part name="U$6" library="supply2" deviceset="PWRIN" device=""/>
<part name="TWI" library="Arduino-connectors" deviceset="MODULINO-4" device="" value="-"/>
<part name="GND8" library="supply1" deviceset="GND" device=""/>
<part name="U$13" library="BCMI-supply" deviceset="+3.3V" device=""/>
<part name="U$14" library="BCMI-supply" deviceset="+3.3V" device=""/>
<part name="U$7" library="BCMI-supply" deviceset="+VM" device=""/>
<part name="U$28" library="BCMI-supply" deviceset="+3.3V" device=""/>
<part name="U$33" library="Arduino-utility" deviceset="FIDUCIAL" device="-1.5MM" value="DNP"/>
<part name="U$34" library="Arduino-utility" deviceset="FIDUCIAL" device="-1.5MM" value="DNP"/>
<part name="U$35" library="Arduino-utility" deviceset="FIDUCIAL" device="-1.5MM" value="DNP"/>
<part name="GND28" library="supply1" deviceset="GND" device=""/>
<part name="GND31" library="supply1" deviceset="GND" device=""/>
<part name="GND37" library="supply1" deviceset="GND" device=""/>
<part name="GND38" library="supply1" deviceset="GND" device=""/>
<part name="U1" library="Arduino-ICs" deviceset="LSF0108PWR" device="" value="LSF0108"/>
<part name="GND39" library="supply1" deviceset="GND" device=""/>
<part name="U$36" library="BCMI-supply" deviceset="+3.3V" device=""/>
<part name="C14" library="Arduino-rcl" deviceset="C" device="-0402" technology="-10NF" value="10n"/>
<part name="C15" library="Arduino-rcl" deviceset="C" device="-0402" technology="-10NF" value="10n"/>
<part name="C16" library="Arduino-rcl" deviceset="C" device="-0402" technology="-10NF" value="10n"/>
<part name="C17" library="Arduino-rcl" deviceset="C" device="-0402" technology="-10NF" value="10n"/>
<part name="R4" library="BCMI-rcl" deviceset="R" device="-0402" technology="-140R" value="140R"/>
<part name="R5" library="BCMI-rcl" deviceset="R" device="-0402" technology="-140R" value="140R"/>
<part name="R6" library="BCMI-rcl" deviceset="R" device="-0402" technology="-140R" value="140R"/>
<part name="R7" library="BCMI-rcl" deviceset="R" device="-0402" technology="-140R" value="140R"/>
<part name="COUT1" library="BCMI-rcl" deviceset="C" device="-0805" technology="-22U" value="22u"/>
<part name="COUT2" library="BCMI-rcl" deviceset="C" device="-0805" technology="-22U" value="22u"/>
<part name="SUPPLY9" library="supply2" deviceset="+5V" device=""/>
<part name="C19" library="Arduino-rcl" deviceset="C" device="-0402" technology="-100NF" value="100n"/>
<part name="GND40" library="supply1" deviceset="GND" device=""/>
<part name="GND41" library="supply1" deviceset="GND" device=""/>
<part name="C20" library="Arduino-rcl" deviceset="C" device="-0402" technology="-100NF" value="100n"/>
<part name="R10" library="BCMI-rcl" deviceset="R" device="-0402" technology="-200K" value="200K"/>
<part name="U$22" library="BCMI-supply" deviceset="+VM" device=""/>
<part name="ON" library="BCMI-diodes" deviceset="LED" device="-0603" technology="-GREEN" value="GREEN"/>
<part name="GND44" library="supply1" deviceset="GND" device=""/>
<part name="GND45" library="supply1" deviceset="GND" device=""/>
<part name="Q1" library="BCMI-transistors" deviceset="AON7407" device="" value="AON7407"/>
<part name="C23" library="Arduino-rcl" deviceset="C" device="-0402" technology="-10NF" value="10n"/>
<part name="Q3" library="BCMI-transistors" deviceset="AON7407" device="" value="AON7407"/>
<part name="U$38" library="BCMI-Switches" deviceset="CL-SB-22A-02T" device=""/>
<part name="GND2" library="supply1" deviceset="GND" device=""/>
<part name="GND50" library="supply1" deviceset="GND" device=""/>
<part name="GND52" library="supply1" deviceset="GND" device=""/>
<part name="CBST" library="Arduino-rcl" deviceset="C" device="-0402" technology="-100NF" value="100n"/>
<part name="U2" library="Arduino-ICs" deviceset="TS3001X" device="TS30013" value="TS30013"/>
<part name="LOUT" library="Arduino-rcl" deviceset="L" device="VLS6045EX-4R7M" value="4.7uH/4A/35mO"/>
<part name="D1" library="Arduino-diodes" deviceset="D_SCHOTTKY" device="-PMEG4050EP" value="40V/5A"/>
<part name="CBYPASS" library="Arduino-rcl" deviceset="C" device="-1206" technology="-10UF" value="10uF"/>
<part name="GND43" library="supply1" deviceset="GND" device=""/>
<part name="GND47" library="supply1" deviceset="GND" device=""/>
<part name="GND48" library="supply1" deviceset="GND" device=""/>
<part name="RN4" library="Arduino-rcl" deviceset="4R-N" device="CAY16" technology="-1K" value="1k"/>
<part name="RN2" library="Arduino-rcl" deviceset="4R-N" device="CAY16" technology="-1K" value="1k"/>
<part name="C5" library="Arduino-rcl" deviceset="C+" device="PAN-C" value="150uF"/>
<part name="C13" library="Arduino-rcl" deviceset="C+" device="PAN-C" value="150uF"/>
<part name="C25" library="Arduino-rcl" deviceset="C+" device="PAN-C" value="150uF"/>
<part name="C31" library="Arduino-rcl" deviceset="C+" device="PAN-C" value="150uF"/>
<part name="U4" library="SOIC127P600X170-8N" deviceset="DRV8871" device="" value="DRV8871"/>
<part name="U7" library="SOIC127P600X170-8N" deviceset="DRV8871" device="" value="DRV8871"/>
<part name="GND4" library="supply1" deviceset="GND" device=""/>
<part name="GND5" library="supply1" deviceset="GND" device=""/>
<part name="C26" library="Arduino-rcl" deviceset="C" device="-0402" technology="-100NF" value="100n"/>
<part name="GND55" library="supply1" deviceset="GND" device=""/>
<part name="GND1" library="supply1" deviceset="GND" device=""/>
<part name="GND7" library="supply1" deviceset="GND" device=""/>
<part name="GND59" library="supply1" deviceset="GND" device=""/>
<part name="U$10" library="BCMI-supply" deviceset="+VM" device=""/>
<part name="C8" library="Arduino-rcl" deviceset="C" device="-0402" technology="-100NF" value="100n"/>
<part name="GND23" library="supply1" deviceset="GND" device=""/>
<part name="GND61" library="supply1" deviceset="GND" device=""/>
<part name="R14" library="Arduino-rcl" deviceset="R" device="-0402" technology="-22K" value="22K"/>
<part name="R17" library="Arduino-rcl" deviceset="R" device="-0402" technology="-22K" value="22K"/>
<part name="JP2" library="MyLibrary" deviceset="M02" device="PTH"/>
<part name="U$12" library="supply2" deviceset="PWRIN" device=""/>
<part name="GND22" library="supply1" deviceset="GND" device=""/>
<part name="GND64" library="supply1" deviceset="GND" device=""/>
<part name="C2" library="BCMI-rcl" deviceset="C+" device="AVX-TCJ" technology="-47UF" value="47uF"/>
<part name="C3" library="BCMI-rcl" deviceset="C+" device="AVX-TCJ" technology="-47UF" value="47uF"/>
<part name="GND3" library="supply1" deviceset="GND" device=""/>
<part name="GND6" library="supply1" deviceset="GND" device=""/>
<part name="U$3" library="BCMI-supply" deviceset="+3.3V" device=""/>
<part name="C1" library="Arduino-rcl" deviceset="C" device="-0402" technology="-100NF" value="100n"/>
<part name="GND10" library="supply1" deviceset="GND" device=""/>
<part name="IC1" library="ATSAMD11D14A-MNT" deviceset="ATSAMD11D14A-MNT" device=""/>
<part name="CN1" library="Arduino-connectors" deviceset="JTAG_CONNECTOR" device="-ROUND" value="DNP"/>
<part name="GND14" library="supply1" deviceset="GND" device=""/>
<part name="U$2" library="BCMI-supply" deviceset="+3.3V" device=""/>
<part name="R3" library="Arduino-rcl" deviceset="R" device="-0402" technology="-10K" value="10k"/>
<part name="R9" library="Arduino-rcl" deviceset="R" device="-0402" technology="-3K3" value="3k3"/>
<part name="GND18" library="supply1" deviceset="GND" device=""/>
<part name="U$5" library="BCMI-supply" deviceset="+VM" device=""/>
<part name="GND19" library="supply1" deviceset="GND" device=""/>
<part name="GND20" library="supply1" deviceset="GND" device=""/>
<part name="U$8" library="BCMI-supply" deviceset="+3.3V" device=""/>
<part name="U$11" library="BCMI-supply" deviceset="+3.3V" device=""/>
<part name="DL9" library="Arduino-diodes" deviceset="LED" device="-0603" technology="-ORANGE" value="ORANGE"/>
<part name="GND21" library="supply1" deviceset="GND" device=""/>
<part name="U$15" library="BCMI-supply" deviceset="+3.3V" device=""/>
<part name="GND33" library="supply1" deviceset="GND" device=""/>
<part name="SJ5" library="jumper" deviceset="SJ" device=""/>
<part name="R11" library="Arduino-rcl" deviceset="R" device="-0402" technology="-1K" value="1k"/>
<part name="R12" library="Arduino-rcl" deviceset="R" device="-0402" technology="-10K" value="10k"/>
<part name="GND34" library="supply1" deviceset="GND" device=""/>
<part name="U$9" library="BCMI-supply" deviceset="+3.3V" device=""/>
<part name="U$16" library="BCMI-supply" deviceset="+VM" device=""/>
<part name="U$17" library="BCMI-supply" deviceset="+VM" device=""/>
<part name="X6" library="BCMI-connectors" deviceset="OSTTE/4" device=""/>
<part name="SUPPLY10" library="supply2" deviceset="+5V" device=""/>
<part name="GND42" library="supply1" deviceset="GND" device=""/>
<part name="SUPPLY11" library="supply2" deviceset="+5V" device=""/>
<part name="X8" library="BCMI-connectors" deviceset="OSTTE/4" device=""/>
<part name="DL1" library="Arduino-diodes" deviceset="LED" device="-0603" technology="-YELLOW" value="YELLOW"/>
<part name="DL2" library="Arduino-diodes" deviceset="LED" device="-0603" technology="-YELLOW" value="YELLOW"/>
<part name="DL3" library="Arduino-diodes" deviceset="LED" device="-0603" technology="-YELLOW" value="YELLOW"/>
<part name="DL4" library="Arduino-diodes" deviceset="LED" device="-0603" technology="-YELLOW" value="YELLOW"/>
<part name="DL5" library="Arduino-diodes" deviceset="LED" device="-0603" technology="-YELLOW" value="YELLOW"/>
<part name="DL6" library="Arduino-diodes" deviceset="LED" device="-0603" technology="-YELLOW" value="YELLOW"/>
<part name="DL7" library="Arduino-diodes" deviceset="LED" device="-0603" technology="-YELLOW" value="YELLOW"/>
<part name="DL8" library="Arduino-diodes" deviceset="LED" device="-0603" technology="-YELLOW" value="YELLOW"/>
<part name="R18" library="Arduino-rcl" deviceset="R" device="-0402" technology="-DNP" value="DNP"/>
<part name="R19" library="Arduino-rcl" deviceset="R" device="-0402" technology="-1K" value="1k"/>
<part name="R20" library="Arduino-rcl" deviceset="R" device="-0402" technology="-1K" value="1k"/>
<part name="R21" library="Arduino-rcl" deviceset="R" device="-0402" technology="-0R" value="0R"/>
<part name="C7" library="Arduino-rcl" deviceset="C" device="-0402" technology="-100NF" value="100n"/>
<part name="GND46" library="supply1" deviceset="GND" device=""/>
<part name="C4" library="Arduino-rcl" deviceset="C" device="-0402" technology="-47N" value="47n"/>
<part name="C6" library="Arduino-rcl" deviceset="C" device="-0402" technology="-47N" value="47n"/>
<part name="C9" library="Arduino-rcl" deviceset="C" device="-0402" technology="-100NF" value="100n"/>
<part name="GND53" library="supply1" deviceset="GND" device=""/>
<part name="GND54" library="supply1" deviceset="GND" device=""/>
<part name="GND56" library="supply1" deviceset="GND" device=""/>
<part name="C10" library="Arduino-rcl" deviceset="C" device="-0402" technology="-1UF" value="1u"/>
<part name="C11" library="Arduino-rcl" deviceset="C" device="-0402" technology="-1UF" value="1u"/>
<part name="R22" library="Arduino-rcl" deviceset="R" device="-0402" technology="-270R" value="270R"/>
<part name="R23" library="Arduino-rcl" deviceset="R" device="-0402" technology="-270R" value="270R"/>
<part name="X1" library="BCMI-connectors" deviceset="OSTTE/4" device=""/>
<part name="X2" library="BCMI-connectors" deviceset="OSTTE/4" device=""/>
<part name="X3" library="BCMI-connectors" deviceset="OSTTE/4" device=""/>
<part name="U$20" library="BCMI-supply" deviceset="+3.3V" device=""/>
<part name="U$21" library="BCMI-supply" deviceset="+3.3V" device=""/>
<part name="U3" library="MC33926PNB-newSymbol" deviceset="MC33926PNB" device=""/>
<part name="U5" library="MC33926PNB-newSymbol" deviceset="MC33926PNB" device=""/>
<part name="R8" library="Arduino-rcl" deviceset="R" device="-0402" technology="-DNP" value="DNP"/>
<part name="R13" library="Arduino-rcl" deviceset="R" device="-0402" technology="-DNP" value="DNP"/>
<part name="R15" library="Arduino-rcl" deviceset="R" device="-0402" technology="-DNP" value="DNP"/>
<part name="R16" library="Arduino-rcl" deviceset="R" device="-0402" technology="-DNP" value="DNP"/>
<part name="SUPPLY7" library="supply2" deviceset="+5V" device=""/>
<part name="SUPPLY8" library="supply2" deviceset="+5V" device=""/>
<part name="SUPPLY12" library="supply2" deviceset="+5V" device=""/>
<part name="GND35" library="supply1" deviceset="GND" device=""/>
<part name="J1" library="Arduino-connectors" deviceset="GROVE-CONNECTOR" device="" technology="-DNP" value="DNP"/>
<part name="J2" library="Arduino-connectors" deviceset="GROVE-CONNECTOR" device="" technology="-DNP" value="DNP"/>
<part name="J3" library="Arduino-connectors" deviceset="GROVE-CONNECTOR" device="" technology="-DNP" value="DNP"/>
<part name="J4" library="Arduino-connectors" deviceset="GROVE-CONNECTOR" device="" technology="-DNP" value="DNP"/>
<part name="R24" library="Arduino-rcl" deviceset="R" device="-0402" technology="-330R" value="330R"/>
<part name="DL10" library="Arduino-diodes" deviceset="LED" device="-0603" technology="-ORANGE" value="ORANGE"/>
<part name="R1" library="Arduino-rcl" deviceset="R" device="-0402" technology="-330R" value="330R"/>
<part name="SERVO1" library="Arduino-connectors" deviceset="MODULINO-3" device="SM" technology="-DNP" value="DNP"/>
<part name="SERVO3" library="Arduino-connectors" deviceset="MODULINO-3" device="SM" technology="-DNP" value="DNP"/>
<part name="SERVO2" library="Arduino-connectors" deviceset="MODULINO-3" device="SM" technology="-DNP" value="DNP"/>
<part name="SERVO4" library="Arduino-connectors" deviceset="MODULINO-3" device="SM" technology="-DNP" value="DNP"/>
<part name="IN2" library="Arduino-connectors" deviceset="MODULINO-3" device="SM" technology="-DNP" value="DNP"/>
<part name="IN4" library="Arduino-connectors" deviceset="MODULINO-3" device="SM" technology="-DNP" value="DNP"/>
<part name="IN1" library="Arduino-connectors" deviceset="MODULINO-3" device="SM" technology="-DNP" value="DNP"/>
<part name="IN3" library="Arduino-connectors" deviceset="MODULINO-3" device="SM" technology="-DNP" value="DNP"/>
<part name="GND57" library="supply1" deviceset="GND" device=""/>
<part name="SUPPLY14" library="supply2" deviceset="+5V" device=""/>
<part name="GND58" library="supply1" deviceset="GND" device=""/>
<part name="GND60" library="supply1" deviceset="GND" device=""/>
<part name="GND62" library="supply1" deviceset="GND" device=""/>
<part name="SUPPLY15" library="supply2" deviceset="+5V" device=""/>
<part name="SUPPLY16" library="supply2" deviceset="+5V" device=""/>
<part name="SUPPLY17" library="supply2" deviceset="+5V" device=""/>
<part name="C12" library="Arduino-rcl" deviceset="C" device="-0402" technology="-100NF" value="100n"/>
<part name="C18" library="Arduino-rcl" deviceset="C" device="-0402" technology="-100NF" value="100n"/>
<part name="GND63" library="supply1" deviceset="GND" device=""/>
<part name="GND65" library="supply1" deviceset="GND" device=""/>
<part name="C21" library="Arduino-rcl" deviceset="C" device="-0402" technology="-100NF" value="100n"/>
<part name="GND66" library="supply1" deviceset="GND" device=""/>
<part name="U$18" library="BCMI-supply" deviceset="+3.3V" device=""/>
<part name="TP1" library="Arduino-utility" deviceset="TP" device="TP-1.00MM" value="DNP"/>
<part name="TP2" library="Arduino-utility" deviceset="TP" device="TP-1.00MM" value="DNP"/>
<part name="TP3" library="Arduino-utility" deviceset="TP" device="TP-1.00MM" value="DNP"/>
<part name="TP4" library="Arduino-utility" deviceset="TP" device="TP-1.00MM" value="DNP"/>
<part name="TP5" library="Arduino-utility" deviceset="TP" device="TP-1.00MM" value="DNP"/>
<part name="U$19" library="BCMI-supply" deviceset="+3.3V" device=""/>
<part name="R26" library="Arduino-rcl" deviceset="R" device="-0402" technology="-10K" value="10k"/>
<part name="C22" library="Arduino-rcl" deviceset="C" device="-0402" technology="-100NF" value="100n"/>
<part name="GND36" library="supply1" deviceset="GND" device=""/>
<part name="R25" library="Arduino-rcl" deviceset="R" device="-0402" technology="-1K" value="1k"/>
<part name="R2" library="Arduino-rcl" deviceset="R" device="-0402" technology="-39R" value="39R"/>
<part name="FRAME1" library="Arduino-utility" deviceset="A3-FRAME" device="-NO-BAN" value="DNP"/>
<part name="FRAME2" library="Arduino-utility" deviceset="A3-FRAME" device="-STANDARD" value="DNP"/>
</parts>
<sheets>
<sheet>
<plain>
<text x="318.77" y="-5.08" size="1.778" layer="94" rot="MR180">A.Guadalupi / E.Lopez</text>
<text x="261.62" y="180.34" size="1.778" layer="91">M3</text>
<text x="246.38" y="238.76" size="1.778" layer="91">M1</text>
<text x="251.46" y="66.04" size="1.778" layer="91">M4</text>
<text x="248.92" y="35.56" size="1.778" layer="91">M2</text>
</plain>
<instances>
<instance part="GND17" gate="1" x="307.34" y="111.76"/>
<instance part="SUPPLY2" gate="+5V" x="307.34" y="129.54"/>
<instance part="GND11" gate="1" x="279.4" y="101.6" rot="R270"/>
<instance part="GND15" gate="1" x="279.4" y="111.76" rot="R270"/>
<instance part="GND16" gate="1" x="279.4" y="132.08" rot="R270"/>
<instance part="GND24" gate="1" x="279.4" y="121.92" rot="R270"/>
<instance part="SUPPLY1" gate="+5V" x="284.48" y="124.46" rot="R90"/>
<instance part="SUPPLY3" gate="+5V" x="284.48" y="134.62" rot="R90"/>
<instance part="SUPPLY4" gate="+5V" x="284.48" y="114.3" rot="R90"/>
<instance part="SUPPLY5" gate="+5V" x="284.48" y="104.14" rot="R90"/>
<instance part="GND12" gate="1" x="193.04" y="228.6" rot="R90"/>
<instance part="GND9" gate="1" x="172.72" y="55.88" rot="R90"/>
<instance part="GND32" gate="1" x="170.18" y="20.32" rot="R90"/>
<instance part="U$1" gate="G$1" x="149.86" y="218.44"/>
<instance part="TWI" gate="G$1" x="325.12" y="157.48" smashed="yes" rot="R270">
<attribute name="NAME" x="327.66" y="163.83" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="GND8" gate="1" x="299.72" y="149.86"/>
<instance part="U$14" gate="G$1" x="198.12" y="231.14" rot="R270"/>
<instance part="U$28" gate="G$1" x="299.72" y="157.48"/>
<instance part="U$33" gate="G$1" x="40.64" y="144.78"/>
<instance part="U$34" gate="G$1" x="48.26" y="144.78"/>
<instance part="U$35" gate="G$1" x="55.88" y="144.78"/>
<instance part="GND28" gate="1" x="193.04" y="33.02" rot="R180"/>
<instance part="GND31" gate="1" x="193.04" y="15.24"/>
<instance part="GND37" gate="1" x="193.04" y="60.96" rot="R180"/>
<instance part="GND38" gate="1" x="193.04" y="43.18"/>
<instance part="U1" gate="G$1" x="241.3" y="121.92"/>
<instance part="GND39" gate="1" x="241.3" y="96.52"/>
<instance part="U$36" gate="G$1" x="238.76" y="152.4"/>
<instance part="C14" gate="G$1" x="193.04" y="48.26" smashed="yes" rot="R180">
<attribute name="NAME" x="191.516" y="51.689" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="191.516" y="46.609" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C15" gate="G$1" x="193.04" y="55.88" smashed="yes" rot="R180">
<attribute name="NAME" x="191.516" y="54.229" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="191.516" y="59.309" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C16" gate="G$1" x="193.04" y="20.32" rot="R180"/>
<instance part="C17" gate="G$1" x="193.04" y="27.94" rot="R180"/>
<instance part="R4" gate="G$1" x="182.88" y="50.8" smashed="yes" rot="R180">
<attribute name="NAME" x="182.88" y="49.53" size="1.778" layer="95" rot="R180" align="bottom-center"/>
<attribute name="VALUE" x="177.8" y="49.53" size="1.778" layer="96" rot="R180" align="top-center"/>
</instance>
<instance part="R5" gate="G$1" x="182.88" y="53.34" smashed="yes" rot="R180">
<attribute name="NAME" x="182.88" y="54.61" size="1.778" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="177.8" y="52.07" size="1.778" layer="96" rot="R180" align="top-center"/>
</instance>
<instance part="R6" gate="G$1" x="182.88" y="22.86" smashed="yes" rot="R180">
<attribute name="NAME" x="182.88" y="21.59" size="1.778" layer="95" rot="R180" align="bottom-center"/>
<attribute name="VALUE" x="177.8" y="21.59" size="1.778" layer="96" rot="R180" align="top-center"/>
</instance>
<instance part="R7" gate="G$1" x="182.88" y="25.4" smashed="yes" rot="R180">
<attribute name="NAME" x="182.88" y="26.67" size="1.778" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="177.8" y="24.13" size="1.778" layer="96" rot="R180" align="top-center"/>
</instance>
<instance part="C19" gate="G$1" x="256.54" y="142.24"/>
<instance part="GND40" gate="1" x="256.54" y="134.62"/>
<instance part="GND41" gate="1" x="220.98" y="134.62"/>
<instance part="C20" gate="G$1" x="220.98" y="142.24" rot="R180"/>
<instance part="R10" gate="G$1" x="248.92" y="152.4"/>
<instance part="U$22" gate="G$1" x="279.4" y="220.98" rot="R270"/>
<instance part="GND44" gate="1" x="320.04" y="101.6" rot="R270"/>
<instance part="GND45" gate="1" x="320.04" y="76.2" rot="MR90"/>
<instance part="GND47" gate="1" x="358.14" y="241.3"/>
<instance part="GND48" gate="1" x="347.98" y="22.86"/>
<instance part="RN4" gate="A" x="347.98" y="246.38" smashed="yes" rot="R180"/>
<instance part="RN4" gate="B" x="347.98" y="243.84" smashed="yes" rot="R180"/>
<instance part="RN4" gate="C" x="347.98" y="241.3" smashed="yes" rot="R180"/>
<instance part="RN4" gate="D" x="347.98" y="238.76" smashed="yes" rot="R180">
<attribute name="VALUE" x="347.98" y="249.428" size="1.778" layer="96" rot="R180"/>
<attribute name="NAME" x="345.44" y="235.712" size="1.778" layer="95"/>
</instance>
<instance part="RN2" gate="A" x="342.9" y="33.02" smashed="yes" rot="R180"/>
<instance part="RN2" gate="B" x="342.9" y="30.48" smashed="yes" rot="R180"/>
<instance part="RN2" gate="C" x="342.9" y="27.94" smashed="yes" rot="R180"/>
<instance part="RN2" gate="D" x="342.9" y="25.4" smashed="yes" rot="R180">
<attribute name="VALUE" x="340.36" y="35.052" size="1.778" layer="96"/>
<attribute name="NAME" x="340.36" y="37.592" size="1.778" layer="95"/>
</instance>
<instance part="C5" gate="G$1" x="307.34" y="119.38"/>
<instance part="C13" gate="G$1" x="317.5" y="119.38"/>
<instance part="C25" gate="G$1" x="327.66" y="119.38"/>
<instance part="C31" gate="G$1" x="337.82" y="119.38"/>
<instance part="U4" gate="G$1" x="241.3" y="231.14"/>
<instance part="U7" gate="G$1" x="251.46" y="33.02"/>
<instance part="GND4" gate="1" x="241.3" y="210.82"/>
<instance part="GND5" gate="1" x="215.9" y="238.76"/>
<instance part="C26" gate="G$1" x="264.16" y="215.9"/>
<instance part="GND55" gate="1" x="259.08" y="231.14" rot="R90"/>
<instance part="GND1" gate="1" x="231.14" y="38.1" rot="R270"/>
<instance part="GND7" gate="1" x="251.46" y="15.24"/>
<instance part="GND59" gate="1" x="271.78" y="33.02" rot="R90"/>
<instance part="U$10" gate="G$1" x="276.86" y="22.86"/>
<instance part="C8" gate="G$1" x="269.24" y="20.32"/>
<instance part="GND23" gate="1" x="271.78" y="15.24"/>
<instance part="GND61" gate="1" x="231.14" y="10.16"/>
<instance part="R14" gate="G$1" x="231.14" y="15.24" rot="R90"/>
<instance part="R17" gate="G$1" x="223.52" y="215.9" rot="R90"/>
<instance part="GND22" gate="1" x="223.52" y="210.82"/>
<instance part="GND64" gate="1" x="269.24" y="208.28"/>
<instance part="C2" gate="G$1" x="271.78" y="218.44"/>
<instance part="C3" gate="G$1" x="276.86" y="20.32"/>
<instance part="GND3" gate="1" x="284.48" y="162.56"/>
<instance part="GND6" gate="1" x="71.12" y="200.66" rot="R90"/>
<instance part="U$3" gate="G$1" x="68.58" y="213.36"/>
<instance part="C1" gate="G$1" x="68.58" y="208.28"/>
<instance part="GND10" gate="1" x="279.4" y="50.8"/>
<instance part="IC1" gate="G$1" x="40.64" y="198.12" rot="R270"/>
<instance part="CN1" gate="G$1" x="76.2" y="172.72" smashed="yes" rot="MR0">
<attribute name="NAME" x="73.66" y="165.481" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="GND14" gate="1" x="83.82" y="162.56"/>
<instance part="U$2" gate="G$1" x="83.82" y="185.42"/>
<instance part="R3" gate="G$1" x="48.26" y="228.6" rot="R180"/>
<instance part="R9" gate="G$1" x="58.42" y="228.6" rot="R180"/>
<instance part="GND18" gate="1" x="63.5" y="226.06"/>
<instance part="U$5" gate="G$1" x="45.72" y="228.6" rot="R90"/>
<instance part="U$8" gate="G$1" x="228.6" y="165.1" rot="R90"/>
<instance part="U$11" gate="G$1" x="259.08" y="200.66" rot="R270"/>
<instance part="DL9" gate="G$1" x="246.38" y="200.66" rot="R270"/>
<instance part="GND21" gate="1" x="231.14" y="170.18" rot="R270"/>
<instance part="U$15" gate="G$1" x="218.44" y="53.34" rot="R90"/>
<instance part="GND33" gate="1" x="223.52" y="58.42" rot="R270"/>
<instance part="SJ5" gate="1" x="124.46" y="213.36" smashed="yes" rot="R180">
<attribute name="NAME" x="127" y="213.36" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="127" y="217.17" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R11" gate="G$1" x="254" y="200.66" rot="R180"/>
<instance part="R12" gate="G$1" x="193.04" y="91.44" rot="R90"/>
<instance part="GND34" gate="1" x="193.04" y="76.2"/>
<instance part="U$9" gate="G$1" x="193.04" y="96.52"/>
<instance part="U$16" gate="G$1" x="289.56" y="190.5"/>
<instance part="U$17" gate="G$1" x="284.48" y="78.74"/>
<instance part="X6" gate="-1" x="317.5" y="226.06" rot="R180"/>
<instance part="X6" gate="-2" x="317.5" y="228.6" rot="R180"/>
<instance part="X6" gate="-3" x="317.5" y="231.14" rot="R180"/>
<instance part="X6" gate="-4" x="317.5" y="233.68" rot="R180"/>
<instance part="SUPPLY10" gate="+5V" x="320.04" y="68.58" rot="R90"/>
<instance part="GND42" gate="1" x="317.5" y="88.9" rot="R270"/>
<instance part="SUPPLY11" gate="+5V" x="314.96" y="93.98" rot="R90"/>
<instance part="DL1" gate="G$1" x="312.42" y="246.38" smashed="yes" rot="R90">
<attribute name="NAME" x="316.992" y="249.936" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="316.992" y="252.095" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="DL2" gate="G$1" x="320.04" y="243.84" smashed="yes" rot="R90">
<attribute name="NAME" x="324.612" y="247.396" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="DL3" gate="G$1" x="327.66" y="241.3" smashed="yes" rot="R90">
<attribute name="NAME" x="332.232" y="244.856" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="DL4" gate="G$1" x="335.28" y="238.76" smashed="yes" rot="R90">
<attribute name="NAME" x="339.852" y="242.316" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="DL5" gate="G$1" x="327.66" y="33.02" smashed="yes" rot="R90">
<attribute name="NAME" x="332.232" y="36.576" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="332.232" y="38.735" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="DL6" gate="G$1" x="322.58" y="30.48" smashed="yes" rot="R90">
<attribute name="NAME" x="327.152" y="34.036" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="DL7" gate="G$1" x="317.5" y="27.94" smashed="yes" rot="R90">
<attribute name="NAME" x="322.072" y="31.496" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="DL8" gate="G$1" x="314.96" y="25.4" smashed="yes" rot="R90">
<attribute name="NAME" x="319.532" y="28.956" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="R18" gate="G$1" x="193.04" y="81.28" rot="R90"/>
<instance part="R19" gate="G$1" x="228.6" y="55.88"/>
<instance part="R20" gate="G$1" x="238.76" y="167.64"/>
<instance part="R21" gate="G$1" x="172.72" y="203.2" smashed="yes">
<attribute name="NAME" x="167.64" y="204.47" size="1.778" layer="95" rot="R180" align="bottom-center"/>
<attribute name="VALUE" x="177.8" y="204.47" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="C7" gate="G$1" x="297.18" y="187.96"/>
<instance part="GND46" gate="1" x="297.18" y="182.88"/>
<instance part="C4" gate="G$1" x="284.48" y="76.2"/>
<instance part="C6" gate="G$1" x="289.56" y="187.96"/>
<instance part="C9" gate="G$1" x="292.1" y="76.2"/>
<instance part="GND53" gate="1" x="292.1" y="71.12"/>
<instance part="GND54" gate="1" x="228.6" y="185.42"/>
<instance part="GND56" gate="1" x="228.6" y="73.66"/>
<instance part="C10" gate="G$1" x="226.06" y="193.04" rot="R180"/>
<instance part="C11" gate="G$1" x="226.06" y="81.28" rot="R180"/>
<instance part="R22" gate="G$1" x="231.14" y="81.28" rot="R90"/>
<instance part="R23" gate="G$1" x="233.68" y="193.04" rot="R90"/>
<instance part="X1" gate="-1" x="309.88" y="45.72" smashed="yes" rot="R180">
<attribute name="NAME" x="313.055" y="44.958" size="1.524" layer="95"/>
<attribute name="VALUE" x="308.61" y="46.99" size="1.778" layer="96"/>
</instance>
<instance part="X1" gate="-2" x="309.88" y="43.18" rot="R180"/>
<instance part="X1" gate="-3" x="309.88" y="40.64" rot="R180"/>
<instance part="X1" gate="-4" x="309.88" y="38.1" rot="R180"/>
<instance part="X2" gate="-1" x="162.56" y="55.88"/>
<instance part="X2" gate="-2" x="162.56" y="53.34"/>
<instance part="X2" gate="-3" x="162.56" y="50.8"/>
<instance part="X2" gate="-4" x="162.56" y="48.26"/>
<instance part="X3" gate="-1" x="162.56" y="27.94"/>
<instance part="X3" gate="-2" x="162.56" y="25.4"/>
<instance part="X3" gate="-3" x="162.56" y="22.86"/>
<instance part="X3" gate="-4" x="162.56" y="20.32"/>
<instance part="U$20" gate="G$1" x="172.72" y="48.26" rot="R270"/>
<instance part="U$21" gate="G$1" x="170.18" y="27.94" rot="R270"/>
<instance part="U3" gate="G$1" x="264.16" y="177.8"/>
<instance part="U5" gate="G$1" x="259.08" y="66.04"/>
<instance part="R8" gate="G$1" x="172.72" y="213.36" smashed="yes" rot="R180">
<attribute name="NAME" x="167.64" y="214.63" size="1.778" layer="95" rot="R180" align="bottom-center"/>
<attribute name="VALUE" x="177.8" y="214.63" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="R13" gate="G$1" x="172.72" y="210.82" smashed="yes" rot="R180">
<attribute name="NAME" x="167.64" y="212.09" size="1.778" layer="95" rot="R180" align="bottom-center"/>
<attribute name="VALUE" x="177.8" y="212.09" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="R15" gate="G$1" x="172.72" y="208.28" smashed="yes" rot="R180">
<attribute name="NAME" x="167.64" y="209.55" size="1.778" layer="95" rot="R180" align="bottom-center"/>
<attribute name="VALUE" x="177.8" y="209.55" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="R16" gate="G$1" x="172.72" y="205.74" smashed="yes" rot="R180">
<attribute name="NAME" x="167.64" y="207.01" size="1.778" layer="95" rot="R180" align="bottom-center"/>
<attribute name="VALUE" x="177.8" y="207.01" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="SUPPLY7" gate="+5V" x="320.04" y="106.68" rot="R90"/>
<instance part="SUPPLY8" gate="+5V" x="320.04" y="81.28" rot="R90"/>
<instance part="SUPPLY12" gate="+5V" x="256.54" y="152.4" rot="R270"/>
<instance part="GND35" gate="1" x="320.04" y="63.5" rot="R270"/>
<instance part="J1" gate="G$1" x="345.44" y="66.04"/>
<instance part="J2" gate="G$1" x="345.44" y="78.74"/>
<instance part="J3" gate="G$1" x="345.44" y="91.44"/>
<instance part="J4" gate="G$1" x="345.44" y="104.14"/>
<instance part="R24" gate="G$1" x="76.2" y="233.68" rot="R180"/>
<instance part="DL10" gate="G$1" x="68.58" y="233.68" rot="R270"/>
<instance part="SERVO1" gate="G$1" x="292.1" y="114.3" rot="R270"/>
<instance part="SERVO3" gate="G$1" x="292.1" y="104.14" rot="R270"/>
<instance part="SERVO2" gate="G$1" x="292.1" y="134.62" rot="R270"/>
<instance part="SERVO4" gate="G$1" x="292.1" y="124.46" rot="R270"/>
<instance part="IN2" gate="G$1" x="325.12" y="104.14" smashed="yes" rot="R270">
<attribute name="NAME" x="322.58" y="107.95" size="1.778" layer="95"/>
</instance>
<instance part="IN4" gate="G$1" x="325.12" y="91.44" smashed="yes" rot="R270">
<attribute name="NAME" x="322.58" y="95.25" size="1.778" layer="95"/>
</instance>
<instance part="IN1" gate="G$1" x="325.12" y="78.74" smashed="yes" rot="R270">
<attribute name="NAME" x="322.58" y="82.55" size="1.778" layer="95"/>
</instance>
<instance part="IN3" gate="G$1" x="325.12" y="66.04" smashed="yes" rot="R270">
<attribute name="NAME" x="322.58" y="69.85" size="1.778" layer="95"/>
</instance>
<instance part="GND57" gate="1" x="339.09" y="62.23" rot="R270"/>
<instance part="SUPPLY14" gate="+5V" x="336.55" y="64.77" rot="R90"/>
<instance part="GND58" gate="1" x="339.09" y="74.93" rot="R270"/>
<instance part="GND60" gate="1" x="339.09" y="87.63" rot="R270"/>
<instance part="GND62" gate="1" x="339.09" y="100.33" rot="R270"/>
<instance part="SUPPLY15" gate="+5V" x="336.55" y="77.47" rot="R90"/>
<instance part="SUPPLY16" gate="+5V" x="336.55" y="90.17" rot="R90"/>
<instance part="SUPPLY17" gate="+5V" x="336.55" y="102.87" rot="R90"/>
<instance part="C12" gate="G$1" x="302.26" y="187.96"/>
<instance part="C18" gate="G$1" x="297.18" y="76.2"/>
<instance part="GND63" gate="1" x="297.18" y="71.12"/>
<instance part="GND65" gate="1" x="302.26" y="182.88"/>
<instance part="C21" gate="G$1" x="53.34" y="223.52"/>
<instance part="GND66" gate="1" x="53.34" y="218.44"/>
<instance part="U$18" gate="G$1" x="81.28" y="233.68" rot="R270"/>
<instance part="TP1" gate="G$1" x="86.36" y="180.34" rot="R270"/>
<instance part="TP2" gate="G$1" x="86.36" y="170.18" rot="R270"/>
<instance part="TP3" gate="G$1" x="66.04" y="160.02" rot="R180"/>
<instance part="TP4" gate="G$1" x="63.5" y="175.26" rot="R90"/>
<instance part="TP5" gate="G$1" x="71.12" y="185.42" rot="R270"/>
<instance part="U$19" gate="G$1" x="55.88" y="172.72"/>
<instance part="R26" gate="G$1" x="55.88" y="167.64" rot="R270"/>
<instance part="C22" gate="G$1" x="50.8" y="167.64" rot="R180"/>
<instance part="GND36" gate="1" x="50.8" y="172.72" rot="R180"/>
<instance part="R25" gate="G$1" x="76.2" y="182.88" rot="R180"/>
<instance part="R2" gate="G$1" x="60.96" y="162.56"/>
<instance part="FRAME2" gate="G$1" x="0" y="-5.08"/>
</instances>
<busses>
<bus name="MOSI,MISO,SCK,SS,SDA,SCL,A1,A2,A4,A3,A6,A5,FB_M3,FB_M4,IN1_M3,IN1_M4,IN1_M1,IN1_M2,IN2_M1,IN2_M2,IN2_M3,IN2_M4,SERVO1,SERVO2,SERVO3,SERVO4,HA_E1,HA_E2,HB_E1,HB_E2,VIN,/SF,SLEW,SD11_INT,SWDIO/SS,SWCLK/MISO">
<segment>
<wire x1="5.08" y1="243.84" x2="5.08" y2="157.48" width="0.762" layer="92"/>
<wire x1="5.08" y1="157.48" x2="88.9" y2="157.48" width="0.762" layer="92"/>
<wire x1="88.9" y1="157.48" x2="88.9" y2="245.3279" width="0.762" layer="92"/>
<wire x1="88.9" y1="245.3279" x2="92.4921" y2="248.92" width="0.762" layer="92" curve="-90"/>
<wire x1="92.4921" y1="248.92" x2="207.2279" y2="248.92" width="0.762" layer="92"/>
<wire x1="207.2279" y1="248.92" x2="210.82" y2="245.3279" width="0.762" layer="92" curve="-90"/>
<wire x1="210.82" y1="245.3279" x2="210.82" y2="22.86" width="0.762" layer="92"/>
</segment>
</bus>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="GND17" gate="1" pin="GND"/>
<wire x1="307.34" y1="114.3" x2="307.34" y2="116.84" width="0.1524" layer="91"/>
<wire x1="317.5" y1="116.84" x2="317.5" y2="114.3" width="0.1524" layer="91"/>
<wire x1="317.5" y1="114.3" x2="307.34" y2="114.3" width="0.1524" layer="91"/>
<junction x="307.34" y="114.3"/>
<wire x1="327.66" y1="116.84" x2="327.66" y2="114.3" width="0.1524" layer="91"/>
<wire x1="327.66" y1="114.3" x2="317.5" y2="114.3" width="0.1524" layer="91"/>
<junction x="317.5" y="114.3"/>
<wire x1="327.66" y1="114.3" x2="337.82" y2="114.3" width="0.1524" layer="91"/>
<wire x1="337.82" y1="114.3" x2="337.82" y2="116.84" width="0.1524" layer="91"/>
<junction x="327.66" y="114.3"/>
<pinref part="C5" gate="G$1" pin="-"/>
<pinref part="C13" gate="G$1" pin="-"/>
<pinref part="C25" gate="G$1" pin="-"/>
<pinref part="C31" gate="G$1" pin="-"/>
</segment>
<segment>
<pinref part="TWI" gate="G$1" pin="4"/>
<wire x1="322.58" y1="152.4" x2="299.72" y2="152.4" width="0.1524" layer="91"/>
<pinref part="GND8" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="GND"/>
<pinref part="GND39" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND37" gate="1" pin="GND"/>
<pinref part="C15" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="GND31" gate="1" pin="GND"/>
<pinref part="C16" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="GND28" gate="1" pin="GND"/>
<pinref part="C17" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="C14" gate="G$1" pin="1"/>
<pinref part="GND38" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C19" gate="G$1" pin="2"/>
<pinref part="GND40" gate="1" pin="GND"/>
<wire x1="256.54" y1="137.16" x2="256.54" y2="139.7" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND41" gate="1" pin="GND"/>
<pinref part="C20" gate="G$1" pin="1"/>
<wire x1="220.98" y1="139.7" x2="220.98" y2="137.16" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="353.06" y1="238.76" x2="353.06" y2="241.3" width="0.1524" layer="91"/>
<wire x1="353.06" y1="241.3" x2="353.06" y2="243.84" width="0.1524" layer="91"/>
<pinref part="GND47" gate="1" pin="GND"/>
<wire x1="353.06" y1="243.84" x2="353.06" y2="246.38" width="0.1524" layer="91"/>
<wire x1="358.14" y1="243.84" x2="353.06" y2="243.84" width="0.1524" layer="91"/>
<junction x="353.06" y="243.84"/>
<pinref part="RN4" gate="A" pin="1"/>
<pinref part="RN4" gate="B" pin="1"/>
<junction x="353.06" y="243.84"/>
<pinref part="RN4" gate="C" pin="1"/>
<junction x="353.06" y="241.3"/>
<pinref part="RN4" gate="D" pin="1"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="GND"/>
<wire x1="226.06" y1="236.22" x2="226.06" y2="241.3" width="0.1524" layer="91"/>
<wire x1="226.06" y1="241.3" x2="215.9" y2="241.3" width="0.1524" layer="91"/>
<pinref part="GND5" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="PPAD"/>
<pinref part="GND4" gate="1" pin="GND"/>
<wire x1="241.3" y1="213.36" x2="241.3" y2="215.9" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="PGND"/>
<pinref part="GND55" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND1" gate="1" pin="GND"/>
<pinref part="U7" gate="G$1" pin="GND"/>
<wire x1="233.68" y1="38.1" x2="236.22" y2="38.1" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U7" gate="G$1" pin="PPAD"/>
<pinref part="GND7" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U7" gate="G$1" pin="PGND"/>
<pinref part="GND59" gate="1" pin="GND"/>
<wire x1="266.7" y1="33.02" x2="269.24" y2="33.02" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C8" gate="G$1" pin="2"/>
<wire x1="276.86" y1="17.78" x2="269.24" y2="17.78" width="0.1524" layer="91"/>
<pinref part="GND23" gate="1" pin="GND"/>
<junction x="269.24" y="17.78"/>
<pinref part="C3" gate="G$1" pin="-"/>
<wire x1="269.24" y1="17.78" x2="271.78" y2="17.78" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="347.98" y1="25.4" x2="347.98" y2="27.94" width="0.1524" layer="91"/>
<wire x1="347.98" y1="27.94" x2="347.98" y2="33.02" width="0.1524" layer="91"/>
<wire x1="347.98" y1="30.48" x2="347.98" y2="33.02" width="0.1524" layer="91"/>
<junction x="347.98" y="33.02"/>
<pinref part="RN2" gate="A" pin="1"/>
<pinref part="RN2" gate="B" pin="1"/>
<junction x="347.98" y="30.48"/>
<pinref part="RN2" gate="C" pin="1"/>
<junction x="347.98" y="27.94"/>
<pinref part="RN2" gate="D" pin="1"/>
<pinref part="GND48" gate="1" pin="GND"/>
<junction x="347.98" y="25.4"/>
</segment>
<segment>
<pinref part="GND61" gate="1" pin="GND"/>
<pinref part="R14" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="R17" gate="G$1" pin="1"/>
<pinref part="GND22" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND64" gate="1" pin="GND"/>
<wire x1="269.24" y1="210.82" x2="264.16" y2="210.82" width="0.1524" layer="91"/>
<pinref part="C26" gate="G$1" pin="2"/>
<wire x1="264.16" y1="210.82" x2="264.16" y2="213.36" width="0.1524" layer="91"/>
<wire x1="269.24" y1="210.82" x2="271.78" y2="210.82" width="0.1524" layer="91"/>
<junction x="269.24" y="210.82"/>
<pinref part="C2" gate="G$1" pin="-"/>
<wire x1="271.78" y1="210.82" x2="271.78" y2="215.9" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="284.48" y1="167.64" x2="284.48" y2="165.1" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="AGND"/>
<junction x="284.48" y="165.1"/>
<pinref part="U3" gate="G$1" pin="PGND"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="GND"/>
<pinref part="GND12" gate="1" pin="GND"/>
<wire x1="190.5" y1="228.6" x2="165.1" y2="228.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND10" gate="1" pin="GND"/>
<wire x1="279.4" y1="55.88" x2="279.4" y2="53.34" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="AGND"/>
<junction x="279.4" y="53.34"/>
<pinref part="U5" gate="G$1" pin="PGND"/>
</segment>
<segment>
<wire x1="68.58" y1="200.66" x2="68.58" y2="205.74" width="0.1524" layer="91"/>
<wire x1="68.58" y1="205.74" x2="63.5" y2="205.74" width="0.1524" layer="91"/>
<pinref part="GND6" gate="1" pin="GND"/>
<pinref part="C1" gate="G$1" pin="2"/>
<junction x="68.58" y="205.74"/>
<pinref part="IC1" gate="G$1" pin="GND(2)"/>
<pinref part="IC1" gate="G$1" pin="GND(1)"/>
<wire x1="63.5" y1="200.66" x2="68.58" y2="200.66" width="0.1524" layer="91"/>
<junction x="68.58" y="200.66"/>
</segment>
<segment>
<pinref part="CN1" gate="G$1" pin="9"/>
<wire x1="81.28" y1="167.64" x2="83.82" y2="167.64" width="0.1524" layer="91"/>
<pinref part="GND14" gate="1" pin="GND"/>
<wire x1="83.82" y1="167.64" x2="83.82" y2="165.1" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="5"/>
<wire x1="81.28" y1="172.72" x2="83.82" y2="172.72" width="0.1524" layer="91"/>
<wire x1="83.82" y1="172.72" x2="83.82" y2="170.18" width="0.1524" layer="91"/>
<junction x="83.82" y="167.64"/>
<pinref part="CN1" gate="G$1" pin="3"/>
<wire x1="83.82" y1="170.18" x2="83.82" y2="167.64" width="0.1524" layer="91"/>
<wire x1="81.28" y1="175.26" x2="83.82" y2="175.26" width="0.1524" layer="91"/>
<wire x1="83.82" y1="175.26" x2="83.82" y2="172.72" width="0.1524" layer="91"/>
<junction x="83.82" y="172.72"/>
<pinref part="TP2" gate="G$1" pin="P$1"/>
<junction x="83.82" y="170.18"/>
</segment>
<segment>
<pinref part="GND18" gate="1" pin="GND"/>
<pinref part="R9" gate="G$1" pin="1"/>
<wire x1="63.5" y1="228.6" x2="60.96" y2="228.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND21" gate="1" pin="GND"/>
<wire x1="233.68" y1="170.18" x2="241.3" y2="170.18" width="0.1524" layer="91"/>
<wire x1="241.3" y1="170.18" x2="243.84" y2="170.18" width="0.1524" layer="91"/>
<wire x1="243.84" y1="175.26" x2="241.3" y2="175.26" width="0.1524" layer="91"/>
<wire x1="241.3" y1="175.26" x2="241.3" y2="170.18" width="0.1524" layer="91"/>
<junction x="241.3" y="170.18"/>
<pinref part="U3" gate="G$1" pin="INV"/>
<pinref part="U3" gate="G$1" pin="D1"/>
</segment>
<segment>
<pinref part="GND33" gate="1" pin="GND"/>
<wire x1="226.06" y1="58.42" x2="231.14" y2="58.42" width="0.1524" layer="91"/>
<wire x1="231.14" y1="58.42" x2="238.76" y2="58.42" width="0.1524" layer="91"/>
<wire x1="238.76" y1="63.5" x2="231.14" y2="63.5" width="0.1524" layer="91"/>
<wire x1="231.14" y1="63.5" x2="231.14" y2="58.42" width="0.1524" layer="91"/>
<junction x="231.14" y="58.42"/>
<pinref part="U5" gate="G$1" pin="INV"/>
<pinref part="U5" gate="G$1" pin="D1"/>
</segment>
<segment>
<pinref part="R18" gate="G$1" pin="1"/>
<pinref part="GND34" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C7" gate="G$1" pin="2"/>
<pinref part="GND46" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND53" gate="1" pin="GND"/>
<pinref part="C9" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="C11" gate="G$1" pin="1"/>
<wire x1="226.06" y1="78.74" x2="226.06" y2="76.2" width="0.1524" layer="91"/>
<wire x1="226.06" y1="76.2" x2="228.6" y2="76.2" width="0.1524" layer="91"/>
<pinref part="GND56" gate="1" pin="GND"/>
<wire x1="228.6" y1="76.2" x2="231.14" y2="76.2" width="0.1524" layer="91"/>
<wire x1="231.14" y1="76.2" x2="231.14" y2="78.74" width="0.1524" layer="91"/>
<junction x="228.6" y="76.2"/>
<pinref part="R22" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="C10" gate="G$1" pin="1"/>
<wire x1="226.06" y1="190.5" x2="226.06" y2="187.96" width="0.1524" layer="91"/>
<pinref part="GND54" gate="1" pin="GND"/>
<wire x1="226.06" y1="187.96" x2="228.6" y2="187.96" width="0.1524" layer="91"/>
<wire x1="228.6" y1="187.96" x2="233.68" y2="187.96" width="0.1524" layer="91"/>
<junction x="228.6" y="187.96"/>
<wire x1="233.68" y1="187.96" x2="233.68" y2="190.5" width="0.1524" layer="91"/>
<pinref part="R23" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="GND9" gate="1" pin="GND"/>
<pinref part="X2" gate="-1" pin="1"/>
<wire x1="170.18" y1="55.88" x2="165.1" y2="55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND15" gate="1" pin="GND"/>
<pinref part="SERVO1" gate="G$1" pin="3"/>
<wire x1="281.94" y1="111.76" x2="289.56" y2="111.76" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND11" gate="1" pin="GND"/>
<pinref part="SERVO3" gate="G$1" pin="3"/>
<wire x1="281.94" y1="101.6" x2="289.56" y2="101.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND16" gate="1" pin="GND"/>
<pinref part="SERVO2" gate="G$1" pin="3"/>
<wire x1="281.94" y1="132.08" x2="289.56" y2="132.08" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND24" gate="1" pin="GND"/>
<pinref part="SERVO4" gate="G$1" pin="3"/>
<wire x1="281.94" y1="121.92" x2="289.56" y2="121.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IN2" gate="G$1" pin="3"/>
<pinref part="GND44" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND42" gate="1" pin="GND"/>
<pinref part="IN4" gate="G$1" pin="3"/>
<wire x1="320.04" y1="88.9" x2="322.58" y2="88.9" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IN1" gate="G$1" pin="3"/>
<pinref part="GND45" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="IN3" gate="G$1" pin="3"/>
<pinref part="GND35" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="4"/>
<pinref part="GND57" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="4"/>
<pinref part="GND58" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="J3" gate="G$1" pin="4"/>
<pinref part="GND60" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="J4" gate="G$1" pin="4"/>
<pinref part="GND62" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C18" gate="G$1" pin="2"/>
<pinref part="GND63" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C12" gate="G$1" pin="2"/>
<pinref part="GND65" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C21" gate="G$1" pin="2"/>
<pinref part="GND66" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="X3" gate="-4" pin="1"/>
<pinref part="GND32" gate="1" pin="GND"/>
<wire x1="167.64" y1="20.32" x2="165.1" y2="20.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C22" gate="G$1" pin="2"/>
<pinref part="GND36" gate="1" pin="GND"/>
</segment>
</net>
<net name="M1+" class="0">
<segment>
<wire x1="309.88" y1="246.38" x2="304.8" y2="246.38" width="0.1524" layer="91"/>
<wire x1="314.96" y1="226.06" x2="304.8" y2="226.06" width="0.1524" layer="91"/>
<wire x1="304.8" y1="246.38" x2="304.8" y2="226.06" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="OUT1"/>
<wire x1="256.54" y1="226.06" x2="304.8" y2="226.06" width="0.1524" layer="91"/>
<junction x="304.8" y="226.06"/>
<label x="279.4" y="226.06" size="1.778" layer="95"/>
<pinref part="DL1" gate="G$1" pin="A"/>
<pinref part="X6" gate="-1" pin="1"/>
</segment>
</net>
<net name="M1-" class="0">
<segment>
<wire x1="307.34" y1="228.6" x2="314.96" y2="228.6" width="0.1524" layer="91"/>
<wire x1="317.5" y1="243.84" x2="307.34" y2="243.84" width="0.1524" layer="91"/>
<wire x1="307.34" y1="243.84" x2="307.34" y2="228.6" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="OUT2"/>
<wire x1="256.54" y1="236.22" x2="271.78" y2="236.22" width="0.1524" layer="91"/>
<wire x1="271.78" y1="236.22" x2="271.78" y2="228.6" width="0.1524" layer="91"/>
<wire x1="271.78" y1="228.6" x2="307.34" y2="228.6" width="0.1524" layer="91"/>
<junction x="307.34" y="228.6"/>
<label x="279.4" y="228.6" size="1.778" layer="95"/>
<pinref part="DL2" gate="G$1" pin="A"/>
<pinref part="X6" gate="-2" pin="1"/>
</segment>
</net>
<net name="M3-" class="0">
<segment>
<wire x1="309.88" y1="231.14" x2="314.96" y2="231.14" width="0.1524" layer="91"/>
<wire x1="325.12" y1="241.3" x2="309.88" y2="241.3" width="0.1524" layer="91"/>
<wire x1="309.88" y1="241.3" x2="309.88" y2="231.14" width="0.1524" layer="91"/>
<label x="309.88" y="231.14" size="1.778" layer="95"/>
<wire x1="284.48" y1="180.34" x2="309.88" y2="180.34" width="0.1524" layer="91"/>
<wire x1="309.88" y1="180.34" x2="309.88" y2="231.14" width="0.1524" layer="91"/>
<junction x="309.88" y="231.14"/>
<pinref part="DL3" gate="G$1" pin="A"/>
<pinref part="X6" gate="-3" pin="1"/>
<pinref part="U3" gate="G$1" pin="OUT1"/>
</segment>
</net>
<net name="M4+" class="0">
<segment>
<label x="287.02" y="45.72" size="1.778" layer="95"/>
<wire x1="304.8" y1="45.72" x2="287.02" y2="45.72" width="0.1524" layer="91"/>
<wire x1="325.12" y1="33.02" x2="304.8" y2="33.02" width="0.1524" layer="91"/>
<wire x1="304.8" y1="33.02" x2="304.8" y2="45.72" width="0.1524" layer="91"/>
<wire x1="279.4" y1="68.58" x2="287.02" y2="68.58" width="0.1524" layer="91"/>
<wire x1="287.02" y1="68.58" x2="287.02" y2="45.72" width="0.1524" layer="91"/>
<pinref part="DL5" gate="G$1" pin="A"/>
<pinref part="X1" gate="-1" pin="1"/>
<wire x1="304.8" y1="45.72" x2="307.34" y2="45.72" width="0.1524" layer="91"/>
<junction x="304.8" y="45.72"/>
<pinref part="U5" gate="G$1" pin="OUT1"/>
</segment>
</net>
<net name="M4-" class="0">
<segment>
<label x="287.02" y="43.18" size="1.778" layer="95"/>
<wire x1="320.04" y1="30.48" x2="302.26" y2="30.48" width="0.1524" layer="91"/>
<wire x1="302.26" y1="30.48" x2="302.26" y2="43.18" width="0.1524" layer="91"/>
<wire x1="302.26" y1="43.18" x2="284.48" y2="43.18" width="0.1524" layer="91"/>
<wire x1="279.4" y1="66.04" x2="284.48" y2="66.04" width="0.1524" layer="91"/>
<wire x1="284.48" y1="66.04" x2="284.48" y2="43.18" width="0.1524" layer="91"/>
<pinref part="DL6" gate="G$1" pin="A"/>
<pinref part="X1" gate="-2" pin="1"/>
<wire x1="302.26" y1="43.18" x2="307.34" y2="43.18" width="0.1524" layer="91"/>
<junction x="302.26" y="43.18"/>
<pinref part="U5" gate="G$1" pin="OUT2"/>
</segment>
</net>
<net name="M2-" class="0">
<segment>
<label x="287.02" y="40.64" size="1.778" layer="95"/>
<wire x1="314.96" y1="27.94" x2="299.72" y2="27.94" width="0.1524" layer="91"/>
<wire x1="299.72" y1="27.94" x2="299.72" y2="40.64" width="0.1524" layer="91"/>
<pinref part="U7" gate="G$1" pin="OUT1"/>
<wire x1="266.7" y1="27.94" x2="284.48" y2="27.94" width="0.1524" layer="91"/>
<wire x1="284.48" y1="27.94" x2="284.48" y2="40.64" width="0.1524" layer="91"/>
<wire x1="284.48" y1="40.64" x2="299.72" y2="40.64" width="0.1524" layer="91"/>
<pinref part="DL7" gate="G$1" pin="A"/>
<pinref part="X1" gate="-3" pin="1"/>
<wire x1="299.72" y1="40.64" x2="307.34" y2="40.64" width="0.1524" layer="91"/>
<junction x="299.72" y="40.64"/>
</segment>
</net>
<net name="M2+" class="0">
<segment>
<wire x1="297.18" y1="25.4" x2="297.18" y2="38.1" width="0.1524" layer="91"/>
<wire x1="312.42" y1="25.4" x2="297.18" y2="25.4" width="0.1524" layer="91"/>
<label x="287.02" y="38.1" size="1.778" layer="95"/>
<pinref part="U7" gate="G$1" pin="OUT2"/>
<wire x1="266.7" y1="38.1" x2="297.18" y2="38.1" width="0.1524" layer="91"/>
<pinref part="DL8" gate="G$1" pin="A"/>
<pinref part="X1" gate="-4" pin="1"/>
<wire x1="297.18" y1="38.1" x2="307.34" y2="38.1" width="0.1524" layer="91"/>
<junction x="297.18" y="38.1"/>
</segment>
</net>
<net name="SCL" class="0">
<segment>
<pinref part="TWI" gate="G$1" pin="1"/>
<wire x1="322.58" y1="160.02" x2="312.42" y2="160.02" width="0.1524" layer="91"/>
<label x="312.42" y="160.02" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<label x="7.62" y="190.5" size="1.778" layer="95"/>
<wire x1="7.62" y1="190.5" x2="5.08" y2="193.04" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="PA15"/>
<wire x1="7.62" y1="190.5" x2="15.24" y2="190.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="12/SCL"/>
<label x="182.88" y="218.44" size="1.778" layer="95"/>
<wire x1="165.1" y1="218.44" x2="208.28" y2="218.44" width="0.1524" layer="91"/>
<wire x1="208.28" y1="218.44" x2="210.82" y2="220.98" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SDA" class="0">
<segment>
<pinref part="TWI" gate="G$1" pin="2"/>
<wire x1="322.58" y1="157.48" x2="320.04" y2="157.48" width="0.1524" layer="91"/>
<label x="320.04" y="157.48" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<label x="7.62" y="193.04" size="1.778" layer="95"/>
<wire x1="7.62" y1="193.04" x2="5.08" y2="195.58" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="PA14"/>
<wire x1="7.62" y1="193.04" x2="15.24" y2="193.04" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="11/SDA"/>
<label x="182.88" y="215.9" size="1.778" layer="95"/>
<wire x1="165.1" y1="215.9" x2="208.28" y2="215.9" width="0.1524" layer="91"/>
<wire x1="208.28" y1="215.9" x2="210.82" y2="218.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SERVO4" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="PA22"/>
<wire x1="33.02" y1="172.72" x2="33.02" y2="160.02" width="0.1524" layer="91"/>
<wire x1="33.02" y1="160.02" x2="30.48" y2="157.48" width="0.1524" layer="91"/>
<label x="33.02" y="160.02" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="213.36" y1="124.46" x2="210.82" y2="127" width="0.1524" layer="91"/>
<label x="215.9" y="124.46" size="1.778" layer="95"/>
<pinref part="U1" gate="G$1" pin="A2"/>
<wire x1="213.36" y1="124.46" x2="226.06" y2="124.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SERVO2" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="PA23"/>
<wire x1="35.56" y1="172.72" x2="35.56" y2="160.02" width="0.1524" layer="91"/>
<wire x1="35.56" y1="160.02" x2="33.02" y2="157.48" width="0.1524" layer="91"/>
<label x="35.56" y="160.02" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="213.36" y1="127" x2="210.82" y2="129.54" width="0.1524" layer="91"/>
<label x="215.9" y="127" size="1.778" layer="95"/>
<pinref part="U1" gate="G$1" pin="A1"/>
<wire x1="213.36" y1="127" x2="226.06" y2="127" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SERVO1" class="0">
<segment>
<label x="215.9" y="121.92" size="1.778" layer="95"/>
<wire x1="213.36" y1="121.92" x2="210.82" y2="124.46" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="A3"/>
<wire x1="213.36" y1="121.92" x2="226.06" y2="121.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="PA17"/>
<wire x1="30.48" y1="172.72" x2="30.48" y2="160.02" width="0.1524" layer="91"/>
<wire x1="30.48" y1="160.02" x2="27.94" y2="157.48" width="0.1524" layer="91"/>
<label x="30.48" y="160.02" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="SERVO3" class="0">
<segment>
<label x="215.9" y="119.38" size="1.778" layer="95"/>
<wire x1="213.36" y1="119.38" x2="210.82" y2="121.92" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="A4"/>
<wire x1="213.36" y1="119.38" x2="226.06" y2="119.38" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="PA16"/>
<wire x1="27.94" y1="172.72" x2="27.94" y2="160.02" width="0.1524" layer="91"/>
<wire x1="27.94" y1="160.02" x2="25.4" y2="157.48" width="0.1524" layer="91"/>
<label x="27.94" y="160.02" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="SUPPLY2" gate="+5V" pin="+5V"/>
<wire x1="307.34" y1="127" x2="317.5" y2="127" width="0.1524" layer="91"/>
<junction x="307.34" y="127"/>
<wire x1="307.34" y1="121.92" x2="307.34" y2="127" width="0.1524" layer="91"/>
<wire x1="317.5" y1="121.92" x2="317.5" y2="127" width="0.1524" layer="91"/>
<wire x1="317.5" y1="127" x2="327.66" y2="127" width="0.1524" layer="91"/>
<wire x1="327.66" y1="127" x2="327.66" y2="121.92" width="0.1524" layer="91"/>
<junction x="317.5" y="127"/>
<wire x1="327.66" y1="127" x2="337.82" y2="127" width="0.1524" layer="91"/>
<wire x1="337.82" y1="127" x2="337.82" y2="121.92" width="0.1524" layer="91"/>
<junction x="327.66" y="127"/>
<pinref part="C5" gate="G$1" pin="+"/>
<pinref part="C13" gate="G$1" pin="+"/>
<pinref part="C25" gate="G$1" pin="+"/>
<pinref part="C31" gate="G$1" pin="+"/>
</segment>
<segment>
<pinref part="R10" gate="G$1" pin="2"/>
<pinref part="SUPPLY12" gate="+5V" pin="+5V"/>
<wire x1="254" y1="152.4" x2="251.46" y2="152.4" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY4" gate="+5V" pin="+5V"/>
<pinref part="SERVO1" gate="G$1" pin="2"/>
<wire x1="287.02" y1="114.3" x2="289.56" y2="114.3" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY5" gate="+5V" pin="+5V"/>
<pinref part="SERVO3" gate="G$1" pin="2"/>
<wire x1="287.02" y1="104.14" x2="289.56" y2="104.14" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY3" gate="+5V" pin="+5V"/>
<pinref part="SERVO2" gate="G$1" pin="2"/>
<wire x1="287.02" y1="134.62" x2="289.56" y2="134.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY1" gate="+5V" pin="+5V"/>
<pinref part="SERVO4" gate="G$1" pin="2"/>
<wire x1="287.02" y1="124.46" x2="289.56" y2="124.46" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IN2" gate="G$1" pin="1"/>
<pinref part="SUPPLY7" gate="+5V" pin="+5V"/>
</segment>
<segment>
<pinref part="SUPPLY11" gate="+5V" pin="+5V"/>
<pinref part="IN4" gate="G$1" pin="1"/>
<wire x1="317.5" y1="93.98" x2="322.58" y2="93.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IN1" gate="G$1" pin="1"/>
<pinref part="SUPPLY8" gate="+5V" pin="+5V"/>
</segment>
<segment>
<pinref part="IN3" gate="G$1" pin="1"/>
<pinref part="SUPPLY10" gate="+5V" pin="+5V"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="3"/>
<pinref part="SUPPLY14" gate="+5V" pin="+5V"/>
<wire x1="339.09" y1="64.77" x2="341.63" y2="64.77" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY17" gate="+5V" pin="+5V"/>
<pinref part="J4" gate="G$1" pin="3"/>
<wire x1="339.09" y1="102.87" x2="341.63" y2="102.87" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY16" gate="+5V" pin="+5V"/>
<wire x1="339.09" y1="90.17" x2="341.63" y2="90.17" width="0.1524" layer="91"/>
<pinref part="J3" gate="G$1" pin="3"/>
</segment>
<segment>
<pinref part="SUPPLY15" gate="+5V" pin="+5V"/>
<pinref part="J2" gate="G$1" pin="3"/>
<wire x1="339.09" y1="77.47" x2="341.63" y2="77.47" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="VIN"/>
<wire x1="165.1" y1="233.68" x2="180.34" y2="233.68" width="0.1524" layer="91"/>
<label x="180.34" y="233.68" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<label x="271.78" y="127" size="1.778" layer="95"/>
<label x="271.78" y="127" size="1.778" layer="95"/>
<label x="271.78" y="127" size="1.778" layer="95"/>
<pinref part="U1" gate="G$1" pin="B2"/>
<wire x1="271.78" y1="124.46" x2="271.78" y2="127" width="0.1524" layer="91"/>
<wire x1="256.54" y1="124.46" x2="271.78" y2="124.46" width="0.1524" layer="91"/>
<pinref part="SERVO4" gate="G$1" pin="1"/>
<wire x1="271.78" y1="127" x2="289.56" y2="127" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<wire x1="269.24" y1="137.16" x2="269.24" y2="127" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="B1"/>
<wire x1="256.54" y1="127" x2="269.24" y2="127" width="0.1524" layer="91"/>
<pinref part="SERVO2" gate="G$1" pin="1"/>
<wire x1="269.24" y1="137.16" x2="289.56" y2="137.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="B4"/>
<wire x1="271.78" y1="106.68" x2="271.78" y2="119.38" width="0.1524" layer="91"/>
<wire x1="271.78" y1="119.38" x2="256.54" y2="119.38" width="0.1524" layer="91"/>
<pinref part="SERVO3" gate="G$1" pin="1"/>
<wire x1="271.78" y1="106.68" x2="289.56" y2="106.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="MOSI" class="0">
<segment>
<wire x1="63.5" y1="195.58" x2="86.36" y2="195.58" width="0.1524" layer="91"/>
<wire x1="86.36" y1="195.58" x2="88.9" y2="198.12" width="0.1524" layer="91"/>
<label x="66.04" y="195.58" size="1.778" layer="95"/>
<pinref part="IC1" gate="G$1" pin="PA24"/>
</segment>
<segment>
<wire x1="210.82" y1="210.82" x2="208.28" y2="208.28" width="0.1524" layer="91"/>
<wire x1="208.28" y1="208.28" x2="175.26" y2="208.28" width="0.1524" layer="91"/>
<label x="182.88" y="208.28" size="1.778" layer="95"/>
<pinref part="R15" gate="G$1" pin="1"/>
</segment>
</net>
<net name="SWCLK/MISO" class="0">
<segment>
<wire x1="63.5" y1="190.5" x2="66.04" y2="190.5" width="0.1524" layer="91"/>
<wire x1="66.04" y1="190.5" x2="86.36" y2="190.5" width="0.1524" layer="91"/>
<wire x1="86.36" y1="190.5" x2="88.9" y2="193.04" width="0.1524" layer="91"/>
<label x="66.04" y="190.5" size="1.778" layer="95"/>
<pinref part="IC1" gate="G$1" pin="SWCLK/PA30"/>
<pinref part="CN1" gate="G$1" pin="4"/>
<wire x1="71.12" y1="175.26" x2="66.04" y2="175.26" width="0.1524" layer="91"/>
<wire x1="66.04" y1="175.26" x2="66.04" y2="182.88" width="0.1524" layer="91"/>
<junction x="66.04" y="190.5"/>
<wire x1="66.04" y1="182.88" x2="66.04" y2="190.5" width="0.1524" layer="91"/>
<wire x1="73.66" y1="182.88" x2="66.04" y2="182.88" width="0.1524" layer="91"/>
<junction x="66.04" y="182.88"/>
<pinref part="TP4" gate="G$1" pin="P$1"/>
<junction x="66.04" y="175.26"/>
<pinref part="R25" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="210.82" y1="215.9" x2="208.28" y2="213.36" width="0.1524" layer="91"/>
<wire x1="208.28" y1="213.36" x2="175.26" y2="213.36" width="0.1524" layer="91"/>
<label x="182.88" y="213.36" size="1.778" layer="95"/>
<pinref part="R8" gate="G$1" pin="1"/>
</segment>
</net>
<net name="SCK" class="0">
<segment>
<wire x1="63.5" y1="198.12" x2="86.36" y2="198.12" width="0.1524" layer="91"/>
<wire x1="86.36" y1="198.12" x2="88.9" y2="200.66" width="0.1524" layer="91"/>
<label x="66.04" y="198.12" size="1.778" layer="95"/>
<pinref part="IC1" gate="G$1" pin="PA25"/>
</segment>
<segment>
<wire x1="210.82" y1="213.36" x2="208.28" y2="210.82" width="0.1524" layer="91"/>
<wire x1="208.28" y1="210.82" x2="175.26" y2="210.82" width="0.1524" layer="91"/>
<label x="182.88" y="210.82" size="1.778" layer="95"/>
<pinref part="R13" gate="G$1" pin="1"/>
</segment>
</net>
<net name="+3.3V" class="0">
<segment>
<pinref part="TWI" gate="G$1" pin="3"/>
<wire x1="322.58" y1="154.94" x2="299.72" y2="154.94" width="0.1524" layer="91"/>
<pinref part="U$28" gate="G$1" pin="+3.3V"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="VCCA"/>
<pinref part="U$36" gate="G$1" pin="+3.3V"/>
<wire x1="238.76" y1="149.86" x2="238.76" y2="144.78" width="0.1524" layer="91"/>
<pinref part="C20" gate="G$1" pin="2"/>
<wire x1="238.76" y1="144.78" x2="238.76" y2="142.24" width="0.1524" layer="91"/>
<wire x1="220.98" y1="144.78" x2="238.76" y2="144.78" width="0.1524" layer="91"/>
<junction x="238.76" y="144.78"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="VCC"/>
<pinref part="U$14" gate="G$1" pin="+3.3V"/>
<wire x1="165.1" y1="231.14" x2="195.58" y2="231.14" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C1" gate="G$1" pin="1"/>
<pinref part="U$3" gate="G$1" pin="+3.3V"/>
<pinref part="IC1" gate="G$1" pin="VDD"/>
<wire x1="63.5" y1="203.2" x2="66.04" y2="203.2" width="0.1524" layer="91"/>
<wire x1="66.04" y1="203.2" x2="66.04" y2="210.82" width="0.1524" layer="91"/>
<wire x1="66.04" y1="210.82" x2="68.58" y2="210.82" width="0.1524" layer="91"/>
<junction x="68.58" y="210.82"/>
</segment>
<segment>
<pinref part="CN1" gate="G$1" pin="1"/>
<pinref part="U$2" gate="G$1" pin="+3.3V"/>
<wire x1="81.28" y1="177.8" x2="83.82" y2="177.8" width="0.1524" layer="91"/>
<wire x1="83.82" y1="177.8" x2="83.82" y2="180.34" width="0.1524" layer="91"/>
<wire x1="83.82" y1="180.34" x2="83.82" y2="182.88" width="0.1524" layer="91"/>
<wire x1="78.74" y1="182.88" x2="83.82" y2="182.88" width="0.1524" layer="91"/>
<junction x="83.82" y="182.88"/>
<pinref part="TP1" gate="G$1" pin="P$1"/>
<junction x="83.82" y="180.34"/>
<pinref part="R25" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="U$8" gate="G$1" pin="+3.3V"/>
<wire x1="243.84" y1="165.1" x2="233.68" y2="165.1" width="0.1524" layer="91"/>
<wire x1="233.68" y1="165.1" x2="231.14" y2="165.1" width="0.1524" layer="91"/>
<wire x1="236.22" y1="167.64" x2="233.68" y2="167.64" width="0.1524" layer="91"/>
<wire x1="233.68" y1="167.64" x2="233.68" y2="165.1" width="0.1524" layer="91"/>
<junction x="233.68" y="165.1"/>
<pinref part="R20" gate="G$1" pin="1"/>
<pinref part="U3" gate="G$1" pin="EN"/>
</segment>
<segment>
<pinref part="U$11" gate="G$1" pin="+3.3V"/>
<pinref part="R11" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="U$15" gate="G$1" pin="+3.3V"/>
<wire x1="238.76" y1="53.34" x2="223.52" y2="53.34" width="0.1524" layer="91"/>
<wire x1="223.52" y1="53.34" x2="220.98" y2="53.34" width="0.1524" layer="91"/>
<wire x1="226.06" y1="55.88" x2="223.52" y2="55.88" width="0.1524" layer="91"/>
<wire x1="223.52" y1="55.88" x2="223.52" y2="53.34" width="0.1524" layer="91"/>
<junction x="223.52" y="53.34"/>
<pinref part="R19" gate="G$1" pin="1"/>
<pinref part="U5" gate="G$1" pin="EN"/>
</segment>
<segment>
<pinref part="U$9" gate="G$1" pin="+3.3V"/>
<pinref part="R12" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="X2" gate="-4" pin="1"/>
<pinref part="U$20" gate="G$1" pin="+3.3V"/>
<wire x1="170.18" y1="48.26" x2="165.1" y2="48.26" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R24" gate="G$1" pin="1"/>
<pinref part="U$18" gate="G$1" pin="+3.3V"/>
</segment>
<segment>
<pinref part="X3" gate="-1" pin="1"/>
<pinref part="U$21" gate="G$1" pin="+3.3V"/>
<wire x1="167.64" y1="27.94" x2="165.1" y2="27.94" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$19" gate="G$1" pin="+3.3V"/>
<pinref part="R26" gate="G$1" pin="1"/>
</segment>
</net>
<net name="+VM" class="0">
<segment>
<pinref part="U$22" gate="G$1" pin="+VM"/>
<wire x1="264.16" y1="220.98" x2="271.78" y2="220.98" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="+"/>
<wire x1="271.78" y1="220.98" x2="279.4" y2="220.98" width="0.1524" layer="91"/>
<junction x="271.78" y="220.98"/>
<pinref part="C26" gate="G$1" pin="1"/>
<wire x1="264.16" y1="218.44" x2="264.16" y2="220.98" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="VM"/>
<wire x1="264.16" y1="220.98" x2="256.54" y2="220.98" width="0.1524" layer="91"/>
<junction x="264.16" y="220.98"/>
</segment>
<segment>
<pinref part="U$10" gate="G$1" pin="+VM"/>
<pinref part="C3" gate="G$1" pin="+"/>
<pinref part="C8" gate="G$1" pin="1"/>
<wire x1="269.24" y1="22.86" x2="266.7" y2="22.86" width="0.1524" layer="91"/>
<pinref part="U7" gate="G$1" pin="VM"/>
<wire x1="269.24" y1="22.86" x2="276.86" y2="22.86" width="0.1524" layer="91"/>
<junction x="269.24" y="22.86"/>
<junction x="276.86" y="22.86"/>
</segment>
<segment>
<pinref part="R3" gate="G$1" pin="2"/>
<pinref part="U$5" gate="G$1" pin="+VM"/>
</segment>
<segment>
<wire x1="289.56" y1="190.5" x2="284.48" y2="190.5" width="0.1524" layer="91"/>
<pinref part="U$16" gate="G$1" pin="+VM"/>
<junction x="289.56" y="190.5"/>
<pinref part="C7" gate="G$1" pin="1"/>
<wire x1="289.56" y1="190.5" x2="297.18" y2="190.5" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="1"/>
<pinref part="U3" gate="G$1" pin="VPWR"/>
<pinref part="C12" gate="G$1" pin="1"/>
<wire x1="297.18" y1="190.5" x2="302.26" y2="190.5" width="0.1524" layer="91"/>
<junction x="297.18" y="190.5"/>
</segment>
<segment>
<pinref part="U$17" gate="G$1" pin="+VM"/>
<wire x1="279.4" y1="78.74" x2="284.48" y2="78.74" width="0.1524" layer="91"/>
<pinref part="C4" gate="G$1" pin="1"/>
<junction x="284.48" y="78.74"/>
<pinref part="C9" gate="G$1" pin="1"/>
<junction x="284.48" y="78.74"/>
<wire x1="284.48" y1="78.74" x2="292.1" y2="78.74" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="VPWR"/>
<pinref part="C18" gate="G$1" pin="1"/>
<wire x1="292.1" y1="78.74" x2="297.18" y2="78.74" width="0.1524" layer="91"/>
<junction x="292.1" y="78.74"/>
</segment>
</net>
<net name="HB_E2" class="0">
<segment>
<wire x1="208.28" y1="53.34" x2="210.82" y2="55.88" width="0.1524" layer="91"/>
<wire x1="193.04" y1="53.34" x2="185.42" y2="53.34" width="0.1524" layer="91"/>
<pinref part="C15" gate="G$1" pin="1"/>
<pinref part="R5" gate="G$1" pin="1"/>
<label x="195.58" y="53.34" size="1.778" layer="95"/>
<wire x1="208.28" y1="53.34" x2="193.04" y2="53.34" width="0.1524" layer="91"/>
<junction x="193.04" y="53.34"/>
</segment>
<segment>
<wire x1="7.62" y1="203.2" x2="5.08" y2="205.74" width="0.1524" layer="91"/>
<label x="7.62" y="203.2" size="1.778" layer="95"/>
<pinref part="IC1" gate="G$1" pin="PA08"/>
<wire x1="7.62" y1="203.2" x2="15.24" y2="203.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="HA_E2" class="0">
<segment>
<wire x1="208.28" y1="50.8" x2="210.82" y2="53.34" width="0.1524" layer="91"/>
<pinref part="C14" gate="G$1" pin="2"/>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="193.04" y1="50.8" x2="185.42" y2="50.8" width="0.1524" layer="91"/>
<label x="195.58" y="50.8" size="1.778" layer="95"/>
<wire x1="208.28" y1="50.8" x2="193.04" y2="50.8" width="0.1524" layer="91"/>
<junction x="193.04" y="50.8"/>
</segment>
<segment>
<wire x1="7.62" y1="200.66" x2="5.08" y2="203.2" width="0.1524" layer="91"/>
<label x="7.62" y="200.66" size="1.778" layer="95"/>
<pinref part="IC1" gate="G$1" pin="PA09"/>
<wire x1="7.62" y1="200.66" x2="15.24" y2="200.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="HB_E1" class="0">
<segment>
<wire x1="193.04" y1="22.86" x2="185.42" y2="22.86" width="0.1524" layer="91"/>
<pinref part="C16" gate="G$1" pin="2"/>
<junction x="193.04" y="22.86"/>
<pinref part="R6" gate="G$1" pin="1"/>
<label x="195.58" y="22.86" size="1.778" layer="95"/>
<wire x1="193.04" y1="22.86" x2="208.28" y2="22.86" width="0.1524" layer="91"/>
<wire x1="208.28" y1="22.86" x2="210.82" y2="25.4" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="7.62" y1="195.58" x2="5.08" y2="198.12" width="0.1524" layer="91"/>
<label x="7.62" y="195.58" size="1.778" layer="95"/>
<pinref part="IC1" gate="G$1" pin="PA11"/>
<wire x1="7.62" y1="195.58" x2="15.24" y2="195.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="HA_E1" class="0">
<segment>
<wire x1="185.42" y1="25.4" x2="193.04" y2="25.4" width="0.1524" layer="91"/>
<pinref part="C17" gate="G$1" pin="1"/>
<junction x="193.04" y="25.4"/>
<pinref part="R7" gate="G$1" pin="1"/>
<label x="195.58" y="25.4" size="1.778" layer="95"/>
<wire x1="193.04" y1="25.4" x2="208.28" y2="25.4" width="0.1524" layer="91"/>
<wire x1="208.28" y1="25.4" x2="210.82" y2="27.94" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="7.62" y1="198.12" x2="5.08" y2="200.66" width="0.1524" layer="91"/>
<label x="7.62" y="198.12" size="1.778" layer="95"/>
<pinref part="IC1" gate="G$1" pin="PA10"/>
<wire x1="7.62" y1="198.12" x2="15.24" y2="198.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="C19" gate="G$1" pin="1"/>
<wire x1="256.54" y1="147.32" x2="256.54" y2="144.78" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="VCCB"/>
<wire x1="243.84" y1="142.24" x2="243.84" y2="147.32" width="0.1524" layer="91"/>
<wire x1="256.54" y1="147.32" x2="243.84" y2="147.32" width="0.1524" layer="91"/>
<junction x="243.84" y="147.32"/>
<pinref part="U1" gate="G$1" pin="OE"/>
<wire x1="226.06" y1="134.62" x2="223.52" y2="134.62" width="0.1524" layer="91"/>
<wire x1="223.52" y1="134.62" x2="223.52" y2="147.32" width="0.1524" layer="91"/>
<wire x1="243.84" y1="147.32" x2="223.52" y2="147.32" width="0.1524" layer="91"/>
<pinref part="R10" gate="G$1" pin="1"/>
<wire x1="246.38" y1="152.4" x2="243.84" y2="152.4" width="0.1524" layer="91"/>
<wire x1="243.84" y1="152.4" x2="243.84" y2="147.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="M3+" class="0">
<segment>
<wire x1="314.96" y1="233.68" x2="312.42" y2="233.68" width="0.1524" layer="91"/>
<wire x1="312.42" y1="233.68" x2="312.42" y2="238.76" width="0.1524" layer="91"/>
<wire x1="332.74" y1="238.76" x2="312.42" y2="238.76" width="0.1524" layer="91"/>
<label x="309.88" y="233.68" size="1.778" layer="95"/>
<wire x1="284.48" y1="177.8" x2="312.42" y2="177.8" width="0.1524" layer="91"/>
<wire x1="312.42" y1="177.8" x2="312.42" y2="233.68" width="0.1524" layer="91"/>
<junction x="312.42" y="233.68"/>
<pinref part="DL4" gate="G$1" pin="A"/>
<pinref part="X6" gate="-4" pin="1"/>
<pinref part="U3" gate="G$1" pin="OUT2"/>
</segment>
</net>
<net name="A6" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="A6"/>
<wire x1="134.62" y1="218.44" x2="91.44" y2="218.44" width="0.1524" layer="91"/>
<wire x1="91.44" y1="218.44" x2="88.9" y2="220.98" width="0.1524" layer="91"/>
<label x="106.68" y="218.44" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="A8"/>
<wire x1="226.06" y1="109.22" x2="213.36" y2="109.22" width="0.1524" layer="91"/>
<wire x1="213.36" y1="109.22" x2="210.82" y2="111.76" width="0.1524" layer="91"/>
<label x="218.44" y="109.22" size="1.778" layer="95"/>
</segment>
</net>
<net name="A5" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="A5"/>
<wire x1="134.62" y1="220.98" x2="91.44" y2="220.98" width="0.1524" layer="91"/>
<wire x1="91.44" y1="220.98" x2="88.9" y2="223.52" width="0.1524" layer="91"/>
<label x="106.68" y="220.98" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="A7"/>
<wire x1="226.06" y1="111.76" x2="213.36" y2="111.76" width="0.1524" layer="91"/>
<wire x1="213.36" y1="111.76" x2="210.82" y2="114.3" width="0.1524" layer="91"/>
<label x="218.44" y="111.76" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="180.34" y1="22.86" x2="165.1" y2="22.86" width="0.1524" layer="91"/>
<pinref part="X3" gate="-3" pin="1"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="180.34" y1="25.4" x2="165.1" y2="25.4" width="0.1524" layer="91"/>
<pinref part="X3" gate="-2" pin="1"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="RN4" gate="A" pin="2"/>
<wire x1="317.5" y1="246.38" x2="342.9" y2="246.38" width="0.1524" layer="91"/>
<pinref part="DL1" gate="G$1" pin="C"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="RN4" gate="D" pin="2"/>
<wire x1="342.9" y1="238.76" x2="340.36" y2="238.76" width="0.1524" layer="91"/>
<pinref part="DL4" gate="G$1" pin="C"/>
</segment>
</net>
<net name="N$36" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="ILIM"/>
<wire x1="226.06" y1="220.98" x2="223.52" y2="220.98" width="0.1524" layer="91"/>
<wire x1="223.52" y1="220.98" x2="223.52" y2="218.44" width="0.1524" layer="91"/>
<pinref part="R17" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$39" class="0">
<segment>
<pinref part="U7" gate="G$1" pin="ILIM"/>
<wire x1="236.22" y1="22.86" x2="231.14" y2="22.86" width="0.1524" layer="91"/>
<wire x1="231.14" y1="22.86" x2="231.14" y2="17.78" width="0.1524" layer="91"/>
<pinref part="R14" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="RN2" gate="A" pin="2"/>
<wire x1="332.74" y1="33.02" x2="337.82" y2="33.02" width="0.1524" layer="91"/>
<pinref part="DL5" gate="G$1" pin="C"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="RN2" gate="B" pin="2"/>
<wire x1="327.66" y1="30.48" x2="337.82" y2="30.48" width="0.1524" layer="91"/>
<pinref part="DL6" gate="G$1" pin="C"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<pinref part="RN2" gate="C" pin="2"/>
<wire x1="322.58" y1="27.94" x2="337.82" y2="27.94" width="0.1524" layer="91"/>
<pinref part="DL7" gate="G$1" pin="C"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<pinref part="RN2" gate="D" pin="2"/>
<wire x1="320.04" y1="25.4" x2="337.82" y2="25.4" width="0.1524" layer="91"/>
<pinref part="DL8" gate="G$1" pin="C"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="RN4" gate="B" pin="2"/>
<wire x1="325.12" y1="243.84" x2="342.9" y2="243.84" width="0.1524" layer="91"/>
<pinref part="DL2" gate="G$1" pin="C"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="RN4" gate="C" pin="2"/>
<wire x1="332.74" y1="241.3" x2="342.9" y2="241.3" width="0.1524" layer="91"/>
<pinref part="DL3" gate="G$1" pin="C"/>
</segment>
</net>
<net name="SWDIO/SS" class="0">
<segment>
<wire x1="63.5" y1="193.04" x2="68.58" y2="193.04" width="0.1524" layer="91"/>
<wire x1="68.58" y1="193.04" x2="86.36" y2="193.04" width="0.1524" layer="91"/>
<wire x1="86.36" y1="193.04" x2="88.9" y2="195.58" width="0.1524" layer="91"/>
<label x="66.04" y="193.04" size="1.778" layer="95"/>
<pinref part="IC1" gate="G$1" pin="SWDIO/PA31"/>
<pinref part="CN1" gate="G$1" pin="2"/>
<wire x1="71.12" y1="177.8" x2="68.58" y2="177.8" width="0.1524" layer="91"/>
<wire x1="68.58" y1="177.8" x2="68.58" y2="185.42" width="0.1524" layer="91"/>
<junction x="68.58" y="193.04"/>
<pinref part="TP5" gate="G$1" pin="P$1"/>
<wire x1="68.58" y1="185.42" x2="68.58" y2="193.04" width="0.1524" layer="91"/>
<junction x="68.58" y="185.42"/>
</segment>
<segment>
<wire x1="210.82" y1="208.28" x2="208.28" y2="205.74" width="0.1524" layer="91"/>
<wire x1="208.28" y1="205.74" x2="175.26" y2="205.74" width="0.1524" layer="91"/>
<label x="182.88" y="205.74" size="1.778" layer="95"/>
<pinref part="R16" gate="G$1" pin="1"/>
</segment>
</net>
<net name="FB_M3" class="0">
<segment>
<wire x1="243.84" y1="187.96" x2="238.76" y2="187.96" width="0.1524" layer="91"/>
<wire x1="238.76" y1="187.96" x2="238.76" y2="198.12" width="0.1524" layer="91"/>
<wire x1="238.76" y1="198.12" x2="233.68" y2="198.12" width="0.1524" layer="91"/>
<wire x1="233.68" y1="198.12" x2="226.06" y2="198.12" width="0.1524" layer="91"/>
<wire x1="226.06" y1="198.12" x2="213.36" y2="198.12" width="0.1524" layer="91"/>
<wire x1="213.36" y1="198.12" x2="210.82" y2="200.66" width="0.1524" layer="91"/>
<label x="213.36" y="198.12" size="1.778" layer="95"/>
<pinref part="C10" gate="G$1" pin="2"/>
<wire x1="226.06" y1="195.58" x2="226.06" y2="198.12" width="0.1524" layer="91"/>
<junction x="226.06" y="198.12"/>
<wire x1="233.68" y1="195.58" x2="233.68" y2="198.12" width="0.1524" layer="91"/>
<junction x="233.68" y="198.12"/>
<pinref part="R23" gate="G$1" pin="2"/>
<pinref part="U3" gate="G$1" pin="FB"/>
</segment>
<segment>
<wire x1="91.44" y1="226.06" x2="88.9" y2="228.6" width="0.1524" layer="91"/>
<label x="106.68" y="226.06" size="1.778" layer="95"/>
<pinref part="U$1" gate="G$1" pin="A3"/>
<wire x1="91.44" y1="226.06" x2="134.62" y2="226.06" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IN1_M3" class="0">
<segment>
<wire x1="243.84" y1="182.88" x2="213.36" y2="182.88" width="0.1524" layer="91"/>
<wire x1="213.36" y1="182.88" x2="210.82" y2="185.42" width="0.1524" layer="91"/>
<label x="213.36" y="182.88" size="1.778" layer="95"/>
<pinref part="U3" gate="G$1" pin="IN1"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="3"/>
<wire x1="134.62" y1="208.28" x2="91.44" y2="208.28" width="0.1524" layer="91"/>
<wire x1="91.44" y1="208.28" x2="88.9" y2="210.82" width="0.1524" layer="91"/>
<label x="106.68" y="208.28" size="1.778" layer="95"/>
</segment>
</net>
<net name="IN2_M3" class="0">
<segment>
<wire x1="243.84" y1="180.34" x2="213.36" y2="180.34" width="0.1524" layer="91"/>
<wire x1="213.36" y1="180.34" x2="210.82" y2="182.88" width="0.1524" layer="91"/>
<label x="213.36" y="180.34" size="1.778" layer="95"/>
<pinref part="U3" gate="G$1" pin="IN2"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="2"/>
<wire x1="134.62" y1="210.82" x2="91.44" y2="210.82" width="0.1524" layer="91"/>
<wire x1="91.44" y1="210.82" x2="88.9" y2="213.36" width="0.1524" layer="91"/>
<label x="106.68" y="210.82" size="1.778" layer="95"/>
</segment>
</net>
<net name="IN1_M4" class="0">
<segment>
<wire x1="238.76" y1="71.12" x2="213.36" y2="71.12" width="0.1524" layer="91"/>
<wire x1="213.36" y1="71.12" x2="210.82" y2="73.66" width="0.1524" layer="91"/>
<label x="213.36" y="71.12" size="1.778" layer="95"/>
<pinref part="U5" gate="G$1" pin="IN1"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="5"/>
<wire x1="134.62" y1="203.2" x2="91.44" y2="203.2" width="0.1524" layer="91"/>
<wire x1="91.44" y1="203.2" x2="88.9" y2="205.74" width="0.1524" layer="91"/>
<label x="106.68" y="203.2" size="1.778" layer="95"/>
</segment>
</net>
<net name="IN2_M4" class="0">
<segment>
<wire x1="238.76" y1="68.58" x2="213.36" y2="68.58" width="0.1524" layer="91"/>
<wire x1="213.36" y1="68.58" x2="210.82" y2="71.12" width="0.1524" layer="91"/>
<label x="213.36" y="68.58" size="1.778" layer="95"/>
<pinref part="U5" gate="G$1" pin="IN2"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="4"/>
<wire x1="134.62" y1="205.74" x2="91.44" y2="205.74" width="0.1524" layer="91"/>
<wire x1="91.44" y1="205.74" x2="88.9" y2="208.28" width="0.1524" layer="91"/>
<label x="106.68" y="205.74" size="1.778" layer="95"/>
</segment>
</net>
<net name="FB_M4" class="0">
<segment>
<wire x1="213.36" y1="86.36" x2="210.82" y2="88.9" width="0.1524" layer="91"/>
<label x="213.36" y="86.36" size="1.778" layer="95"/>
<wire x1="238.76" y1="76.2" x2="236.22" y2="76.2" width="0.1524" layer="91"/>
<wire x1="236.22" y1="76.2" x2="236.22" y2="86.36" width="0.1524" layer="91"/>
<wire x1="213.36" y1="86.36" x2="226.06" y2="86.36" width="0.1524" layer="91"/>
<wire x1="226.06" y1="86.36" x2="231.14" y2="86.36" width="0.1524" layer="91"/>
<wire x1="231.14" y1="86.36" x2="236.22" y2="86.36" width="0.1524" layer="91"/>
<wire x1="231.14" y1="83.82" x2="231.14" y2="86.36" width="0.1524" layer="91"/>
<junction x="231.14" y="86.36"/>
<pinref part="C11" gate="G$1" pin="2"/>
<wire x1="226.06" y1="83.82" x2="226.06" y2="86.36" width="0.1524" layer="91"/>
<junction x="226.06" y="86.36"/>
<pinref part="R22" gate="G$1" pin="2"/>
<pinref part="U5" gate="G$1" pin="FB"/>
</segment>
<segment>
<wire x1="91.44" y1="223.52" x2="88.9" y2="226.06" width="0.1524" layer="91"/>
<label x="106.68" y="223.52" size="1.778" layer="95"/>
<pinref part="U$1" gate="G$1" pin="A4"/>
<wire x1="91.44" y1="223.52" x2="134.62" y2="223.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IN2_M2" class="0">
<segment>
<pinref part="U7" gate="G$1" pin="IN2"/>
<wire x1="236.22" y1="33.02" x2="213.36" y2="33.02" width="0.1524" layer="91"/>
<wire x1="213.36" y1="33.02" x2="210.82" y2="35.56" width="0.1524" layer="91"/>
<label x="213.36" y="33.02" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="88.9" y1="241.3" x2="86.36" y2="238.76" width="0.1524" layer="91"/>
<wire x1="86.36" y1="238.76" x2="33.02" y2="238.76" width="0.1524" layer="91"/>
<label x="50.8" y="238.76" size="1.778" layer="95"/>
<pinref part="IC1" gate="G$1" pin="PA05"/>
<wire x1="33.02" y1="238.76" x2="33.02" y2="220.98" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IN1_M2" class="0">
<segment>
<pinref part="U7" gate="G$1" pin="IN1"/>
<wire x1="236.22" y1="27.94" x2="213.36" y2="27.94" width="0.1524" layer="91"/>
<wire x1="213.36" y1="27.94" x2="210.82" y2="30.48" width="0.1524" layer="91"/>
<label x="213.36" y="27.94" size="1.778" layer="95"/>
</segment>
<segment>
<label x="50.8" y="236.22" size="1.778" layer="95"/>
<wire x1="88.9" y1="238.76" x2="86.36" y2="236.22" width="0.1524" layer="91"/>
<wire x1="86.36" y1="236.22" x2="35.56" y2="236.22" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="PA04"/>
<wire x1="35.56" y1="236.22" x2="35.56" y2="220.98" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IN2_M1" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="IN2"/>
<wire x1="226.06" y1="231.14" x2="213.36" y2="231.14" width="0.1524" layer="91"/>
<wire x1="213.36" y1="231.14" x2="210.82" y2="233.68" width="0.1524" layer="91"/>
<label x="213.36" y="231.14" size="1.778" layer="95"/>
</segment>
<segment>
<label x="50.8" y="243.84" size="1.778" layer="95"/>
<wire x1="88.9" y1="246.38" x2="86.36" y2="243.84" width="0.1524" layer="91"/>
<wire x1="86.36" y1="243.84" x2="27.94" y2="243.84" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="PA07"/>
<wire x1="27.94" y1="243.84" x2="27.94" y2="220.98" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IN1_M1" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="IN1"/>
<wire x1="226.06" y1="226.06" x2="213.36" y2="226.06" width="0.1524" layer="91"/>
<wire x1="213.36" y1="226.06" x2="210.82" y2="228.6" width="0.1524" layer="91"/>
<label x="213.36" y="226.06" size="1.778" layer="95"/>
</segment>
<segment>
<label x="50.8" y="241.3" size="1.778" layer="95"/>
<wire x1="88.9" y1="243.84" x2="86.36" y2="241.3" width="0.1524" layer="91"/>
<wire x1="86.36" y1="241.3" x2="30.48" y2="241.3" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="PA06"/>
<wire x1="30.48" y1="241.3" x2="30.48" y2="220.98" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="10"/>
<wire x1="71.12" y1="167.64" x2="71.12" y2="162.56" width="0.1524" layer="91"/>
<wire x1="71.12" y1="162.56" x2="66.04" y2="162.56" width="0.1524" layer="91"/>
<pinref part="TP3" gate="G$1" pin="P$1"/>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="63.5" y1="162.56" x2="66.04" y2="162.56" width="0.1524" layer="91"/>
<junction x="66.04" y="162.56"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="PA28/!RESET"/>
<wire x1="55.88" y1="162.56" x2="50.8" y2="162.56" width="0.1524" layer="91"/>
<wire x1="50.8" y1="162.56" x2="40.64" y2="162.56" width="0.1524" layer="91"/>
<wire x1="40.64" y1="162.56" x2="40.64" y2="172.72" width="0.1524" layer="91"/>
<pinref part="R26" gate="G$1" pin="2"/>
<wire x1="55.88" y1="165.1" x2="55.88" y2="162.56" width="0.1524" layer="91"/>
<pinref part="C22" gate="G$1" pin="1"/>
<wire x1="50.8" y1="165.1" x2="50.8" y2="162.56" width="0.1524" layer="91"/>
<junction x="50.8" y="162.56"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="55.88" y1="162.56" x2="58.42" y2="162.56" width="0.1524" layer="91"/>
<junction x="55.88" y="162.56"/>
</segment>
</net>
<net name="V_READ" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="PA02"/>
<wire x1="40.64" y1="226.06" x2="40.64" y2="220.98" width="0.1524" layer="91"/>
<wire x1="53.34" y1="226.06" x2="40.64" y2="226.06" width="0.1524" layer="91"/>
<pinref part="R9" gate="G$1" pin="2"/>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="50.8" y1="228.6" x2="53.34" y2="228.6" width="0.1524" layer="91"/>
<label x="53.34" y="220.98" size="1.778" layer="95" rot="R180"/>
<wire x1="53.34" y1="228.6" x2="55.88" y2="228.6" width="0.1524" layer="91"/>
<wire x1="53.34" y1="226.06" x2="53.34" y2="228.6" width="0.1524" layer="91"/>
<junction x="53.34" y="228.6"/>
<pinref part="C21" gate="G$1" pin="1"/>
<junction x="53.34" y="226.06"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="165.1" y1="53.34" x2="180.34" y2="53.34" width="0.1524" layer="91"/>
<pinref part="X2" gate="-2" pin="1"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="165.1" y1="50.8" x2="180.34" y2="50.8" width="0.1524" layer="91"/>
<pinref part="X2" gate="-3" pin="1"/>
</segment>
</net>
<net name="/SF" class="0">
<segment>
<wire x1="210.82" y1="203.2" x2="213.36" y2="200.66" width="0.1524" layer="91"/>
<wire x1="213.36" y1="200.66" x2="241.3" y2="200.66" width="0.1524" layer="91"/>
<label x="213.36" y="200.66" size="1.778" layer="95"/>
<pinref part="DL9" gate="G$1" pin="C"/>
<wire x1="243.84" y1="190.5" x2="241.3" y2="190.5" width="0.1524" layer="91"/>
<wire x1="241.3" y1="190.5" x2="241.3" y2="200.66" width="0.1524" layer="91"/>
<junction x="241.3" y="200.66"/>
<pinref part="U3" gate="G$1" pin="!SF"/>
</segment>
<segment>
<wire x1="210.82" y1="91.44" x2="213.36" y2="88.9" width="0.1524" layer="91"/>
<wire x1="213.36" y1="88.9" x2="238.76" y2="88.9" width="0.1524" layer="91"/>
<wire x1="238.76" y1="88.9" x2="238.76" y2="78.74" width="0.1524" layer="91"/>
<label x="213.36" y="88.9" size="1.778" layer="95"/>
<pinref part="U5" gate="G$1" pin="!SF"/>
</segment>
<segment>
<pinref part="SJ5" gate="1" pin="2"/>
<wire x1="88.9" y1="215.9" x2="91.44" y2="213.36" width="0.1524" layer="91"/>
<wire x1="91.44" y1="213.36" x2="119.38" y2="213.36" width="0.1524" layer="91"/>
<label x="106.68" y="213.36" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="SJ5" gate="1" pin="1"/>
<pinref part="U$1" gate="G$1" pin="1"/>
<wire x1="129.54" y1="213.36" x2="134.62" y2="213.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SLEW" class="0">
<segment>
<wire x1="238.76" y1="60.96" x2="213.36" y2="60.96" width="0.1524" layer="91"/>
<wire x1="213.36" y1="60.96" x2="210.82" y2="63.5" width="0.1524" layer="91"/>
<label x="213.36" y="60.96" size="1.778" layer="95"/>
<pinref part="U5" gate="G$1" pin="SLEW"/>
</segment>
<segment>
<wire x1="210.82" y1="175.26" x2="213.36" y2="172.72" width="0.1524" layer="91"/>
<wire x1="213.36" y1="172.72" x2="243.84" y2="172.72" width="0.1524" layer="91"/>
<label x="213.36" y="172.72" size="1.778" layer="95"/>
<pinref part="U3" gate="G$1" pin="SLEW"/>
</segment>
<segment>
<wire x1="208.28" y1="86.36" x2="210.82" y2="88.9" width="0.1524" layer="91"/>
<label x="198.12" y="86.36" size="1.778" layer="95"/>
<pinref part="R12" gate="G$1" pin="1"/>
<wire x1="193.04" y1="88.9" x2="193.04" y2="86.36" width="0.1524" layer="91"/>
<wire x1="193.04" y1="86.36" x2="208.28" y2="86.36" width="0.1524" layer="91"/>
<pinref part="R18" gate="G$1" pin="2"/>
<wire x1="193.04" y1="86.36" x2="193.04" y2="83.82" width="0.1524" layer="91"/>
<junction x="193.04" y="86.36"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<wire x1="289.56" y1="185.42" x2="284.48" y2="185.42" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="2"/>
<pinref part="U3" gate="G$1" pin="CCP"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<wire x1="284.48" y1="73.66" x2="279.4" y2="73.66" width="0.1524" layer="91"/>
<pinref part="C4" gate="G$1" pin="2"/>
<pinref part="U5" gate="G$1" pin="CCP"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="R11" gate="G$1" pin="2"/>
<pinref part="DL9" gate="G$1" pin="A"/>
<wire x1="248.92" y1="200.66" x2="251.46" y2="200.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IN4_A2" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="B6"/>
<wire x1="266.7" y1="91.44" x2="266.7" y2="114.3" width="0.1524" layer="91"/>
<wire x1="266.7" y1="114.3" x2="256.54" y2="114.3" width="0.1524" layer="91"/>
<pinref part="IN4" gate="G$1" pin="2"/>
<wire x1="266.7" y1="91.44" x2="322.58" y2="91.44" width="0.1524" layer="91"/>
<pinref part="J3" gate="G$1" pin="1"/>
<wire x1="332.74" y1="95.25" x2="341.63" y2="95.25" width="0.1524" layer="91"/>
<wire x1="322.58" y1="91.44" x2="332.74" y2="91.44" width="0.1524" layer="91"/>
<wire x1="332.74" y1="91.44" x2="332.74" y2="95.25" width="0.1524" layer="91"/>
<junction x="322.58" y="91.44"/>
</segment>
</net>
<net name="N$35" class="0">
<segment>
<wire x1="243.84" y1="167.64" x2="241.3" y2="167.64" width="0.1524" layer="91"/>
<pinref part="R20" gate="G$1" pin="2"/>
<pinref part="U3" gate="G$1" pin="!D2"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<wire x1="238.76" y1="55.88" x2="231.14" y2="55.88" width="0.1524" layer="91"/>
<pinref part="R19" gate="G$1" pin="2"/>
<pinref part="U5" gate="G$1" pin="!D2"/>
</segment>
</net>
<net name="SD11_INT" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="PA27"/>
<wire x1="38.1" y1="172.72" x2="38.1" y2="160.02" width="0.1524" layer="91"/>
<wire x1="38.1" y1="160.02" x2="35.56" y2="157.48" width="0.1524" layer="91"/>
<label x="38.1" y="160.02" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="175.26" y1="203.2" x2="208.28" y2="203.2" width="0.1524" layer="91"/>
<wire x1="208.28" y1="203.2" x2="210.82" y2="205.74" width="0.1524" layer="91"/>
<label x="182.88" y="203.2" size="1.778" layer="95"/>
<pinref part="R21" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$38" class="0">
<segment>
<pinref part="R21" gate="G$1" pin="1"/>
<pinref part="U$1" gate="G$1" pin="6"/>
<wire x1="165.1" y1="203.2" x2="170.18" y2="203.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$40" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="10/MISO"/>
<wire x1="165.1" y1="213.36" x2="170.18" y2="213.36" width="0.1524" layer="91"/>
<pinref part="R8" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$41" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="9/SCK"/>
<wire x1="165.1" y1="210.82" x2="170.18" y2="210.82" width="0.1524" layer="91"/>
<pinref part="R13" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$43" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="8/MOSI"/>
<wire x1="165.1" y1="208.28" x2="170.18" y2="208.28" width="0.1524" layer="91"/>
<pinref part="R15" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$45" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="7"/>
<wire x1="165.1" y1="205.74" x2="170.18" y2="205.74" width="0.1524" layer="91"/>
<pinref part="R16" gate="G$1" pin="2"/>
</segment>
</net>
<net name="IN3_A5" class="0">
<segment>
<wire x1="304.8" y1="66.04" x2="304.8" y2="86.36" width="0.1524" layer="91"/>
<wire x1="304.8" y1="86.36" x2="259.08" y2="86.36" width="0.1524" layer="91"/>
<wire x1="259.08" y1="86.36" x2="259.08" y2="111.76" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="B7"/>
<wire x1="259.08" y1="111.76" x2="256.54" y2="111.76" width="0.1524" layer="91"/>
<pinref part="IN3" gate="G$1" pin="2"/>
<wire x1="304.8" y1="66.04" x2="322.58" y2="66.04" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="1"/>
<wire x1="341.63" y1="69.85" x2="330.2" y2="69.85" width="0.1524" layer="91"/>
<wire x1="322.58" y1="66.04" x2="330.2" y2="66.04" width="0.1524" layer="91"/>
<wire x1="330.2" y1="66.04" x2="330.2" y2="69.85" width="0.1524" layer="91"/>
<junction x="322.58" y="66.04"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<wire x1="274.32" y1="116.84" x2="274.32" y2="121.92" width="0.1524" layer="91"/>
<wire x1="274.32" y1="121.92" x2="256.54" y2="121.92" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="B3"/>
<pinref part="SERVO1" gate="G$1" pin="1"/>
<wire x1="274.32" y1="116.84" x2="289.56" y2="116.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IN1_A6" class="0">
<segment>
<wire x1="307.34" y1="78.74" x2="307.34" y2="88.9" width="0.1524" layer="91"/>
<wire x1="307.34" y1="88.9" x2="261.62" y2="88.9" width="0.1524" layer="91"/>
<wire x1="261.62" y1="88.9" x2="261.62" y2="109.22" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="B8"/>
<wire x1="256.54" y1="109.22" x2="261.62" y2="109.22" width="0.1524" layer="91"/>
<pinref part="IN1" gate="G$1" pin="2"/>
<wire x1="307.34" y1="78.74" x2="322.58" y2="78.74" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="1"/>
<wire x1="332.74" y1="82.55" x2="341.63" y2="82.55" width="0.1524" layer="91"/>
<wire x1="322.58" y1="78.74" x2="332.74" y2="78.74" width="0.1524" layer="91"/>
<wire x1="332.74" y1="78.74" x2="332.74" y2="82.55" width="0.1524" layer="91"/>
<junction x="322.58" y="78.74"/>
</segment>
</net>
<net name="A1" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="A1"/>
<wire x1="134.62" y1="231.14" x2="91.44" y2="231.14" width="0.1524" layer="91"/>
<wire x1="91.44" y1="231.14" x2="88.9" y2="233.68" width="0.1524" layer="91"/>
<label x="106.68" y="231.14" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="213.36" y1="116.84" x2="210.82" y2="119.38" width="0.1524" layer="91"/>
<label x="218.44" y="116.84" size="1.778" layer="95"/>
<pinref part="U1" gate="G$1" pin="A5"/>
<wire x1="213.36" y1="116.84" x2="226.06" y2="116.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="A2" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="A2"/>
<wire x1="134.62" y1="228.6" x2="91.44" y2="228.6" width="0.1524" layer="91"/>
<wire x1="91.44" y1="228.6" x2="88.9" y2="231.14" width="0.1524" layer="91"/>
<label x="106.68" y="228.6" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="A6"/>
<wire x1="226.06" y1="114.3" x2="213.36" y2="114.3" width="0.1524" layer="91"/>
<wire x1="213.36" y1="114.3" x2="210.82" y2="116.84" width="0.1524" layer="91"/>
<label x="218.44" y="114.3" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$42" class="0">
<segment>
<pinref part="R24" gate="G$1" pin="2"/>
<pinref part="DL10" gate="G$1" pin="A"/>
<wire x1="71.12" y1="233.68" x2="73.66" y2="233.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IN2_A1" class="0">
<segment>
<wire x1="307.34" y1="104.14" x2="307.34" y2="96.52" width="0.1524" layer="91"/>
<wire x1="307.34" y1="96.52" x2="269.24" y2="96.52" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="B5"/>
<wire x1="269.24" y1="96.52" x2="269.24" y2="116.84" width="0.1524" layer="91"/>
<wire x1="269.24" y1="116.84" x2="256.54" y2="116.84" width="0.1524" layer="91"/>
<pinref part="IN2" gate="G$1" pin="2"/>
<wire x1="307.34" y1="104.14" x2="322.58" y2="104.14" width="0.1524" layer="91"/>
<pinref part="J4" gate="G$1" pin="1"/>
<wire x1="330.2" y1="107.95" x2="341.63" y2="107.95" width="0.1524" layer="91"/>
<wire x1="322.58" y1="104.14" x2="330.2" y2="104.14" width="0.1524" layer="91"/>
<wire x1="330.2" y1="104.14" x2="330.2" y2="107.95" width="0.1524" layer="91"/>
<junction x="322.58" y="104.14"/>
</segment>
</net>
<net name="LED_S" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="PA03"/>
<pinref part="DL10" gate="G$1" pin="C"/>
<wire x1="38.1" y1="220.98" x2="38.1" y2="233.68" width="0.1524" layer="91"/>
<wire x1="38.1" y1="233.68" x2="63.5" y2="233.68" width="0.1524" layer="91"/>
<label x="50.8" y="233.68" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="218.44" y="109.22" size="1.778" layer="96">TS30013-M050QFNR</text>
<text x="195.58" y="121.92" size="1.778" layer="98">Internal
Pull-Up</text>
<text x="259.08" y="160.02" size="3.81" layer="94">POWER</text>
<text x="154.94" y="121.92" size="1.778" layer="98">OFF
ON</text>
</plain>
<instances>
<instance part="GND13" gate="1" x="177.8" y="129.54"/>
<instance part="GND25" gate="1" x="205.74" y="106.68"/>
<instance part="GND26" gate="1" x="256.54" y="121.92"/>
<instance part="GND27" gate="1" x="185.42" y="111.76"/>
<instance part="GND29" gate="1" x="215.9" y="106.68"/>
<instance part="GND30" gate="1" x="261.62" y="121.92"/>
<instance part="SUPPLY6" gate="+5V" x="256.54" y="139.7"/>
<instance part="SUPPLY13" gate="+5V" x="109.22" y="157.48"/>
<instance part="U$4" gate="G$1" x="104.14" y="160.02"/>
<instance part="U$6" gate="G$1" x="144.78" y="152.4"/>
<instance part="U$13" gate="G$1" x="114.3" y="160.02"/>
<instance part="U$7" gate="G$1" x="185.42" y="139.7"/>
<instance part="COUT1" gate="G$1" x="256.54" y="127" rot="R180"/>
<instance part="COUT2" gate="G$1" x="261.62" y="127"/>
<instance part="SUPPLY9" gate="+5V" x="132.08" y="114.3"/>
<instance part="ON" gate="G$1" x="137.16" y="106.68" rot="R90"/>
<instance part="Q1" gate="G$1" x="175.26" y="139.7" rot="MR90"/>
<instance part="C23" gate="G$1" x="165.1" y="129.54" smashed="yes" rot="R180">
<attribute name="NAME" x="171.196" y="130.429" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="171.196" y="127.889" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="Q3" gate="G$1" x="162.56" y="139.7" rot="R90"/>
<instance part="U$38" gate="G$1" x="152.4" y="124.46" rot="R180"/>
<instance part="GND2" gate="1" x="144.78" y="127" rot="R270"/>
<instance part="GND50" gate="1" x="165.1" y="124.46"/>
<instance part="GND52" gate="1" x="144.78" y="119.38" rot="R270"/>
<instance part="CBST" gate="G$1" x="233.68" y="139.7" rot="R90"/>
<instance part="U2" gate="G$1" x="210.82" y="129.54"/>
<instance part="LOUT" gate="&gt;NAME" x="241.3" y="134.62"/>
<instance part="D1" gate="D" x="236.22" y="129.54" smashed="yes" rot="R90">
<attribute name="NAME" x="234.315" y="127.254" size="1.778" layer="95" rot="R90"/>
</instance>
<instance part="CBYPASS" gate="G$1" x="185.42" y="119.38" rot="R180"/>
<instance part="GND43" gate="1" x="241.3" y="127" rot="R90"/>
<instance part="JP2" gate="G$1" x="91.44" y="147.32" rot="R90"/>
<instance part="U$12" gate="G$1" x="88.9" y="160.02"/>
<instance part="GND19" gate="1" x="119.38" y="157.48" rot="R180"/>
<instance part="GND20" gate="1" x="91.44" y="157.48" rot="R180"/>
<instance part="X8" gate="-1" x="104.14" y="152.4" rot="R90"/>
<instance part="X8" gate="-2" x="109.22" y="152.4" rot="R90"/>
<instance part="X8" gate="-3" x="114.3" y="152.4" rot="R90"/>
<instance part="X8" gate="-4" x="119.38" y="152.4" rot="R90"/>
<instance part="R1" gate="G$1" x="147.32" y="106.68"/>
<instance part="FRAME1" gate="G$1" x="0" y="0"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="GND27" gate="1" pin="GND"/>
<wire x1="185.42" y1="116.84" x2="185.42" y2="114.3" width="0.1524" layer="91"/>
<pinref part="CBYPASS" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="GND13" gate="1" pin="GND"/>
<wire x1="177.8" y1="134.62" x2="177.8" y2="132.08" width="0.1524" layer="91"/>
<pinref part="Q1" gate="G$1" pin="G"/>
</segment>
<segment>
<pinref part="GND26" gate="1" pin="GND"/>
<pinref part="COUT1" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="GND30" gate="1" pin="GND"/>
<pinref part="COUT2" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="U$38" gate="G$1" pin="4"/>
<pinref part="GND2" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C23" gate="G$1" pin="1"/>
<pinref part="GND50" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U$38" gate="G$1" pin="1"/>
<pinref part="GND52" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND25" gate="1" pin="GND"/>
<pinref part="U2" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="GND29" gate="1" pin="GND"/>
<pinref part="U2" gate="G$1" pin="PGND"/>
</segment>
<segment>
<pinref part="GND43" gate="1" pin="GND"/>
<pinref part="D1" gate="D" pin="A"/>
<wire x1="236.22" y1="127" x2="238.76" y2="127" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP2" gate="G$1" pin="1"/>
<pinref part="GND20" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND19" gate="1" pin="GND"/>
<pinref part="X8" gate="-4" pin="1"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="SUPPLY9" gate="+5V" pin="+5V"/>
<pinref part="ON" gate="G$1" pin="A"/>
<wire x1="134.62" y1="106.68" x2="132.08" y2="106.68" width="0.1524" layer="91"/>
<wire x1="132.08" y1="106.68" x2="132.08" y2="111.76" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="256.54" y1="134.62" x2="261.62" y2="134.62" width="0.1524" layer="91"/>
<wire x1="261.62" y1="134.62" x2="261.62" y2="129.54" width="0.1524" layer="91"/>
<label x="269.24" y="134.62" size="1.778" layer="95"/>
<pinref part="COUT2" gate="G$1" pin="1"/>
<pinref part="SUPPLY6" gate="+5V" pin="+5V"/>
<wire x1="248.92" y1="134.62" x2="248.92" y2="124.46" width="0.1524" layer="91"/>
<wire x1="256.54" y1="137.16" x2="256.54" y2="134.62" width="0.1524" layer="91"/>
<junction x="256.54" y="134.62"/>
<pinref part="COUT1" gate="G$1" pin="2"/>
<wire x1="256.54" y1="129.54" x2="256.54" y2="134.62" width="0.1524" layer="91"/>
<wire x1="256.54" y1="134.62" x2="248.92" y2="134.62" width="0.1524" layer="91"/>
<pinref part="LOUT" gate="&gt;NAME" pin="2"/>
<wire x1="248.92" y1="134.62" x2="246.38" y2="134.62" width="0.1524" layer="91"/>
<junction x="248.92" y="134.62"/>
<wire x1="248.92" y1="124.46" x2="231.14" y2="124.46" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="FB"/>
<wire x1="261.62" y1="134.62" x2="284.48" y2="134.62" width="0.1524" layer="91"/>
<junction x="261.62" y="134.62"/>
<label x="284.48" y="134.62" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="SUPPLY13" gate="+5V" pin="+5V"/>
<pinref part="X8" gate="-2" pin="1"/>
</segment>
</net>
<net name="+3.3V" class="0">
<segment>
<wire x1="114.3" y1="157.48" x2="114.3" y2="154.94" width="0.1524" layer="91"/>
<pinref part="U$13" gate="G$1" pin="+3.3V"/>
<pinref part="X8" gate="-3" pin="1"/>
</segment>
</net>
<net name="+VM" class="0">
<segment>
<junction x="185.42" y="139.7"/>
<wire x1="185.42" y1="139.7" x2="190.5" y2="139.7" width="0.1524" layer="91"/>
<wire x1="185.42" y1="139.7" x2="185.42" y2="121.92" width="0.1524" layer="91"/>
<junction x="185.42" y="139.7"/>
<wire x1="180.34" y1="139.7" x2="185.42" y2="139.7" width="0.1524" layer="91"/>
<pinref part="Q1" gate="G$1" pin="S"/>
<label x="185.42" y="132.08" size="1.778" layer="95"/>
<pinref part="U$7" gate="G$1" pin="+VM"/>
<pinref part="U2" gate="G$1" pin="VCC"/>
<pinref part="CBYPASS" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$133" class="0">
<segment>
<pinref part="ON" gate="G$1" pin="C"/>
<wire x1="142.24" y1="106.68" x2="144.78" y2="106.68" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="CBST" gate="G$1" pin="2"/>
<wire x1="236.22" y1="134.62" x2="231.14" y2="134.62" width="0.1524" layer="91"/>
<wire x1="236.22" y1="139.7" x2="236.22" y2="134.62" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="VSW"/>
<pinref part="LOUT" gate="&gt;NAME" pin="1"/>
<junction x="236.22" y="134.62"/>
<pinref part="D1" gate="D" pin="C"/>
<wire x1="236.22" y1="132.08" x2="236.22" y2="134.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PWRIN" class="0">
<segment>
<pinref part="U$4" gate="G$1" pin="PWRIN"/>
<wire x1="104.14" y1="157.48" x2="104.14" y2="154.94" width="0.1524" layer="91"/>
<pinref part="X8" gate="-1" pin="1"/>
</segment>
<segment>
<pinref part="U$6" gate="G$1" pin="PWRIN"/>
<pinref part="Q3" gate="G$1" pin="S"/>
<wire x1="144.78" y1="149.86" x2="144.78" y2="139.7" width="0.1524" layer="91"/>
<pinref part="U$38" gate="G$1" pin="6"/>
<wire x1="144.78" y1="139.7" x2="157.48" y2="139.7" width="0.1524" layer="91"/>
<wire x1="147.32" y1="132.08" x2="144.78" y2="132.08" width="0.1524" layer="91"/>
<wire x1="144.78" y1="132.08" x2="144.78" y2="139.7" width="0.1524" layer="91"/>
<junction x="144.78" y="139.7"/>
</segment>
<segment>
<pinref part="JP2" gate="G$1" pin="2"/>
<pinref part="U$12" gate="G$1" pin="PWRIN"/>
<wire x1="88.9" y1="157.48" x2="88.9" y2="154.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="Q3" gate="G$1" pin="D"/>
<pinref part="Q1" gate="G$1" pin="D"/>
<wire x1="167.64" y1="139.7" x2="170.18" y2="139.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="U$38" gate="G$1" pin="5"/>
<wire x1="157.48" y1="129.54" x2="160.02" y2="129.54" width="0.1524" layer="91"/>
<pinref part="Q3" gate="G$1" pin="G"/>
<wire x1="160.02" y1="129.54" x2="160.02" y2="132.08" width="0.1524" layer="91"/>
<pinref part="C23" gate="G$1" pin="2"/>
<wire x1="160.02" y1="132.08" x2="160.02" y2="134.62" width="0.1524" layer="91"/>
<wire x1="165.1" y1="132.08" x2="160.02" y2="132.08" width="0.1524" layer="91"/>
<junction x="160.02" y="132.08"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="U$38" gate="G$1" pin="2"/>
<wire x1="149.86" y1="106.68" x2="157.48" y2="106.68" width="0.1524" layer="91"/>
<wire x1="157.48" y1="106.68" x2="157.48" y2="121.92" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="CBST" gate="G$1" pin="1"/>
<pinref part="U2" gate="G$1" pin="BST"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
</compatibility>
</eagle>
