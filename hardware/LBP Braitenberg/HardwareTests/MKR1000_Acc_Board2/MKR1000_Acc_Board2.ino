#include <SPI.h>
//#include <WiFiNINA.h>
#include <WiFi101.h>
#include <WiFiUdp.h>
#include <OSCMessage.h>
//#include <MKRMotorCarrier.h>
#include <Arduino_LSM9DS1.h>

int status = WL_IDLE_STATUS;

IPAddress local_IP(192, 168, 0, 51);  // Board ip address
unsigned int localPort = 2390;         // local port to listen on
char ssid[] = "TP-Link_72DE";          // your network SSID (name)
char pass[] = "86171342";              // your network password (use for WPA, or use as key for WEP)

WiFiUDP Udp;
IPAddress ip;

char packetBuffer[255];                //buffer to hold incoming packet
char ReplyBuffer[] = "/acknowledged";  // a string to send back

int serialBaud = 9600;
int packetNumber = 0;
int WIFI_LED_PIN = 14;

int counter = 0;




void setup() 
{

  initLED();
  initSerial();
  initWifi();
  initSocket();

  digitalWrite(WIFI_LED_PIN, HIGH);
  printWiFiStatus();
  IMU.begin();
  Serial.println("Init complete!");
}


void loop() 
{
  readSocket();
  sendData();
}


void initLED()
{
  pinMode(WIFI_LED_PIN, OUTPUT);
  digitalWrite(WIFI_LED_PIN, LOW);
}

void initSerial() 
{
  Serial.begin(serialBaud);
  delay(2000);
  Serial.println();
  Serial.println();
  Serial.println("Serial connected!");
}


int readSocket() {

  int packetSize = Udp.parsePacket();

  if (packetSize > 0) 
  {
    Serial.print("Network address: ");
    Serial.print(ip);
    Serial.print(", ");
    Serial.println(packetSize);
  }

}


void sendData() {

  float x,y,z;

  // accelerometers
  if (IMU.accelerationAvailable()) {
    IMU.readAcceleration(x, y, z);
    Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
    OSCMessage msg4("/acc");
    msg4.add(packetNumber);
    msg4.add(x);
    msg4.add(y);
    msg4.add(z);
    msg4.send(Udp);  // send the bytes to the SLIP stream
    Udp.endPacket();
    msg4.empty();  // free space occupied by message
    Serial.print("Acc: ");
    Serial.print(x);
    Serial.print(", ");
    Serial.print(y);
    Serial.print(", ");
    Serial.println(z);
  }



  packetNumber++;

}



void initWifi() {
  int attemptNumber = 0;

   // check for the presence of the shield:
  Serial.println("Starting Wifi... ");
  
  if (WiFi.status() == WL_NO_SHIELD) {
    Serial.println("WiFi shield not present");
    while (true)
      ;
  }

  // attempt to connect to WiFi network:
  while (status != WL_CONNECTED) {
    attemptNumber++;
  
    Serial.print(attemptNumber);
    Serial.print(" - Trying to connect to SSID: ");
    Serial.print(ssid);
    Serial.print(", ");
    Serial.println(status);
    
    WiFi.disconnect();  // WORKAROUND!!!!!!!!

    IPAddress gateway(192, 168, 0, 1);  // Set your Gateway IP address
    IPAddress subnet(255, 255, 255, 0);

    WiFi.config(local_IP, gateway, subnet);
    // Connect to WPA/WPA2 network. Change this line if using open or WEP network:
    status = WiFi.begin(ssid, pass);
    //status = WiFi.begin(ssid);
    delay(1000);
  }

 
}

void initPortModes()
{
  pinMode(0, INPUT_PULLUP);
  pinMode(8, INPUT_PULLUP);
  pinMode(13, INPUT_PULLUP);
  pinMode(10, INPUT_PULLUP);

}


void initSocket() {
  // if you get a connection, report back via serial:
  Udp.begin(localPort);
}


void printWiFiStatus() {

    // print the SSID of the network you're attached to:
    Serial.print("SSID: ");
    Serial.println(WiFi.SSID());

    // print your WiFi shield's IP address:
    ip = WiFi.localIP();
    Serial.print("IP Address: ");
    Serial.println(ip);

    // print the received signal strength:
    long rssi = WiFi.RSSI();

    Serial.print("signal strength (RSSI):");
    Serial.print(rssi);
    Serial.println(" dBm");
  
}
