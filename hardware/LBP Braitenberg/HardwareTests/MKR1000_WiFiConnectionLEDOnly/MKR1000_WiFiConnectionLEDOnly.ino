#include <SPI.h>
//#include <WiFiNINA.h>
#include <WiFi101.h>
#include <WiFiUdp.h>
#include <OSCMessage.h>


int status = WL_IDLE_STATUS;

IPAddress local_IP(192, 168, 0, 51);  // Board ip address
unsigned int localPort = 2390;         // local port to listen on
char ssid[] = "TP-Link_72DE";          // your network SSID (name)
char pass[] = "86171342";              // your network password (use for WPA, or use as key for WEP)

WiFiUDP Udp;
IPAddress ip;

char packetBuffer[255];                //buffer to hold incoming packet
char ReplyBuffer[] = "/acknowledged";  // a string to send back

int serialBaud = 9600;
int packetNumber = 0;
int WIFI_LED_PIN = 14;

int counter = 0;


void setup() 
{

  initLED();
  initSerial();
  initWifi();
  initSocket();

  digitalWrite(WIFI_LED_PIN, HIGH);
  printWiFiStatus();
  Serial.println("Init complete!");
}


void loop() 
{
  delay(1000);
}


void initLED()
{
  pinMode(WIFI_LED_PIN, OUTPUT);
  digitalWrite(WIFI_LED_PIN, LOW);
}

void initSerial() 
{
  Serial.begin(serialBaud);
  delay(2000);
  Serial.println();
  Serial.println();
  Serial.println("Serial connected!");
}


int readSocket() {

  int packetSize = Udp.parsePacket();

  if (packetSize > 0) 
  {
    Serial.print("Network address: ");
    Serial.print(ip);
    Serial.print(", ");
    Serial.println(packetSize);
  }

}


void initWifi() {
  int attemptNumber = 0;

   // check for the presence of the shield:
  Serial.println("Starting Wifi... ");
  
  if (WiFi.status() == WL_NO_SHIELD) {
    Serial.println("WiFi shield not present");
    while (true)
      ;
  }

  // attempt to connect to WiFi network:
  while (status != WL_CONNECTED) {
    attemptNumber++;
  
    Serial.print(attemptNumber);
    Serial.print(" - Trying to connect to SSID: ");
    Serial.print(ssid);
    Serial.print(", ");
    Serial.println(status);
    
    WiFi.disconnect();  // WORKAROUND!!!!!!!!

    IPAddress gateway(192, 168, 0, 1);  // Set your Gateway IP address
    IPAddress subnet(255, 255, 255, 0);

    WiFi.config(local_IP, gateway, subnet);
    // Connect to WPA/WPA2 network. Change this line if using open or WEP network:
    status = WiFi.begin(ssid, pass);
    //status = WiFi.begin(ssid);
    delay(1000);
  }

 
}

void initPortModes()
{
  pinMode(0, INPUT_PULLUP);
  pinMode(8, INPUT_PULLUP);
  pinMode(13, INPUT_PULLUP);
  pinMode(10, INPUT_PULLUP);

}


void initSocket() {
  // if you get a connection, report back via serial:
  Udp.begin(localPort);
}


void printWiFiStatus() {

    // print the SSID of the network you're attached to:
    Serial.print("SSID: ");
    Serial.println(WiFi.SSID());

    // print your WiFi shield's IP address:
    ip = WiFi.localIP();
    Serial.print("IP Address: ");
    Serial.println(ip);

    // print the received signal strength:
    long rssi = WiFi.RSSI();

    Serial.print("signal strength (RSSI):");
    Serial.print(rssi);
    Serial.println(" dBm");
  
}
