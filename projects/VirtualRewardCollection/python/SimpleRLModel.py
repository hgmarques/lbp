print('Python scripting started... Loading libraries')

import numpy as np
print('numpy loaded...')
import torch
print('torch loaded...')
import torch.nn as nn
print('torch.nn loaded...')
import torch.optim as optim
print('torch.optim loaded...')

print('Done!')

# Define the model
class SimpleRLModel(nn.Module):
     
    beta = 2
    ninputs = 0
    noutputs = 0
    total_loss = 0
    selected_action = 0
    predicted_reward = 0.0
    predicted_output = (0.0, 0.0)       # network with full prediction    
    output_array = (2.2, 1)
    np_outputs = np.zeros(2, dtype='float64')
    
    def __init__(self, ninputs, noutputs):
        super(SimpleRLModel, self).__init__()
        self.ninputs = ninputs
        self.noutputs = noutputs
        self.linear = nn.Linear(ninputs, noutputs, bias=False)  # One input and one output
        self.linear.weight.data.fill_(0.01)
        
    def set_loss_and_optimizer(self, criterion, optimizer):      
        self.criterion = criterion
        self.optimizer = optimizer
        
    def forward(self, x):
        self.outputs = self.linear(x)
        return self.outputs
    
    def backward(self, y):        
        self.loss = self.criterion(self.outputs, y)
        self.total_loss += self.loss.item()       
        self.optimizer.zero_grad()
        self.loss.backward()
        self.optimizer.step()

    def select_action(self, x):
        x_tensor = torch.Tensor(x)
        self.forward(x_tensor)
        
        self.np_outputs = self.outputs.detach().numpy()
        action = softmax(self.np_outputs, self.beta)
        self.selected_action = action
        self.predicted_reward = self.np_outputs[action].item()
        self.predicted_output = self.outputs.clone()
        self.predicted_output[action] = self.predicted_reward
        return action.item()
    
    def deliver_reward(self, reward):
        y = self.outputs.clone()
        y[self.selected_action] = reward
        self.backward(y)
        return 0

        
    def get_action_values(self):
        return self.np_outputs.tolist()
            
    
    def getOutputArray(self):
        return self.output_array


    def getRLWeights(self):
        return self.linear.weight.data.flatten().tolist()


def softmax(q, beta):
    qvalues = np.exp(beta * q) / np.sum(beta* np.exp(q))
    probs = qvalues / np.sum(qvalues)
    return np.random.choice(range(np.shape(q)[0]), p = probs)    


# Create model
model = SimpleRLModel(2,2)

# Define the loss function and optimizer
criterion = nn.MSELoss()
optimizer = optim.SGD(model.parameters(), lr=0.05)

# Create model
model.set_loss_and_optimizer(criterion, optimizer)

a = model.output_array

print('The end')


