import cv2
print('cv loaded')
import numpy as np
print('np loaded')
import ctypes
print('ctypes loaded')

def predict_behavior(frame):
    # Process each frame
    frame = np.asarray(frame)

    #green_red_diff = process_frame(frame)
    green_red_diff = frame
    max_values = collapse_max_values(green_red_diff)
    #left_right_diff = average_left_right_difference(max_values)
    left_avg, center_avg, right_avg = average_left_right_difference(max_values)
    sum_of_values = np.sum(max_values)
    #state = discretize_state(left_right_diff, sum_of_values)
    state = discretize_state(sum_of_values, left_avg, right_avg, center_avg)
     
    #Dan policy
    #qvalues = np.array([[1191.55966526,   89.59372968, 1191.64760236, 1191.72737494],
      # [1191.96156892,    3.62736342,    3.14947825,   29.20708039],
       #[   1.19682632,    1.88896034, 1192.26739621,    2.24167288],
      # [  29.95648496,   28.31915011,   29.30885884, 1192.35904184]]) 
    #qvalues = np.clip(qvalues, -100, 100)

    #Qtable_basic_online(alpha = 0.05, gamma = 0.99, episodes =100)
    #qvalues = np.array([[14.14467826, 13.67943478, 14.00363264],
     #  [13.45609612, 13.22187472, 13.5973779 ],
      # [13.08995875, 13.09164334, 13.22479223],
       #[0.00000000001, 0.00000000001, 13.03935384]]) 
    #qvalues = np.clip(qvalues, -100, 100)
    
    
    #Qtable_sarsa_online (alpha = 0.05, gamma = 0.99, episodes =100)
    qvalues = np.array([[2.0170786 , 1.07920661, 3.21218668],
       [1.64259455, 0.31790138, 1.22045275],
       [0.11083967, 0.17312375, 0.10226278],
       [0.        , 0.        , 0.94624347]])
    qvalues = np.clip(qvalues, -100, 100)
    # Define the mapping between predicted actions and wheel turn values
    
    action_to_wheel_turns = {
        0: [40, 40],
        1: [30, -30],
        2: [-30, 30]
    }
    #action_to_wheel_turns = {
     #   0: [40, 40],
      #  1: [-30, -30],
      #  2: [30, -30],
      #  3: [-30, 30]
    #}
    
    beta = 10.
    p_action = np.exp(qvalues[state]*beta)/(np.exp(qvalues[state]*beta).sum())
    action = np.random.choice(range(3), p=p_action)
    predicted_wheel_turn = action_to_wheel_turns[action]
    print(p_action)
    print(action)
    print(type(predicted_wheel_turn))
    #return predicted_action.item()
    return predicted_wheel_turn

def normalize_channel(channel):
    norm_channel = cv2.normalize(channel, None, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX)
    return norm_channel

# Function to process a single frame
def process_frame(frame):
    # Split the frame into its respective BGR channels
    blue, green, red = cv2.split(frame)
    # Normalize the green channel
    # green = normalize_channel(green)
    # Subtract the red channel from the green channel
    green_minus_red = cv2.subtract(green, red)
    return green_minus_red

def collapse_max_values(green_minus_red, threshold = 50):
    max_values = np.max(green_minus_red, axis=0)
    max_values[max_values<threshold] = 0
    return max_values

#def average_left_right_difference(max_values):
 #   mid_point = len(max_values) // 2
  #  left_avg = np.mean(max_values[:mid_point])
   # right_avg = np.mean(max_values[mid_point:])
    #return left_avg - right_avg

def average_left_right_difference(max_values):
    thirds = len(max_values) // 3
    left_avg = np.mean(max_values[:thirds])
    center_avg = np.mean(max_values[thirds:thirds*2])
    right_avg = np.mean(max_values[thirds*2:])
    
    return left_avg, center_avg, right_avg

def discretize_state(sum_value, left_avg, right_avg, center_avg,sum_threshold_min=10000):

    if sum_value < sum_threshold_min:
        state =  0 #“no reward”
    elif (left_avg < center_avg) and (center_avg > right_avg):
        state =  1 #“reward to the left”
    elif (left_avg > center_avg) and (left_avg >right_avg):
        state =  2 #“reward to the left
    elif (right_avg > center_avg) and (right_avg > left_avg):
        state =  3 #“reward to the right
    return state

#def discretize_state(left_right_diff, sum_value, left_right_threshold_min=-20, left_right_threshold_max=20, sum_threshold_min=8515):
    #if (sum_value > sum_threshold_min) and (left_right_diff < left_right_threshold_min):
     #   state =  3 #“reward to the right”
    #elif (sum_value > sum_threshold_min) and (left_right_threshold_min <= left_right_diff <= left_right_threshold_max):
    #    state =  1 #“reward in front”
    #elif (sum_value > sum_threshold_min) and (left_right_diff > left_right_threshold_max):
      #  state =  2 #“reward to the left”
    #elif sum_value < sum_threshold_min:
      #  state =  0 #“no reward”
    #return state