#include <SPI.h>
#include <WiFiNINA.h>
//#include <WiFi101.h>
#include <WiFiUdp.h>
#include <OSCMessage.h>
//#include <MKRMotorCarrier.h>
#include <ArduinoMotorCarrier.h>

int status = WL_IDLE_STATUS;

IPAddress local_IP(192, 168, 0, 100);  // Board ip address
unsigned int localPort = 2390;         // local port to listen on
char ssid[] = "TP-Link_72DE";          // your network SSID (name)
char pass[] = "86171342";              // your network password (use for WPA, or use as key for WEP)

WiFiUDP Udp;
IPAddress ip;

char packetBuffer[255];                //buffer to hold incoming packet
char ReplyBuffer[] = "/acknowledged";  // a string to send back

int serialBaud = 9600;
int packetNumber = 0;
int WIFI_LED_PIN = 14;

int counter = 0;

int v1;
int v2;

int motor1_duty = 0;  // duration of the motor command in millis
long motor1_time = 0;  // duration of the motor command in millis
int motor2_duty = 0;
long motor2_time = 0;
long motor1_clock = 0;
long motor2_clock = 0;
bool motor1_running = false;
bool motor2_running = false;
bool motor1_command = false;
bool motor2_command = false;


void setup() 
{

  initLED();
  initSerial();
  initPortModes();
  initWifi();
  initSocket();
  initMotorController();

  digitalWrite(WIFI_LED_PIN, HIGH);
  printWiFiStatus();
  Serial.println("Init complete!");
}


void loop() 
{
  
  readSocket();
  sendData();

  runM3(millis());
  runM4(millis());  
  
  
  controller.ping();
}


void runM3(long curr_time)
{
  if(motor1_command)
  { 
    M3.setDuty(motor1_duty);
    motor1_running = true;
    motor1_command = false;
    motor1_clock = curr_time;

  }

  
  long x = (curr_time-motor1_clock);
  Serial.print(motor1_clock);
  Serial.print("   ");
  Serial.println(x);

  if(((curr_time-motor1_clock ) >= motor1_time) && motor1_running)
  {
    M3.setDuty(0);
    motor1_running = false;
    Serial.println("DONNNE!!");
  }
}



void runM4(long curr_time)
{
  if(motor2_command)
  { 
    M4.setDuty(motor2_duty);
    motor2_running = true;
    motor2_command = false;
    motor2_clock = curr_time;
  }
  if(((curr_time-motor2_clock) >= motor2_time) && motor2_running)
  {
    M4.setDuty(0);
    motor2_running = false;
  }
}

void motorControl1(OSCMessage &msg, int offset) {
  motor1_duty = msg.getInt(0);
  motor1_time = (long) msg.getInt(1);
  Serial.print("Motor1 Command:  ");
  Serial.print(motor1_duty);
  Serial.print(", ");
  Serial.println(motor1_time);
  motor1_command = true;
}

void motorControl2(OSCMessage &msg, int offset) {
  motor2_duty = msg.getInt(0);
  motor2_time = (long) msg.getInt(1);
  motor2_command = true;
}

void initLED()
{
  pinMode(WIFI_LED_PIN, OUTPUT);
  digitalWrite(WIFI_LED_PIN, LOW);
}

void initSerial() 
{
  Serial.begin(serialBaud);
  delay(2000);
  Serial.println();
  Serial.println();
  Serial.println("Serial connected!");
}


int readSocket() {

  int packetSize = Udp.parsePacket();

  if (packetSize > 0) 
  {
    Serial.print("Network address: ");
    Serial.print(ip);
    Serial.print(", ");
    Serial.println(packetSize);
    parseOscMessage(packetSize);
  }

}


void sendData() {

  int bumper0 = !digitalRead(0);
  int bumper1 = digitalRead(8);
  int bumper2 = !digitalRead(13);
  int bumper3 = digitalRead(10);

  int analog0 = analogRead(A1);
  int analog1 = analogRead(A2);
  int analog2 = analogRead(A5);
  int analog3 = analogRead(A6);

/*
  Serial.print("analog0: ");
  Serial.print(analog0);
  Serial.print("\tanalog1: ");
  Serial.print(analog1);
  Serial.print("\tanalog2: ");
  Serial.print(analog2);
  Serial.print("\tanalog3: ");
  Serial.println(analog3);
*/

  Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
  OSCMessage msg2("/analogs");
  msg2.add(packetNumber);
  msg2.add(analog0);
  msg2.add(analog1);
  msg2.add(analog2);
  msg2.add(analog3);
  msg2.send(Udp);  // send the bytes to the SLIP stream
  Udp.endPacket();
  msg2.empty();  // free space occupied by message

  // bumpers
  Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
  OSCMessage msg3("/bumpers");
  msg3.add(packetNumber);
  msg3.add(bumper0);
  msg3.add(bumper1);
  msg3.add(bumper2);
  msg3.add(bumper3);
  msg3.send(Udp);  // send the bytes to the SLIP stream
  Udp.endPacket();
  msg3.empty();  // free space occupied by message

  packetNumber++;

}


void parseOscMessage(int packetSize) {

  OSCMessage msgIN;

  while (packetSize--) {
    msgIN.fill(Udp.read());
  }

  if (!msgIN.hasError()) {
    msgIN.route("/wheels", WheelControl);
    msgIN.route("/motor1", motorControl1); 
    msgIN.route("/motor2", motorControl2); 
  }

}


void WheelControl(OSCMessage &msg, int offset) {
  Serial.println("Message received!");
  v1 = msg.getInt(0);
  v2 = msg.getInt(1);

  M1.setDuty(v1);
  M2.setDuty(v2);

  Serial.print("Values received: ");
  Serial.print(v1);
  Serial.print(", ");
  Serial.println(v2);
}



void initMotorController() {
  if (controller.begin()) {
    Serial.print("MKR Motor Connected connected, firmware version ");
    Serial.println(controller.getFWVersion());
  } else {
    Serial.println("Couldn't connect! Is the red LED blinking? You may need to update the firmware with FWUpdater sketch");
    while (1);
  }
  controller.reboot();
  M1.setDuty(0);
  M2.setDuty(0);
  M3.setDuty(0);
  M4.setDuty(0);

}


void initWifi() {
  int attemptNumber = 0;

   // check for the presence of the shield:
  Serial.println("Starting Wifi... ");
  
  if (WiFi.status() == WL_NO_SHIELD) {
    Serial.println("WiFi shield not present");
    while (true)
      ;
  }

  // attempt to connect to WiFi network:
  while (status != WL_CONNECTED) {
    attemptNumber++;
  
    Serial.print(attemptNumber);
    Serial.print(" - Trying to connect to SSID: ");
    Serial.print(ssid);
    Serial.print(", ");
    Serial.println(status);
    
    WiFi.disconnect();  // WORKAROUND!!!!!!!!

    IPAddress gateway(192, 168, 0, 1);  // Set your Gateway IP address
    IPAddress subnet(255, 255, 255, 0);

    WiFi.config(local_IP, gateway, subnet);
    // Connect to WPA/WPA2 network. Change this line if using open or WEP network:
    status = WiFi.begin(ssid, pass);
    //status = WiFi.begin(ssid);
    delay(1000);
  }

 
}

void initPortModes()
{
  pinMode(0, INPUT_PULLUP);
  pinMode(8, INPUT);
  pinMode(13, INPUT_PULLUP);
  pinMode(10, INPUT);

}


void initSocket() {
  // if you get a connection, report back via serial:
  Udp.begin(localPort);
}


void printWiFiStatus() {

    // print the SSID of the network you're attached to:
    Serial.print("SSID: ");
    Serial.println(WiFi.SSID());

    // print your WiFi shield's IP address:
    ip = WiFi.localIP();
    Serial.print("IP Address: ");
    Serial.println(ip);

    // print the received signal strength:
    long rssi = WiFi.RSSI();

    Serial.print("signal strength (RSSI):");
    Serial.print(rssi);
    Serial.println(" dBm");
  
}
