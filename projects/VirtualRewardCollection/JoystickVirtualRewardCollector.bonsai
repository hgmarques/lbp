﻿<?xml version="1.0" encoding="utf-8"?>
<WorkflowBuilder Version="2.8.1"
                 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                 xmlns:osc="clr-namespace:Bonsai.Osc;assembly=Bonsai.Osc"
                 xmlns:rx="clr-namespace:Bonsai.Reactive;assembly=Bonsai.Core"
                 xmlns:cv="clr-namespace:Bonsai.Vision;assembly=Bonsai.Vision"
                 xmlns:io="clr-namespace:Bonsai.IO;assembly=Bonsai.System"
                 xmlns:vid="clr-namespace:Bonsai.Video;assembly=Bonsai.Video"
                 xmlns:gli="clr-namespace:Bonsai.Shaders.Input;assembly=Bonsai.Shaders"
                 xmlns:scr="clr-namespace:Bonsai.Scripting.Expressions;assembly=Bonsai.Scripting.Expressions"
                 xmlns:dsp="clr-namespace:Bonsai.Dsp;assembly=Bonsai.Dsp"
                 xmlns="https://bonsai-rx.org/2018/workflow">
  <Workflow>
    <Nodes>
      <Expression xsi:type="Combinator">
        <Combinator xsi:type="osc:CreateUdpClient">
          <osc:Name>Ole</osc:Name>
          <osc:Port>9998</osc:Port>
          <osc:RemoteHostName>192.168.0.100</osc:RemoteHostName>
          <osc:RemotePort>2390</osc:RemotePort>
        </Combinator>
      </Expression>
      <Expression xsi:type="Combinator">
        <Combinator xsi:type="osc:CreateUdpClient">
          <osc:Name>Environment</osc:Name>
          <osc:Port>10002</osc:Port>
          <osc:RemoteHostName>localhost</osc:RemoteHostName>
          <osc:RemotePort>10001</osc:RemotePort>
        </Combinator>
      </Expression>
      <Expression xsi:type="GroupWorkflow">
        <Name>Robot Comm</Name>
        <Workflow>
          <Nodes>
            <Expression xsi:type="SubscribeSubject">
              <Name>Wheel Commands</Name>
            </Expression>
            <Expression xsi:type="osc:SendMessage">
              <osc:Connection>Ole</osc:Connection>
              <osc:Address>/wheels</osc:Address>
            </Expression>
            <Expression xsi:type="osc:ReceiveMessage">
              <osc:Address>/analogs</osc:Address>
              <osc:TypeTag>iiiii</osc:TypeTag>
              <osc:Connection>Ole</osc:Connection>
            </Expression>
            <Expression xsi:type="MemberSelector">
              <Selector>Item2</Selector>
            </Expression>
            <Expression xsi:type="MemberSelector">
              <Selector>Item5</Selector>
            </Expression>
            <Expression xsi:type="osc:ReceiveMessage">
              <osc:Address>/bumpers</osc:Address>
              <osc:TypeTag>iiiii</osc:TypeTag>
              <osc:Connection>Ole</osc:Connection>
            </Expression>
            <Expression xsi:type="rx:BehaviorSubject">
              <Name>Bumpers</Name>
            </Expression>
          </Nodes>
          <Edges>
            <Edge From="0" To="1" Label="Source1" />
            <Edge From="2" To="3" Label="Source1" />
            <Edge From="2" To="4" Label="Source1" />
            <Edge From="5" To="6" Label="Source1" />
          </Edges>
        </Workflow>
      </Expression>
      <Expression xsi:type="GroupWorkflow">
        <Name>Log Data</Name>
        <Workflow>
          <Nodes>
            <Expression xsi:type="SubscribeSubject">
              <Name>CameraImage</Name>
            </Expression>
            <Expression xsi:type="Combinator">
              <Combinator xsi:type="cv:VideoWriter">
                <cv:FileName>../../../data/CameraImage.avi</cv:FileName>
                <cv:Suffix>Timestamp</cv:Suffix>
                <cv:Buffered>true</cv:Buffered>
                <cv:Overwrite>false</cv:Overwrite>
                <cv:FourCC>FMP4</cv:FourCC>
                <cv:FrameRate>30</cv:FrameRate>
                <cv:FrameSize>
                  <cv:Width>0</cv:Width>
                  <cv:Height>0</cv:Height>
                </cv:FrameSize>
                <cv:ResizeInterpolation>NearestNeighbor</cv:ResizeInterpolation>
              </Combinator>
            </Expression>
            <Expression xsi:type="SubscribeSubject">
              <Name>CameraImage</Name>
            </Expression>
            <Expression xsi:type="Combinator">
              <Combinator xsi:type="rx:Timestamp" />
            </Expression>
            <Expression xsi:type="MemberSelector">
              <Selector>Timestamp.TimeOfDay.TotalMilliseconds</Selector>
            </Expression>
            <Expression xsi:type="io:CsvWriter">
              <io:FileName>../../../data/CameraTimestamps.csv</io:FileName>
              <io:Append>false</io:Append>
              <io:Overwrite>false</io:Overwrite>
              <io:Suffix>Timestamp</io:Suffix>
              <io:IncludeHeader>false</io:IncludeHeader>
            </Expression>
            <Expression xsi:type="SubscribeSubject">
              <Name>Reward</Name>
            </Expression>
            <Expression xsi:type="Combinator">
              <Combinator xsi:type="rx:Timestamp" />
            </Expression>
            <Expression xsi:type="MemberSelector">
              <Selector>Timestamp.TimeOfDay.TotalMilliseconds</Selector>
            </Expression>
            <Expression xsi:type="MemberSelector">
              <Selector>Value</Selector>
            </Expression>
            <Expression xsi:type="Combinator">
              <Combinator xsi:type="rx:Zip" />
            </Expression>
            <Expression xsi:type="io:CsvWriter">
              <io:FileName>../../../data/Rewards.csv</io:FileName>
              <io:Append>false</io:Append>
              <io:Overwrite>false</io:Overwrite>
              <io:Suffix>Timestamp</io:Suffix>
              <io:IncludeHeader>false</io:IncludeHeader>
            </Expression>
            <Expression xsi:type="SubscribeSubject">
              <Name>Wheel Commands</Name>
            </Expression>
            <Expression xsi:type="Combinator">
              <Combinator xsi:type="rx:Timestamp" />
            </Expression>
            <Expression xsi:type="MemberSelector">
              <Selector>Timestamp.TimeOfDay.TotalMilliseconds</Selector>
            </Expression>
            <Expression xsi:type="MemberSelector">
              <Selector>Value</Selector>
            </Expression>
            <Expression xsi:type="Combinator">
              <Combinator xsi:type="rx:Zip" />
            </Expression>
            <Expression xsi:type="io:CsvWriter">
              <io:FileName>../../../data/WheelCommands.csv</io:FileName>
              <io:Append>false</io:Append>
              <io:Overwrite>false</io:Overwrite>
              <io:Suffix>Timestamp</io:Suffix>
              <io:IncludeHeader>false</io:IncludeHeader>
            </Expression>
            <Expression xsi:type="SubscribeSubject">
              <Name>Bumpers</Name>
            </Expression>
            <Expression xsi:type="Combinator">
              <Combinator xsi:type="rx:Timestamp" />
            </Expression>
            <Expression xsi:type="MemberSelector">
              <Selector>Timestamp.TimeOfDay.TotalMilliseconds</Selector>
            </Expression>
            <Expression xsi:type="MemberSelector">
              <Selector>Value</Selector>
            </Expression>
            <Expression xsi:type="Combinator">
              <Combinator xsi:type="rx:Zip" />
            </Expression>
            <Expression xsi:type="io:CsvWriter">
              <io:FileName>../../../data/Bumpers.csv</io:FileName>
              <io:Append>false</io:Append>
              <io:Overwrite>false</io:Overwrite>
              <io:Suffix>Timestamp</io:Suffix>
              <io:IncludeHeader>false</io:IncludeHeader>
            </Expression>
          </Nodes>
          <Edges>
            <Edge From="0" To="1" Label="Source1" />
            <Edge From="2" To="3" Label="Source1" />
            <Edge From="3" To="4" Label="Source1" />
            <Edge From="4" To="5" Label="Source1" />
            <Edge From="6" To="7" Label="Source1" />
            <Edge From="7" To="8" Label="Source1" />
            <Edge From="7" To="9" Label="Source1" />
            <Edge From="8" To="10" Label="Source1" />
            <Edge From="9" To="10" Label="Source2" />
            <Edge From="10" To="11" Label="Source1" />
            <Edge From="12" To="13" Label="Source1" />
            <Edge From="13" To="14" Label="Source1" />
            <Edge From="13" To="15" Label="Source1" />
            <Edge From="14" To="16" Label="Source1" />
            <Edge From="15" To="16" Label="Source2" />
            <Edge From="16" To="17" Label="Source1" />
            <Edge From="18" To="19" Label="Source1" />
            <Edge From="19" To="20" Label="Source1" />
            <Edge From="19" To="21" Label="Source1" />
            <Edge From="20" To="22" Label="Source1" />
            <Edge From="21" To="22" Label="Source2" />
            <Edge From="22" To="23" Label="Source1" />
          </Edges>
        </Workflow>
      </Expression>
      <Expression xsi:type="osc:ReceiveMessage">
        <osc:Address>/reward</osc:Address>
        <osc:TypeTag>i</osc:TypeTag>
        <osc:Connection>Environment</osc:Connection>
      </Expression>
      <Expression xsi:type="rx:BehaviorSubject">
        <Name>Reward</Name>
      </Expression>
      <Expression xsi:type="Combinator">
        <Combinator xsi:type="vid:MjpegStream">
          <vid:SourceUrl>http://192.168.0.184:81/stream</vid:SourceUrl>
        </Combinator>
      </Expression>
      <Expression xsi:type="rx:BehaviorSubject">
        <Name>CameraImage</Name>
      </Expression>
      <Expression xsi:type="Combinator">
        <Combinator xsi:type="rx:Timer">
          <rx:DueTime>PT0S</rx:DueTime>
          <rx:Period>PT0.1S</rx:Period>
        </Combinator>
      </Expression>
      <Expression xsi:type="Combinator">
        <Combinator xsi:type="gli:GamePad">
          <gli:Index>0</gli:Index>
        </Combinator>
      </Expression>
      <Expression xsi:type="MemberSelector">
        <Selector>ThumbSticks.Left.Y</Selector>
      </Expression>
      <Expression xsi:type="MemberSelector">
        <Selector>ThumbSticks.Left.X</Selector>
      </Expression>
      <Expression xsi:type="GroupWorkflow">
        <Name>Joystick to Polar Coord</Name>
        <Workflow>
          <Nodes>
            <Expression xsi:type="WorkflowInput">
              <Name>Source1</Name>
            </Expression>
            <Expression xsi:type="MemberSelector">
              <Selector>ThumbSticks.Left.X</Selector>
            </Expression>
            <Expression xsi:type="MemberSelector">
              <Selector>ThumbSticks.Left.Y</Selector>
            </Expression>
            <Expression xsi:type="Combinator">
              <Combinator xsi:type="rx:Zip" />
            </Expression>
            <Expression xsi:type="scr:ExpressionTransform">
              <scr:Name>Joystick Angle</scr:Name>
              <scr:Expression>Math.atan2(Item2,Item1)</scr:Expression>
            </Expression>
            <Expression xsi:type="scr:ExpressionTransform">
              <scr:Name>Joystick Radius</scr:Name>
              <scr:Expression>Math.sqrt(Item1*Item1 + Item2 * Item2)</scr:Expression>
            </Expression>
            <Expression xsi:type="Combinator">
              <Combinator xsi:type="dsp:Rescale">
                <dsp:Min>0</dsp:Min>
                <dsp:Max>1</dsp:Max>
                <dsp:RangeMin>0</dsp:RangeMin>
                <dsp:RangeMax>1</dsp:RangeMax>
                <dsp:RescaleType>Clamp</dsp:RescaleType>
              </Combinator>
            </Expression>
            <Expression xsi:type="Combinator">
              <Combinator xsi:type="rx:Zip" />
            </Expression>
            <Expression xsi:type="WorkflowOutput" />
          </Nodes>
          <Edges>
            <Edge From="0" To="1" Label="Source1" />
            <Edge From="0" To="2" Label="Source1" />
            <Edge From="1" To="3" Label="Source1" />
            <Edge From="2" To="3" Label="Source2" />
            <Edge From="3" To="4" Label="Source1" />
            <Edge From="3" To="5" Label="Source1" />
            <Edge From="4" To="7" Label="Source1" />
            <Edge From="5" To="6" Label="Source1" />
            <Edge From="6" To="7" Label="Source2" />
            <Edge From="7" To="8" Label="Source1" />
          </Edges>
        </Workflow>
      </Expression>
      <Expression xsi:type="GroupWorkflow">
        <Name>Differential Drive</Name>
        <Workflow>
          <Nodes>
            <Expression xsi:type="WorkflowInput">
              <Name>Source1</Name>
            </Expression>
            <Expression xsi:type="MemberSelector">
              <Selector>Item1</Selector>
            </Expression>
            <Expression xsi:type="GroupWorkflow">
              <Name>Wheel Proportions</Name>
              <Workflow>
                <Nodes>
                  <Expression xsi:type="WorkflowInput">
                    <Name>Source1</Name>
                  </Expression>
                  <Expression xsi:type="rx:Condition">
                    <Name>Front Left Quadrant?</Name>
                    <Workflow>
                      <Nodes>
                        <Expression xsi:type="WorkflowInput">
                          <Name>Source1</Name>
                        </Expression>
                        <Expression xsi:type="scr:ExpressionTransform">
                          <scr:Name>Front Left Quadrant?</scr:Name>
                          <scr:Expression>it &gt; Math.PI/2 &amp;&amp; it &lt; Math.PI</scr:Expression>
                        </Expression>
                        <Expression xsi:type="WorkflowOutput" />
                      </Nodes>
                      <Edges>
                        <Edge From="0" To="1" Label="Source1" />
                        <Edge From="1" To="2" Label="Source1" />
                      </Edges>
                    </Workflow>
                  </Expression>
                  <Expression xsi:type="Combinator">
                    <Combinator xsi:type="dsp:Rescale">
                      <dsp:Min>1.57</dsp:Min>
                      <dsp:Max>3.14</dsp:Max>
                      <dsp:RangeMin>1</dsp:RangeMin>
                      <dsp:RangeMax>-1</dsp:RangeMax>
                      <dsp:RescaleType>Linear</dsp:RescaleType>
                    </Combinator>
                  </Expression>
                  <Expression xsi:type="Combinator">
                    <Combinator xsi:type="DoubleProperty">
                      <Value>1</Value>
                    </Combinator>
                  </Expression>
                  <Expression xsi:type="Combinator">
                    <Combinator xsi:type="rx:Zip" />
                  </Expression>
                  <Expression xsi:type="rx:Condition">
                    <Name>Front Right Quadrant?</Name>
                    <Workflow>
                      <Nodes>
                        <Expression xsi:type="WorkflowInput">
                          <Name>Source1</Name>
                        </Expression>
                        <Expression xsi:type="scr:ExpressionTransform">
                          <scr:Name>Front Left Quadrant?</scr:Name>
                          <scr:Expression>it &gt;= 0 &amp;&amp; it &lt; Math.PI/2</scr:Expression>
                        </Expression>
                        <Expression xsi:type="WorkflowOutput" />
                      </Nodes>
                      <Edges>
                        <Edge From="0" To="1" Label="Source1" />
                        <Edge From="1" To="2" Label="Source1" />
                      </Edges>
                    </Workflow>
                  </Expression>
                  <Expression xsi:type="Combinator">
                    <Combinator xsi:type="DoubleProperty">
                      <Value>1</Value>
                    </Combinator>
                  </Expression>
                  <Expression xsi:type="Combinator">
                    <Combinator xsi:type="dsp:Rescale">
                      <dsp:Min>0</dsp:Min>
                      <dsp:Max>1.57</dsp:Max>
                      <dsp:RangeMin>-1</dsp:RangeMin>
                      <dsp:RangeMax>1</dsp:RangeMax>
                      <dsp:RescaleType>Linear</dsp:RescaleType>
                    </Combinator>
                  </Expression>
                  <Expression xsi:type="Combinator">
                    <Combinator xsi:type="rx:Zip" />
                  </Expression>
                  <Expression xsi:type="Combinator">
                    <Combinator xsi:type="rx:Merge" />
                  </Expression>
                  <Expression xsi:type="WorkflowOutput" />
                </Nodes>
                <Edges>
                  <Edge From="0" To="1" Label="Source1" />
                  <Edge From="0" To="5" Label="Source1" />
                  <Edge From="1" To="2" Label="Source1" />
                  <Edge From="1" To="3" Label="Source1" />
                  <Edge From="2" To="4" Label="Source1" />
                  <Edge From="3" To="4" Label="Source2" />
                  <Edge From="4" To="9" Label="Source1" />
                  <Edge From="5" To="6" Label="Source1" />
                  <Edge From="5" To="7" Label="Source1" />
                  <Edge From="6" To="8" Label="Source1" />
                  <Edge From="7" To="8" Label="Source2" />
                  <Edge From="8" To="9" Label="Source2" />
                  <Edge From="9" To="10" Label="Source1" />
                </Edges>
              </Workflow>
            </Expression>
            <Expression xsi:type="MemberSelector">
              <Selector>Item2</Selector>
            </Expression>
            <Expression xsi:type="MemberSelector">
              <Selector>Item2</Selector>
            </Expression>
            <Expression xsi:type="Combinator">
              <Combinator xsi:type="rx:Zip" />
            </Expression>
            <Expression xsi:type="Multiply" />
            <Expression xsi:type="MemberSelector">
              <Selector>Item1</Selector>
            </Expression>
            <Expression xsi:type="Combinator">
              <Combinator xsi:type="rx:Zip" />
            </Expression>
            <Expression xsi:type="Multiply" />
            <Expression xsi:type="Combinator">
              <Combinator xsi:type="rx:Zip" />
            </Expression>
            <Expression xsi:type="WorkflowOutput" />
          </Nodes>
          <Edges>
            <Edge From="0" To="1" Label="Source1" />
            <Edge From="0" To="4" Label="Source1" />
            <Edge From="1" To="2" Label="Source1" />
            <Edge From="2" To="3" Label="Source1" />
            <Edge From="2" To="7" Label="Source1" />
            <Edge From="3" To="5" Label="Source1" />
            <Edge From="4" To="5" Label="Source2" />
            <Edge From="4" To="8" Label="Source2" />
            <Edge From="5" To="6" Label="Source1" />
            <Edge From="6" To="10" Label="Source1" />
            <Edge From="7" To="8" Label="Source1" />
            <Edge From="8" To="9" Label="Source1" />
            <Edge From="9" To="10" Label="Source2" />
            <Edge From="10" To="11" Label="Source1" />
          </Edges>
        </Workflow>
      </Expression>
      <Expression xsi:type="Disable">
        <Builder xsi:type="GroupWorkflow">
          <Name>Differential Drive</Name>
          <Workflow>
            <Nodes>
              <Expression xsi:type="WorkflowInput">
                <Name>Source1</Name>
              </Expression>
              <Expression xsi:type="MemberSelector">
                <Selector>Item1</Selector>
              </Expression>
              <Expression xsi:type="GroupWorkflow">
                <Name>Wheel Proportions</Name>
                <Workflow>
                  <Nodes>
                    <Expression xsi:type="WorkflowInput">
                      <Name>Source1</Name>
                    </Expression>
                    <Expression xsi:type="rx:Condition">
                      <Name>Front Left Quadrant?</Name>
                      <Workflow>
                        <Nodes>
                          <Expression xsi:type="WorkflowInput">
                            <Name>Source1</Name>
                          </Expression>
                          <Expression xsi:type="scr:ExpressionTransform">
                            <scr:Name>Front Left Quadrant?</scr:Name>
                            <scr:Expression>it &gt; Math.PI/2 &amp;&amp; it &lt; 3*Math.PI/4</scr:Expression>
                          </Expression>
                          <Expression xsi:type="WorkflowOutput" />
                        </Nodes>
                        <Edges>
                          <Edge From="0" To="1" Label="Source1" />
                          <Edge From="1" To="2" Label="Source1" />
                        </Edges>
                      </Workflow>
                    </Expression>
                    <Expression xsi:type="Combinator">
                      <Combinator xsi:type="dsp:Rescale">
                        <dsp:Min>1.58</dsp:Min>
                        <dsp:Max>2.36</dsp:Max>
                        <dsp:RangeMin>1</dsp:RangeMin>
                        <dsp:RangeMax>0.2</dsp:RangeMax>
                        <dsp:RescaleType>Linear</dsp:RescaleType>
                      </Combinator>
                    </Expression>
                    <Expression xsi:type="Combinator">
                      <Combinator xsi:type="DoubleProperty">
                        <Value>1</Value>
                      </Combinator>
                    </Expression>
                    <Expression xsi:type="Combinator">
                      <Combinator xsi:type="rx:Zip" />
                    </Expression>
                    <Expression xsi:type="rx:Condition">
                      <Name>Front Right Quadrant?</Name>
                      <Workflow>
                        <Nodes>
                          <Expression xsi:type="WorkflowInput">
                            <Name>Source1</Name>
                          </Expression>
                          <Expression xsi:type="scr:ExpressionTransform">
                            <scr:Name>Front Right Quadrant?</scr:Name>
                            <scr:Expression>it &gt; Math.PI/4 &amp;&amp; it&lt;= Math.PI/2</scr:Expression>
                          </Expression>
                          <Expression xsi:type="WorkflowOutput" />
                        </Nodes>
                        <Edges>
                          <Edge From="0" To="1" Label="Source1" />
                          <Edge From="1" To="2" Label="Source1" />
                        </Edges>
                      </Workflow>
                    </Expression>
                    <Expression xsi:type="Combinator">
                      <Combinator xsi:type="DoubleProperty">
                        <Value>1</Value>
                      </Combinator>
                    </Expression>
                    <Expression xsi:type="Combinator">
                      <Combinator xsi:type="dsp:Rescale">
                        <dsp:Min>0.79</dsp:Min>
                        <dsp:Max>1.58</dsp:Max>
                        <dsp:RangeMin>0.2</dsp:RangeMin>
                        <dsp:RangeMax>1</dsp:RangeMax>
                        <dsp:RescaleType>Linear</dsp:RescaleType>
                      </Combinator>
                    </Expression>
                    <Expression xsi:type="Combinator">
                      <Combinator xsi:type="rx:Zip" />
                    </Expression>
                    <Expression xsi:type="rx:Condition">
                      <Name>Back Left Quadrant?</Name>
                      <Workflow>
                        <Nodes>
                          <Expression xsi:type="WorkflowInput">
                            <Name>Source1</Name>
                          </Expression>
                          <Expression xsi:type="scr:ExpressionTransform">
                            <scr:Name>Back Left Quadrant?</scr:Name>
                            <scr:Expression>it &gt; -3*Math.PI/4 &amp;&amp; it &lt; -Math.PI/2</scr:Expression>
                          </Expression>
                          <Expression xsi:type="WorkflowOutput" />
                        </Nodes>
                        <Edges>
                          <Edge From="0" To="1" Label="Source1" />
                          <Edge From="1" To="2" Label="Source1" />
                        </Edges>
                      </Workflow>
                    </Expression>
                    <Expression xsi:type="Combinator">
                      <Combinator xsi:type="dsp:Rescale">
                        <dsp:Min>-1.58</dsp:Min>
                        <dsp:Max>-2.36</dsp:Max>
                        <dsp:RangeMin>-1</dsp:RangeMin>
                        <dsp:RangeMax>-0.2</dsp:RangeMax>
                        <dsp:RescaleType>Linear</dsp:RescaleType>
                      </Combinator>
                    </Expression>
                    <Expression xsi:type="Combinator">
                      <Combinator xsi:type="DoubleProperty">
                        <Value>-1</Value>
                      </Combinator>
                    </Expression>
                    <Expression xsi:type="Combinator">
                      <Combinator xsi:type="rx:Zip" />
                    </Expression>
                    <Expression xsi:type="rx:Condition">
                      <Name>Back Right Quadrant?</Name>
                      <Workflow>
                        <Nodes>
                          <Expression xsi:type="WorkflowInput">
                            <Name>Source1</Name>
                          </Expression>
                          <Expression xsi:type="scr:ExpressionTransform">
                            <scr:Name>Back Right Quadrant?</scr:Name>
                            <scr:Expression>it &gt; -Math.PI/2 &amp;&amp; it &lt; -Math.PI/4</scr:Expression>
                          </Expression>
                          <Expression xsi:type="WorkflowOutput" />
                        </Nodes>
                        <Edges>
                          <Edge From="0" To="1" Label="Source1" />
                          <Edge From="1" To="2" Label="Source1" />
                        </Edges>
                      </Workflow>
                    </Expression>
                    <Expression xsi:type="Combinator">
                      <Combinator xsi:type="DoubleProperty">
                        <Value>-1</Value>
                      </Combinator>
                    </Expression>
                    <Expression xsi:type="Combinator">
                      <Combinator xsi:type="dsp:Rescale">
                        <dsp:Min>-0.79</dsp:Min>
                        <dsp:Max>-1.58</dsp:Max>
                        <dsp:RangeMin>-0.2</dsp:RangeMin>
                        <dsp:RangeMax>-1</dsp:RangeMax>
                        <dsp:RescaleType>Linear</dsp:RescaleType>
                      </Combinator>
                    </Expression>
                    <Expression xsi:type="Combinator">
                      <Combinator xsi:type="rx:Zip" />
                    </Expression>
                    <Expression xsi:type="rx:Condition">
                      <Name>Dead Zones?</Name>
                      <Workflow>
                        <Nodes>
                          <Expression xsi:type="WorkflowInput">
                            <Name>Source1</Name>
                          </Expression>
                          <Expression xsi:type="scr:ExpressionTransform">
                            <scr:Name>Dead Zone?</scr:Name>
                            <scr:Expression>(it &lt;= -3*Math.PI/4) || (it &gt;= 3*Math.PI/4 ) || (it &gt;= -Math.PI/4 &amp;&amp; it &lt;= Math.PI/4)</scr:Expression>
                          </Expression>
                          <Expression xsi:type="WorkflowOutput" />
                        </Nodes>
                        <Edges>
                          <Edge From="0" To="1" Label="Source1" />
                          <Edge From="1" To="2" Label="Source1" />
                        </Edges>
                      </Workflow>
                    </Expression>
                    <Expression xsi:type="Combinator">
                      <Combinator xsi:type="DoubleProperty">
                        <Value>0</Value>
                      </Combinator>
                    </Expression>
                    <Expression xsi:type="Combinator">
                      <Combinator xsi:type="DoubleProperty">
                        <Value>0</Value>
                      </Combinator>
                    </Expression>
                    <Expression xsi:type="Combinator">
                      <Combinator xsi:type="rx:Zip" />
                    </Expression>
                    <Expression xsi:type="Combinator">
                      <Combinator xsi:type="rx:Merge" />
                    </Expression>
                    <Expression xsi:type="WorkflowOutput" />
                  </Nodes>
                  <Edges>
                    <Edge From="0" To="1" Label="Source1" />
                    <Edge From="0" To="5" Label="Source1" />
                    <Edge From="0" To="9" Label="Source1" />
                    <Edge From="0" To="13" Label="Source1" />
                    <Edge From="0" To="17" Label="Source1" />
                    <Edge From="1" To="2" Label="Source1" />
                    <Edge From="1" To="3" Label="Source1" />
                    <Edge From="2" To="4" Label="Source1" />
                    <Edge From="3" To="4" Label="Source2" />
                    <Edge From="4" To="21" Label="Source1" />
                    <Edge From="5" To="6" Label="Source1" />
                    <Edge From="5" To="7" Label="Source1" />
                    <Edge From="6" To="8" Label="Source1" />
                    <Edge From="7" To="8" Label="Source2" />
                    <Edge From="8" To="21" Label="Source2" />
                    <Edge From="9" To="10" Label="Source1" />
                    <Edge From="9" To="11" Label="Source1" />
                    <Edge From="10" To="12" Label="Source1" />
                    <Edge From="11" To="12" Label="Source2" />
                    <Edge From="12" To="21" Label="Source3" />
                    <Edge From="13" To="14" Label="Source1" />
                    <Edge From="13" To="15" Label="Source1" />
                    <Edge From="14" To="16" Label="Source1" />
                    <Edge From="15" To="16" Label="Source2" />
                    <Edge From="16" To="21" Label="Source4" />
                    <Edge From="17" To="18" Label="Source1" />
                    <Edge From="17" To="19" Label="Source1" />
                    <Edge From="18" To="20" Label="Source1" />
                    <Edge From="19" To="20" Label="Source2" />
                    <Edge From="20" To="21" Label="Source5" />
                    <Edge From="21" To="22" Label="Source1" />
                  </Edges>
                </Workflow>
              </Expression>
              <Expression xsi:type="MemberSelector">
                <Selector>Item2</Selector>
              </Expression>
              <Expression xsi:type="MemberSelector">
                <Selector>Item2</Selector>
              </Expression>
              <Expression xsi:type="Combinator">
                <Combinator xsi:type="rx:Zip" />
              </Expression>
              <Expression xsi:type="Multiply" />
              <Expression xsi:type="MemberSelector">
                <Selector>Item1</Selector>
              </Expression>
              <Expression xsi:type="Combinator">
                <Combinator xsi:type="rx:Zip" />
              </Expression>
              <Expression xsi:type="Multiply" />
              <Expression xsi:type="Combinator">
                <Combinator xsi:type="rx:Zip" />
              </Expression>
              <Expression xsi:type="WorkflowOutput" />
            </Nodes>
            <Edges>
              <Edge From="0" To="1" Label="Source1" />
              <Edge From="0" To="4" Label="Source1" />
              <Edge From="1" To="2" Label="Source1" />
              <Edge From="2" To="3" Label="Source1" />
              <Edge From="2" To="7" Label="Source1" />
              <Edge From="3" To="5" Label="Source1" />
              <Edge From="4" To="5" Label="Source2" />
              <Edge From="4" To="8" Label="Source2" />
              <Edge From="5" To="6" Label="Source1" />
              <Edge From="6" To="10" Label="Source1" />
              <Edge From="7" To="8" Label="Source1" />
              <Edge From="8" To="9" Label="Source1" />
              <Edge From="9" To="10" Label="Source2" />
              <Edge From="10" To="11" Label="Source1" />
            </Edges>
          </Workflow>
        </Builder>
      </Expression>
      <Expression xsi:type="GroupWorkflow">
        <Name>Rescale Commands</Name>
        <Workflow>
          <Nodes>
            <Expression xsi:type="WorkflowInput">
              <Name>Source1</Name>
            </Expression>
            <Expression xsi:type="MemberSelector">
              <Selector>Item1</Selector>
            </Expression>
            <Expression xsi:type="scr:ExpressionTransform">
              <scr:Expression>Convert.ToInt32(it*80)</scr:Expression>
            </Expression>
            <Expression xsi:type="MemberSelector">
              <Selector>Item2</Selector>
            </Expression>
            <Expression xsi:type="scr:ExpressionTransform">
              <scr:Expression>Convert.ToInt32(it*80)</scr:Expression>
            </Expression>
            <Expression xsi:type="Combinator">
              <Combinator xsi:type="rx:Zip" />
            </Expression>
            <Expression xsi:type="WorkflowOutput" />
          </Nodes>
          <Edges>
            <Edge From="0" To="1" Label="Source1" />
            <Edge From="0" To="3" Label="Source1" />
            <Edge From="1" To="2" Label="Source1" />
            <Edge From="2" To="5" Label="Source1" />
            <Edge From="3" To="4" Label="Source1" />
            <Edge From="4" To="5" Label="Source2" />
            <Edge From="5" To="6" Label="Source1" />
          </Edges>
        </Workflow>
      </Expression>
      <Expression xsi:type="GroupWorkflow">
        <Name>Convert Motor Signal</Name>
        <Workflow>
          <Nodes>
            <Expression xsi:type="WorkflowInput">
              <Name>Source1</Name>
            </Expression>
            <Expression xsi:type="MemberSelector">
              <Selector>Item1</Selector>
            </Expression>
            <Expression xsi:type="MemberSelector">
              <Selector>Item2</Selector>
            </Expression>
            <Expression xsi:type="Multiply">
              <Operand xsi:type="IntProperty">
                <Value>-1</Value>
              </Operand>
            </Expression>
            <Expression xsi:type="Combinator">
              <Combinator xsi:type="rx:Zip" />
            </Expression>
            <Expression xsi:type="WorkflowOutput" />
          </Nodes>
          <Edges>
            <Edge From="0" To="1" Label="Source1" />
            <Edge From="0" To="2" Label="Source1" />
            <Edge From="1" To="4" Label="Source1" />
            <Edge From="2" To="3" Label="Source1" />
            <Edge From="3" To="4" Label="Source2" />
            <Edge From="4" To="5" Label="Source1" />
          </Edges>
        </Workflow>
      </Expression>
      <Expression xsi:type="MulticastSubject">
        <Name>Wheel Commands</Name>
      </Expression>
      <Expression xsi:type="Combinator">
        <Combinator xsi:type="IntProperty">
          <Value>0</Value>
        </Combinator>
      </Expression>
      <Expression xsi:type="Combinator">
        <Combinator xsi:type="IntProperty">
          <Value>0</Value>
        </Combinator>
      </Expression>
      <Expression xsi:type="Combinator">
        <Combinator xsi:type="rx:Zip" />
      </Expression>
      <Expression xsi:type="rx:BehaviorSubject">
        <Name>Wheel Commands</Name>
      </Expression>
    </Nodes>
    <Edges>
      <Edge From="4" To="5" Label="Source1" />
      <Edge From="6" To="7" Label="Source1" />
      <Edge From="8" To="9" Label="Source1" />
      <Edge From="9" To="10" Label="Source1" />
      <Edge From="9" To="11" Label="Source1" />
      <Edge From="9" To="12" Label="Source1" />
      <Edge From="12" To="13" Label="Source1" />
      <Edge From="13" To="14" Label="Source1" />
      <Edge From="14" To="15" Label="Source1" />
      <Edge From="15" To="16" Label="Source1" />
      <Edge From="16" To="17" Label="Source1" />
      <Edge From="18" To="20" Label="Source1" />
      <Edge From="19" To="20" Label="Source2" />
      <Edge From="20" To="21" Label="Source1" />
    </Edges>
  </Workflow>
</WorkflowBuilder>